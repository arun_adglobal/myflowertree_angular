export const environment = {
  production: true,
  CDN_PATH:"https://d3cif2hu95s88v.cloudfront.net/live-site-2016/",
  CDN_DOMAIN : "https://d3cif2hu95s88v.cloudfront.net/",
  //API_URL : "https://api.myflowertree.in/"
  API_URL : "http://localhost:4600/"
};
