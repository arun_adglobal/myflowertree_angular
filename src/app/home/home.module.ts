import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';

import { HomeComponent } from './home.component';
import { MainLayoutModule } from '../main-layout.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import {  AuthGuardService1 } from '../auth-guard.service';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
declarations: [ HomeComponent],
imports: [ RouterModule.forChild([
    { path: '', component: HomeComponent, canActivate:[AuthGuardService1] }
]), CommonModule, ReactiveFormsModule, NgxJsonLdModule,NgxUsefulSwiperModule,MainLayoutModule,NgxSpinnerModule, NgxSkeletonLoaderModule,NgbModule]
})
export class HomeModule{

}