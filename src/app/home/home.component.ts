import { Component, OnInit,Injectable, Inject, PLATFORM_ID, HostListener  } from '@angular/core';
import { HomeService } from './home.service';
import { ProductService } from '../miscellaneous/product/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { SEOService } from '../seo.service';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'ngx-useful-swiper';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import{ ReviewService } from '../review/review.service';
import { isPlatformBrowser } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";


declare const getDevice : any;


declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HomeService],

})
export class HomeComponent implements OnInit {
  home_data: any = [];
  best_products : any = [];
  banners: any = [];
  blog :any=[];

  review :any=[];
  reviewcount :any=['1','2','3','4','5'];

  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  flag = 0;

  deviceType='Desktop';
  bottomcontent = 0;
  showContent  = false;
  schema = {};
  cssDy :any = {};
  postss: any = [];
  showbanner=false;
  sliderbanner=false;
  isDataFetched : boolean;
 
  

  //images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
  // config: SwiperOptions = {
  //   pagination: { el: '.swiper-pagination', clickable: true },
  //   autoplay: {
  //     delay: 2000,
  //     disableOnInteraction: false
  //   },
  //   speed: 800,
  //   loop: true,
  //   lazy:true,
  //   effect: 'fade',
  //   grabCursor: true,
  //   fadeEffect: {
  //     crossFade: true
  //   },
  //   navigation: {
  //     nextEl: '.swiper-button-next',
  //     prevEl: '.swiper-button-prev'
  //   },
  // };

  defaultImage = this.CDN_PATH+'2020/myflowertree-loader.jpg';
  totalCount: number;
  cssDyss:any={};
  constructor(private homeService: HomeService,
    public sanitizer: DomSanitizer,
    private router:Router,private gtmService: GoogleTagManagerService,
    private seoservice: SEOService,private NgxSpinner:NgxSpinnerService,

    @Inject(PLATFORM_ID) private platformId: object) {
    }
  ngOnInit() {

    this.NgxSpinner.show();
    this.resize()
    this.SEO();
    this.schemaOfPage();

    this.cssDy = { stylecss : this.getSafeCDNUrl('2019/new_home_page/stylesheets/style.css.gz')};
    


    if (isPlatformBrowser(this.platformId)) {
    this.showbanner = true;
    //this.deviceInfo = this.deviceDetectorService.getDeviceInfo();
    //deviceType = localStorage.getItem('deviceType');
    }else{
      this.showbanner = false;
     // this.sliderbanner = false;
    }
    this.isDataFetched = false;
    this.homeService.getHomePageBanner(localStorage.getItem('deviceType')).subscribe(dataa =>{
      this.isDataFetched = true;
      this.banners = dataa['banners'];
      this.NgxSpinner.hide();
    });

    this.homeService.getHomeReview(localStorage.getItem('deviceType')).subscribe(review =>{
      this.review = review['reviews'];
      this.NgxSpinner.hide();
    });
    this.homeService.getHomeBlog(localStorage.getItem('deviceType')).subscribe(blogg =>{
      this.blog = blogg['blog'];
      this.NgxSpinner.hide();
    });
    this.homeService.getHomeBestSeller(localStorage.getItem('deviceType')).subscribe(bestsell =>{
      this.best_products = bestsell['best_products'];
      this.NgxSpinner.hide();
    });
    this.homeService.getHomeConBottom(localStorage.getItem('deviceType')).subscribe(contentbot =>{


      var mmdata = JSON.parse(contentbot['content_bottom'][0].module_data);
     var contectBot = mmdata.sections;
     this.postss = contectBot[0].text[1];
     this.bottomcontent = 0;
     this.NgxSpinner.hide();

    });

    this.homeService.getHomePageData(localStorage.getItem('deviceType')).subscribe(data => {
    this.home_data = data['home_data'];
     //this.best_products = data['best_products'];
    // this.banners = data['banners'];
     //this.review = data['reviews'];
     //this.blog = data['blog'];



     if (isPlatformBrowser(this.platformId)) {
     //this.deviceType = getDevice();
     this.deviceType = localStorage.getItem('deviceType');
      if(this.deviceType=='Desktop')
      {
        this.showContent = true;
      }
    }
    this.NgxSpinner.hide();

    });


    /*this.reviewService.getReviews().subscribe(review => {
       this.review = review;
    });
    */
    this.reviewcount = this.reviewcount;

   /* this.homeService.getData_contentbottom().subscribe(postss => {
      var mmdata = JSON.parse(postss[0].module_data);
      var contectBot = mmdata.sections;
      this.postss = contectBot[0].text[1];
      this.bottomcontent = 0;
    });
    */
   /* this.homeService.getBlogs().subscribe(blog => {
      this.blog = blog;
   });
   */



    

          //===================================================================//
         
        

  }




  @HostListener('window:resize',['$event'])
    resize(event?) {
        //console.log('event=s====',event);
        const width  = event ? event.target.innerWidth : window.innerWidth;
        if(width < 420 ) {
          this.totalCount = 4
        } else if(width < 767) {
          this.totalCount = 6;
        } else {
          this.totalCount = 9;
        }
    }

  contenttoggle($event){
    let clickedElement = $event.target || $event.srcElement;

    if( clickedElement.nodeName === "BUTTON" ) {

      let isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector(".active");
      // if a Button already has Class: .active
      if( isCertainButtonAlreadyActive ) {
        isCertainButtonAlreadyActive.classList.remove("active");
      }

      clickedElement.className += " active";
    }


  }


  productClick(product_id)
  {
    //console.log('dsdad');
    for(let home of this.home_data)
    {
      for(let product of home['products'])
      {
        if(product['product_id']==product_id)
        {

          var gtmTag =  {
            'event': 'productClick',
            'ecommerce': {
              'click': {
                'actionField': {'list': 'Home Page'},
                'products': [{
                  'name': product['product_name'],
                  'id': product['product_id'],
                  'price': product['price'],
                  'brand': 'MYFLOWERTREE',
                  'category': home['category_name'],
                  'variant':  product['product_type'],
                  'position': product['sort_order'],
                }]
              }
            },
            'eventCallback': function() {
            }
          }
          //console.log('home page GTM Tags ',gtmTag);
          if(this.seoservice.isBrowserPlatform()){
            this.gtmService.pushTag(gtmTag);
          }
          //window.location.href = '/'+product['href'];
         // this.router.navigate(['/'+product['href']]);
          break;
        }
      }
    }
   // this.router.navigate(['/product/'+datalayer_productClicked.href]);
  }

  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
    return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }

  SEO()
  {
  //  this.seoservice.addCSSLink({href: "https://d3cif2hu95s88v.cloudfront.net/live-site-2016/live-new/css/nav.css.gz"});
    this.seoservice.setTitle('#1 Online Flowers, Cake and Gifts Online Delivery in India | India’s Leading Florist - MyFlowerTree');
    this.seoservice.setMeta({ property:"og:title", content:"#1 Online Flowers, Cake and Gifts Online Delivery in India | India’s Leading Florist - MyFlowerTree"});
    this.seoservice.setMeta({ name:"twitter:title", content:"#1 Online Flowers, Cake and Gifts Online Delivery in India | India’s Leading Florist - MyFlowerTree"});

    this.seoservice.setMeta({id:"desc", name:"description", content:"Flowers, Gifts & Cakes - We promise a delivery in 2 hours across India with FREE shipping!! Order from MyFlowerTree, Order now to get best discounts on fresh cake, gifts and flowers with same day delivery, midnight delivery, fixed time delivery options. "});
    this.seoservice.setMeta({property:"og:description", content:"Flowers, Gifts & Cakes - We promise a delivery in 2 hours across India with FREE shipping!! Order from MyFlowerTree, Order now to get best discounts on fresh cake, gifts and flowers with same day delivery, midnight delivery, fixed time delivery options. "});
    this.seoservice.setMeta({name:"twitter:description", content:"Flowers, Gifts & Cakes - We promise a delivery in 2 hours across India with FREE shipping!! Order from MyFlowerTree, Order now to get best discounts on fresh cake, gifts and flowers with same day delivery, midnight delivery, fixed time delivery options. "});

    this.seoservice.setMeta({property:"og:image", content:"https://www.myflowertree.com/"});
    this.seoservice.setMeta({property:"og:url", content:"https://www.myflowertree.com/"});
    this.seoservice.setMeta({name:"twitter:url", content:"https://www.myflowertree.com/"});
    this.seoservice.createLinkForCanonicalURL({href: "https://www.myflowertree.com/"});
  }
  schemaOfPage(){
    this.schema = {
      "@context": "https://schema.org",
      "@type": "WebSite",
      "url": "https://www.myflowertree.com/",
        "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.myflowertree.com/index.php?route=product/search&search={search_term}",
        "query-input": "required name=search_term"
        }
      }
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }

  navigateUrl(event:Event) {
    event.preventDefault();
    this.router.navigateByUrl('/combos');
  }
}
