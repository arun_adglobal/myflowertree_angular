import { Injectable,Inject, PLATFORM_ID  } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { isPlatformBrowser,isPlatformServer } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class HomeService {
  APIURL  = environment.API_URL;
  constructor(private http: HttpClient, 
    @Inject(PLATFORM_ID) private platformId: object) {}
  getHomePageData(devicetype) {
    return this.http.get(this.APIURL+'api/home?device='+devicetype, {
    withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getHomePageBanner(devicetype) {
    return this.http.get(this.APIURL+'api/homebanners?device='+devicetype, {
    withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getHomeReview(devicetype) {
    return this.http.get(this.APIURL+'api/homereview?device='+devicetype, {
    withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getHomeBlog(devicetype) {
    return this.http.get(this.APIURL+'api/homeblog?device='+devicetype, {
    withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getHomeBestSeller(devicetype) {
    return this.http.get(this.APIURL+'api/homebest?device='+devicetype, {
    withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getData_contentbottom() {
    return this.http.get(this.APIURL+'api/content_bottom/', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getHomeConBottom(devicetype) {
    return this.http.get(this.APIURL+'api/home_content_bottom?device='+devicetype, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  
  franchise(request_data) {
    return this.http.post(this.APIURL+'api/franchise',request_data, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }  
  corporate(request_data) {
    return this.http.post(this.APIURL+'api/corporate',request_data, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }  
  getBlogs() {
    return this.http.get(this.APIURL+'api/blog/', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  addcontactUs(customerdata)
  {
    return this.http.post(this.APIURL+'api/addcontactus',customerdata, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getData_Contactus() {
    return this.http.get(this.APIURL+'api/contactus_Desc/', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
}
