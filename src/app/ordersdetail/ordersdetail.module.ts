import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { OrdersdetailComponent } from './ordersdetail.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ OrdersdetailComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: OrdersdetailComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class OrdersdetailModule{

}