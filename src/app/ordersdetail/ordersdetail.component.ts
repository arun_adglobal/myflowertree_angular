import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { MyprofileService } from '../myprofile/myprofile.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-ordersdetail',
  templateUrl: './ordersdetail.component.html',
  styleUrls: ['./ordersdetail.component.css']
})
export class OrdersdetailComponent implements OnInit {

  orders:any;
  products:any;
  vouchers:any;
  ordertotal:any;
  orderid:number = 0;
  suborderid:number  =0;
  queryParams = {};
  constructor(private route:ActivatedRoute, private myprofileService:MyprofileService) { }

  ngOnInit(): void {

    combineLatest(this.route.params, this.route.queryParams).subscribe(
      ([params, qparams]) =>{
        this.orderid = params['string']; 
        this.suborderid = params['string1']; 
        this.myprofileService.getOrder(this.orderid, this.suborderid).subscribe((order:any) => {
         // console.log(order);          
          this.orders = order[0];
        });
      /*  this.myprofileService.getOrderProducts(this.orderid).subscribe((products:any) => {
          //console.log(products);
          this.products = products;
        });
        this.myprofileService.getOrderVouchers(this.orderid).subscribe((vouchers:any) => {
          //console.log(vouchers);
          this.vouchers = vouchers;
        });
        this.myprofileService.getOrderTotals(this.orderid).subscribe((ordertotal:any) => {
          //console.log(ordertotal);
          this.ordertotal = ordertotal;
        });

        */
        
      }
    );

  }

}
