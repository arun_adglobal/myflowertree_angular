import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import{map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class NewsroomsService {
APIURL  = environment.API_URL;
  constructor(private http:HttpClient) { }

  getAllNews(){    
    
    return this.http.get(this.APIURL+'api/newsroom', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
}
