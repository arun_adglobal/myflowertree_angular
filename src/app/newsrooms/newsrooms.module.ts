import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { NewsroomsComponent } from './newsrooms.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ NewsroomsComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: NewsroomsComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class NewsroomsModule{

}