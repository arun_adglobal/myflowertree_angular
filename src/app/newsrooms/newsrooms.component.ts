import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NewsroomsService } from './newsrooms.service';
import { SEOService } from '../seo.service';


@Component({
  selector: 'app-newsrooms',
  templateUrl: './newsrooms.component.html',
  styleUrls: ['./newsrooms.component.css'],
  providers:[NewsroomsService]
})
export class NewsroomsComponent implements OnInit {
  newroomsdata:any = [];
  constructor(private route:ActivatedRoute, 
    private seoservice: SEOService,
    private NewsroomsService:NewsroomsService) { }

  ngOnInit(): void {
    this.SEO();
    this.NewsroomsService.getAllNews().subscribe(newroomsdata => {          
      this.newroomsdata = newroomsdata;         
     // console.log(this.newroomsdata);
    });
  }
  SEO()
  {
    this.seoservice.setTitle("News Room");
}

}
