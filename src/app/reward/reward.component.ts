import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { MyprofileService } from '../myprofile/myprofile.service';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-reward',
  templateUrl: './reward.component.html',
  styleUrls: ['./reward.component.css']
})
export class RewardComponent implements OnInit {
  rewards = [];
  totalpoints = 0;
  allSubscription:Array<Subscription> = [];
  constructor(private router:Router, private myprofileService:MyprofileService,
    @Inject(PLATFORM_ID) private platformId: object ) { }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.allSubscription.push(this.myprofileService.getRewards().subscribe((data:any) => {
     
        this.totalpoints = data['totalpoints'];
        
        for(var i=0; i< data['rewards'].length; i++)
        {
          var description = '';
          if(data['rewards'][i]['points'] > 0)
          {
            description =  data['rewards'][i]['description'].replace("ORDER ID #","Earned for Order "); 
          }else{
            description =  data['rewards'][i]['description'].replace("Redeem Reword Point against ORDER ID #","Redeemed for Order "); 
          }
          var points = '';
          if(data['rewards'][i]['points'] > 0)
          {
            points =   "<span class='add-balance'>+ &#8360; "+data['rewards'][i]['points']+'</span>';
          }else{
            points = (data['rewards'][i]['points']).toString().replace("-","");
            points =  "<span class='remove-balance'>- &#8360; "+points+'</span>';
          }

          var balance = 0;
          if(i==0)
          {
            balance = this.totalpoints;
          }else{
            balance = balance - data['rewards'][i-1]['points'];
          }

          var date = new Date(data['rewards'][i]['date_added']);
          var dat = date.getDate().toString();
          if(dat.length==1)
          {
            dat = '0'+dat;
          }
          var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
          var d = dat+' '+month[date.getMonth()]+' '+date.getFullYear()+' '+this.formatAMPM(date);
          this.rewards.push({
            'order_id'    : data['rewards'][i]['order_id'],
            'points'      : points,
            'balance'  : balance,
            'description' : description,
            'date_added'  : d,
          });
        }
    }));
  }

  }
  formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    if(hours.length==1)
    {
      hours = '0'+hours;
    }
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}
