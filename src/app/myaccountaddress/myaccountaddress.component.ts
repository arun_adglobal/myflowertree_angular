import { Component, OnInit,TemplateRef ,ViewContainerRef,ViewChild,  Inject, PLATFORM_ID } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MyaccountaddressService } from './myaccountaddress.service';
import { MyprofileService } from '../myprofile/myprofile.service';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-myaccountaddress',
  templateUrl: './myaccountaddress.component.html',
  styleUrls: ['./myaccountaddress.component.css']
})
export class MyaccountaddressComponent implements OnInit {
  useraddress = [];
  addressEditForm: FormGroup;
  showModal = 0;
  defaultAddressType = 'Home';
  alternate_number_show = false;

  // Modal
  @ViewChild('modal_1') modal_1: TemplateRef<any>;
  @ViewChild('vc', {read: ViewContainerRef}) vc: ViewContainerRef;
  backdrop: any 
  // Modal
  
  constructor(private router:Router, 
    private myaccountaddressservice:MyaccountaddressService, 
    private myprofileService:MyprofileService,
    @Inject(PLATFORM_ID) private platformId: object ) { }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
    this.addressForm();
    this.myprofileService.getAddresses().subscribe((data:any) => {
      if(data.length!=0)
      {
        this.useraddress = data;
      }
    });
  }


  }
  addressForm(){
    this.addressEditForm = new FormGroup({
      'fname' : new FormControl(null,Validators.required),
      'pcode' : new FormControl(null,[Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
      'addresstype' : new FormControl(null,Validators.required),
      'customer_address' : new FormControl(null,Validators.required),
      'customer_mobile' : new FormControl(null,[Validators.required, Validators.minLength(8), Validators.maxLength(10)]),
      'customer_mobile_alternate' : new FormControl(null,[Validators.minLength(8), Validators.maxLength(10)]),
      'city_name' : new FormControl(null),
      'address_id' : new FormControl(0),
    });
  }

  addaccountAddress()
  {
    var request_data = {};
    if(this.showModal)
    {
      //console.log('dsdsdsds');
      this.addressEditForm.value['city_id'] = this.city_id;
      request_data['form_data'] = this.addressEditForm.value;
    } 
    this.myprofileService.addaccountAddress(request_data).subscribe((data:any) => {
      console.log(request_data);
      if(data.length!=0)
      {
        this.useraddress = data;
        if(this.showModal){
          this.closeDialog();
          this.addressForm();
        }
      }
    });
  }



  city_name = '';
  city_id = 0;
  delivery_charges = 0;
  searchPincodes(pincode)
  {
    
    if(pincode && pincode.length == 6)
    {
      this.myprofileService.searchPincodes(pincode).subscribe(data => {
        if(Object.keys(data).length)
        {
          this.city_name = data['city_name'];
          this.city_id = data['city_id'];
          this.delivery_charges = data['delivery_charges'];
          if(this.showModal)
          {
            this.addressEditForm.patchValue({city_name: data['city_name']});
          }
        }
      });
    }
  }
  editAddress(address_id){
    this.showModal = 1;
    if(address_id !=0)
    {
      for(let addr of this.useraddress)
      {
        if(parseInt(addr['address_id'])==parseInt(address_id))
        {
          var alternate_telephone = '';
          if(addr['alternate_telephone'] !='null')
          {
            alternate_telephone = addr['alternate_telephone'];
          }
          this.city_id = addr['city_id'];
        this.addressEditForm.patchValue({
          'fname' : addr['firstname'],
          'pcode' : addr['postcode'],
          'addresstype' : addr['address_type'],
          'customer_address' : addr['address_1'],
          'customer_mobile' : addr['telephone'],
          'customer_mobile_alternate' : alternate_telephone,
          'city_name' : addr['city'],
          'address_id' : addr['address_id']
        });

        }
      }
    }



    let view = this.modal_1.createEmbeddedView(null);
    this.vc.insert(view); 
    this.modal_1.elementRef.nativeElement.previousElementSibling.style.display = 'block';
    this.backdrop = document.createElement('DIV')
    this.backdrop.className = 'modal-backdrop';
    document.body.appendChild(this.backdrop);
}
deleteAddress(address_id)
  {
  if(confirm("Are you sure ? This Can't be undone"))
   {
    this.myprofileService.deleteAddress(address_id).subscribe((data:any) => {      
      this.useraddress = data;    
    });
   }
  }
  setDefault(address_id){
    this.myprofileService.defaultAddress(address_id).subscribe((data:any) => {      
      this.useraddress = data;    
    });
  }
closeDialog() {
  this.showModal = 0;
    this.vc.clear()
    document.body.removeChild(this.backdrop)
}



}
