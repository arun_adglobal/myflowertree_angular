import { Injectable,EventEmitter } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MyaccountaddressService {
APIURL  = environment.API_URL;
  constructor(private http: HttpClient) { }

  getAddress() {
    return this.http.get(this.APIURL+'api/useraddress', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
}
