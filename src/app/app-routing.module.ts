import { NgModule } from '@angular/core';
import { RouterModule, Routes,PreloadAllModules } from '@angular/router';
 
const routes: Routes = [
  { path: 'cart', loadChildren: () => import('./cart/cart.module').then(m => m.CartModule)}, 
  { path: 'account', loadChildren: () => import('./myaccount/account.module').then(m => m.AccountModule) },
  { path: 'customized-cake', loadChildren: () => import('./customized-cake/customized-cake.module').then(m => m.CustomizedCakeModule) },
  { path: 'corporate', loadChildren: () => import('./corporate/corporate.module').then(m => m.CorporateModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'floristlogin', loadChildren: () => import('./floristlogin/floristlogin.module').then(m => m.FloristLoginModule) },
  { path: 'customer_satisfaction', loadChildren: () => import('./feedback/feedback.module').then(m => m.FeedbackModule) },
  { path: 'contact-us', loadChildren: () => import('./contactus/contactus.module').then(m => m.ContactusModule)},
  { path: 'custom_order', loadChildren: () => import('./custom-order/custom-order.module').then(m => m.CustomOrderModule) },  
  { path: 'news-room', loadChildren: () => import('./newsrooms/newsrooms.module').then(m => m.NewsroomsModule) },
  { path: 'franchise', loadChildren: () => import('./franchise/franchise.module').then(m => m.FranchiseModule)},
  { path: 'faq', loadChildren: () => import('./faq/faq.module').then(m => m.FaqModule)},
  { path: 'about-us', loadChildren: () => import('./aboutus/aboutus.module').then(m => m.AboutusModule)},
  { path: 'privacy-policy', loadChildren: () => import('./privacy-policy/privacy-policy.module').then(m => m.PrivacyPolicyModule)},
  { path: 'refund-policy', loadChildren: () => import('./refund-policy/refund-policy.module').then(m => m.RefundPolicyModule)},
  { path: 'terms-of-use', loadChildren: () => import('./terms-of-use/terms-of-use.module').then(m => m.TermsOfUseModule)},
  { path: 'special-offers', loadChildren: () => import('./offers/offers.module').then(m => m.OffersModule)},
  { path: 'CustomizeYourBouquet', loadChildren: () => import('./customizeyourbouquet/customizeyourbouquet.module').then(m => m.CustomizeyourbouquetModule)},
  { path: 'recently-viewed', loadChildren: () => import('./recentlyviewed/recentlyviewed.module').then(m => m.RecentlyviewedModule)},
  { path: 'reviewall/:string', loadChildren: () => import('./reviewall/reviewall.module').then(m => m.ReviewallModule)},
  { path: 'reviews', loadChildren: () => import('./reviews/reviews.module').then(m => m.ReviewsModule)},
  { path: 'track-order', loadChildren: () => import('./trackorder/trackorder.module').then(m => m.TrackorderModule)},
  { path: 'st-search', loadChildren: () => import('./st-search/st-search.module').then(m => m.StSearchModule)},
  { path: 'sitemap.html', loadChildren: () => import('./sitemap/sitemap.module').then(m => m.SitemapModule)},
  { path: 'orders/info/:string/:string1', loadChildren: () => import('./ordersdetail/ordersdetail.module').then(m => m.OrdersdetailModule)},
  { path: 'paypage', loadChildren: () => import('./paypage/paypage.module').then(m => m.PaypageModule)},
  { path: 'payextra', loadChildren: () => import('./payextra/payextra.module').then(m => m.PayextraModule)},
  { path: 'personalised/:orderid/:suborderid', loadChildren: () => import('./personalised/personalised.module').then(m => m.PersonalisedModule)},
  { path: 'customer_invoice/:orderid', loadChildren: () => import('./customer-invoice/customer-invoice.module').then(m => m.CustomerInvoiceModule)},
 { path: 'checkout', loadChildren: () => import('./checkout/checkout.module').then(m => m.CheckoutModule)},
  { path: 'payment/success', loadChildren: () => import('./thankyou/thankyou.module').then(m => m.ThankyouModule)},
  { path: 'thankyou', loadChildren: () => import('./confirmation/confirmation.module').then(m => m.ConfirmationModule)},
  { path: 'orderticket', loadChildren: () => import('./orderticket/orderticket.module').then(m => m.OrderticketModule)},
  { path: 'complaintticket', loadChildren: () => import('./complaintticket/complaintticket.module').then(m => m.ComplaintticketModule)},
  { path: 'cs', loadChildren: () => import('./cs/cs.module').then(m => m.CsModule)},
  { path: 'order', loadChildren: () => import('./orders/orders.module').then(m => m.OrdersModule)},
  { path: 'buy-again', loadChildren: () => import('./orders/orders.module').then(m => m.OrdersModule)},
  { path: 'best-sellers', loadChildren: () => import('./best-sellers/best-sellers.module').then(m => m.BestSellersModule)},
  { path: 'blog', loadChildren: () => import('./blog-category/blog-category.module').then(m => m.BlogCategoryModule)},
  { path: 'product/search', loadChildren: () => import('./product-search/product-search.module').then(m => m.ProductSearchModule)},
  { path: 'quotes', loadChildren: () => import('./quotes/quotes.module').then(m => m.QuotesModule)}, 
  { path: 'article', loadChildren: () => import('./quotes/quotes.module').then(m => m.QuotesModule)}, 
  { path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule)}, 
  { path: '**', loadChildren: () => import('./miscellaneous/miscellaneous.module').then(m => m.MiscellaneousModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    relativeLinkResolution: 'legacy',
    preloadingStrategy: PreloadAllModules
})],
  exports: [RouterModule]
})
export class AppRoutingModule {
 }