import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class QuotesService {
APIURL  = environment.API_URL;
  constructor(private http: HttpClient) {}
  getQuotesPageData(slug) {
    var url = this.APIURL+'api/qcategory?slug='+slug;
    return this.http.get(url, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
 
}
