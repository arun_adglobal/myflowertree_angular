import { Component, OnInit,Injectable, Inject, PLATFORM_ID, Sanitizer } from '@angular/core';
import { QuotesService } from './quotes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { SEOService } from '../seo.service';
import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { ViewportScroller } from '@angular/common';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
declare const getDevice : any;


@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css'],
  providers: [QuotesService]
})
export class QuotesComponent implements OnInit {

  slug = '';
  deviceType = '';
  banner = '';
  mobile_banner = '';
  category_data:any = {};
  quote_products = [];
  greeting_products = [];
  CDN_PATH  = environment.CDN_PATH;
  fragment: string = 'section1';
  sanitizer: any;
  safeUrl: any;
  cssDy:any={};
  constructor(private route: ActivatedRoute,
    private quotesService: QuotesService, 
    private seoservice: SEOService, 
    public DomSanitizer:Sanitizer,
    private router:Router,
    @Inject(PLATFORM_ID) private platformId: object,
    private viewportScroller: ViewportScroller) { }

  ngOnInit(): void {
    this.cssDy = { demo_tab : this.getSafeCDNUrl('quotes/demo-tab.css.gz'),tabscss : this.getSafeCDNUrl('quotes/tabs.css.gz'),tabstyles : this.getSafeCDNUrl('quotes/tabstyles.css.gz')};
    combineLatest([this.route.params, this.route.queryParams]).subscribe(
      ([params, qparams]) => {
        this.slug = this.router.url.replace("/", "");
        this.quotesService.getQuotesPageData(this.slug).subscribe(page_data => {
          this.category_data = page_data;
          this.quote_products = page_data['quote_products'];
          this.greeting_products = page_data['greeting_products'];
          if (isPlatformBrowser(this.platformId)) {
            this.deviceType = getDevice();
            if(this.deviceType=='Desktop')
            {
            this.banner = page_data['banner'];
            }else{
              this.mobile_banner = page_data['mobile_banner'];
            }
          }
          
          if(Object.keys(this.category_data).length != 0)
          {
            this.SEO();
          }
        });

      });
  }
  toHTML(input) : any {
   // return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
  }
  onClick(elementId: string): void { 
    this.fragment = elementId; 
    this.viewportScroller.scrollToAnchor(elementId);
  }
  SEO()
  {
    this.seoservice.setTitle(this.category_data.meta_title);
    this.seoservice.setMeta({ property:"og:title", content:this.category_data.meta_title});
    this.seoservice.setMeta({ name:"twitter:title", content:this.category_data.meta_title});

    this.seoservice.setMeta({id:"desc", name:"description", content:this.category_data.meta_description});
    this.seoservice.setMeta({property:"og:description", content:this.category_data.meta_description});
    this.seoservice.setMeta({name:"twitter:description", content:this.category_data.meta_description});

    this.seoservice.setMeta({property:"og:image", content:this.category_data.banner});
    this.seoservice.setMeta({property:"og:url", content:"https://www.myflowertree.com/"+this.slug});
    this.seoservice.setMeta({name:"twitter:url", content:"https://www.myflowertree.com/"+this.slug});
    //this.seoservice.createLinkForCanonicalURL({href: "https://www.myflowertree.com/"+this.slug});
  }
  shareonWhatsapp(url){
    if (isPlatformBrowser(this.platformId)) {
    var message = encodeURIComponent(url);
    var whatsapp_url = "https://api.whatsapp.com/send?text=" + message;
   // console.log(whatsapp_url);
    window.open(whatsapp_url, '_blank');
   // window.location.href = whatsapp_url;
    }
  }
  copyCode(val: string){
    if (isPlatformBrowser(this.platformId)) {
    alert('Text Copied');
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    }
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
}
