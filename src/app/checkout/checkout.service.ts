import { Injectable,EventEmitter, Inject, PLATFORM_ID } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
APIURL  = environment.API_URL;

  loggedIn = new Subject<boolean>();
  addressSelected = new Subject<boolean>();
  addressBlock = new Subject<boolean>();
  deliveryBlock = new Subject<boolean>();
  shippingBlock = new Subject<boolean>();
  deliverySelected = new Subject<object>();
  shippingSelected = new Subject<object>();
  getMainCalendar = new Subject<object>();
  getCourierCalendar = new Subject<object>();
  getInternationalCalendar = new Subject<object>();
  getMethods = new Subject<object>();
  calendarSelected = new Subject<object>();
  refreshSideBar = new Subject<boolean>();
  resetSteps = new Subject<object>();
  
  constructor(private http: HttpClient, @Inject(PLATFORM_ID) private platformId: object) { }
  
  check_customer(request_data) {
    if(typeof(request_data.email) != 'undefined')
    {
      return this.http.get(this.APIURL+'api/check_customer?email='+request_data.email, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
    }
  }
  check_customers(request_data) {
    if(typeof(request_data.email) != 'undefined')
    {
      return this.http.get(this.APIURL+'api/check_customers?email='+request_data.email, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
    }
  }
  login(request_data) {
    if(typeof(request_data.email) != 'undefined' && typeof(request_data.password) != 'undefined')
    {
      return this.http.post(this.APIURL+'api/login',request_data, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
    }
  }
  socialLoginUpdate(request_data) {
    if(typeof(request_data.email) != 'undefined' && typeof(request_data.customer_id) != 'undefined')
    {
      return this.http.post(this.APIURL+'api/socialLoginUpdate',request_data, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
    }
  }
  sociallogin(request_data) {
    if(typeof(request_data.email) != 'undefined' && typeof(request_data.social_type) != 'undefined')
    {
      return this.http.post(this.APIURL+'api/sociallogin',request_data, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
    }
  }

  loginotp(request_data) {
    if(typeof(request_data.email) != 'undefined')
    {
      return this.http.post(this.APIURL+'api/loginotp',request_data, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
    }
  }
  getotp(request_data) {
    if(typeof(request_data.email) != 'undefined')
    {
      return this.http.post(this.APIURL+'api/optgeneration',request_data, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
    }
  }
  
  addAddress(request_data) {
      return this.http.post(this.APIURL+'api/address/addAddress',request_data, {
        headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
  }
  addAddressInternational(request_data) {
    return this.http.post(this.APIURL+'api/address/addAddress/international',request_data, {
      headers : { Authorization : `${this.getToken()}` },
    withCredentials: true
  }).pipe(map(data => {
      return data;
     }));
}
  deliverHere(request_data) {
    return this.http.post(this.APIURL+'api/address/deliverHere',request_data, {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
}
deliverHereInternational(request_data) {
  return this.http.post(this.APIURL+'api/address/deliverHere/international',request_data, {
    headers : { Authorization : `${this.getToken()}` },
    withCredentials: true
  }).pipe(map(data => {
    return data;
   }));
}
setShipping(request_data) {
  return this.http.post(this.APIURL+'api/setShipping',request_data, {
    headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
setShippingInternational(request_data) {
  return this.http.post(this.APIURL+'api/setShipping/international',request_data, {
    headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
getMethodeByPincode(request_data) {
  return this.http.post(this.APIURL+'api/getMethodeByPincode',request_data, {
    headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
  getCustomer() {
    return this.http.get(this.APIURL+'api/customer', {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }

  getTrackOrderBanner() {
    return this.http.get(this.APIURL+'api/occasionBanner', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }

  getProductView() {
    return this.http.get(this.APIURL+'api/getProductView', {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getProductViewInternational() {
    return this.http.get(this.APIURL+'api/getProductView/international', {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  coupon(coupon) {
    return this.http.get(this.APIURL+'api/coupon?coupon='+coupon, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  paypage(orderid) {
    return this.http.get(this.APIURL+'api/paypage?orderid='+orderid, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  applyReward(reward) {
    return this.http.get(this.APIURL+'api/applyReward?reward='+reward, {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  removeCoupon() {
    return this.http.get(this.APIURL+'api/removeCoupon', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  removeReward() {
    return this.http.get(this.APIURL+'api/removeReward', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getAddress() {
    return this.http.get(this.APIURL+'api/address', {
      headers : { Authorization : `${this.getToken()}` },
	  withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getAddressInternational() {
    return this.http.get(this.APIURL+'api/address/international', {
      headers : { Authorization : `${this.getToken()}` },
	  withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  searchPincode(pincode) {
    return this.http.get(this.APIURL+'api/searchPincode?pincode='+pincode, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  searchCity(city) {
    return this.http.get(this.APIURL+'api/searchCity?city='+city, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  deleteAddress(address_id) {
    return this.http.delete(this.APIURL+'api/address/delete',{params: {address_id: address_id}, headers : { Authorization : `${this.getToken()}` },withCredentials: true}).pipe(map(data => {
      return data;
     }));
  }
  deleteAddressInternational(address_id) {
    return this.http.delete(this.APIURL+'api/address/delete/international',{params: {address_id: address_id}, headers : { Authorization : `${this.getToken()}` },withCredentials: true}).pipe(map(data => {
      return data;
     }));
  }
  addCustomerChk(customerdata)
  {
    return this.http.post(this.APIURL+'api/addcustomer',customerdata, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  directPay(request)
  {
    return this.http.post(this.APIURL+'api/directPay',request, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  extraPay(request)
  {
    return this.http.post(this.APIURL+'api/extraPay',request, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  updateCustomer(customerdata)
  {
    var request = {}
    request['name'] = customerdata['register_name'];
    request['telephone'] = customerdata['register_telephone'];
    request['country_code'] = customerdata['register_country_code'];
    return this.http.post(this.APIURL+'api/updateCustomer',request, {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  updateCart(request_data)
  {
    return this.http.post(this.APIURL+'api/updateCart',request_data, {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  updateCartInternational(request_data)
  {
    return this.http.post(this.APIURL+'api/updateCart/international',request_data, {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  private getToken():string{
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem('userToken');
    }
  }

  getShipping(device,browser) {
    return this.http.get(this.APIURL+'api/getShipping?device='+device+'&browser='+browser, {
      headers : { Authorization : `${this.getToken()}`},
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getInternationalShipping(device,browser) {
    return this.http.get(this.APIURL+'api/getInternationalShipping?device='+device+'&browser='+browser, {
      headers : { Authorization : `${this.getToken()}`},
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  initiateCheckout() {
    return this.http.get(this.APIURL+'api/initiateCheckout', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getMFTWallet() {
    return this.http.get(this.APIURL+'api/getMFTWallet', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  addCustomerChkSocial(customerdata)
  {
    return this.http.post(this.APIURL+'api/addcustomerSocial',customerdata, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }

  addFlorist(customerdata)
  {
    return this.http.post(this.APIURL+'api/addflorist',customerdata, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
}
