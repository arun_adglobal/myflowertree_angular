import { Component, OnInit, ViewEncapsulation, Inject, PLATFORM_ID } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CheckoutService } from '../checkout.service';
import { AuthenticationService } from '../../authentication.service';
import { AuthService, FacebookLoginProvider, SocialUser,GoogleLoginProvider } from 'angularx-social-login';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';

declare var $:any;

@Component({
 selector: 'app-checkout-one-international',
 templateUrl: './checkout-one-international.component.html',
 styleUrls: ['./checkout-one-international.component.css'],
 encapsulation : ViewEncapsulation.None
})
export class CheckoutOneComponentInternational implements OnInit {
 loginForm: FormGroup;
 signupForm: FormGroup;
 CDN_PATH  = environment.CDN_PATH;
 signupSocailForm: FormGroup;
 loginOTPForm: FormGroup;
 editForm: FormGroup;
 is_customer = false;
 view = 1;
 checked = "true";
 customer_email = '';
 otp = '';
 telephone = '';
 error = '';
 private allSubscription:Array<Subscription> = [];
 customer_data = {};
 user: SocialUser;
 loggedIn: boolean;
 constructor(private checkoutService: CheckoutService, private auth:AuthenticationService,
   private router:Router,
   private authService: AuthService,
   private socialAuthService: AuthService,
   private gtmService: GoogleTagManagerService,private NgxSpinner:NgxSpinnerService,
   @Inject(PLATFORM_ID) private platformId: object

   ) {  }
 ngOnInit(): void {
   this.allSubscription.push(this.authService.authState.subscribe((user) => {
     this.user = user;
     this.loggedIn = (user != null);
     if(user != null){
     }
   }));

   this.allSubscription.push(this.checkoutService.getCustomer().subscribe((customer_data:any[]) => {
     if(typeof customer_data['email'] !='undefined')
     {
       this.customer_data = customer_data;
       this.view = 5;
       this.checkoutService.loggedIn.next(true);
       var gtmTag = {
         'event': 'user-login',
         'user_data': {
           'reward_points': this.customer_data['user_points'],
           'wallet': this.customer_data['user_cash'],
           'we_first_name': this.customer_data['we_first_name'],
           'we_last_name': this.customer_data['we_last_name'],
           'we_email': this.customer_data['we_email'],
           'we_phone': this.customer_data['we_phone'],
           'fname': this.customer_data['fname'],
           'telephone': this.customer_data['telephone'],
           'email': this.customer_data['email'],
           'login_medium': this.customer_data['login_medium'],
           'login_activity': this.customer_data['login_activity']
       }}
       //console.log('Checkout one GTM Tag ',gtmTag);
       this.gtmService.pushTag(gtmTag);
       this.checkoutService.refreshSideBar.next(true); // on refresh page update right bar
       this.NgxSpinner.hide();
     }
   }));

   this.loginForm = new FormGroup({
     'login_email' : new FormControl(null,[Validators.required,Validators.email])
   });
 }


 public socialSignIn_11(socialPlatform : string) {
   //console.log('dsdsdd');

   let socialPlatformProvider;
   if(socialPlatform == "facebook"){
     socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
   }else if(socialPlatform == "google"){
     socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
   }
   //console.log(socialPlatformProvider);
   this.socialAuthService.signIn(socialPlatformProvider).then(
     (userData) => {


    //  console.log(socialPlatform+" sign in data : " , userData.facebook);
   //   console.log(socialPlatform+" sign in data : " , userData);
       // Now sign-in with userData
       // ...
         if(socialPlatform == "facebook"){
           var request_data = {};
           //console.log(userData.facebook['email']);
           request_data['email'] = userData.facebook['email'];
           request_data['first_name'] = userData.facebook['first_name'];
           request_data['last_name'] = userData.facebook['last_name'];
           request_data['social_type'] = socialPlatform;
           request_data['id'] = userData.facebook['id'];

          this.allSubscription.push(this.checkoutService.addCustomerChkSocial(request_data).subscribe(response => {

             //console.log(response);
             if (isPlatformBrowser(this.platformId)) {
               $("#callbackloader").css('display','none');
             }
             if(typeof response['email'] !='undefined')
             {
               this.customer_data = response;
               this.auth.saveToken(response['token']);
               this.view = 5;
               this.checkoutService.loggedIn.next(true);
               var gtmTag = {
                 'event': 'user-login',
                 'user_data': {
                   'email': this.customer_data['email'],
                   'login_medium': this.customer_data['login_medium'],
                   'login_activity': this.customer_data['login_activity'],
                   'reward_points': this.customer_data['user_points'],
                   'wallet': this.customer_data['user_cash'],
                   'we_first_name': this.customer_data['we_first_name'],
                   'we_last_name': this.customer_data['we_last_name'],
                   'we_email': this.customer_data['we_email'],
                   'we_phone': this.customer_data['we_phone'],
                   'fname': this.customer_data['fname'],
                   'telephone': this.customer_data['telephone'],
               }}
               this.gtmService.pushTag(gtmTag);
               this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
             }else if(typeof response['cart_empty'] !='undefined')
             {
               this.router.navigate(['/']);
             }else if(typeof response['error'] !='undefined')
             {
               this.error = response['error'];
             }

           }));
         }
         this.NgxSpinner.hide();
     }
   );
 }




 loggedin_as = '';
 social_customer_id = '';
 social_email = '';

 signInWithGoogle(): void {
   this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
 }

 socialSignIn(socialPlatform : string) {
  this.NgxSpinner.show();
   var request_data = {};
   let socialPlatformProvider;
   if(socialPlatform == "facebook"){
     socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
   }else if(socialPlatform == "google"){
     socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
   }
   this.socialAuthService.signIn(socialPlatformProvider).then(
   (userData) => {
     //console.log("userData "+userData.facebook['email']);
     if(socialPlatform == "facebook"){
       request_data['social_type'] = 'fb';
       request_data['email'] = userData.facebook['email'];
       request_data['first_name'] = userData.facebook['first_name'];
       request_data['last_name'] = userData.facebook['last_name'];
       request_data['id'] = userData.facebook['id'];
     }else if(socialPlatform == "google"){
       request_data['social_type'] = 'google';
       request_data['email'] = userData['email'];
       request_data['first_name'] = userData['firstName'];
       request_data['last_name'] = userData['lastName'];
       request_data['id'] = userData['id'];
     }
    // console.log("request_data "+request_data);
    this.allSubscription.push(this.checkoutService.sociallogin(request_data).subscribe((customer_data:any[]) => {
       if(typeof customer_data !='undefined' && customer_data['telephone'] !='')
       {
         this.customer_data = customer_data;
         this.auth.saveToken(customer_data['token']);
         this.view = 5;
         this.checkoutService.loggedIn.next(true);
         var gtmTag = {
           'event': 'user-login',
           'user_data': {
             'email': this.customer_data['email'],
             'login_medium': this.customer_data['login_medium'],
             'login_activity': this.customer_data['login_activity'],
             'reward_points': this.customer_data['user_points'],
             'wallet': this.customer_data['user_cash'],
             'we_first_name': this.customer_data['we_first_name'],
             'we_last_name': this.customer_data['we_last_name'],
             'we_email': this.customer_data['we_email'],
             'we_phone': this.customer_data['we_phone'],
             'fname': this.customer_data['fname'],
             'telephone': this.customer_data['telephone']
         }}
         this.gtmService.pushTag(gtmTag);
         this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
       }else if(typeof customer_data !='undefined' && customer_data['telephone']=='')
       {
         var gtmTag = {
           'event': 'user-login',
           'user_data': {
             'email': this.customer_data['email'],
             'login_medium': this.customer_data['login_medium'],
             'login_activity': this.customer_data['login_activity'],
             'reward_points': this.customer_data['user_points'],
             'wallet': this.customer_data['user_cash'],
             'we_first_name': this.customer_data['we_first_name'],
             'we_last_name': this.customer_data['we_last_name'],
             'we_email': this.customer_data['we_email'],
             'we_phone': this.customer_data['we_phone'],
             'fname': this.customer_data['fname'],
             'telephone': this.customer_data['telephone']
         }}
         this.gtmService.pushTag(gtmTag);
         this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
         this.loggedin_as = customer_data['email'];

         this.social_customer_id = customer_data['customer_id'];
         this.social_email = customer_data['email'];
         this.customer_data = {};
         this.signupSocailForm = new FormGroup({
           'register_name' : new FormControl(customer_data['fname'], Validators.required),
           'register_telephone' : new FormControl(null,  [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
           'register_country_code' : new FormControl('91', Validators.required)
         });
         this.view = 4;
       }

       this.NgxSpinner.hide();
      }));
     
   })
 }
 valid_social_from = true;
 submitSocialSignupForm(){
   if(! this.signupSocailForm.valid)
   {
     this.valid_social_from = false;
     this.signupSocailForm.markAllAsTouched();
   }else{
     this.valid_social_from = true;
   }
   if(this.valid_social_from)
   {
     var request_data = {};
     request_data['name'] = this.signupSocailForm.value.register_name;
     request_data['country_code'] = this.signupSocailForm.value.register_country_code;
     request_data['telephone'] = this.signupSocailForm.value.register_telephone;
     request_data['customer_id'] =  this.social_customer_id;
     request_data['email'] = this.social_email;

     this.allSubscription.push(this.checkoutService.socialLoginUpdate(request_data).subscribe((customer_data:any[]) => {
       if(typeof customer_data['email'] !='undefined')
       {
         this.customer_data = customer_data;
         this.auth.saveToken(customer_data['token']);
         this.view = 5;
         this.checkoutService.loggedIn.next(true);
       }else if(typeof customer_data['cart_empty'] !='undefined')
       {
         this.router.navigate(['/']);
       }else if(typeof customer_data['error'] !='undefined')
       {
         this.error = customer_data['error'];
       }
     }));
   }
 }
 valid_login = true;
 submitLoginForm()
 {

   if(! this.loginForm.valid)
     {
       this.valid_from = false;
       this.loginForm.markAllAsTouched();

     }else{
       this.valid_from = true;
     }

  // console.log('dssdsdsd');

   var request_data = {};
   request_data['email'] = this.loginForm.value.login_email;
   if(! this.is_customer && this.valid_from)
   {
    this.allSubscription.push(this.checkoutService.check_customer(request_data).subscribe(response => {

       if(typeof(response['email']) !='undefined')
       {
         this.loginForm = new FormGroup({
           'login_email' : new FormControl(response['email'],[Validators.required,Validators.email]),
           'login_password' : new FormControl(null, Validators.required)
         });
         this.is_customer = true;
       }else if(typeof(response['email']) =='undefined')
       {
         this.view = 2;
         this.signupForm = new FormGroup({
           'register_email' : new FormControl(request_data['email'],[Validators.required,Validators.email]),
           'register_name' : new FormControl(null, Validators.required),
           'register_password' : new FormControl(null, Validators.required),
           'register_telephone' : new FormControl(null, [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
           'register_country_code' : new FormControl('91', Validators.required),
           'gender' : new FormControl(null, Validators.required)
         });
       }
     }));
   }else
   {
     request_data['password'] = this.loginForm.value.login_password;
    //  if (isPlatformBrowser(this.platformId)) {
    //    $("#callbackloader").css('display','block');
    //  }
     this.NgxSpinner.show();
    this.allSubscription.push(this.checkoutService.login(request_data).subscribe((customer_data:any[]) => {
      //  if (isPlatformBrowser(this.platformId)) {
      //    $("#callbackloader").css('display','none');
      //  }
       if(typeof customer_data['email'] !='undefined')
       {
         this.customer_data = customer_data;
         this.auth.saveToken(customer_data['token']);
         this.view = 5;
         this.checkoutService.loggedIn.next(true);
         var gtmTag = {
           'event': 'user-login',
           'user_data': {
             'email': this.customer_data['email'],
             'login_medium': this.customer_data['login_medium'],
             'login_activity': this.customer_data['login_activity'],
             'reward_points': this.customer_data['user_points'],
             'wallet': this.customer_data['user_cash'],
             'we_first_name': this.customer_data['we_first_name'],
             'we_last_name': this.customer_data['we_last_name'],
             'we_email': this.customer_data['we_email'],
             'we_phone': this.customer_data['we_phone'],
             'fname': this.customer_data['fname'],
             'telephone': this.customer_data['telephone']
         }}
         this.gtmService.pushTag(gtmTag);
         this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
         
       }else if(typeof customer_data['cart_empty'] !='undefined')
       {
         this.router.navigate(['/']);
       }else if(typeof customer_data['error'] !='undefined')
       {
         this.error = customer_data['error'];
       }
       this.NgxSpinner.hide();
     }));
   }

 }
 valid_from = true;
 submitSignupForm(){

   if(! this.signupForm.valid)
     {
       this.valid_from = false;
       this.signupForm.markAllAsTouched();
     }else{
       this.valid_from = true;
     }
   if(this.valid_from)
   {
     if (isPlatformBrowser(this.platformId)) {
       $("#callbackloader").css('display','block');
     }

       var request_data = {};
       request_data['register_email'] = this.signupForm.value.register_email;
       request_data['register_name'] = this.signupForm.value.register_name;
       request_data['register_password'] = this.signupForm.value.register_password;
       request_data['register_country_code'] = this.signupForm.value.register_country_code;
       request_data['register_telephone'] = this.signupForm.value.register_telephone;
       request_data['gender'] = this.signupForm.value.gender;
       request_data['email'] = this.signupForm.value.register_email;
       request_data['password'] = this.signupForm.value.register_password;
       //console.log(request_data);
       this.NgxSpinner.show();
       this.allSubscription.push(this.checkoutService.addCustomerChk(request_data).subscribe(response => {
         this.checkoutService.login(request_data).subscribe((customer_data:any[]) => {
          //  if (isPlatformBrowser(this.platformId)) {
          //    $("#callbackloader").css('display','none');
          //  }

           if(typeof customer_data['email'] !='undefined')
           {
             this.customer_data = customer_data;
             this.auth.saveToken(customer_data['token']);
             this.view = 5;
             this.checkoutService.loggedIn.next(true);
             var gtmTag = {
               'event': 'user-login',
               'user_data': {
                 'email': this.customer_data['email'],
                 'login_medium': this.customer_data['login_medium'],
                 'login_activity': 'signup',
                 'reward_points': this.customer_data['user_points'],
                 'wallet': this.customer_data['user_cash'],
                 'we_first_name': this.customer_data['we_first_name'],
                 'we_last_name': this.customer_data['we_last_name'],
                 'we_email': this.customer_data['we_email'],
                 'we_phone': this.customer_data['we_phone'],
                 'fname': this.customer_data['fname'],
                 'telephone': this.customer_data['telephone']
             }}
             this.gtmService.pushTag(gtmTag);
             this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
           }else if(typeof customer_data['cart_empty'] !='undefined')
           {
             this.router.navigate(['/']);
           }else if(typeof customer_data['error'] !='undefined')
           {
             this.error = customer_data['error'];
           }
           this.NgxSpinner.hide();
         });

       }));
     }

 }

 checkloginotp()
 {
   var request_data = {};

   request_data['email'] = this.loginForm.value.login_email;
   var n = 90;
   var tm = setInterval(function(){
      n--;
   if(n == 0){
     clearInterval(tm);
     if (isPlatformBrowser(this.platformId)) {
       $('#resendcheckloginotp').css('display','block');
       $('#resend_otp_waiting_msg').html('');
     }
   }else{
     if (isPlatformBrowser(this.platformId)) {
       $('#resendcheckloginotp').css('display','none');
       $('#resend_otp_waiting_msg').html('Resend OTP in ('+n+') sec');
     }
   }
   }, 1000);
    this.allSubscription.push(this.checkoutService.getotp(request_data).subscribe(responses => {
       this.otp = responses['otp'];
       this.telephone = responses['telephone'];
     }));
     this.view = 10;
     this.loginOTPForm = new FormGroup({
       'login_OTP_email' : new FormControl(request_data['email'],[Validators.required,Validators.email]),
       'login_otp_password' : new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
       'matchotp' : new FormControl(this.otp)
     });
 }
 resendcheckloginotp()
 {
   var request_data = {};
   request_data['email'] = this.loginForm.value.login_email;
   var n = 90;
   var tm = setInterval(function(){
      n--;
   if(n == 0){
     clearInterval(tm);
     if (isPlatformBrowser(this.platformId)) {
       $('#resendcheckloginotp').css('display','block');
       $('#resend_otp_waiting_msg').html('');
     }
   }else{
     if (isPlatformBrowser(this.platformId)) {
       $('#resendcheckloginotp').css('display','none');
       $('#resend_otp_waiting_msg').html('Resend OTP in ('+n+') sec');
     }
   }
   }, 1000);
    this.allSubscription.push(this.checkoutService.getotp(request_data).subscribe(responses => {
       this.otp = responses['otp'];
       this.telephone = responses['telephone'];
     }));
 }
 submitLoginwithOTPForm()
 {
   var request_data = {};
   request_data['email'] = this.loginOTPForm.value.login_OTP_email;
   request_data['otp_password'] = this.loginOTPForm.value.login_otp_password;
   request_data['matchotp'] = this.loginOTPForm.value.matchotp;
 //console.log(request_data);
   if(request_data['otp_password']==this.otp)
   {
    //  if (isPlatformBrowser(this.platformId)) {
    //    $("#callbackloader").css('display','block');
    //  }
    this.NgxSpinner.show();
    this.allSubscription.push(this.checkoutService.loginotp(request_data).subscribe((customer_data:any[]) => {
      //  if (isPlatformBrowser(this.platformId)) {
      //    $("#callbackloader").css('display','none');
      //  }
       if(typeof customer_data['email'] !='undefined')
       {
         this.customer_data = customer_data;
         this.auth.saveToken(customer_data['token']);
         this.view = 5;
         this.checkoutService.loggedIn.next(true);

         var gtmTag = {
           'event': 'user-login',
           'user_data': {
             'email': this.customer_data['email'],
             'login_medium': this.customer_data['login_medium'],
             'login_activity': this.customer_data['login_activity']
         }}
         this.gtmService.pushTag(gtmTag);
         this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
       }else if(typeof customer_data['cart_empty'] !='undefined')
       {
         this.router.navigate(['/']);
       }else if(typeof customer_data['error'] !='undefined')
       {
         this.error = customer_data['error'];
       }
       this.NgxSpinner.show();
     }));
   }else
   {
    // console.log(this.otp);
     this.error = 'Please enter correct OTP';
     this.loginOTPForm = new FormGroup({
       'login_OTP_email' : new FormControl(request_data['email'],[Validators.required,Validators.email]),
       'login_otp_password' : new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
       'matchotp' : new FormControl(this.otp)
     });
   }
 }
 editLogin(){
   this.checkoutService.resetSteps.next({'login':1});
   this.view = 6;
   this.loggedin_as = this.customer_data['email'];

   this.editForm = new FormGroup({
     'register_name' : new FormControl(this.customer_data['firstname']+' '+ this.customer_data['lastname'], Validators.required),
     'register_telephone' : new FormControl(this.customer_data['telephone'],  [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
     'register_country_code' : new FormControl('91', Validators.required)
   });
 }
 valid_edit_from = true;
 submitEditForm()
 {
   var request_data = {};
   request_data['register_name'] = this.editForm.value.register_name;
   request_data['register_telephone'] = this.editForm.value.register_telephone;
   request_data['register_country_code'] = this.editForm.value.register_country_code;
   if(! this.editForm.valid)
     {
       this.valid_edit_from = false;
       this.editForm.markAllAsTouched();
     }else{
       this.valid_edit_from = true;
     }
   if(this.valid_edit_from)
   {
    //  if (isPlatformBrowser(this.platformId)) {
    //    $("#callbackloader").css('display','block');
    //  }
    this.NgxSpinner.show();
      this.allSubscription.push(this.checkoutService.updateCustomer(request_data).subscribe((customer_data:any[]) => {
        //  if (isPlatformBrowser(this.platformId)) {
        //    $("#callbackloader").css('display','none');
        //  }

         if(typeof customer_data['email'] !='undefined')
         {
           customer_data['fname']=this.editForm.value.register_name;
           customer_data['telephoneCode']=this.editForm.value.register_telephone;
           this.customer_data = customer_data;
           this.auth.saveToken(customer_data['token']);
           this.view = 5;
           this.checkoutService.loggedIn.next(true);
         }else if(typeof customer_data['cart_empty'] !='undefined')
         {
           this.router.navigate(['/']);
         }else if(typeof customer_data['error'] !='undefined')
         {
           this.error = customer_data['error'];
         }
         this.NgxSpinner.hide();
       }));
   }

 }
  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}


