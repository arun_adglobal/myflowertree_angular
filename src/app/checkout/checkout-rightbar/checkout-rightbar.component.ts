import { Component, OnInit, ViewEncapsulation,OnDestroy,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { CheckoutService } from '../checkout.service';
import { CartService } from '../../cart/cart.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import { isPlatformBrowser } from '@angular/common';
declare const getDevice : any;

@Component({
  selector: 'app-checkout-rightbar',
  templateUrl: './checkout-rightbar.component.html',
  styleUrls: ['./checkout-rightbar.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class CheckoutRightbarComponent implements OnInit,OnDestroy {

  products = [];
  totals = [];
  deviceType='Desktop';
  filter_total = [];
  CDN_PATH  = environment.CDN_PATH;
  coupon_error = '';
  coupon_msg = '';
  coupon =  '';
  reward_error = '';
  reward_msg = '';
  reward =  '';
  coupon_input = false;
  reward_input = false;
  available_rewards = 0;
  allSubscription:Array<Subscription> = [];
  offer_applied = false;
  private refreshSideBarSubscribe: Subscription;
  constructor(private checkoutService: CheckoutService,
    private cartService:CartService,
    private gtmService: GoogleTagManagerService,@Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.getCart();

    if (isPlatformBrowser(this.platformId)) {
      this.deviceType = getDevice();

      if(localStorage.getItem('coupon') !=null)
      {
        //this.offer_applied = true;
        this.coupon = localStorage.getItem('coupon');
        //localStorage.removeItem('coupon');
      }
      if(localStorage.getItem('reward') !=null)
      {
        //this.offer_applied = true;
        this.reward = localStorage.getItem('reward');
        //localStorage.removeItem('reward');
      }
    }
    this.allSubscription.push(this.checkoutService.refreshSideBar.subscribe(flag => {
      if(flag)
      {
        this.getCart();
      }
    }));
  }

  getCart(){
    this.allSubscription.push(this.cartService.getCartData().subscribe((cart_data:any) => {
    //  console.log("cart_data[0]['coupon'] "+cart_data[0]['coupon']);
      if(cart_data[0]['coupon']=="")
      {
        if (isPlatformBrowser(this.platformId)) {
          if(localStorage.getItem('coupon') !=null)
          {
            localStorage.removeItem('coupon');
            this.coupon = '';
          }
        }
      }
   //   console.log("cart_data[0]['cart_data'][0]['applied_reward'] "+cart_data[0]['cart_data'][0]['applied_reward']);
      if(cart_data && cart_data[0] && cart_data[0]['cart_data'] && cart_data[0]['cart_data'][0] && (typeof(cart_data[0]['cart_data'][0]['applied_reward'])!='undefined' && cart_data[0]['cart_data'][0]['applied_reward']==0))
      {
        if (isPlatformBrowser(this.platformId)) {
          if(localStorage.getItem('reward') !=null)
          {
            localStorage.removeItem('reward');
            this.reward = '';
          }
        }
      }
      if(cart_data[0]['available_rewards']==null)
      {
        this.available_rewards = 0;
      }else
      {
        this.available_rewards = cart_data[0]['available_rewards'];
      }
      
     console.log("this.available_rewards "+this.available_rewards);
      this.products = cart_data[0]['cart_data'];
      this.totals = cart_data[0]['totals'];
      this.filter_total = cart_data[0]['filter_total'];
      var gtmTag = {
        'contentGrouping': (this.products && this.products[0]) ? this.products[0]['parent_category_path'] : null,
        'products': this.gtag_products(),
        'cartTotals': this.filter_total
      }
      this.gtmService.pushTag(gtmTag);

      if(this.fire_coupon_event==1)
      {
        var shipping = 0;
          var sub = 0;
          var total = 0;
          for(let t of this.totals)
          {
            if(t['code']=='sub_total')
            {
              sub = t['value'];
            }
            if(t['code']=='shipping')
            {
              shipping = t['value'];
            }
            if(t['code']=='total')
            {
              total = t['value'];
            }
          }
          var cart_before_discount = sub;
          var cart_after_discount = total;
          var is_coupon = this.coupon;
          var discount_value = (cart_before_discount+shipping) - cart_after_discount;
          var gtmTag1 = {
            'event': 'couponApplied',
            'coupon_data': {
              'before_discount': cart_before_discount,
              'after_discount': cart_after_discount,
              'discount_value': discount_value,
              'coupon': is_coupon
            }}
          this.gtmService.pushTag(gtmTag1);
      }


     }));
  }
  gtag_products()
  {
    var products = [];
    for(let product of this.products)
    {
      products.push({
        'cart_id'   : product['cart_id'],
        'key'    : product['key'],
        'queryParams' : product['queryParams'],
        'product_id'   : product['product_id'],
        'product_type' : product['product_type'],
        'thumb'     : product['thumb'],
        'name'     : product['name'],
        'model'     : product['model'],
        'cate_name'   : product['cate_name'],
        'parent_category_path' : product['parent_category_path'],
        'option'    : product['option'],
        'quantity'  : product['quantity'],
        'delivery_date'  : product['delivery_date'],
        'shipping_time_slot'  : product['shipping_time_slot'],
        'price'     : product['price'],
        'pro_price'     : product['price'],
        'addon'     : product['addon'],
        'total'    : product['total'],
        'href'      : product['href'],
        'images' : product['images'],
        'product_url' : product['product_url'],
        'url' :  product['url'],
      });
    }
    return products;
  }
  applyReward(rewardCode){
    this.coupon = '';
    this.coupon_msg = '';
    this.coupon_error = '';
    this.reward = '';
    this.reward_msg = '';
    this.reward_error = '';
    if(rewardCode.value=='')
    {
      this.reward_error = 'Please enter reward points';
    }else if(rewardCode.value > 100)
    {
      this.reward_error = 'Maximum 100 Rewards Points can be redeemed against an order!';
    }else{
      this.reward_error = '';
      this.allSubscription.push(this.checkoutService.applyReward(rewardCode.value).subscribe((data) => {
        if(typeof(data['error']) !='undefined')
        {
          this.reward_error = data['error'];
        }
        if(typeof(data['success']) !='undefined')
        {

          this.offer_applied = true;
          this.reward_msg = data['success'];
          if (isPlatformBrowser(this.platformId)) {
            localStorage.setItem('reward', data['reward']);
          }
          this.reward = data['reward'];
          this.checkoutService.refreshSideBar.next(true);
          this.coupon_input = false;
        }
      }));
    }
  }


  fire_coupon_event = 0;
  applyCoupon(couponCode){
    this.coupon = '';
    this.coupon_msg = '';
    this.coupon_error = '';
    this.reward = '';
    this.reward_msg = '';
    this.reward_error = '';
    if(couponCode.value=='')
    {
      this.coupon_error = 'Please enter a coupon code';
    }else{
      this.allSubscription.push(this.checkoutService.coupon(couponCode.value).subscribe((data) => {
        if(typeof(data['default_coupon']) !='undefined')
        {
          if(data['default_coupon'] !="")
          {
            this.coupon_msg = data['default_coupon'];
          }
        }
        if(typeof(data['error']) !='undefined')
        {
          this.coupon_error = data['error'];
        }
        if(typeof(data['success']) !='undefined')
        {
          this.offer_applied = true;
          this.coupon_msg = data['success'];
          if (isPlatformBrowser(this.platformId)) {
            localStorage.setItem('coupon', data['coupon']);
          }
          this.coupon = data['coupon'];
          this.checkoutService.refreshSideBar.next(true);
          this.fire_coupon_event = 1;
        }
      }));
    }
  }
  removeCoupon(){
    this.allSubscription.push(this.checkoutService.removeCoupon().subscribe((data) => {
      if (isPlatformBrowser(this.platformId)) {
        localStorage.removeItem('coupon');
      }
      this.offer_applied = false;
      this.coupon = '';
      this.coupon_msg = 'Coupon is removed successfully';
      this.checkoutService.refreshSideBar.next(true);
    }));
  }
  removeReward(){
    this.allSubscription.push(this.checkoutService.removeReward().subscribe((data) => {
      if (isPlatformBrowser(this.platformId)) {
        localStorage.removeItem('reward');
      }
      this.offer_applied = false;
      this.reward = '';
      this.reward_msg = 'Reward is removed successfully';
      this.checkoutService.refreshSideBar.next(true);
    }));
  }
  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}
