import { Component, OnInit,TemplateRef ,ViewContainerRef,ViewChild,OnDestroy,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CheckoutService } from '../checkout.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';
declare var $:any;

@Component({
  selector: 'app-checkout-two',
  templateUrl: './checkout-two.component.html',
  styleUrls: ['./checkout-two.component.css']
})
export class CheckoutTwoComponent implements OnInit,OnDestroy {
  addressForm: FormGroup;
  addressEditForm: FormGroup;
  CDN_PATH  = environment.CDN_PATH;
  view = 1;
  addinfo = [];
  select_addinfo = {};
  alternate_number_show = false;
  allSubscription:Array<Subscription> = [];
  showModal = 0;
  defaultAddressType = 'Home';
  // Modal
  @ViewChild('modal_1') modal_1: TemplateRef<any>;
  @ViewChild('vc', {read: ViewContainerRef}) vc: ViewContainerRef;
  backdrop: any
  // Modal
  constructor(private checkoutService: CheckoutService,
    private gtmService: GoogleTagManagerService, private NgxSpinner:NgxSpinnerService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.allSubscription.push(this.checkoutService.resetSteps.subscribe(data => {
      if(typeof(data['login']) !='undefined')
      {
        this.view = 1;
      }
    }));
    this.addressForm = new FormGroup({
      'fname' : new FormControl(null,Validators.required),
      'pcode' : new FormControl(null,[Validators.required, Validators.minLength(5), Validators.maxLength(6)]),
      'addresstype' : new FormControl(null,Validators.required),
      'customer_address' : new FormControl(null,Validators.required),
      'customer_mobile' : new FormControl(null,[Validators.required, Validators.minLength(8), Validators.maxLength(10)]),
      'customer_mobile_alternate' : new FormControl(null,[Validators.minLength(8), Validators.maxLength(10)]),
      'city_name' : new FormControl(null,Validators.required)
    });
    this.allSubscription.push(this.checkoutService.loggedIn.subscribe(flag => {
      if(flag)
      {
        this.addressList();
      }
    }));
  }

  deleteAddress(address_id)
  {
    if(confirm("Are you sure ? This Can't be undone"))
    {
     this.allSubscription.push(this.checkoutService.deleteAddress(address_id).subscribe(data => {
        if(Object.keys(data).length)
        {
          this.addinfo = data['addinfo'];
          this.view = 2;
          if(typeof(data['addinfo']) !='undefined' && data['addinfo'].length ==0)
          {
            window.location.reload();
          }
        }else
        {
          this.view = 3;
        }
      }));
    }
  }
  valid_from = true;
  addAddress()
  {
    var request_data = {};
    if(this.showModal)
    {
      this.addressEditForm.value['city'] = this.city_id;
      request_data['form_data'] = this.addressEditForm.value;
      if(! this.addressEditForm.valid)
      {
        this.valid_from = false;
        this.addressEditForm.markAllAsTouched();
      }else{
        this.valid_from = true;
      }
    }else
    {

      this.addressForm.value['address_id'] = 0;
      this.addressForm.value['city'] = this.city_id;
      request_data['form_data'] = this.addressForm.value;
      if(! this.addressForm.valid)
      {
        this.valid_from = false;
        this.addressForm.markAllAsTouched();
      }else{
        this.valid_from = true;
      }
    }
    if(this.valid_from)
    {
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','block');
      // }
      this.NgxSpinner.show();
      this.allSubscription.push(this.checkoutService.addAddress(request_data).subscribe(data => {
        // if (isPlatformBrowser(this.platformId)) {
        //  $("#callbackloader").css('display','none');
        // }
        this.NgxSpinner.hide();

        this.checkoutService.addressSelected.next(true);
        if(Object.keys(data).length)
        {
          //console.log('Checkout two GTM ',{"pageType":"delivery",'event':'checkout'});
          this.gtmService.pushTag({"pageType":"delivery",'event':'checkout'});
          this.select_addinfo = data['address'];
          this.view = 4;
          if(this.showModal){
            this.closeDialog();
          }
          this.checkoutService.refreshSideBar.next(true);
        }
      }));
    }
  }

  city_name = '';
  city_id = 0;
  delivery_charges = 0;
  notavailable_delivery = 0;
  searchPincode(pincode)
  {

    if(pincode && (pincode.length == 6))
    {

      this.notavailable_delivery = 0;
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','block');
      // }
      this.NgxSpinner.show();
      this.allSubscription.push(this.checkoutService.searchPincode(pincode).subscribe(data => {
        // if (isPlatformBrowser(this.platformId)) {
        //   $("#callbackloader").css('display','none');
        // }
        this.NgxSpinner.hide();

        if(typeof(data['error']) =='undefined')
        {
          this.city_name = data['city_name'];
          this.city_id = data['city_id'];
          this.delivery_charges = data['delivery_charges'];
          if(this.showModal)
          {
            this.addressEditForm.patchValue({city_name: data['city_name']});
          }else
          {
            this.addressForm.patchValue({city_name: data['city_name']});
          }
          $("#pincode-info-error").css('display','none');
        }else{

          this.notavailable_delivery = 1;
          this.city_name = '';
          this.city_id = 0;
          this.delivery_charges = 0;
          if(this.showModal)
          {
            this.addressEditForm.patchValue({city_name: ''});
          }else
          {
            this.addressForm.patchValue({city_name: ''});
          }
        }
      }));
    }
  }

  addressList()
  {
    this.checkoutService.resetSteps.next({'address':1});
    this.checkoutService.addressBlock.next(true);
    this.allSubscription.push(this.checkoutService.getAddress().subscribe(data => {
      if(typeof(data['addinfo']) !='undefined' && data['addinfo'].length !=0)
      {
        this.addinfo = data['addinfo'];
        this.view = 2;
      }else
      {
        this.view = 3;
      }
    }));
  }
  ngOnDestroy():void {
   _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
  editAddress(address_id){
    this.showModal = 1;
    this.addressEditForm = new FormGroup({
      'fname' : new FormControl(null,Validators.required),
      'pcode' : new FormControl(null,[Validators.required, Validators.minLength(5), Validators.maxLength(6)]),
      'addresstype' : new FormControl(null,Validators.required),
      'customer_address' : new FormControl(null,Validators.required),
      'customer_mobile' : new FormControl(null,[Validators.required, Validators.minLength(8), Validators.maxLength(10)]),
      'customer_mobile_alternate' : new FormControl(null,[Validators.minLength(8), Validators.maxLength(10)]),
      'city_name' : new FormControl(null,Validators.required),
      'address_id' : new FormControl(address_id),
    });
    if(address_id !=0)
    {
      for(let addr of this.addinfo)
      {
        if(parseInt(addr['address_id'])==parseInt(address_id))
        {
          var alternate_telephone = '';
          if(addr['alternate_telephone'] !='null')
          {
            alternate_telephone = addr['alternate_telephone'];
          }
          this.city_id = addr['city_id'];
        this.addressEditForm.patchValue({
          'fname' : addr['firstname'],
          'pcode' : addr['postcode'],
          'addresstype' : addr['address_type'],
          'customer_address' : addr['address_1'],
          'customer_mobile' : addr['telephone'],
          'customer_mobile_alternate' : alternate_telephone,
          'city_name' : addr['city'],
          'address_id' : addr['address_id']
        });

        }
      }
    }

    let view = this.modal_1.createEmbeddedView(null);
    this.vc.insert(view);
    this.modal_1.elementRef.nativeElement.previousElementSibling.style.display = 'block';
    this.backdrop = document.createElement('DIV')
    this.backdrop.className = 'modal-backdrop';
    //document.body.appendChild(this.backdrop);
}

closeDialog() {
  this.showModal = 0;
   this.vc.clear()
    //document.body.removeChild(this.backdrop)
}

not_deliverable = 0;
selected_address_id = 0;
  deliverHere(address_id,postcode)
  {
    this.not_deliverable = 0;
    this.selected_address_id  = 0;
    var request_data = {};
    request_data['address_id'] = address_id;
    request_data['postcode'] = postcode;
    // if (isPlatformBrowser(this.platformId)) {
    //   $("#callbackloader").css('display','block');
    // }
    this.NgxSpinner.show();

    this.allSubscription.push(this.checkoutService.deliverHere(request_data).subscribe(data => {
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','none');
      // }
      this.NgxSpinner.hide();
      if(Object.keys(data).length)
      {
        if(typeof(data['error']) =='undefined')
        {
          this.gtmService.pushTag({"pageType":"delivery",'event':'checkout'});
          this.checkoutService.addressSelected.next(true);
          this.select_addinfo = data;
          this.view = 4;
          this.checkoutService.refreshSideBar.next(true);
        }else if(typeof(data['error']) !='undefined')
        {
            this.not_deliverable = 1;
            this.selected_address_id  = address_id;
        }

      }
    }));
  }

}
