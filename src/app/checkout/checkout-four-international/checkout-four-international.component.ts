import { Component, OnInit,OnDestroy,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { CheckoutService } from '../checkout.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../authentication.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';

declare var $: any;
declare const getDevice : any;
declare const getBrowser : any;

@Component({
  selector: 'app-checkout-four-international',
  templateUrl: './checkout-four-international.component.html',
  styleUrls: ['./checkout-four-international.component.css']
})
export class CheckoutFourInternationalComponent implements OnInit,OnDestroy {
  view = 1;
  shippingForm: FormGroup;
  sendername = ''
  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  occasion = '';
  to = '';
  message = '';
  allSubscription:Array<Subscription> = [];
  private deliveryBlockSubscribe: Subscription;
  private deliverySelectedSubscribe: Subscription;
  private resetStepsSubscribe: Subscription;

  constructor(private checkoutService: CheckoutService,
    public auth: AuthenticationService,
    public sanitizer: DomSanitizer,  private NgxSpinner:NgxSpinnerService,
    private gtmService: GoogleTagManagerService,@Inject(PLATFORM_ID) private platformId: object) { }
  ngOnInit(): void {



    this.allSubscription.push(this.resetStepsSubscribe = this.checkoutService.resetSteps.subscribe(data => {
      if((typeof(data['login']) !='undefined') || (typeof(data['address']) !='undefined') || (typeof(data['delivery']) !='undefined'))
      {
        this.view = 1;
      }
    }));
    this.allSubscription.push(this.checkoutService.deliveryBlock.subscribe(flag =>
      {
          this.view = 1;
    }));

    this.allSubscription.push(this.checkoutService.deliverySelected.subscribe(customer_data =>
      {
        this.sendername = customer_data['firstname'];
       // console.log('customer_data ' + customer_data);
        this.shippingForm = new FormGroup({
          'cardMessage' : new FormControl(null),
          'payment_firstname' : new FormControl(this.sendername),
          'occasiontype' : new FormControl(null)
        });
        this.view = 2;
    }));
    if (isPlatformBrowser(this.platformId)) {
      $(document).on("click", ".shipping-suggested-message", function() {
        $('#cardMessage').val($(this).html().trim());
        document.getElementById('shipping-popup').style.display = "none";
      });
    }
  }

  ngOnDestroy():void {
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
   }
  occassionActiveClass = '';
  displayShippingPopup  =0;
  occassionlelabel_click(msg)
  {
    this.displayShippingPopup = 1;
    if (isPlatformBrowser(this.platformId)) {
      $('.shipping-suggested-message').css('display','none');
      $('.occassionlelabel').removeClass('selected');
      $('#occassionlelabel').removeClass('selected');
    }
    //$(this).addClass('selected');
    var occassionlelabel = msg.toString().trim();
    this.occassionActiveClass = occassionlelabel;
    //console.log(occassionlelabel);
    if (isPlatformBrowser(this.platformId)) {
      $('.message-category').html(occassionlelabel);
      $('#occasiontype').val(occassionlelabel);
    }
    this.shippingForm.patchValue({
      'occasiontype' : occassionlelabel
    });
    var occassion = occassionlelabel.replace(/\s/g, "-");
    //console.log(occassion);
    if (isPlatformBrowser(this.platformId)) {
      if(occassion=='')
      {
        $('.suggested-msg').css('display','none');
      }else if(occassion=="Women's-day")
      {
        $('.suggested-msg').css('display','block');
        $(".Womens-day").css('display','block');
      }else
      {
        $('.suggested-msg').css('display','block');
        $('.'+occassion).css('display','block');
      }
    }

  }
  occassionlelabel_change()
  {
    this.displayShippingPopup = 1;
    if (isPlatformBrowser(this.platformId)) {
      $('.shipping-suggested-message').css('display','none');

      var occassionlelabel = $('#occassionlelabel').val().toString().trim();
      $('.message-category').html(occassionlelabel);
      $('#occasiontype').val(occassionlelabel);
    }
    this.shippingForm.patchValue({
      'occasiontype' : occassionlelabel
    });
    if (isPlatformBrowser(this.platformId)) {
      $('.occassionlelabel').removeClass('selected');
      $('#occassionlelabel').addClass('selected');
      var occassion = occassionlelabel.replace(/\s/g, "-");
      //console.log(occassion);
      if(occassion=='other')
      {
        $('.suggested-msg').css('display','none');
      }else if(occassion=="Women's-day")
      {
        $('.suggested-msg').css('display','block');
        $(".Womens-day").css('display','block');
      }else
      {
        $('.suggested-msg').css('display','block');
        $('.'+occassion).css('display','block');
      }
    }
  }
  shippingPopup()
  {
    document.getElementById('shipping-popup').style.display = "block";
  }
  closeShippingPopup()
  {
    document.getElementById('shipping-popup').style.display = "none";
  }
  keepMyNameSecret(event)
  {
    //console.log('event.target.checked'+event.target.checked);
    if ( event.target.checked ) {
      this.shippingForm.patchValue({
        'payment_firstname' : 'Secret',
        'surprise' : 'Suprise',
      });
    }else
    {
      this.shippingForm.patchValue({
        'payment_firstname' : this.sendername,
        'surprise' : '',
      });
    }
  }

  shippingSubmit()
  {
    var cardMessage = '';
    //console.log(this.shippingForm.value.surprise_option);
    if (isPlatformBrowser(this.platformId)) {
      cardMessage = $('#cardMessage').val();
    }
    var request_data = {};
    request_data['cardMessage'] = cardMessage;
    request_data['occasiontype'] = this.shippingForm.value.occasiontype;
    request_data['payment_firstname'] = this.shippingForm.value.payment_firstname;
    request_data['surprise_option'] = this.shippingForm.value.surprise;
    request_data['messageoncake'] = '';
    if (isPlatformBrowser(this.platformId)) {
      if(localStorage.getItem('messageoncake') !=null)
      {
        request_data['messageoncake'] = localStorage.getItem('messageoncake');
      }
    }
    request_data['deviceType'] = '';
    request_data['browser'] = '';
    if (isPlatformBrowser(this.platformId)) {
    //this.deviceInfo = this.deviceDetectorService.getDeviceInfo();
    request_data['deviceType']  = localStorage.getItem('deviceType');
    //request_data['deviceType'] = getDevice();
    request_data['browser'] = localStorage.getItem('browser');

    }
    // if (isPlatformBrowser(this.platformId)) {
    //   $("#callbackloader").css('display','block');
    // }
    this.NgxSpinner.show();
    this.checkoutService.setShippingInternational(request_data).subscribe(data => {
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','none');
      // }
      this.NgxSpinner.hide();

      if(typeof(data['order_id']) !='undefined')
      {
       // console.log('gtag in step 4');
        this.gtmService.pushTag({"pageType":"shipping",'event':'checkout'});
        this.occasion = data['occasion'];
        this.to = data['to'];
        this.message = data['message'];
        this.view = 3;
        this.checkoutService.shippingSelected.next(data);
      }
    });
  }
  openShippingSection()
  {
    this.checkoutService.resetSteps.next({'shipping':1});
    this.checkoutService.shippingBlock.next(true);
    this.view = 2;
  }


  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
}
