import { Component, OnInit,OnDestroy, Inject, PLATFORM_ID } from '@angular/core';
import { CheckoutService } from '../checkout.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../../environments/environment';
import { CartService } from '../../cart/cart.service';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
declare const getDevice : any;

declare var $: any;
declare const getActiveMethods: any;
declare const getWallet : any;
@Component({
  selector: 'app-checkout-five',
  templateUrl: './checkout-five.component.html',
  styleUrls: ['./checkout-five.component.css']
})
export class CheckoutFiveComponent implements OnInit,OnDestroy {
  view =1;
  deviceType = 'Desktop';
  total = 0;
  walletAmount = 0;
  allSubscription:Array<Subscription> = [];
  CDN_PATH  = environment.CDN_PATH;
  private shippingBlockSubscribe: Subscription;
  private shippingSelectedSubscribe: Subscription;
  private resetStepsSubscribe: Subscription;
  private refreshSideBarSubscribe: Subscription;
  constructor(private checkoutService: CheckoutService,
    private cartService:CartService,
    private gtmService: GoogleTagManagerService,
    @Inject(PLATFORM_ID) private platformId: object) { }
  ngOnInit(): void {
   this.allSubscription.push(this.resetStepsSubscribe = this.checkoutService.resetSteps.subscribe(data => {
      if((typeof(data['login']) !='undefined') || (typeof(data['address']) !='undefined') || (typeof(data['delivery']) !='undefined') || (typeof(data['shipping']) !='undefined'))
      {
        this.view = 1;
      }
    }));
    if (isPlatformBrowser(this.platformId)) {
    this.deviceType = getDevice();
    }

   this.allSubscription.push(this.shippingBlockSubscribe = this.checkoutService.shippingBlock.subscribe(flag =>
    {
          this.view = 1;
    }));
    this.allSubscription.push(this.checkoutService.shippingSelected.subscribe(data =>
    {
      if(typeof(data) !='undefined')
      {
        getWallet();
        getActiveMethods();
        this.total = data['order_total'];
        this.view = 2;
      }
    }));
    this.allSubscription.push(this.refreshSideBarSubscribe = this.checkoutService.refreshSideBar.subscribe(flag => {
      if(flag)
      {
        this.cartService.getCartData().subscribe((cart_data:any) => {
          this.total = parseInt(cart_data[0]['filter_total']['total'],10);
          if (isPlatformBrowser(this.platformId)) {
            $(".pay-now").val("Proceed to Pay Rs. " + parseInt(cart_data[0]['filter_total']['total'],10));
            $(".m-pay-now").val("Proceed to Pay Rs. " + parseInt(cart_data[0]['filter_total']['total'],10));
            getWallet();
          }
         });

        //  this.headerService.cartTotalCount().subscribe((cartCount:any) => {

        //    if(typeof(cartCount['total_amount']) !='undefined')
        //    {
        //     this.total = parseInt(cartCount['total_amount'],10);
        //     if (isPlatformBrowser(this.platformId)) {
        //       $(".pay-now").val("Proceed to Pay Rs. " + parseInt(cartCount['total_amount'],10));
        //       $(".m-pay-now").val("Proceed to Pay Rs. " + parseInt(cartCount['total_amount'],10));
        //       getWallet();
        //     }

        //    }
        // });
      }
    }));
  }

  ngOnDestroy():void {
     _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
  checkoutPayment()
  {
    console.log('gtag in payment');
    this.gtmService.pushTag({"pageType":"payment",'event':'checkout'});
  }
  viewShoppingBag(){
    document.getElementById('shopping-bag-modal-popup').style.display = "block";
  }
  closeShoppingBag(){
    document.getElementById('shopping-bag-modal-popup').style.display = "none";
  }
}

