import { Component, OnInit,TemplateRef ,ViewContainerRef,ViewChild,OnDestroy,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CheckoutService } from '../checkout.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../../environments/environment';
import { of, Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
import { debounceTime } from 'rxjs/operators';
import * as _ from 'lodash';
declare var $:any;

@Component({
  selector: 'app-checkout-two-international',
  templateUrl: './checkout-two-international.component.html',
  styleUrls: ['./checkout-two-international.component.css']
})
export class CheckoutTwoComponentInternational implements OnInit,OnDestroy {
  addressForm: FormGroup;
  addressEditForm: FormGroup;
  CDN_PATH  = environment.CDN_PATH;
  view = 1;
  addinfo = [];
  select_addinfo = {};
  alternate_number_show = false;
  showModal = 0;
  defaultAddressType = 'Home';
  selected: string = "";
  inter_cities_options : any;

  // Modal
  @ViewChild('modal_1') modal_1: TemplateRef<any>;
  @ViewChild('vc', {read: ViewContainerRef}) vc: ViewContainerRef;
  backdrop: any
  // Modal
  private allSubscription:Array<Subscription> = [];
  constructor(private checkoutService: CheckoutService,
    private gtmService: GoogleTagManagerService,private NgxSpinner:NgxSpinnerService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.allSubscription.push(this.checkoutService.resetSteps.subscribe(data => {
      if(typeof(data['login']) !='undefined')
      {
        this.view = 1;
      }
    }));
    this.addressForm = new FormGroup({
      'fname' : new FormControl(null,Validators.required),
      'pcode' : new FormControl(null,Validators.required),
      'addresstype' : new FormControl(null,Validators.required),
      'customer_address' : new FormControl(null,Validators.required),
      'customer_mobile' : new FormControl(null,Validators.required),
      'customer_mobile_alternate' : new FormControl(null),
      'filter_inter_city' : new FormControl(null,Validators.required),
      'city' : new FormControl(null),
      'country' : new FormControl(null)
    });
    this.allSubscription.push(this.checkoutService.loggedIn.subscribe(flag => {
      if(flag)
      {
        this.addressList();
      }
    }));

    this.allSubscription.push(this.addressForm.controls.filter_inter_city.valueChanges.pipe(debounceTime(500)).subscribe(res=>{
      this.searchCity(res);
    }))
  }
  selectedCityData = {};
  selectCityName(name) {
    this.selectedCityData = this.getSelectedCity(name);
  }
  selectCityName1(name) {
    this.selectedCityData = this.getSelectedCity(name);
  }

  getSelectedCity(selectedName: string) {
    return this.inter_cities.find(product => product.city === selectedName);
  }

  deleteAddress(address_id)
  {
    if(confirm("Are you sure ? This Can't be undone"))
    {
      this.allSubscription.push(this.checkoutService.deleteAddressInternational(address_id).subscribe(data => {
        if(Object.keys(data).length)
        {
          this.addinfo = data['addinfo'];
          this.view = 2;
          if(typeof(data['addinfo']) !='undefined' && data['addinfo'].length ==0)
          {
            window.location.reload();
          }
        }else
        {
          this.view = 3;
        }
      }));
    }
  }
  valid_from = true;
  addAddress()
  {
    var request_data = {};
    if(this.showModal)
    {
      this.selectedCityData = this.getSelectedCity(this.selected);

      this.addressEditForm.value['city'] = this.selectedCityData['id'];
      this.addressEditForm.value['country'] = this.selectedCityData['country_name'];
      request_data['form_data'] = this.addressEditForm.value;
      if(! this.addressEditForm.valid)
      {
        this.valid_from = false;
        this.addressEditForm.markAllAsTouched();
      }else{
        this.valid_from = true;
      }
    }else
    {
      this.addressForm.value['address_id'] = 0;
      this.addressForm.value['city'] = this.selectedCityData['id'];
      this.addressForm.value['country'] = this.selectedCityData['country_name'];
      request_data['form_data'] = this.addressForm.value;
      if(! this.addressForm.valid)
      {
        this.valid_from = false;
        this.addressForm.markAllAsTouched();
      }else{
        this.valid_from = true;
      }
    }
    if(this.valid_from)
    {
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','block');
      // }
      this.NgxSpinner.show();
      this.allSubscription.push(this.checkoutService.addAddressInternational(request_data).subscribe(data => {
        // if (isPlatformBrowser(this.platformId)) {
        //  $("#callbackloader").css('display','none');
        // }
        this.NgxSpinner.hide();

        this.checkoutService.addressSelected.next(true);
        if(Object.keys(data).length)
        {
          this.gtmService.pushTag({"pageType":"delivery",'event':'checkout'});
          this.select_addinfo = data['address'];
          this.view = 4;
          if(this.showModal){
            this.closeDialog();
          }
          this.checkoutService.refreshSideBar.next(true);
        }
      }));
    }
  }

  city_name = '';
  city_id = 0;
  delivery_charges = 0;
  notavailable_delivery = 0;
  searchPincode(pincode)
  {

    if(pincode && (pincode.length == 6))
    {

      this.notavailable_delivery = 0;
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','block');
      // }
      this.NgxSpinner.show();
      this.allSubscription.push(this.checkoutService.searchPincode(pincode).subscribe(data => {
        // if (isPlatformBrowser(this.platformId)) {
        //   $("#callbackloader").css('display','none');
        // }
        this.NgxSpinner.hide();

        if(typeof(data['error']) =='undefined')
        {
          this.city_name = data['city_name'];
          this.city_id = data['city_id'];
          this.delivery_charges = data['delivery_charges'];
          if(this.showModal)
          {
            this.addressEditForm.patchValue({city_name: data['city_name']});
          }else
          {
            this.addressForm.patchValue({city_name: data['city_name']});
          }
          $("#pincode-info-error").css('display','none');
        }else{

          this.notavailable_delivery = 1;
          this.city_name = '';
          this.city_id = 0;
          this.delivery_charges = 0;
          if(this.showModal)
          {
            this.addressEditForm.patchValue({city_name: ''});
          }else
          {
            this.addressForm.patchValue({city_name: ''});
          }
        }
      }));
    }
  }
  inter_cities = [];
  searchCity(ct)
  {
    if(ct)
    //if(ct && (ct.length > 2))
    {
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','block');
      // }
      this.checkoutService.searchCity(ct).subscribe(data => {
        // if (isPlatformBrowser(this.platformId)) {
        //   $("#callbackloader").css('display','none');
        // }

       this.inter_cities = data['inter_cities'];
       this.inter_cities_options = of(this.inter_cities);
      });
    }
  }
  addressList()
  {
    this.checkoutService.resetSteps.next({'address':1});
    this.checkoutService.addressBlock.next(true);
    this.allSubscription.push(this.checkoutService.getAddressInternational().subscribe(data => {
      if(typeof(data['addinfo']) !='undefined' && data['addinfo'].length !=0)
      {
        this.addinfo = data['addinfo'];
        this.view = 2;
      }else
      {
        this.view = 3;
      }
    }));
  }
  ngOnDestroy():void {
   _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
  editAddress(address_id){
    this.showModal = 1;
    this.addressEditForm = new FormGroup({
      'fname' : new FormControl(null,Validators.required),
      'pcode' : new FormControl(null,Validators.required),
      'addresstype' : new FormControl(null,Validators.required),
      'customer_address' : new FormControl(null,Validators.required),
      'customer_mobile' : new FormControl(null,Validators.required),
      'customer_mobile_alternate' : new FormControl(null),
      'address_id' : new FormControl(address_id),
      'filter_inter_city' : new FormControl(null,Validators.required),
      'city' : new FormControl(null),
      'country' : new FormControl(null)
    });
    if(address_id !=0)
    {
      for(let addr of this.addinfo)
      {
        if(parseInt(addr['address_id'])==parseInt(address_id))
        {
          var alternate_telephone = '';
          if(addr['alternate_telephone'] !='null')
          {
            alternate_telephone = addr['alternate_telephone'];
          }
          this.city_id = addr['city_id'];
          this.selected = addr['city'];
        this.addressEditForm.patchValue({
          'fname' : addr['firstname'],
          'pcode' : addr['postcode'],
          'addresstype' : addr['address_type'],
          'customer_address' : addr['address_1'],
          'customer_mobile' : addr['telephone'],
          'customer_mobile_alternate' : alternate_telephone,
          'address_id' : addr['address_id'],
          'filter_inter_city' : addr['city'],
          'city' : addr['city_id'],
          'country' : addr['country']
        });

        }
      }
    }

    let view = this.modal_1.createEmbeddedView(null);
    this.vc.insert(view);
    this.modal_1.elementRef.nativeElement.previousElementSibling.style.display = 'block';
    this.backdrop = document.createElement('DIV')
    this.backdrop.className = 'modal-backdrop';
    //document.body.appendChild(this.backdrop);
}

closeDialog() {
  this.showModal = 0;
   this.vc.clear()
    //document.body.removeChild(this.backdrop)
}

not_deliverable = 0;
selected_address_id = 0;
  deliverHere(address_id,postcode,city)
  {
    this.not_deliverable = 0;
    this.selected_address_id  = 0;
    var request_data = {};
    request_data['address_id'] = address_id;
    request_data['city'] = city;
    request_data['postcode'] = postcode;
    // if (isPlatformBrowser(this.platformId)) {
    //   $("#callbackloader").css('display','block');
    // }

    this.NgxSpinner.show();
    this.checkoutService.deliverHereInternational(request_data).subscribe(data => {
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','none');
      // }
      this.NgxSpinner.hide();
      if(Object.keys(data).length)
      {
        if(typeof(data['error']) =='undefined')
        {
          this.gtmService.pushTag({"pageType":"delivery",'event':'checkout'});
          this.checkoutService.addressSelected.next(true);
          this.select_addinfo = data;
          this.view = 4;
          this.checkoutService.refreshSideBar.next(true);
        }else if(typeof(data['error']) !='undefined')
        {
            this.not_deliverable = 1;
            this.selected_address_id  = address_id;
        }

      }
    });
  }

}
