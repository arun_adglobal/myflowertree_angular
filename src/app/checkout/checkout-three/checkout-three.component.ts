import { Component, OnInit,OnDestroy, Inject, PLATFORM_ID  } from '@angular/core';
import { CheckoutService } from '../checkout.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';

declare var $: any;
@Component({
  selector: 'app-checkout-three',
  templateUrl: './checkout-three.component.html',
  styleUrls: ['./checkout-three.component.css']
})
export class CheckoutThreeComponent implements OnInit,OnDestroy {
  view = 1;
  express_products = [];
  express_not_available = [];
  express_earliest_date = '';
  express_earliest_delivery_date = '';
  courier_products = [];
  courier_not_available = [];
  courier_earliest_date = '';
  courier_earliest_delivery_date = '';
  cutoff_count = 0;
  cutoff_product_count = 0;
  cutoff_products = [];
  express_date_array = [];
  clldate = [];
  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  cldate = [];
  product_mnbdt = [];
  selected_pincode = 0;
  allSubscription:Array<Subscription> = [];

  international_products = [];
  international_not_available = [];
  international_earliest_date = '';
  international_earliest_delivery_date = '';
  international_date_array = [];
  cssDy:any={};
  constructor(private checkoutService: CheckoutService,
    public sanitizer: DomSanitizer,
    private gtmService: GoogleTagManagerService, private NgxSpinner:NgxSpinnerService,
    @Inject(PLATFORM_ID) private platformId: object) { }
  ngOnInit(): void {

    this.cssDy = { datetimepickercss : this.getSafeCDNUrl('live-new/css/bootstrap-datetimepicker.min.css.gz'),jqueryuicss : this.getSafeCDNUrl('live-new/css/jquery-ui.css.gz')};

    this.allSubscription.push(this.checkoutService.resetSteps.subscribe(data => {
      if(typeof(data['login']) !='undefined')
      {
        this.view = 1;
      }else if(typeof(data['address']) !='undefined')
      {
        this.view = 1;
      }
    }));
    this.allSubscription.push(this.checkoutService.addressBlock.subscribe(flag =>
    {
      if(flag)
      this.view = 1;
    }));
    this.allSubscription.push(this.checkoutService.addressSelected.subscribe(showaddon =>
      {
        //============= Express ====================//
        this.if_express_selected_date_other ='';
        this.express_other_date = '';
        this.if_express_selected_date_today = '';
        this.if_express_selected_date_tomorrow = '';
        this.if_express_selected_date_other = '';
        this.display_express_today = 0;
        this.display_express_tomorrow = 0;
        this.express_today_date = '';
        this.express_tomorrow_date = '';
        this.express_other_date = '';
        this.express_delivery_text = '';

        //============= Courier ========================//
        this.if_courier_selected_date_today = '';
        this.if_courier_selected_date_other = '';
        this.courier_today_date = '';
        this.courier_other_date = '';

        this.display_courier_today = 0;




        this.checkoutService.getProductView().subscribe(data => {
            if(typeof(data['international']) !="undefined")
            {
              this.internationalCalculation(data);
            }else{
              this.productViewCalculation(data);
            }
        });
      }
    ));

    this.allSubscription.push(this.checkoutService.calendarSelected.subscribe(data => {
      this.reset();
      if(typeof(data['international']) !="undefined")
      {
        this.internationalCalculation(data);
      }else{
        this.productViewCalculation(data);
      }
    }));
  }

  ngOnDestroy():void {
  _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
  internationalCalculation(data)
  {
    this.product_mnbdt = [];
    this.selected_pincode = data['selected_pincode'];
    this.international_not_available = data['international_not_available'];
    this.cutoff_count = data['cutoff_count'];
    this.cutoff_product_count = data['cutoff_product_count'];
    this.cutoff_products = data['cutoff_products'];
    if(this.cutoff_products.length != 0 && (this.cutoff_count !=this.cutoff_product_count))
    {
        for(let product of this.cutoff_products)
        {
          this.product_mnbdt.push(product['product_id']);
        }
    }

    this.clldate = data['clldate'];
    this.cldate = data['cldate'];
    if(typeof(data['international']) !='undefined')
    {
      this.international_products = data['international'];
      this.international_earliest_date = data['international_earliest_date'];
      this.international_earliest_delivery_date = data['international_earliest_delivery_date'];
      this.international_block();
    }
    this.view = 4;
  }
  productViewCalculation(data)
  {
    this.selected_pincode = data['selected_pincode'];
    this.express_not_available = data['express_not_available'];
    this.courier_not_available = data['courier_not_available'];
    this.cutoff_count = data['cutoff_count'];
    this.cutoff_product_count = data['cutoff_product_count'];
    this.cutoff_products = data['cutoff_products'];
    if(this.cutoff_products.length != 0 && (this.cutoff_count !=this.cutoff_product_count))
    {
        for(let product of this.cutoff_products)
        {
          this.product_mnbdt.push(product['product_id']);
        }
    }

    this.clldate = data['clldate'];
    this.cldate = data['cldate'];
    if(typeof(data['express']) !='undefined')
    {
      //console.log(data['express']);
      this.express_products = data['express'];
      this.express_earliest_date = data['express_earliest_date'];
      this.express_earliest_delivery_date = data['express_earliest_delivery_date'];
      this.express_block();
    }
    if(typeof(data['courier']) !='undefined')
    {
      this.courier_products = data['courier'];
      this.courier_earliest_date = data['courier_earliest_date'];
      this.courier_earliest_delivery_date = data['courier_earliest_delivery_date'];
      this.courier_block();
    }
    this.view = 2;
  }


  if_international_selected_date_other = '';
  international_other_date = '';
  international_delivery_text = '';
  international_block()
  {
    this.international_date_array = ['00-00-0000'];
    if((this.international_date_array.includes(this.international_earliest_delivery_date)==false) && this.international_earliest_date !='')
    {
      this.if_international_selected_date_other = 'selected';
    }
    if((this.international_date_array.includes(this.international_products[0]['delivery_date'])==false))
    {
      var date = this.international_products[0]['delivery_date'].split('-');
      this.international_other_date = date[2]+'/'+date[1]+'/'+date[0];
    }
    if(this.international_products[0]['shipping_method']=='international.international')
    {
      this.international_delivery_text = 'International';
    }
  }

  if_express_selected_date_today = '';
  if_express_selected_date_tomorrow = '';
  if_express_selected_date_other = '';
  display_express_today = 0;
  display_express_tomorrow = 0;
  express_today_date = '';
  express_tomorrow_date = '';
  express_other_date = '';
  express_delivery_text = '';
  express_block()
  {
    var cd,cm,cy;
    var current = new Date();
    cd = current.getDate();
    cm = current.getMonth() + 1;
    cy = current.getFullYear();
    var currentdate = cd+'-'+cm+'-'+cy;
    var currentdate_1 = cy+'-'+cm+'-'+cd;
    var currentdate_2 = cy+'/'+cm+'/'+cd;
    this.express_today_date = currentdate_2;

    var disabledate_1 = cy+'-'+cm+'-'+cd;
    if((cm.toString()).length==1)
    {
      disabledate_1 = cy+'-'+'0'+cm+'-'+cd;
    }


    var nd,nm,ny;
    var next = new Date();
    next.setDate(next.getDate() + 1);
    nd = next.getDate();
    nm = next.getMonth() + 1;
    ny = next.getFullYear();
    var nextdate = nd+'-'+nm+'-'+ny;
    var nextdate_1 = ny+'-'+nm+'-'+nd;
    var nextdate_2 = ny+'/'+nm+'/'+nd;
    this.express_tomorrow_date = nextdate_2;

    var disabledate_2 = ny+'-'+nm+'-'+nd;
    if((nm.toString()).length==1)
    {
      disabledate_2 = ny+'-'+'0'+nm+'-'+nd;
    }
    this.express_date_array = [currentdate,nextdate,'00-00-0000'];
    if(this.express_earliest_delivery_date==this.express_date_array[0])
    {
      this.if_express_selected_date_today = 'selected';
    }else if(this.express_earliest_delivery_date==this.express_date_array[1])
    {
      this.if_express_selected_date_tomorrow = 'selected';
    }else if((this.express_date_array.includes(this.express_earliest_delivery_date)==false) && this.express_earliest_date !='')
    {
      this.if_express_selected_date_other = 'selected';
    }

 //   console.log(this.clldate);
 //   console.log(currentdate_1);
    if(this.express_earliest_date !='' && this.express_earliest_date==currentdate_2 &&  (this.clldate.includes(disabledate_1)==false))
    {
        this.display_express_today = 1;
    }
    var d1 = new Date(this.express_earliest_date).getTime();
    var d2 = new Date(nextdate_2).getTime();

    if(this.express_earliest_date !='' && d1 <= d2 &&  (this.clldate.includes(disabledate_2)==false))
    {
        this.display_express_tomorrow = 1;
    }
    //console.log(this.express_date_array);
    if((this.express_date_array.includes(this.express_products[0]['delivery_date'])==false))
    {
      //console.log('test');
      //console.log(this.express_products[0]['delivery_date']);
      var date = this.express_products[0]['delivery_date'].split('-');
      this.express_other_date = date[2]+'/'+date[1]+'/'+date[0];
    }
    if(this.express_products[0]['shipping_method']=='day.day')
    {
      this.express_delivery_text = 'Standard';
    }else if(this.express_products[0]['shipping_method']=='night.night')
    {
      this.express_delivery_text = 'Midnight';
    }else if(this.express_products[0]['shipping_method']=='express.express')
    {
      this.express_delivery_text = 'Express';
    }else if(this.express_products[0]['shipping_method']=='morning.morning')
    {
      this.express_delivery_text = 'Morning';
    }
  }

  if_courier_selected_date_today = '';
  if_courier_selected_date_other = '';
  courier_today_date = '';
  courier_other_date = '';
  courier_date_array = [];
  display_courier_today = 0;
  courier_block()
  {
    var cd,cm,cy;
    var current = new Date();
    cd = current.getDate();
    cm = current.getMonth() + 1;
    cy = current.getFullYear();
    var currentdate = cd+'-'+cm+'-'+cy;
    var currentdate_1 = cy+'-'+cm+'-'+cd;
    var currentdate_2 = cy+'/'+cm+'/'+cd;
    this.courier_today_date = currentdate_2;

    this.courier_date_array = [currentdate,'00-00-0000'];
    if(this.courier_earliest_delivery_date==this.courier_date_array[0])
    {
      this.if_courier_selected_date_today = 'selected';
    }else if((this.courier_date_array.includes(this.courier_earliest_delivery_date)==false) && this.courier_earliest_date !='')
    {
      this.if_courier_selected_date_other = 'selected';
    }
    if(this.courier_earliest_date !='' && this.courier_earliest_date==currentdate_2)
    {
        this.display_courier_today = 1;
    }
    if((this.courier_date_array.includes(this.courier_products[0]['delivery_date'])==false))
    {
      var date = this.courier_products[0]['delivery_date'].split('-');
      this.courier_other_date = date[2]+'/'+date[1]+'/'+date[0];
    }

  }
  express_date_not_selected_error = '';
  courier_date_not_selected_error = '';
  international_date_not_selected_error = '';
 editProduct(type)
  {
    var data = {};
    if(type=='express')
    {
      if(this.express_earliest_date=='')
      {
        this.express_date_not_selected_error = 'Sorry,this product cannot be delivered';
      }else
      {
        data['courier_product'] = 0;
        data['subCalendar'] = 0;
        data['earliest_date'] = this.express_earliest_date;
        data['product_id'] = this.express_products[0]['product_id'];
        data['product_type'] = this.express_products[0]['product_type'];
        data['selected_pincode'] = this.selected_pincode;
        data['product_mnbdt'] = this.product_mnbdt;
        data['cldate'] = this.cldate;
        data['clldate'] = this.clldate;
        this.checkoutService.getMainCalendar.next(data);
      }
    }else if(type=='courier')
    {
      if(this.courier_earliest_date=='')
      {
        this.courier_date_not_selected_error = 'Sorry,this product cannot be delivered';
      }else
      {
        data['courier_product'] = 1;
        data['subCalendar'] = 0;
        data['earliest_date'] = this.courier_earliest_date;
        data['product_id'] = this.courier_products[0]['product_id'];
        data['product_type'] = this.courier_products[0]['product_type'];
        data['selected_pincode'] = this.selected_pincode;
        data['product_mnbdt'] = this.product_mnbdt;
        data['cldate'] = this.cldate;
        data['clldate'] = this.clldate;
        this.checkoutService.getCourierCalendar.next(data);
      }
    }else if(type=='international')
    {
      if(this.international_earliest_date=='')
      {
        this.international_date_not_selected_error = 'Sorry,this product cannot be delivered';
      }else
      {
        data['international_product'] = 0;
        data['subCalendar'] = 0;
        data['earliest_date'] = this.international_earliest_date;
        data['product_id'] = this.international_products[0]['product_id'];
        data['product_type'] = this.international_products[0]['product_type'];
        data['selected_pincode'] = this.selected_pincode;
        data['product_mnbdt'] = this.product_mnbdt;
        data['cldate'] = this.cldate;
        data['clldate'] = this.clldate;
        this.checkoutService.getInternationalCalendar.next(data);
      }
    }
  }
  delivery_date = '';
  getShipping()
  {
    //console.log(this.courier_products[0]['delivery_date']);
    //console.log(this.express_products[0]['delivery_date']);
    if((this.courier_products.length > 0) && (this.courier_products[0]['delivery_date']=='00-00-0000'))
    {
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','none');
      // }
      this.NgxSpinner.hide();
      
        this.express_date_not_selected_error = 'Please select delivery date';

    }else if((this.express_products.length > 0) && (this.express_products[0]['delivery_date']=='00-00-0000'))
    {
      // if (isPlatformBrowser(this.platformId)) {
      //   $("#callbackloader").css('display','none');
      // }
      this.NgxSpinner.hide();
        this.express_date_not_selected_error = 'Please select delivery date';

    }else
    {
      var devicetype = '';
      var browser = '';
      // if (isPlatformBrowser(this.platformId)) {
      //     $("#callbackloader").css('display','block');

      // }
      this.NgxSpinner.show();
          this.checkoutService.getShipping(localStorage.getItem('deviceType'),localStorage.getItem('browser')).subscribe(data =>
          {
            if(typeof(data['reload']) !='undefined')
            {
              // if (isPlatformBrowser(this.platformId)) {
              //   $("#callbackloader").css('display','none');
              // }
              this.NgxSpinner.hide();
              window.location.reload();
            }
            if(data['filter_products'] !='undefined')
            {
              // if (isPlatformBrowser(this.platformId)) {
              //   $("#callbackloader").css('display','none');
              // }
              this.NgxSpinner.hide();
              this.gtmService.pushTag({"pageType":"orderSummary",'event':'checkout','products': data['filter_products'],'cartTotals': data['filter_total']});
              this.gtmService.pushTag({'event': 'cartUpdated', 'updatedProducts': data['filter_products']});
            }
            if(this.express_products.length !=0)
            {
              // if (isPlatformBrowser(this.platformId)) {
              //   $("#callbackloader").css('display','none');
              // }
              this.NgxSpinner.hide();
              var date = this.express_products[0]['delivery_date'].split('-');
              this.delivery_date = date[2]+'/'+date[1]+'/'+date[0];
            }else if(this.express_products.length ==0 && this.courier_products.length !=0)
            {
              // if (isPlatformBrowser(this.platformId)) {
              //   $("#callbackloader").css('display','none');
              // }
              this.NgxSpinner.hide();
              var date = this.courier_products[0]['delivery_date'].split('-');
              this.delivery_date = date[2]+'/'+date[1]+'/'+date[0];
            }

            if(this.express_products.length !=0)
            {
              // if (isPlatformBrowser(this.platformId)) {
              //  $("#callbackloader").css('display','none');
              // }
              this.NgxSpinner.hide();
              var date = this.express_products[0]['delivery_date'].split('-');
            }
            if(this.express_products.length ==0 && this.courier_products.length !=0)
            {
              // if (isPlatformBrowser(this.platformId)) {
              //   $("#callbackloader").css('display','none');
              // }
              this.NgxSpinner.hide();
              var date = this.courier_products[0]['delivery_date'].split('-');
            }
            if(this.courier_products.length !=0)
            {
              // if (isPlatformBrowser(this.platformId)) {
              //   $("#callbackloader").css('display','none');
              // }
              this.NgxSpinner.hide();
              var date = this.courier_products[0]['delivery_date'].split('-');
            }

            if(date=="00,00,0000")
            {
              // if (isPlatformBrowser(this.platformId)) {
              //   $("#callbackloader").css('display','none');
              // }
              this.NgxSpinner.hide();
              this.express_date_not_selected_error = 'Please select delivery date';
            }else{
              // if (isPlatformBrowser(this.platformId)) {
              //   $("#callbackloader").css('display','none');
              // }
              this.NgxSpinner.hide();
              this.view = 3;
              this.checkoutService.deliverySelected.next(data['customer_data']);
            }
          }
        );
    }

  }
  getInternationalShipping()
  {
    var devicetype = '';
    var browser = '';
    // if (isPlatformBrowser(this.platformId)) {
    //   $("#callbackloader").css('display','block');

    // }
    this.NgxSpinner.show();
    this.checkoutService.getInternationalShipping(localStorage.getItem('deviceType'),localStorage.getItem('browser')).subscribe(data =>
      {
        if(typeof(data['reload']) !='undefined')
        {
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          window.location.reload();
        }
        if(data['filter_products'] !='undefined')
        {
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          this.gtmService.pushTag({"pageType":"orderSummary",'event':'checkout','products': data['filter_products'],'cartTotals': data['filter_total']});
          this.gtmService.pushTag({'event': 'cartUpdated', 'updatedProducts': data['filter_products']});
        }
        if(this.international_products.length !=0)
        {
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          var date = this.international_products[0]['delivery_date'].split('-');
          this.delivery_date = date[2]+'/'+date[1]+'/'+date[0];
        }

        if(date=="00,00,0000")
        {
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          this.international_date_not_selected_error = 'Please select delivery date';
        }else{
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          this.view = 3;
          this.checkoutService.deliverySelected.next(data['customer_data']);
        }
      }
    );
  }
  getMethods(day,type)
  {
    var data = {};
    data['day'] = day;
    data['selected_pincode'] = this.selected_pincode;
    if(type=='courier')
    {
      data['cart_id'] = this.courier_products[0]['cart_id'];
      data['product_id'] = this.courier_products[0]['product_id'];

      if(data['day']=='today')
        data['date'] = this.courier_today_date;
    }else if(type=='express')
    {
      data['cart_id'] = this.express_products[0]['cart_id'];
      data['product_id'] = this.express_products[0]['product_id'];

      if(data['day']=='today')
        data['date'] = this.express_today_date;
      else if(data['day']=='tomorrow')
        data['date'] = this.express_tomorrow_date;


      ////////////////// Back to Calender Data /////////////
      data['courier_product'] = 0;
      data['subCalendar'] = 1;
      data['earliest_date'] = this.express_earliest_date;
      data['product_type'] = this.express_products[0]['product_type'];
      data['selected_pincode'] = this.selected_pincode;
      data['product_mnbdt'] = this.product_mnbdt;
      data['cldate'] = this.cldate;
      data['clldate'] = this.clldate;
      ////////////////// END Back to Calender Data /////////////
    }
    data['type'] = type;
    this.checkoutService.getMethods.next(data);
  }

  reset()
  {
    this.express_products = [];
    this.express_not_available = [];
    this.express_earliest_date = '';
    this.express_earliest_delivery_date = '';
    this.courier_products = [];
    this.courier_not_available = [];
    this.courier_earliest_date = '';
    this.courier_earliest_delivery_date = '';
    this.cutoff_count = 0;
    this.cutoff_product_count = 0;
    this.cutoff_products = [];
    this.express_date_array = [];
    this.clldate = [];
    this.cldate = [];
    this.product_mnbdt = [];
    this.selected_pincode = 0;
    this.if_express_selected_date_today = '';
    this.if_express_selected_date_tomorrow = '';
    this.if_express_selected_date_other = '';
    this.display_express_today = 0;
    this.display_express_tomorrow = 0;
    this.express_today_date = '';
    this.express_tomorrow_date = '';
    this.express_other_date = '';
    this.express_delivery_text = '';
    this.if_courier_selected_date_today = '';
    this.if_courier_selected_date_other = '';
    this.courier_today_date = '';
    this.courier_other_date = '';
    this.courier_date_array = [];
    this.display_courier_today = 0;
    this.express_date_not_selected_error = '';
    this.courier_date_not_selected_error = '';

  }
  openDeliverySection()
  {
    this.checkoutService.resetSteps.next({'delivery':1});
    this.checkoutService.deliveryBlock.next(true);
    this.view = 2;
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
}
