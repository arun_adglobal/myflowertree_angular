import { Component,OnInit,OnDestroy, Inject, PLATFORM_ID} from '@angular/core';
import { CheckoutService } from '../checkout.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import * as _ from 'lodash';

//import * as $ from 'jquery';
declare var $: any;
declare function shippingname1(): any;
declare function getToken(): any;
@Component({
  selector: 'app-couriercalendar',
  templateUrl: './courier-calendar.component.html'
})
export class CourierCalendarComponent implements OnInit,OnDestroy {
  APIURL  = environment.API_URL;

  product_mnbdt = [];
  private allSubscription:Array<Subscription> = [];
  constructor(private checkoutService: CheckoutService,private router: Router,
    @Inject(PLATFORM_ID) private platformId: object) { }
  expressshiping_next_day_html = '';
  dayshipping_html = '';
  expressshiping_html = '';
  backtocalender = {};
  ngOnInit(): void {
    //console.log(this.APIURL);
    this.allSubscription.push(this.checkoutService.getCourierCalendar.subscribe(calendardata =>
      {
        if (isPlatformBrowser(this.platformId)) {
          $('.delivertimebox_courier').show();
          $('#courier_datetimepicker').show();
        }
        this.backtocalender = calendardata;
        //console.log(data);
        this.getCourierCalendar(calendardata);        
      }
    ));
    if (isPlatformBrowser(this.platformId)) {
    $( document ).ready(function() {
      $("#deliverydatemodal_Courier").on('click','.getcalender',function() {
          $('#shippingnamevalue_courier').val('');
          $('#deliverytimeslot_courier').val('');
          $('.delivertimebox_courier').show();    
          $('#resultLoading').hide();
          $('#courier_datetimepicker').show();
      });
    
    });
  }
  
  }
  
  ngOnDestroy():void {
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }

  getCourierCalendar(data){
     var cld = '';
 
     for(let cldate of data['cldate'])
     {
       cld +='"'+cldate+'",';
     }
     //console.log(cld);
     this.product_mnbdt = data['product_mnbdt'];
     var months = [ "Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec" ];
     if (isPlatformBrowser(this.platformId)) {
      $('#courier_datetimepicker').html('<input data-date="05-09-2017" data-date-format="DD-MM-YYYY" class="courier_datetimepicker" id="pickselectdate_courier" type="hidden">');
      $('#courier_datetimepicker').removeClass('hasDatepicker');
     }
     if(data['subCalendar']==0)
     {
       document.getElementById('deliverydatemodal_Courier').style.display = "block";

     }
     var calendar = data['earliest_date'].split('/');
     var dataYear = calendar[0];
     var dataMonth = calendar[1]-1;
     var dataDay = calendar[2];
     if (isPlatformBrowser(this.platformId)) {
     $(document).ready(function(){
       var disableDates = data['cldate'];
 
       //console.log(disableDates);
     $('#courier_datetimepicker').datepicker(
     {
       inline: true,
       format: 'dd-mm-yyyy',
       startDate: new Date(dataYear,dataMonth,dataDay),
       defaultDate: new Date(dataYear,dataMonth,dataDay),
       minDate:new Date(dataYear,dataMonth,dataDay),
       beforeShowDay: function(date){
         var d = date;


         var curr_date = d.getDate();
         var curr_month = d.getMonth() + 1; 
         var curr_year = d.getFullYear();
         
         
         var formattedDate = curr_date + "/" + curr_month + "/" + curr_year;
        // console.log('formattedDate'+formattedDate);
         var activeDate = dataDay + "/" + (dataMonth+1) + "/" + dataYear;
       //  console.log('activeDate'+activeDate);
       if( curr_month.toString().length==1)
       {
        curr_month = '0'+curr_month;
       }
       if( curr_date.toString().length==1)
       {
        curr_date = '0'+curr_date;
       }
 
       var dmy = curr_year + "/" + (curr_month) + "/" + curr_date;
        
         
 
       
       
       if (formattedDate==activeDate){
           return {
           classes: 'active'
           };
         }
       if(disableDates.indexOf(dmy) != -1){
 
           return false;
 
       }else
       {
         return true;
       }
         
       }
     }).on("changeDate", function (date) {

       $("#pickselectdate_courier").val(date.format());
       var dts =   $("#pickselectdate_courier").val(); 
       data['dts'] = dts;
       var pincode =   data['selected_pincode']; 
       var productId = data['product_id'];
         var cityid =  $("#city"+data['product_id']).val();          
         
         var parts = dts.split("-");
         var sd = new Date(parts[2],parts[1]-1,parts[0]);
         var month = new Array();
          month[0] = "Jan";
          month[1] = "Feb";
          month[2] = "Mar";
          month[3] = "Apr";
          month[4] = "May";
          month[5] = "Jun";
          month[6] = "Jul";
          month[7] = "Aug";
          month[8] = "Sep";
          month[9] = "Oct";
          month[10] = "Nov";
          month[11] = "Dec";
         var n = month[sd.getMonth()];
         var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
         var dayName = days[sd.getDay()];
         
         $('#delivery-details-date-'+data['product_id']).html(dayName+', '+parts[0]+' '+n+', '+parts[2].toString().substr(2,2));
         $('#selected-date').val(dayName+', '+parts[0]+' '+n+', '+parts[2].toString().substr(2,2));
         
         
         var d_tmp = new Date();
         var date_tmp = d_tmp.getDate();
         var month_tmp = d_tmp.getMonth() + 1; 
         var year_tmp = d_tmp.getFullYear(); 
         const tomorrow = new Date(d_tmp)
         tomorrow.setDate(tomorrow.getDate() + 1)
         
 
         var d = date_tmp+'-'+month_tmp+'-'+year_tmp;
         var currentHour = d_tmp.getHours();            
         var tomorrowdate = tomorrow.getDate()+'-'+tomorrow.getMonth() + 1+'-'+tomorrow.getFullYear();        

         $('#delivery_date_courier').val(dts);
         $('.delivertimebox_courier').hide();
         
         if(parseInt(data['courier_product']) && data['product_type']!='International') {
           $('#deliverytimeslot_courier').val('3-4 business Days');
           $('#shippingnamevalue_courier').val('courier');
           document.getElementById('deliverydatemodal_Courier').style.display = "none";
           $("#courier_datetimepicker").datepicker("destroy");
           $('#delivery-details-date-slot'+data['product_id']).html('');
           $('#delivery-details-shipping-method'+data['product_id']).html('Courier: ');
           $('#delivery-details-shipping-charge'+data['product_id']).html('+ Rs. 0');
           $('#updateCart_courier').trigger('click');
         }
        }
       );
   });
  }
 
   }

  closeCalendar_Courier() {
    document.getElementById('deliverydatemodal_Courier').style.display = "none";
    if (isPlatformBrowser(this.platformId)) {
      $("#courier_datetimepicker").datepicker("destroy");
    }
  }

  updateCart_courier()
  {
    if (isPlatformBrowser(this.platformId)) {
      if($('#deliverytimeslot_courier').val()=='')
      {
        this.router.navigate(['/checkout']);
      }
      var filter = {};
      filter['shipping_name'] = $('#shippingnamevalue_courier').val();
      filter['shipping_time_slot'] = $('#deliverytimeslot_courier').val();
      filter['delivery_date'] = $('#delivery_date_courier').val();
      filter['product_mnbdt'] = this.product_mnbdt;
      $("#callbackloader").css('display','block');   
    }
    this.allSubscription.push(this.checkoutService.updateCart(filter).subscribe(data =>
      {
        if (isPlatformBrowser(this.platformId)) {
          $("#callbackloader").css('display','none');
        }   

        this.checkoutService.calendarSelected.next(data);
        this.checkoutService.refreshSideBar.next(true);
      }
    ));
  }

}
