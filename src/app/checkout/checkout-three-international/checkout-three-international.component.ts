import { Component, OnInit,OnDestroy, Inject, PLATFORM_ID  } from '@angular/core';
import { CheckoutService } from '../checkout.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';

declare var $: any;
@Component({
  selector: 'app-checkout-three-international',
  templateUrl: './checkout-three-international.component.html',
  styleUrls: ['./checkout-three-international.component.css']
})
export class CheckoutThreeInternationalComponent implements OnInit,OnDestroy {
  view = 1;
  express_products = [];
  express_not_available = [];
  express_earliest_date = '';
  express_earliest_delivery_date = '';
  courier_products = [];
  courier_not_available = [];
  courier_earliest_date = '';
  courier_earliest_delivery_date = '';
  cutoff_count = 0;
  cutoff_product_count = 0;
  cutoff_products = [];
  express_date_array = [];
  clldate = [];
  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  cldate = [];
  product_mnbdt = [];
  selected_pincode = 0;
  

  international_products = [];
  international_not_available = [];
  international_earliest_date = '';
  international_earliest_delivery_date = '';
  international_date_array = [];
  cssDy:any={};
  private allSubscription:Array<Subscription> = [];

  constructor(private checkoutService: CheckoutService,
    public sanitizer: DomSanitizer,
    private gtmService: GoogleTagManagerService, private NgxSpinner:NgxSpinnerService,
    @Inject(PLATFORM_ID) private platformId: object) { }
  ngOnInit(): void {

    this.cssDy = { datetimepickercss : this.getSafeCDNUrl('live-new/css/bootstrap-datetimepicker.min.css.gz'),jqueryuicss : this.getSafeCDNUrl('live-new/css/jquery-ui.css.gz')};

    this.allSubscription.push(this.checkoutService.resetSteps.subscribe(data => {
      if(typeof(data['login']) !='undefined')
      {
        this.view = 1;
      }else if(typeof(data['address']) !='undefined')
      {
        this.view = 1;
      }
    }));
    this.allSubscription.push(this.checkoutService.addressBlock.subscribe(flag =>
    { 
      if(flag) 
      this.view = 1;
    }));
    this.allSubscription.push(this.checkoutService.addressSelected.subscribe(showaddon =>
      {
        this.if_international_selected_date_other = '';
        this.international_other_date = '';
        this.international_delivery_text = '';
         this.checkoutService.getProductViewInternational().subscribe(data => {
          this.internationalCalculation(data);  
        });
      }
    ));
   
    this.allSubscription.push(this.checkoutService.calendarSelected.subscribe(data => {
      this.reset();
      this.internationalCalculation(data);
    }));
  }

  ngOnDestroy():void {
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
  internationalCalculation(data)
  {
    this.product_mnbdt = [];
    this.selected_pincode = data['selected_pincode'];
    this.international_not_available = data['international_not_available'];
    this.cutoff_count = data['cutoff_count'];
    this.cutoff_product_count = data['cutoff_product_count'];
    this.cutoff_products = data['cutoff_products'];
    if(this.cutoff_products.length != 0 && (this.cutoff_count !=this.cutoff_product_count))
    {
        for(let product of this.cutoff_products)
        {
          this.product_mnbdt.push(product['product_id']);
        }
    }

    this.clldate = data['clldate'];
    this.cldate = data['cldate'];
    if(typeof(data['international']) !='undefined')
    {
      this.international_products = data['international'];
      this.international_earliest_date = data['international_earliest_date'];
      this.international_earliest_delivery_date = data['international_earliest_delivery_date'];
      this.international_block();
    }
    this.view = 4;
  }
  
  if_international_selected_date_other = '';
  international_other_date = '';
  international_delivery_text = '';
  international_block()
  {
    this.international_date_array = ['00-00-0000'];  
    if((this.international_date_array.includes(this.international_earliest_delivery_date)==false) && this.international_earliest_date !='')
    {
      this.if_international_selected_date_other = 'selected';
    }
    if((this.international_date_array.includes(this.international_products[0]['delivery_date'])==false))
    {
      var date = this.international_products[0]['delivery_date'].split('-');
      this.international_other_date = date[2]+'/'+date[1]+'/'+date[0];
    }
    if(this.international_products[0]['shipping_method']=='international.international')
    {
      this.international_delivery_text = 'International';
    }
  }

  international_date_not_selected_error = '';
 editProduct(type)
  {
    var data = {};
    if(this.international_earliest_date=='')
    {
      this.international_date_not_selected_error = 'Sorry,this product cannot be delivered';
    }else
    {
      data['international_product'] = 0;
      data['subCalendar'] = 0;
      data['earliest_date'] = this.international_earliest_date;
      data['product_id'] = this.international_products[0]['product_id'];
      data['product_type'] = this.international_products[0]['product_type'];
      data['deliveryCharge'] = this.international_products[0]['deliveryCharge'];
      data['selected_pincode'] = this.selected_pincode;
      data['product_mnbdt'] = this.product_mnbdt;
      data['cldate'] = this.cldate;
      data['clldate'] = this.clldate;
      console.log('type data',data);

      this.checkoutService.getInternationalCalendar.next(data);
    }       
  }
  delivery_date = '';
  getInternationalShipping()
  {
    var devicetype = '';
    var browser = '';
    // if (isPlatformBrowser(this.platformId)) {
    //   $("#callbackloader").css('display','block');
      
    // } 
    this.NgxSpinner.show();
    this.checkoutService.getInternationalShipping(localStorage.getItem('deviceType'),localStorage.getItem('browser')).subscribe(data =>
      {
        if(typeof(data['reload']) !='undefined')
        {
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          window.location.reload();
        }
        if(data['filter_products'] !='undefined')
        {
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          this.gtmService.pushTag({"pageType":"orderSummary",'event':'checkout','products': data['filter_products'],'cartTotals': data['filter_total']});
          this.gtmService.pushTag({'event': 'cartUpdated', 'updatedProducts': data['filter_products']});
        }
        if(this.international_products.length !=0)
        {
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          var date = this.international_products[0]['delivery_date'].split('-');
          this.delivery_date = date[2]+'/'+date[1]+'/'+date[0];
        }

        if(date=="00,00,0000")
        {
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          this.international_date_not_selected_error = 'Please select delivery date';
        }else{
          // if (isPlatformBrowser(this.platformId)) {
          //   $("#callbackloader").css('display','none');
          // }
          this.NgxSpinner.hide();
          this.view = 3;
          this.checkoutService.deliverySelected.next(data['customer_data']);          
        }        
      }
    );
  }

  reset()
  {
    this.express_products = [];
    this.express_not_available = [];
    this.express_earliest_date = '';
    this.express_earliest_delivery_date = '';
    this.courier_products = [];
    this.courier_not_available = [];
    this.courier_earliest_date = '';
    this.courier_earliest_delivery_date = '';
    this.cutoff_count = 0;
    this.cutoff_product_count = 0;
    this.cutoff_products = [];
    this.express_date_array = [];
    this.clldate = [];
    this.cldate = [];
    this.product_mnbdt = [];
    this.selected_pincode = 0;
    this.if_international_selected_date_other = '';
    this.international_other_date = '';
    this.international_delivery_text = '';
  }
  openDeliverySection()
  {
    this.checkoutService.resetSteps.next({'delivery':1});
    this.checkoutService.deliveryBlock.next(true);
    this.view = 4;
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
}
