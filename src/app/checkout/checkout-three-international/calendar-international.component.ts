import { Component,OnInit,OnDestroy, Inject, PLATFORM_ID} from '@angular/core';
import { CheckoutService } from '../checkout.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import * as _ from 'lodash';

//import * as $ from 'jquery';
declare var $: any;
declare function shippingname1(): any;
declare function getToken(): any;
@Component({
  selector: 'app-calendar-international',
  templateUrl: './calendar-international.component.html'
})
export class CalendarInternationalComponent implements OnInit,OnDestroy {
  APIURL  = environment.API_URL;

  product_mnbdt = [];
  constructor(private checkoutService: CheckoutService,private router: Router,
    @Inject(PLATFORM_ID) private platformId: object) { }
  expressshiping_next_day_html = '';
  dayshipping_html = '';
  expressshiping_html = '';
  backtocalender = {};
  private allSubscription:Array<Subscription> = [];
  ngOnInit(): void {

    this.allSubscription.push(this.checkoutService.getInternationalCalendar.subscribe(data =>
      {
        if (isPlatformBrowser(this.platformId)) {
          $('.delivertimebox1').show();
          $('.delivertimebox2').hide();
          $('.delivertimebox3').hide(); 
          $('#datetimepicker12pp').show();
        }
        this.backtocalender = data;
        this.getInternationalCalendar(data);        
      }
    ));

    if (isPlatformBrowser(this.platformId)) {
    $( document ).ready(function() {
      
      $('.backtocalender').click(function(){
        $('.delivertimebox2').hide();
        $('.delivertimebox1').show();
        //$(".delivertimebox1").load(" .delivertimebox1 > *");
      });
      $('.backtoMethod').click(function(){
        $('.delivertimebox2').show();
        $('.delivertimebox3').hide();        
      });
      $("#deliverydatemodal").on('click','.getcalender',function() {
          $('#shippingnamevalue').val('');
          $('#deliverytimeslot1').val('');
          $('#deliverytimeslot').val('');
          $('.delivertimebox1').show();
          $('.delivertimebox2').hide();
          $('.delivertimebox3').hide();
        
          $('#deli_detail_open').hide();
      
          $("#remotecity").val('');
          $('#resultLoading').hide();
          $('#datetimepicker12pp').show();
      });
    
    });
  }
  
  }
  
  ngOnDestroy():void {
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }

   getInternationalCalendar(data){
   var cld = '';
    /*for(let cldate of data['cldate'])
    {
      cld +='"'+cldate+'",';
    }
    */
    this.product_mnbdt = data['product_mnbdt'];
    var months = [ "Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec" ];
    if (isPlatformBrowser(this.platformId)) {
    $('#datetimepicker12pp').html('<input data-date="05-09-2017" data-date-format="DD-MM-YYYY" class="datetimepicker12pp" id="pickselectdate" type="hidden">');
    $('#datetimepicker12pp').removeClass('hasDatepicker');
    $('.backtocalender').show();
    }
    if(data['subCalendar']==0)
    {
      document.getElementById('deliverydatemodal').style.display = "block";
      if (isPlatformBrowser(this.platformId)) {
        $('.getcalender').removeClass('getcalender').addClass('backtocalender');
      }
    }
    var calendar = data['earliest_date'].split('/');
    var dataYear = calendar[0];
    var dataMonth = calendar[1]-1;
    var dataDay = calendar[2];
    if (isPlatformBrowser(this.platformId)) {
    $(document).ready(function(){
    //  var disableDates = data['cldate'];
      var disableDates = [];

      //console.log(disableDates);
    $('#datetimepicker12pp').datepicker(
    {
      inline: true,
      format: 'dd-mm-yyyy',
      startDate: new Date(dataYear,dataMonth,dataDay),
			defaultDate: new Date(dataYear,dataMonth,dataDay),
      minDate:new Date(dataYear,dataMonth,dataDay),
      beforeShowDay: function(date){
        var d = date;
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1; 
        var curr_year = d.getFullYear();
        var formattedDate = curr_date + "/" + curr_month + "/" + curr_year;
       // console.log('formattedDate'+formattedDate);
        var activeDate = dataDay + "/" + (dataMonth+1) + "/" + dataYear;
      //  console.log('activeDate'+activeDate);
        var dmy = curr_year + "/" + (curr_month) + "/" + curr_date;
      
      if (formattedDate==activeDate){
          return {
          classes: 'active'
          };
        }
      if(disableDates.indexOf(dmy) != -1){

          return false;

      }else
      {
        return true;
      }
        
      }
    }).on("changeDate", function (date) {
      $("#pickselectdate").val(date.format());
      var dts =   $("#pickselectdate").val(); 
      data['dts'] = dts;
      var pincode =   data['selected_pincode']; 
      var productId = data['product_id'];
     // var arr = ['15-02-2019','08-02-2019','09-02-2019','10-02-2019', '11-02-2019', '12-02-2019', '13-02-2019','07-02-2019', '14-02-2019'];
     // var arrs = ['15-02-2019','13-02-2019','14-02-2019'];
     $('.international_dc').html('Free');
      if(data['deliveryCharge']){			  
        $('.international_dc').html('Rs. '+data['deliveryCharge']);     
      }
      $('#methodshipping').show(); 
        var cityid =  $("#city"+data['product_id']).val();          
        var remotecitys =   $("#remotecity").val(); 
        var morningShip =   $("#morningShip").val();
        
        var parts = dts.split("-");
        var sd = new Date(parts[2],parts[1]-1,parts[0]);
        var month = new Array();
         month[0] = "Jan";
         month[1] = "Feb";
         month[2] = "Mar";
         month[3] = "Apr";
         month[4] = "May";
         month[5] = "Jun";
         month[6] = "Jul";
         month[7] = "Aug";
         month[8] = "Sep";
         month[9] = "Oct";
         month[10] = "Nov";
         month[11] = "Dec";
        var n = month[sd.getMonth()];
        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var dayName = days[sd.getDay()];
        
        $('#delivery-details-date-'+data['product_id']).html(dayName+', '+parts[0]+' '+n+', '+parts[2].toString().substr(2,2));
        $('#selected-date').val(dayName+', '+parts[0]+' '+n+', '+parts[2].toString().substr(2,2));
        
        
        var d_tmp = new Date();
        var date_tmp = d_tmp.getDate();
        var month_tmp = d_tmp.getMonth() + 1; 
        var year_tmp = d_tmp.getFullYear(); 
        const tomorrow = new Date(d_tmp)
        tomorrow.setDate(tomorrow.getDate() + 1)
        

        var d = date_tmp+'-'+month_tmp+'-'+year_tmp;
        var currentHour = d_tmp.getHours();            
        var tomorrowdate = tomorrow.getDate()+'-'+tomorrow.getMonth() + 1+'-'+tomorrow.getFullYear();        
        if(dts==d) {
          $('#shippinglabelmorning_time').hide();
          $('#morning_time_label').hide();
          $('#morning_time_label').parent().parent().hide();
        } else {
          $('#shippinglabelmorning_time').show();
          $('#morning_time_label').show();
        
          $('#morning_time_label').parent().parent().show();
        }          
      
        if(dts==tomorrowdate && currentHour >=18){
          $('#shippinglabelmorning_time').hide();
          $('#morning_time_label').hide();
          $('#morning_time_label').parent().parent().hide();
        } 
        
        if(dts==d && currentHour >=18){            
          $('#shippinglabelday_time').hide();
          $('#day_time_label').hide();
        } 
        
        if(dts!=d && currentHour >=18){            
          $('#shippinglabelday_time').show();
          $('#day_time_label').show();
        }
        
        if(dts==d && currentHour >=20){
          
          $('#shippinglabelfixtime').hide();
          $('#fixtime').hide();
        }
        
        if(dts==d && currentHour >=20){            
          $('#shippinglabelmidnight').hide();
          $('#midnight').hide();
        }
        $('#delivery_date').val(dts);
        $('.delivertimebox1').hide();
        $('.delivertimebox2').show();

       }
      ); 
  });
  }

  }

  internationalMethod()
  {
    if (isPlatformBrowser(this.platformId)) {
      $('#shippingnamevalue').val('international.international');
      $('.delivertimebox2').hide();
      $(".delivertimebox3").show();
   }
  }
  internationalSlot()
  {
    if (isPlatformBrowser(this.platformId)) {
      $('#deliverytimeslot').val('06:00 - 21:00 hrs');
      document.getElementById('deliverydatemodal').style.display = "none";
      $("#datetimepicker12pp").datepicker("destroy");
      $('#updateCart').trigger('click');
    }
  }

  closeCalendar() {
    document.getElementById('deliverydatemodal').style.display = "none";
    if (isPlatformBrowser(this.platformId)) {
      $("#datetimepicker12pp").datepicker("destroy");
    }
  }

  updateCart()
  {
    if (isPlatformBrowser(this.platformId)) {
      if($('#deliverytimeslot').val()=='')
      {
        this.router.navigate(['/checkout']);
      }
      var filter = {};
      filter['shipping_name'] = $('#shippingnamevalue').val();
      filter['shipping_time_slot'] = $('#deliverytimeslot').val();
      filter['delivery_date'] = $('#delivery_date').val();
      filter['product_mnbdt'] = this.product_mnbdt;
      $("#callbackloader").css('display','block');   
    }
    this.allSubscription.push(this.checkoutService.updateCartInternational(filter).subscribe(data =>
      {
        if (isPlatformBrowser(this.platformId)) {
          $("#callbackloader").css('display','none');
        }   

        this.checkoutService.calendarSelected.next(data);
        this.checkoutService.refreshSideBar.next(true);
      }
    ));
  }

}
