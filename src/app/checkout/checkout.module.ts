import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CheckoutComponent } from './checkout.component';
import { CheckoutRightbarComponent } from './checkout-rightbar/checkout-rightbar.component';
import { CheckoutOneComponent } from './checkout-one/checkout-one.component';
import { CheckoutOneComponentInternational } from './checkout-one-international/checkout-one-international.component';

import { CheckoutTwoComponent } from './checkout-two/checkout-two.component';
import { CheckoutTwoComponentInternational } from './checkout-two-international/checkout-two-international.component';
import { CheckoutThreeComponent } from './checkout-three/checkout-three.component';
import { CalendarComponent } from './checkout-three/calendar.component';
import { CourierCalendarComponent } from './checkout-three/courier-calendar.component';
import { CheckoutThreeInternationalComponent } from './checkout-three-international/checkout-three-international.component';
import { CalendarInternationalComponent } from './checkout-three-international/calendar-international.component';


import { CheckoutFourComponent } from './checkout-four/checkout-four.component';
import { CheckoutFourInternationalComponent } from './checkout-four-international/checkout-four-international.component';
import { CheckoutFiveComponent } from './checkout-five/checkout-five.component';
import { CheckoutFiveInternationalComponent } from './checkout-five-international/checkout-five-international.component';
import { ShoppingBagComponent } from '../shopping-bag/shopping-bag.component';
import { CartLayoutModule } from '../cart-layout.module';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
declarations: [ CheckoutComponent,  CheckoutRightbarComponent,
    CheckoutOneComponent,
    CheckoutOneComponentInternational,
    CheckoutTwoComponent,
    CheckoutTwoComponentInternational,
    CheckoutThreeComponent,
    CheckoutThreeInternationalComponent,
    CalendarInternationalComponent,
    CalendarComponent,
    CourierCalendarComponent,
    CheckoutFourComponent,
    CheckoutFourInternationalComponent,
    CheckoutFiveInternationalComponent,
    CheckoutFiveComponent, ShoppingBagComponent],
imports: [ RouterModule.forChild([
    { path: '', component: CheckoutComponent }
]), CommonModule, ReactiveFormsModule,CartLayoutModule,NgxSpinnerModule ]
})
export class CheckoutModule{

}