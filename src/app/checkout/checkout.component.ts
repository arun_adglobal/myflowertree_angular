import { Component, OnInit , Inject, PLATFORM_ID} from '@angular/core';
import { Router } from '@angular/router';

import { CheckoutService } from './checkout.service';
import { SEOService } from '../seo.service';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  constructor(private checkoutService: CheckoutService,
    public sanitizer: DomSanitizer,
    private seoservice: SEOService,
    private router: Router,private NgxSpinner:NgxSpinnerService,
    @Inject(PLATFORM_ID) private platformId: object) { }
  products = [];
  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  totals = [];
  cssDy:any={};
  showcheckout = false;
  checkoutType=  'local';
  private subscription: Subscription;
  ngOnInit(): void {
    this.NgxSpinner.show();
    this.cssDy = { checkoutcss : this.getSafeCDNUrl('2019/newcheckout/css/checkout.css.gz')};


    if (isPlatformBrowser(this.platformId)) {
      this.showcheckout = true;
    this.subscription = this.checkoutService.initiateCheckout().subscribe((cart_data:any) => {
      if(cart_data[0]['cart_data'].length==0)
      {
        this.router.navigate(['/']);
      }else{
        if(cart_data[0]['cart_data'][0]['product_type']=='International')
        {
          this.NgxSpinner.hide();
          this.checkoutType = 'International';
        }else{
          this.NgxSpinner.hide();
          this.checkoutType = 'local';
        }
      }
     });
    this.SEO();
    }else{
      this.showcheckout = false;
    }
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  SEO()
  {
    this.seoservice.setTitle('Checkout | MyFlowerTree');
 }

  ngOnDestroy(){
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }
}
