import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ReviewallComponent } from './reviewall.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ ReviewallComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: ReviewallComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class ReviewallModule{

}