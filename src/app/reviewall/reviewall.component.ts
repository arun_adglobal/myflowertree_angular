import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../review/review.service';
import { ActivatedRoute} from '@angular/router';
import { combineLatest } from 'rxjs';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-reviewall',
  templateUrl: './reviewall.component.html',
  styleUrls: ['./reviewall.component.css'],
  providers:[ReviewService]
})
export class ReviewallComponent implements OnInit {
  review:any;
  product_id:string = '';
  allSubscription:Array<Subscription> = [];
  
  constructor(private route: ActivatedRoute,private reviewService:ReviewService) { }

  ngOnInit(): void {

    this.allSubscription.push(combineLatest(this.route.params).subscribe(
      ([params]) =>{
        this.product_id = params['string']; 
        this.allSubscription.push(this.reviewService.getReviewsByProductId(this.product_id).subscribe(review => {
          
           this.review = review;
        }));
      }));

  }

  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }

}
