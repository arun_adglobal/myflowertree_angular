import { Injectable,EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  showSearch = new EventEmitter<boolean>();
  constructor() { }
}
