import { Component, OnInit } from '@angular/core';
import { SearchService } from '../st-search/search.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-st-search',
  templateUrl: './st-search.component.html',
  styleUrls: ['./st-search.component.css']
})
export class StSearchComponent implements OnInit {
  showTap = true;
  subscription:Subscription;
  constructor(private search : SearchService) { }
  ngOnInit(): void {
    this.subscription = this.search.showSearch.subscribe(flag => this.showTap = flag );
  }

  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }
}
