import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { StSearchComponent } from './st-search.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ StSearchComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: StSearchComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class StSearchModule{

}