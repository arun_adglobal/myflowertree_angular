import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from '../miscellaneous/product/product.service';
import { FloristLoginService } from '../floristlogin/floristlogin.service';
import { CheckoutService } from '../checkout/checkout.service';
import { AuthenticationService } from '../authentication.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angularx-social-login';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-floristlogin',
  templateUrl: './floristlogin.component.html',
  styleUrls: ['./floristlogin.component.css']
})
export class FloristLoginComponent implements OnInit {
  CDN_PATH  = environment.CDN_PATH;
  safeUrl: SafeResourceUrl;
  floristloginForm: FormGroup;
  floristsignupForm: FormGroup;
  
  cssDy:any={};
  step = 1;
  view = 1;
  is_customer = false;
  customer_data = {};
  error = '';
  alreadymsg = '';
  otp = '';
  telephone = '';
  constructor(private ProductService: ProductService,
    private floristLoginService: FloristLoginService, 
    private checkoutService: CheckoutService, 
    private auth: AuthenticationService, 
    public sanitizer: DomSanitizer,
    private router:Router,
    private socialAuthService: AuthService,
    private gtmService: GoogleTagManagerService) {
   }

  ngOnInit(): void {
    this.cssDy = { faviconpng : this.getSafeCDNUrl('live-new/images/favicon.png'),bootstrapMin : this.getSafeCDNUrl('live-new/css/bootstrap.min.css.gz'),customCss : this.getSafeCDNUrl('live-new/css/custom.css.gz'),responsiveCss : this.getSafeCDNUrl('live-new/css/responsive.css.gz'),fontAwsCss : this.getSafeCDNUrl('live-new/font-awesome/css/font-awesome.min.css.gz')};
    this.floristsignupForm = new FormGroup({
      'register_email' : new FormControl(null,[Validators.required,Validators.email]),
      'register_name' : new FormControl(null, Validators.required),
      'register_password' : new FormControl(null, Validators.required),
      'company_name' : new FormControl(null, Validators.required),
      'full_address' : new FormControl(null, Validators.required),
      'pin_code' : new FormControl(null, Validators.required),
      'gst_number' : new FormControl(null, Validators.required),
      'bank_detail' : new FormControl(null, Validators.required),
      'contact_person' : new FormControl(null, Validators.required),
      'register_telephone' : new FormControl(null, Validators.required),
      'service_speciality' : new FormControl("Default", Validators.required)
    });

  }
  

  valid_from = true;
  submitSignupForm(){
    
    if(! this.floristsignupForm.valid)
      {
        this.valid_from = false;
        this.floristsignupForm.markAllAsTouched();
        
      }
   //   this.valid_from = true;
    if(this.valid_from)
    {
      document.getElementById('resultLoading').style.display = "block";

      var request_data = {};
      request_data['register_email'] = this.floristsignupForm.value.register_email;
      request_data['register_name'] = this.floristsignupForm.value.register_name;
      request_data['register_password'] = this.floristsignupForm.value.register_password;
      request_data['register_country_code'] = '91';
      request_data['register_telephone'] = this.floristsignupForm.value.register_telephone;


      request_data['company_name'] = this.floristsignupForm.value.company_name;
      request_data['full_address'] = this.floristsignupForm.value.full_address;
      request_data['pin_code'] = this.floristsignupForm.value.pin_code;
      request_data['gst_number'] = this.floristsignupForm.value.gst_number;
      request_data['bank_detail'] = this.floristsignupForm.value.bank_detail;
      request_data['contact_person'] = this.floristsignupForm.value.contact_person;
      request_data['service_speciality'] = this.floristsignupForm.value.service_speciality;

      this.checkoutService.addFlorist(request_data).subscribe(response => {
         document.getElementById('resultLoading').style.display = "none";

          if(response['message']=='success')
          {
            $('.logintext').hide();
            $('.flodiv').hide();
            $('.loginheading').html('Thank you for signing up. One of our associate shall be in touch with you shortly.');
          }else if(response['message']=='Vendor email id already exist')
          {
            $('.logintext').hide();
            $('.flodiv').hide();
            $('.loginheading').html('Vendor email id is already registered with us');
         
          }
        });
    }
  }
  
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
}
