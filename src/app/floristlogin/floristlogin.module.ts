import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { FloristLoginComponent } from './floristlogin.component';
import { CartLayoutModule } from '../cart-layout.module';
@NgModule({
declarations: [ FloristLoginComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: FloristLoginComponent }
]), CommonModule, ReactiveFormsModule,CartLayoutModule ]
})
export class FloristLoginModule{

}