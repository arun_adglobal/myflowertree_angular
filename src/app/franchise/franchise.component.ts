import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HomeService } from '../home/home.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-franchise',
  templateUrl: './franchise.component.html',
  styleUrls: ['./franchise.component.css']
})
export class FranchiseComponent implements OnInit {
  franchiseForm: FormGroup;
  CDN_PATH  = environment.CDN_PATH;
  constructor( private router:Router, private homeService: HomeService) {
    
  }

  ngOnInit(): void {
    this.franchiseForm = new FormGroup({
      'email' : new FormControl(null,[Validators.required,Validators.email]),
      'name' : new FormControl(null, Validators.required),
      'mobile' : new FormControl(null, Validators.required),
      'city' : new FormControl(null, Validators.required)
    });
  }

  
 
  valid_from = true;
  submitFranchiseForm(){
   
    if(! this.franchiseForm.valid)
      {
        this.valid_from = false;
        this.franchiseForm.markAllAsTouched();
        
      }
   //   this.valid_from = true;
    if(this.valid_from)
    {
      //console.log('eseses');
      var request_data = {};
      request_data['email'] = this.franchiseForm.value.email;
      request_data['name'] = this.franchiseForm.value.name;
      request_data['city'] = this.franchiseForm.value.city;
      request_data['mobile'] = this.franchiseForm.value.mobile;

      this.homeService.franchise(request_data).subscribe((response_data:any[]) => {
        this.router.navigate(['thankyou']);

      });
    }

  }

}
