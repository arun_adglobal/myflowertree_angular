import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { FranchiseComponent } from './franchise.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ FranchiseComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: FranchiseComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class FranchiseModule{

}