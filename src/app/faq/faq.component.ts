import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FaqService } from './faq.service';
import { isPlatformBrowser } from '@angular/common';
import { SEOService } from '../seo.service';


@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css'],
  providers:[FaqService]
})
export class FaqComponent implements OnInit {
  faqdata :any = [];
  constructor(private route: ActivatedRoute,
    private seoservice: SEOService,
    private FaqService:FaqService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {

    this.FaqService.getPageData().subscribe(faqdata => {          
      this.faqdata = faqdata;         
      //console.log(this.faqdata);
    });
    this.SEO();
  }
  SEO()
  {
    this.seoservice.setTitle("FAQ");
}
  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }

}
