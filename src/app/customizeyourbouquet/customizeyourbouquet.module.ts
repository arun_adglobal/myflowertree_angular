import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CustomizeyourbouquetComponent } from './customizeyourbouquet.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ CustomizeyourbouquetComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: CustomizeyourbouquetComponent }
]), CommonModule,ReactiveFormsModule,MainLayoutModule ]
})
export class CustomizeyourbouquetModule{

}