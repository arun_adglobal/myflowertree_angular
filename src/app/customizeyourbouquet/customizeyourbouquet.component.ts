import { Component, OnInit, ViewChild,ElementRef } from '@angular/core';
import { SEOService } from '../seo.service';
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-customizeyourbouquet',
  templateUrl: './customizeyourbouquet.component.html',
  styleUrls: ['./customizeyourbouquet.component.css']
})
export class CustomizeyourbouquetComponent implements OnInit {
  CDN_PATH  = environment.CDN_PATH;
  CDN_DOMAIN  = environment.CDN_DOMAIN;
  constructor(public elementRef: ElementRef,private seoservice: SEOService,) { }

  ngOnInit(): void {
    this.SEO();
  }
  SEO()
  {
    this.seoservice.setTitle("Customized Your Bouquet");
  }
  
  @ViewChild ('roseQuantity') roseQuantity;
  @ViewChild ('carnationQuantity') carnationQuantity;
  @ViewChild ('gerberasQuantity') gerberasQuantity;
  @ViewChild ('orchidsQuantity') orchidsQuantity;
  @ViewChild ('liliesQuantity') liliesQuantity;
  @ViewChild ('cakeQuantity') cakeQuantity;
  @ViewChild ('chocolatesQuantity') chocolatesQuantity;
  
  
  
  totalPrice = 0;
  specification = '';
  selectedRoses(){
    
    /*var optname = '';
    if(this.cakeQuantity.nativeElement.value)
    {
      optname += this.cakeQuantity.nativeElement.options[this.cakeQuantity.nativeElement.selectedIndex].getAttribute('size');
    }
    if(this.chocolatesQuantity.nativeElement.value)
    {
      optname += this.chocolatesQuantity.nativeElement.options[this.chocolatesQuantity.nativeElement.selectedIndex].getAttribute('name');
    }*/
    
    
    
    
    var total = 0;
    total = this.roseQuantity.nativeElement.value * 40;
    total += this.carnationQuantity.nativeElement.value * 50;
    total += this.gerberasQuantity.nativeElement.value * 40;
    total += this.orchidsQuantity.nativeElement.value * 90;
    total += this.liliesQuantity.nativeElement.value * 200;
    total += this.cakeQuantity.nativeElement.value * 1;
    total += this.chocolatesQuantity.nativeElement.value * 1;
    
    
      this.totalPrice = total;
      //this.specification = optname;
     
  }
  @ViewChild ('rosesSpeci') rosesSpeci;
  @ViewChild ('carnationSpeci') carnationSpeci;
  @ViewChild ('gerberasSpeci') gerberasSpeci;
  @ViewChild ('orchidsSpeci') orchidsSpeci;
  @ViewChild ('liliesSpeci') liliesSpeci;
  

  
  selectflowers(){
    
    var speci = '';
    speci +=   this.roseQuantity.nativeElement.value+' ' + this.rosesSpeci.nativeElement.value+' ';
    speci +=  this.carnationQuantity.nativeElement.value+' ' + this.carnationSpeci.nativeElement.value+' ';
    speci +=  this.gerberasQuantity.nativeElement.value+' ' + this.gerberasSpeci.nativeElement.value+' ';
    speci +=  this.orchidsQuantity.nativeElement.value+' ' + this.orchidsSpeci.nativeElement.value+' ';
    speci +=  this.liliesQuantity.nativeElement.value+' ' + this.liliesSpeci.nativeElement.value+' ';

    this.specification = speci;
  }
  product_options = [];
  selectedhowshell(productprice,optionid,specification1){
   // console.log(productprice);
   // console.log('dsds');
    if(this.product_options.includes(optionid))
    {
      //console.log('aaa');
      var index = this.product_options.indexOf(optionid);
      if (index > -1) {
        this.product_options.splice(index, 1);
      }
      
      this.totalPrice = this.totalPrice-parseInt(productprice);
      
      
      this.specification = this.specification.replace(specification1, '');


    }else
    {
      
      this.product_options.push(optionid);
     
        this.specification += specification1;
      
      
      this.totalPrice = this.totalPrice+parseInt(productprice);
      
      
    }
   
   }
  

}
