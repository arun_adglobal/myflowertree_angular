import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart/cart.service';
import { CheckoutService } from '../checkout/checkout.service';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ProductService } from '../miscellaneous/product/product.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { SEOService } from '../seo.service';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
@Component({
  selector: 'app-shopping-bag',
  templateUrl: './shopping-bag.component.html',
  styleUrls: ['./shopping-bag.component.css']
})
export class ShoppingBagComponent implements OnInit {
  cart_data = [];
  totals = [];
  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  filter_total = [];
  allSubscription:Array<Subscription> = [];
  constructor(private cartService: CartService, private checkoutService: CheckoutService, 
    private router: Router,
    public sanitizer: DomSanitizer,
    private gtmService: GoogleTagManagerService) { }

  ngOnInit(): void {
    this.getCart();
  }
  getCart(){    
    this.allSubscription.push(this.cartService.getCartData().subscribe((cart_data:any) => {
      if(cart_data[0]['cart_data'].length==0)
      {
        this.router.navigate(['/']);
      }
     
      this.cart_data = cart_data[0]['cart_data'];
      //console.log("this.cart_data " + this.cart_data);
      this.totals = cart_data[0]['totals'];
      this.filter_total = cart_data[0]['filter_total'];
      var gtmTag = {
        'contentGrouping': this.cart_data[0]['parent_category_path'],
        "pageType": "cart",
        'products': this.gtag_products(),
        'cartTotals': this.filter_total 
      }
      console.log('gtm tage in get cart')
      this.gtmService.pushTag(gtmTag);
     }));
  }
  gtag_products()
  {
    var products = [];
    for(let product of this.cart_data)
    {
      products.push({
        'cart_id'   : product['cart_id'],
        'key'    : product['key'],
        'queryParams' : product['queryParams'],
        'product_id'   : product['product_id'],
        'product_type' : product['product_type'],
        'thumb'     : product['thumb'],
        'name'     : product['name'],
        'model'     : product['model'],
        'cate_name'   : product['cate_name'],
        'parent_category_path' : product['parent_category_path'],
        'option'    : product['option'],
        'quantity'  : product['quantity'],
        'delivery_date'  : product['delivery_date'],
        'shipping_time_slot'  : product['shipping_time_slot'],
        'price'     : product['price'],
        'pro_price'     : product['price'],
        'addon'     : product['addon'],
        'total'    : product['total'],
        'href'      : product['href'],
        'images' : product['images'],
        'product_url' : product['product_url'],
        'url' :  product['url'],
      });
    }
    return products;
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  cart_delete(cart_id, key){
    this.gtag_removeCart(cart_id);
    this.allSubscription.push(this.cartService.cart_delete(cart_id,key).subscribe((cart_data:any) => {
      if(cart_data[0]['cart_data'].length==0)
      {
        this.router.navigate(['/']);
      }
      this.cart_data = cart_data[0]['cart_data'];
      this.totals = cart_data[0]['totals'];
      this.filter_total = cart_data[0]['filter_total'];
      console.log('gtage in shoppin g bage component cart delete');
      this.gtmService.pushTag({'event': 'cartUpdated', 'updatedProducts': this.gtag_products()});
      window.location.reload();
     }));
    
  }
  gtag_removeCart(cart_id)
  {
    for(let product of this.cart_data)
    {
      if(product['cart_id']==cart_id)
      {
        var product_detail = {
          'cart_id'   : product['cart_id'],
          'key'    : product['key'],
          'queryParams' : product['queryParams'],
          'product_id'   : product['product_id'],
          'product_type' : product['product_type'],
          'thumb'     : product['thumb'],
          'name'     : product['name'],
          'model'     : product['model'],
          'cate_name'   : product['cate_name'],
          'parent_category_path' : product['parent_category_path'],
          'option'    : product['option'],
          'quantity'  : product['quantity'],
          'delivery_date'  : product['delivery_date'],
          'shipping_time_slot'  : product['shipping_time_slot'],
          'price'     : product['price'],
          'pro_price'     : product['price'],
          'addon'     : product['addon'],
          'total'    : product['total'],
          'href'      : product['href'],
          'images' : product['images'],
          'product_url' : product['product_url'],
          'url' :  product['url'],
        }
        console.log('gtm tag in remove cart');
        this.gtmService.pushTag({'event':'removeFromCart','products': product_detail});
      }     
    }
  }
  update_cart(cart_id,quantity)
  {
    this.allSubscription.push(this.cartService.cart_update(cart_id, quantity).subscribe((cart_data:any) => {
      this.cart_data = cart_data[0]['cart_data'];
      this.totals = cart_data[0]['totals'];
      this.filter_total = cart_data[0]['filter_total'];
      console.log('gt, tag im update cart ')
      this.gtmService.pushTag({'event': 'cartUpdated', 'updatedProducts': this.gtag_products()});
     // this.router.navigate(['/checkout']);
     this.checkoutService.refreshSideBar.next(true);
     
      //window.location.reload();
     }));
  }

  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}
