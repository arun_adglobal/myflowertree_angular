import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { OrderticketComponent } from './orderticket.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ OrderticketComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: OrderticketComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class OrderticketModule {

}