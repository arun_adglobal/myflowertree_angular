import { Component, OnInit, ViewEncapsulation,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute,Router} from '@angular/router';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { FileUploadService } from "../shared/file-upload.service";
import { FeedbackService } from '../feedback/feedback.service';
import { environment } from '../../environments/environment';
import { combineLatest } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-orderticket',
  templateUrl: './orderticket.component.html',
  styleUrls: ['./orderticket.component.css']
})
export class OrderticketComponent implements OnInit {
  preview: string;
  orderTicketForm: FormGroup;
  queryParams = {};
  orderno = '';
  tickets:any = [];
  message = '';
  percentDone: any = 0;
  file_2:any = 0;
  file_3:any = 0;
  CDN_PATH  = environment.CDN_PATH;
  constructor(private route:ActivatedRoute,public router: Router, private FeedbackService:FeedbackService,public fileUploadService: FileUploadService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {

    combineLatest(this.route.params, this.route.queryParams).subscribe(
      ([params, qparams]) => {
          this.orderno  =  qparams['orderno'];
      });
      this.FeedbackService.orderTickets(this.orderno).subscribe(data => {
        console.log(data);
        this.tickets = data;

      });
      //this.orderno  =  atob(qparams['orderno']);


    this.orderTicketForm = new FormGroup({
      'comment' : new FormControl(null, Validators.required),
      'avatar' : new FormControl(null)
    });

  }
  // Image Preview
  uploadedImageArray = [];
file_code = [];
file_name = [];

  imageCode = '';
  fileUploadMsg = '';
  onUploadFile(event) {
    if (isPlatformBrowser(this.platformId)) {
      $("#callbackloader").css('display','block');

    }
    const file = (event.target as HTMLInputElement).files[0];
    this.orderTicketForm.patchValue({
      avatar: file
    });
    this.orderTicketForm.get('avatar').updateValueAndValidity()

    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.preview = reader.result as string;
    }
    reader.readAsDataURL(file);
    var request_data = {};
    request_data['avatar'] = this.orderTicketForm.value.avatar;
    this.fileUploadService.orderticketImageUpload(request_data).subscribe(data => {
      if (isPlatformBrowser(this.platformId)) {
        $("#callbackloader").css('display','none');
      }
      if(data['code'] !="")
      {
        this.fileUploadMsg = 'success';
        /*(this.uploadedImageArray = $.grep(this.uploadedImageArray, function(e){ 
          return e['option'] != product_option_id; 
        });*/
        this.file_code.push(data['code']);  
        this.file_name.push(data['filename']);      
      }else{
        this.fileUploadMsg = 'fail';
      }
      //console.log("this.uploadedImageArray " + this.uploadedImageArray);
    });
  }
  submitOrderTicketForm()
  {
    var request_data = {};
   
    request_data['comment'] = this.orderTicketForm.value.comment;
    request_data['orderID'] = this.orderno;
    request_data['code'] = this.file_code.join(",");
    request_data['filename'] = this.file_name.join(",");
    //request_data['city'] = 0;
    //console.log(request_data);
    this.FeedbackService.usersOrderticket(request_data).subscribe((data) => {
      setTimeout(function(){
        location.reload();
      },2000);
    });

  }
  

addMore()
{
  if (isPlatformBrowser(this.platformId)) {
    if(this.file_2==0)
    {
      $('#file_block_2').show();
      this.file_2 = 1;
    }else if(this.file_3==0)
    {
      $('#file_block_3').show();
      this.file_3 = 1;
    }
  }
}

remove(id)
{
  if (isPlatformBrowser(this.platformId)) {
    if(id===2)
    {
      $('#file_block_2').hide();
      this.file_2 = 0;
    }else if(id===3)
    {
      $('#file_block_3').hide();
      this.file_3 = 0;
    }
  }
	
}                                                                                                                                                               

}
