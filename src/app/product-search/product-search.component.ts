import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { ProductSearchService } from './product-search.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { combineLatest } from 'rxjs';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { SEOService } from '../seo.service';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';

declare function gtag_productClick(data): any;
declare function gtag_contentGrouping(category_name): any;
declare function gtag_productImpressions(data): any;
declare const getDevice : any;

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.css'],
  providers: [ProductSearchService]
})
export class ProductSearchComponent implements OnInit {
  category_slug:string = '';
  category_url:string = '';
  safeUrl: SafeResourceUrl;
  safetext:SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  queryParams = {};
  category_data:any = {};
  custom_layout:any = {};
  custom_layout_product:any = {};
  faqs:any = [];
  blogposts:any = [];
  quick_links:any = [];
  nearby_locations:any = [];
  products:any = [];
  deviceType='Desktop';
  showContent  = false;
  notScrolly = true;
  notEmptyProducts = true;
  offset:number = 0;
  limit = 20;
  breadcrumbSchema = {};
  itemListSchema = {};
  productSchema = {};
  faqSchema = {};
  breadcrumbs = [];
  product_total = 0;
  cssDy:any={};
  defaultImage = this.CDN_PATH+'live-site-2016/2020/myflowertree-loader.jpg';
  constructor(private route: ActivatedRoute,
    private productSearchService: ProductSearchService,
    private spinner: NgxSpinnerService,
    private router:Router,private gtmService: GoogleTagManagerService,
    private seoservice: SEOService,
    public sanitizer: DomSanitizer,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit() {

    this.cssDy = { birthdayCss : this.getSafeCDNUrl('2019/birthday/stylesheets/birthday.css.gz'),jquerylazy : this.getSafeCDNUrl('live-new/js/jquery.lazy.1.6.min.js.gz'),popperMin : this.getSafeCDNUrl('live-new/js/popper.min.js.gz')};

    combineLatest([this.route.params, this.route.queryParams]).subscribe(
      ([params, qparams]) => {
        this.breadcrumbs.push({'text':'Home','href':'https://www.myflowertree.com/'});
        this.breadcrumbs.push({'text':'Search','href':''});
        this.offset = 0;
        this.category_slug = this.router.url.replace("/", "");
        this.category_url = this.category_slug.split("?")[0];
        this.queryParams['sort'] = qparams['sort'];
        this.queryParams['order'] = qparams['order'];
        this.queryParams['type'] = qparams['type'];
        this.queryParams['price'] = qparams['price'];

        this.queryParams['page'] = 1;
        if(typeof(qparams['page']) !="undefined")
        {
          this.queryParams['page'] = qparams['page'];
          if((this.queryParams['page'] - 1) * this.limit>0){
            this.offset = this.limit*(this.queryParams['page'] - 1);
          }else{
            this.offset = (this.queryParams['page'] - 1) * this.limit;
          }
        }
        this.queryParams['pincode'] = '';
        if(typeof(qparams['pincode']) !="undefined")
        {
          this.queryParams['pincode'] = qparams['pincode'];
        }
        this.queryParams['product_type'] = '';
        if(typeof(qparams['product_type']) !="undefined")
        {
          this.queryParams['product_type'] = qparams['product_type'];
        }
        this.products = [];
        this.getSearchProducts();
        if (isPlatformBrowser(this.platformId)) {
        this.deviceType = getDevice();
        }
      }
    );
   }


  onScroll() {
    if(this.notScrolly && this.notEmptyProducts)
    {
    //  this.spinner.show();
      this.offset = this.offset + 20;
      this.notScrolly = false;
      this.getSearchProducts();
    }

  }
  getSearchProducts() {
    this.queryParams['offset'] = this.offset;
    this.productSearchService.getSearchProducts(this.queryParams).subscribe((products_data : any[]) => {
     // this.spinner.hide();
      if(products_data['products'].length===0)
      {
        this.notEmptyProducts = false;
      }
      this.product_total = products_data['product_total'];
      this.seoservice.setTitle(`Search`);
      this.products = this.products.concat(products_data['products']);
      this.notScrolly  =true;
    });
  }

  productClick(product_id)
  {
    for(let product of this.products)
    {
      if(product['product_id']==product_id)
      {
       var gtmTag =  {
        'event': 'productClick',
        'ecommerce': {
          'click': {
            'actionField': {'list': product['cate_name']},
            'products': [{
              'name': product['product_name'],
              'id': product['product_id'],
              'price': product['price'],
              'brand': 'MYFLOWERTREE',
              'category': product['cate_name'],
              'variant':  product['product_type'],
              'position': product['sort_order'],
            }]
          }
        },
        'eventCallback': function() {
        }
      }
      if(this.seoservice.isBrowserPlatform()){
        this.gtmService.pushTag(gtmTag);
      }
        //this.router.navigate(['/'+product['keyw']]);
        break;
      }
    }
  }


   toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  getSafetext(text:string = ''){
    this.safetext= this.sanitizer.bypassSecurityTrustResourceUrl(text);
    return this.safetext;
  }



}
