import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';


@Injectable()
export class ProductSearchService {
APIURL  = environment.API_URL;
  constructor(private http: HttpClient,
    @Inject(PLATFORM_ID) private platformId: object) {}
  getCategoryPageData(category_slug) {
    if(category_slug.includes("?"))
    {
      category_slug = category_slug.replace("?", "&");
    }
    var url = this.APIURL+'api/category?category='+category_slug;
    if (isPlatformBrowser(this.platformId)) {
      if(localStorage.getItem('filter_city') !=null)
      {
        url += '&filter_city='+localStorage.getItem('filter_city');
      }
    }
    return this.http.get(url, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  /*getCategoryCustomroducts(category_slug) {
    return this.http.get(this.APIURL+'api/getCategoryCustomroducts?category='+category_slug).pipe(map(data => {
      return data;
     }));
  }*/

  
  getBottomContent(category_slug) {
    if(category_slug.includes("?"))
    {
      category_slug = category_slug.replace("?", "&");
    }
    return this.http.get(this.APIURL+'api/bottomContent?category='+category_slug, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getSearchProducts(queryParams) {
    var url = this.APIURL+'api/searchProducts?offset='+queryParams.offset;
    if(typeof queryParams.pincode !='undefined' && queryParams.pincode !='')
    {
      url += '&pincode='+queryParams.pincode;
    }
    if(typeof queryParams.product_type !='undefined' && queryParams.product_type !='')
    {
      url += '&product_type='+queryParams.product_type;
    }
   
    if(typeof queryParams.sort !='undefined' && queryParams.sort !='')
    {
      url += '&sort='+queryParams.sort;
    }
    if(typeof queryParams.order !='undefined' && queryParams.order !='')
    {
      url += '&order='+queryParams.order;
    }
    if(typeof queryParams.type !='undefined' && queryParams.type !='')
    {
      url += '&type='+queryParams.type;
    }
    
    return this.http.get(url, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }


}
