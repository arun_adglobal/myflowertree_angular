import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser'
import { PipeTransform, Pipe } from "@angular/core";
import { ProductSearchComponent } from './product-search.component';
import { MainLayoutModule } from '../main-layout.module';

@Pipe({ name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@NgModule({
declarations: [ SafeHtmlPipe,ProductSearchComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: ProductSearchComponent }
]), CommonModule, ReactiveFormsModule, NgxJsonLdModule,InfiniteScrollModule,LazyLoadImageModule,
NgxSpinnerModule,MainLayoutModule]
})
export class ProductSearchModule{

}

