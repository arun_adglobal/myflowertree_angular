import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from '../miscellaneous/product/product.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../environments/environment';
import { CheckoutService } from '../checkout/checkout.service';
import { AuthenticationService } from '../authentication.service';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
 
@Component({
 selector: 'app-cslogin',
 templateUrl: './cslogin.component.html',
 styleUrls: ['./cslogin.component.css']
})
export class CsloginComponent implements OnInit {
 loginForm: FormGroup;
 signupForm: FormGroup;
 loginOTPForm: FormGroup;
 
 step = 1;
 view = 1;
 is_customer = false;
 customer_data = {};
 error = '';
 otp = '';
 telephone = '';
 CDN_PATH  = environment.CDN_PATH;
 private allSubscription:Array<Subscription> = [];
 constructor(private ProductService: ProductService,
   private checkoutService: CheckoutService,
   private auth: AuthenticationService,
   private gtmService: GoogleTagManagerService) { }
 
 ngOnInit(): void {
   this.loginForm = new FormGroup({
     'login_email' : new FormControl(null,[Validators.required,Validators.email])
   });
 
 
 }
 submitLoginForm(){
   var request_data = {};
   request_data['email'] = this.loginForm.value.login_email;
   if(! this.is_customer)
   {
     this.allSubscription.push(this.checkoutService.check_customer(request_data).subscribe(response => {
       if(typeof(response['email']) !='undefined')
       {
         this.step = 2;
         this.loginForm = new FormGroup({
           'login_email' : new FormControl(response['email'],[Validators.required,Validators.email]),
           'login_password' : new FormControl(null, Validators.required)
         });
         this.is_customer = true;
       }else if(typeof(response['email']) =='undefined')
       {
         this.view = 2;
         this.signupForm = new FormGroup({
           'register_email' : new FormControl(request_data['email'],[Validators.required,Validators.email]),
           'register_name' : new FormControl(null, Validators.required),
           'register_password' : new FormControl(null, Validators.required),
           'register_telephone' : new FormControl(null, Validators.required),
           'gender' : new FormControl("Male", Validators.required)
         });
       }
     }));
   }else
   {
     document.getElementById('resultLoading').style.display = "block";
 
     request_data['password'] = this.loginForm.value.login_password;
     request_data['direct_login'] = 1;
     this.allSubscription.push(this.checkoutService.login(request_data).subscribe((customer_data:any[]) => {
       if(typeof customer_data['email'] !='undefined')
       {
         this.customer_data = customer_data;
         this.auth.saveToken(customer_data['token']);
         var gtmTag = {
           'event': 'user-login',
           'user_data': {
             'email': this.customer_data['email'],
             'login_medium': this.customer_data['login_medium'],
             'login_activity': this.customer_data['login_activity']
         }}
         console.log('gtm tag inn cs login in submit login form====');
         this.gtmService.pushTag(gtmTag);
         this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
         this.ProductService.header.next(true);
         this.ProductService.footer.next(true);
         //this.router.navigate(['/account']);
         location.reload();
       }else if(typeof customer_data['error'] !='undefined')
       {
         this.error = customer_data['error'];
         document.getElementById('resultLoading').style.display = "none";
       }
     }));
   }
 }
 
 submitSignupForm(){
   document.getElementById('resultLoading').style.display = "block";
 
   var request_data = {};
   request_data['register_email'] = this.signupForm.value.register_email;
   request_data['register_name'] = this.signupForm.value.register_name;
   request_data['register_password'] = this.signupForm.value.register_password;
   request_data['register_country_code'] = '91';
   request_data['register_telephone'] = this.signupForm.value.register_telephone;
   request_data['gender'] = this.signupForm.value.gender;
   request_data['email'] = this.signupForm.value.register_email;
   request_data['password'] = this.signupForm.value.register_password;
   request_data['direct_login'] = 1;
   this.allSubscription.push(this.checkoutService.addCustomerChk(request_data).subscribe(response => {
     this.checkoutService.login(request_data).subscribe((customer_data:any[]) => {
       if(typeof customer_data['email'] !='undefined')
       {
         this.customer_data = customer_data;
         this.auth.saveToken(customer_data['token']);
         var gtmTag = {
           'event': 'user-login',
           'user_data': {
             'fname': response['fname'],
             'telephone': response['telephoneCode'],
             'email': this.customer_data['email'],
             'login_medium': this.customer_data['login_medium'],
             'login_activity': 'signup'
         }}
         console.log('cs login gtm tag, add customer chk');
         this.gtmService.pushTag(gtmTag);
         this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
         this.ProductService.header.next(true);
         this.ProductService.footer.next(true);
         location.reload();
       }else if(typeof customer_data['error'] !='undefined')
       {
         this.error = customer_data['error'];
         document.getElementById('resultLoading').style.display = "none";
       }
     });
   }));
 }
 alreadyAUser(){
   this.step = 1;
   this.view = 1;
 }
 
 
 checkloginotp()
 {
   document.getElementById('resultLoading').style.display = "block";
 
   var request_data = {};
   request_data['email'] = this.loginForm.value.login_email;
     this.allSubscription.push(this.checkoutService.getotp(request_data).subscribe(responses => {
       this.otp = responses['otp'];
       this.telephone = responses['telephone'];
       document.getElementById('resultLoading').style.display = "none";
     }));
     this.view = 3;
     this.loginOTPForm = new FormGroup({
       'login_OTP_email' : new FormControl(request_data['email'],[Validators.required,Validators.email]),
       'login_otp_password' : new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
       'matchotp' : new FormControl(this.otp)
     });
 }
 submitLoginwithOTPForm()
 {
   document.getElementById('resultLoading').style.display = "block";
   var request_data = {};
   request_data['email'] = this.loginOTPForm.value.login_OTP_email;
   request_data['direct_login'] = 1;
   if(this.loginOTPForm.value.login_otp_password==this.otp)
   {
     this.allSubscription.push(this.checkoutService.loginotp(request_data).subscribe((customer_data:any[]) => {
       if(typeof customer_data['email'] !='undefined')
       {
         this.customer_data = customer_data;
         this.auth.saveToken(customer_data['token']);
         var gtmTag = {
           'event': 'user-login',
           'user_data': {
             'email': this.customer_data['email'],
             'login_medium': this.customer_data['login_medium'],
             'login_activity': this.customer_data['login_activity']
         }}
         console.log('gtm tag in login with otp');
         this.gtmService.pushTag(gtmTag);
         this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
         this.ProductService.header.next(true);
         this.ProductService.footer.next(true);
         location.reload();
       }else if(typeof customer_data['error'] !='undefined')
       {
         this.error = customer_data['error'];
         document.getElementById('resultLoading').style.display = "none";
 
       }
     }));
   }else
   {
     document.getElementById('resultLoading').style.display = "none";
 
     this.error = 'Please enter correct OTP';
     this.loginOTPForm = new FormGroup({
       'login_OTP_email' : new FormControl(request_data['email'],[Validators.required,Validators.email]),
       'login_otp_password' : new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
       'matchotp' : new FormControl(this.otp)
     });
   }
 }
 resendcheckloginotp()
 {
   document.getElementById('resultLoading').style.display = "block";
   this.otp = '';
   this.telephone = '';
   var request_data = {};
   request_data['email'] = this.loginForm.value.login_email;
     this.allSubscription.push(this.checkoutService.getotp(request_data).subscribe(responses => {
       document.getElementById('resultLoading').style.display = "none";
       this.otp = responses['otp'];
       this.telephone = responses['telephone'];
     }));
 }
 closemyModal_offer(){
 
   document.getElementById('loginsubmit').style.display = "none";
 }
ngOnDestroy(){
 _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe())
}
}
 

