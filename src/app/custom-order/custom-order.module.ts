import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CustomOrderComponent } from './custom-order.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ CustomOrderComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: CustomOrderComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class CustomOrderModule{

}