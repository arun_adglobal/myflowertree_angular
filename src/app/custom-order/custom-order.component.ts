import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { FileUploadService } from "../shared/file-upload.service";
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-custom-order',
  templateUrl: './custom-order.component.html',
  styleUrls: ['./custom-order.component.css']
})
export class CustomOrderComponent implements OnInit {

  preview: string;
  customOrderForm: FormGroup;
  percentDone: any = 0;
  users = [];
  CDN_PATH  = environment.CDN_PATH;

  submit = false;

  constructor(
    public fb: FormBuilder,
    public router: Router,
    public fileUploadService: FileUploadService
  ) {
    // Reactive Form
    this.customOrderForm = this.fb.group({
      description: [''],
      custom_amount: [''],
      avatar: [null]
    })
  }

  ngOnInit(): void {
  }

    // Image Preview
    uploadFile(event) {
      const file = (event.target as HTMLInputElement).files[0];
      this.customOrderForm.patchValue({
        avatar: file
      });
      this.customOrderForm.get('avatar').updateValueAndValidity()
  
      // File Preview
      const reader = new FileReader();
      reader.onload = () => {
        this.preview = reader.result as string;
      }
      reader.readAsDataURL(file)
    }
    submitCustomOrder() {
      this.submit = true;
      var request_data = {};
      request_data['description'] = this.customOrderForm.value.description;
      request_data['custom_amount'] = this.customOrderForm.value.custom_amount;
      request_data['avatar'] = this.customOrderForm.value.avatar;
      this.fileUploadService.custom_order(request_data).subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.percentDone = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.percentDone}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.percentDone = false;
            this.router.navigate(['cart'])
        }
      })
    }
}
