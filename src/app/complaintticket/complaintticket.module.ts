import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ComplaintticketComponent } from './complaintticket.component';
import { MainLayoutModule } from '../main-layout.module';

@NgModule({
declarations: [ ComplaintticketComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: ComplaintticketComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class ComplaintticketModule {

}