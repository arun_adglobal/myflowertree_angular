import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {observable,of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Router} from '@angular/router';
import { environment } from '../environments/environment';
import { isPlatformBrowser } from '@angular/common';
export interface UserDetails {
  id: number,
  firstname : string,
  lastname : string,
  email : string,
  telephone : string,
  customer_group_id : number,
  exp : number
}

@Injectable({providedIn: "root"})
export class AuthenticationService {  
  private token : string;
  APIURL  = environment.API_URL;

  constructor(private http: HttpClient, private router: Router, @Inject(PLATFORM_ID) private platformId: object) { }

  public saveToken(token : string) : void{
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('userToken', token);
    }
    this.token = token;
  }
  private getToken():string{
    if(!this.token)
    {
      if (isPlatformBrowser(this.platformId)) {
        if(localStorage.getItem('userToken') !=null)
        {
          this.token = localStorage.getItem('userToken');
        }
      }
    }
    return this.token;
  }

  public getUserDetails(): UserDetails{
    const token = this.getToken();
    let payload;
    if(token){
      payload = token.split('.')[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    }else{
      return null
    }
  }

  public isLoggedIn() : boolean {
    const user = this.getUserDetails()
    if(user){
      return user.exp > Date.now() / 1000
    }else{
      return false
    }
  }
  logout() {
    this.token = '';
   
    if (isPlatformBrowser(this.platformId)) {
      localStorage.removeItem('userToken');
    }
    return this.http.get(this.APIURL+'api/logout', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }

}
