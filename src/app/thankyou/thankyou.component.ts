import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ThankyouService } from './thankyou.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css'],
  providers:[ThankyouService]
})
export class ThankyouComponent implements OnInit {
  order_data:any={};
  occbaner:any={};
  orderproduct:any={};
  filter_total = {};
  CDN_PATH  = environment.CDN_PATH;
  cssDy:any={};
  reward_balance = 0;
  reward_used = 0;
  wallet_balance = 0;
  wallet_used = 0;
  thankyouesubscription:Subscription;

  safeUrl: SafeResourceUrl;
  constructor(private route:ActivatedRoute,
    private thankyouService:ThankyouService,
    private router : Router,
    public sanitizer: DomSanitizer,
    private gtmService: GoogleTagManagerService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {

    this.cssDy = { mftcss : this.getSafeCDNUrl('css/mft.css.gz')}

    if (isPlatformBrowser(this.platformId)) {
      if(localStorage.getItem('utm_source') !=null)
      {
        localStorage.removeItem('utm_source');
      }
      if(localStorage.getItem('utm_medium') !=null)
      {
        localStorage.removeItem('utm_medium');
      }
      if(localStorage.getItem('source') !=null)
      {
        localStorage.removeItem('source');
      }
      this.thankyouesubscription = this.thankyouService.getThankyoudata().subscribe(data =>{
        if(Object.keys(data).length === 0)
        {
          this.router.navigate(['/order']);
        }
        if(typeof(data['order_data']) !='undefined')
        {
          this.order_data = data['order_data'];
          this.occbaner = data['banner'];
          this.orderproduct = data['order_products'];
          this.filter_total = data['filter_total'];
          this.reward_balance = data['reward_balance'];
          this.reward_used = data['reward_used'];
          this.wallet_balance = data['wallet_balance'];
          this.wallet_used = data['wallet_used'];
          this.tags_thankyou();
        }
      });   
    }
 
  }
  tags_thankyou()
  {
    var products = [];
    for(let product of this.orderproduct)
    {
      var price  = product['price'] * product['quantity'];
      products.push({
        'id' : product['product_id'],
        'name' : product['name'],
        'variant' : product['product_type'],
        'category' : product['category_name'],
        'quantity' : product['quantity'],
        'images' : [product['product_image']],
        'price' : price,
        'brand': 'MyFlowerTree',
        'coupon' : product['couponcode']
      });
    }
    var gtmTag = {
      'event': 'gtm4wp.orderCompletedEEC',
      'payment_method': this.order_data['payment_method'],
      'payment_mode': this.order_data['payment_code'],
      'ecommerce': {
        'purchase': {
          'actionField': {
            'id': this.order_data['order_id'],                
            'affiliation': 'MyFlowerTree',
            'revenue': this.order_data['total'],  
            'tax':'',
            'shipping': '',
            'occasiontype': this.order_data['occasiontype'],  
            'coupon': products[0]['coupon'],
          },
        'products': products,
        'reward_balance': this.reward_balance,
        'reward_used': this.reward_used,
        'wallet_balance': this.wallet_balance,
        'wallet_used': this.wallet_used
        }
      }
    }
    this.gtmService.pushTag(gtmTag);
    
    var gtm = {
      'actionField':{
        'id': this.order_data['order_id'],                       
        'affiliation': 'MyFlowerTree',
        'revenue': this.order_data['total'],        
        'tax':'',
        'shipping': '',
        'occasiontype': this.order_data['occasiontype'],  
        'coupon': products[0]['coupon']
      },
      "event":"orderPlaced",
      "pageType":"success",
      'cartTotals': this.filter_total
    }
    this.gtmService.pushTag(gtm);
    this.gtmService.pushTag({
      'event':'OrderCompleted',
      'order_value': this.order_data['total'],
      'order_id':this.order_data['order_id'],
      'currency': 'INR',
      "email": this.order_data['email'], 
      "phone_number": this.order_data['shipping_telephone'], 
      "address": {
        "first_name": this.order_data['shipping_firstname'],
        "last_name": this.order_data['shipping_lastname'], 
        "street": this.order_data['shipping_address_1'], 
        "city": this.order_data['shipping_city'], 
        "region": this.order_data['shipping_zone'], 
        "postal_code": this.order_data['shipping_postcode'], 
        "country": this.order_data['shipping_country'] 
      }
    });

  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }

  ngOnDestroy():void {
    if(this.thankyouesubscription)
    {
      this.thankyouesubscription.unsubscribe();
    }
  }
}


