import { Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ThankyouService {
	APIURL  = environment.API_URL;
  constructor(private http: HttpClient) { }
  getThankyoudata() {
    
    return this.http.get(this.APIURL+'api/thankyou', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
}
