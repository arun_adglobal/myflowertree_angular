import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ThankyouComponent } from './thankyou.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ ThankyouComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: ThankyouComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class ThankyouModule{

}