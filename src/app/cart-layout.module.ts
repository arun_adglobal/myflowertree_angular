import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { CartHeaderComponent } from './cart-header/cart-header.component';
import { RouterModule } from '@angular/router';
@NgModule({
declarations: [  CartHeaderComponent],
exports: [  CartHeaderComponent],
imports: [ CommonModule, ReactiveFormsModule,RouterModule]
})
export class CartLayoutModule{

}