import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import{map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class SitemapService {
  APIURL  = environment.API_URL;
  constructor(private http:HttpClient) { }
  getCategories(catid){    
    
    return this.http.get(this.APIURL+'api/sitemap?cat='+catid, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  geturlAlias(catid){    
    
    return this.http.get(this.APIURL+'api/urlalias?cat='+catid, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getInformations(){    
    
    return this.http.get(this.APIURL+'api/information', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  
}
