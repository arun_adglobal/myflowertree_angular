import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SitemapService } from './sitemap.service';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.css'],
  providers:[SitemapService]
})
export class SitemapComponent implements OnInit {
  catid:number = 0;
  categories = [];
  information = [];
  subscriptions : Array<Subscription> = [];
  constructor(private route:ActivatedRoute, private SitemapService:SitemapService) { }

  ngOnInit(): void {
    this.subscriptions.push(
      this.SitemapService.getCategories(this.catid).subscribe((cat1:any[]) => {
      
        for(let category1 of cat1)
        {
          var level_2_data = [];
          this.subscriptions.push(
            this.SitemapService.getCategories(category1.category_id).subscribe((cat2:any[]) => {
              
              for(let category2 of cat2)
              {
                
                var level_3_data = [];
                this.subscriptions.push(
                  this.SitemapService.getCategories(category2.category_id).subscribe((cat3:any[]) => {
                    
                    for(let category3 of cat3)
                    {
                      var level_4_data = [];
                      this.SitemapService.getCategories(category3.category_id).subscribe((cat4:any[]) => {
                        
                        for(let category4 of cat4)
                        {
                          var level_5_data = [];
                          this.SitemapService.getCategories(category4.category_id).subscribe((cat5:any[]) => {
                            for(let category5 of cat5)
                            {
                              this.SitemapService.geturlAlias(category5.category_id).subscribe((cat6:any[]) => {
                                
                                level_5_data.push({
                                  'name': category5.name,
                                  'href' : cat6[0].keyword
                                });

                              });
                            }
                            this.SitemapService.geturlAlias(category4.category_id).subscribe((cat7:any[]) => {
                                
                              level_4_data.push({
                                'name': category4.name,
                                'href' : cat7[0].keyword,
                                'children' : level_5_data
                              });

                            });


                          });
                        }

                        this.SitemapService.geturlAlias(category3.category_id).subscribe((cat8:any[]) => {
                                
                          level_3_data.push({
                            'name': category3.name,
                            'href' : cat8[0].keyword,
                            'children' : level_4_data
                          });

                        });

                      });
                    }
                    this.SitemapService.geturlAlias(category2.category_id).subscribe((cat9:any[]) => {
                                
                      level_2_data.push({
                        'name': category2.name,
                        'href' : cat9[0].keyword,
                        'children' : level_3_data
                      });

                    });

                  })
                )
              }

              this.subscriptions.push(
                this.SitemapService.geturlAlias(category1.category_id).subscribe((cat10:any[]) => {         
                  this.categories.push({
                    'name': category1.name,
                    'href' : cat10[0].keyword,
                    'children' : level_2_data
                  });
                })
              );

            })
          )
        }

      })
    );

    this.subscriptions.push(
      this.SitemapService.getInformations().subscribe((information:any[]) => {
        this.information = information;
      })
    );
  }

  ngOnDestroy():void {
    _.forEach(this.subscriptions,(sub)=>sub.unsubscribe());
    this.subscriptions = null;
  }

}
