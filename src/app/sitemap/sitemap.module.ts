import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SitemapComponent } from './sitemap.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ SitemapComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: SitemapComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class SitemapModule{

}