import { Injectable,EventEmitter, Inject, PLATFORM_ID } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class MyprofileService {
APIURL  = environment.API_URL;
  constructor(private http: HttpClient,
    @Inject(PLATFORM_ID) private platformId: object) { }

  getCustomerDetail() {
    return this.http.get(this.APIURL+'api/customerdetail',{
      headers : { Authorization : `${this.getToken()}` },
	   withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  updateuser(customerdata) {

    return this.http.post(this.APIURL+'api/updateuser',customerdata, {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));

     
  }
  
  changepass(customerdata) {
    return this.http.post(this.APIURL+'api/changepass',customerdata, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  trackorder(customerdata) {
    return this.http.post(this.APIURL+'api/trackorder',customerdata, {
      withCredentials: true
    }).pipe(map(data => {
      //console.log(data);
      return data;
     }));
  }
  
  
  getAddresses() {
    return this.http.get(this.APIURL+'api/useraddress', {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  searchPincodes(pincode) {
    return this.http.get(this.APIURL+'api/searchPincodes?pincode='+pincode, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  addaccountAddress(request_data) {
    return this.http.post(this.APIURL+'api/address/addaccountAddress',request_data, {
      headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }


  
deleteAddress(address_id) {
  return this.http.delete(this.APIURL+'api/address/deleteaddress',{params: {address_id: address_id},withCredentials: true,headers : { Authorization : `${this.getToken()}` }}).pipe(map(data => {
    return data;
   }));
}
defaultAddress(address_id) {
  return this.http.get(this.APIURL+'api/address/defaultAddress?address_id='+address_id, {
    headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
//======================= Wallet =======================//
getWallet()
{
  return this.http.get(this.APIURL+'api/wallet', {
    headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
getRewards()
{
  return this.http.get(this.APIURL+'api/rewards', {
    headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
getRewardsTotalPoints()
{
  return this.http.get(this.APIURL+'api/rewardstotal', {
    headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
getOrders()
{
  return this.http.get(this.APIURL+'api/orderhistory', {
    headers : { Authorization : `${this.getToken()}` },
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
getOrder(orderid, suborderid)
{
  return this.http.get(this.APIURL+'api/orderdetail?orderid='+orderid+'&suborderid='+suborderid, {
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
getOrderInvoice(orderid)
{
  return this.http.get(this.APIURL+'api/orderinvoice?orderid='+orderid, {
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
getOrderProducts(orderid)
{
  return this.http.get(this.APIURL+'api/orderproducts?orderid='+orderid, {
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
getOrderVouchers(orderid)
{
  return this.http.get(this.APIURL+'api/ordervouchers?orderid='+orderid, {
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}
getOrderTotals(orderid)
{
  return this.http.get(this.APIURL+'api/ordertotal?orderid='+orderid, {
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
}


  private getToken():string{
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem('userToken');
    }
   }

}
