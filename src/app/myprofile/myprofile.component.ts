import { Component, OnInit, ViewEncapsulation,Inject, PLATFORM_ID } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MyprofileService } from './myprofile.service';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {
  editprofileForm: FormGroup;
  customer_data = {};
  successmsg = "";
  name_field:boolean = false;
  email_field:boolean = false;
  telephone_field:boolean = false;
  password_field:boolean = false;
  edit_div:boolean = false;

  constructor(private router:Router, private myprofileService:MyprofileService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {

   

    if (isPlatformBrowser(this.platformId)) {

    this.editprofileForm = new FormGroup({
      'firstname' : new FormControl(null,Validators.required),
      'lastname' : new FormControl(null),
      'email' : new FormControl(null, [Validators.required,Validators.email]),
      'telephone' : new FormControl(null, [Validators.required, Validators.minLength(8), Validators.maxLength(10)]),
      'password' : new FormControl(null)
    });

    this.myprofileService.getCustomerDetail().subscribe(customer_data => {
      this.customer_data = customer_data;
      this.editprofileForm.patchValue({
        'firstname' : customer_data[0]['firstname']+' '+customer_data[0]['lastname'],
        'email' : customer_data[0]['email'],
        'telephone' : customer_data[0]['telephone']
      });
    });
  }

    
  }
  profileEdit(field:string = ''){
    this.name_field = false;
    this.email_field = false;
    this.telephone_field = false;
    this.password_field = false;
    if(field=='name')
    {
      this.name_field = true;
      this.edit_div = true;
    }else if(field=='email')
    {
      this.email_field = true;
      this.edit_div = true;
    }else if(field=='telephone')
    {
      this.telephone_field = true;
      this.edit_div = true;
    }else if(field=='password')
    {
      this.password_field = true;
      this.edit_div = true;
    }
  }
  submitProfileForm()
  {
    var request_data = {};
    request_data['firstname'] = this.editprofileForm.value.firstname;
   // request_data['lastname'] = this.editprofileForm.value.lastname;
    request_data['email'] = this.editprofileForm.value.email;
    request_data['telephone'] = this.editprofileForm.value.telephone;
    request_data['password'] = this.editprofileForm.value.password;
    //request_data['fax'] = this.editprofileForm.value.fax;
    
    this.myprofileService.updateuser(request_data).subscribe(customer_data => {
        this.customer_data = customer_data;
        this.editprofileForm.patchValue({
          'firstname' : customer_data[0]['firstname']+' '+customer_data[0]['lastname'],
          'email' : customer_data[0]['email'],
          'telephone' : customer_data[0]['telephone']
        });
        this.successmsg = "Profile updated successfully";
        this.name_field = false;
        this.email_field = false;
        this.telephone_field = false;
        this.password_field = false;
        this.edit_div = false;
    });
  }

}
