import { Component, OnInit, ViewChild, ElementRef,Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { MyprofileService } from '../myprofile/myprofile.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../environments/environment';

declare var $:any;

@Component({
  selector: 'app-customer-invoice',
  templateUrl: './customer-invoice.component.html',
  styleUrls: ['./customer-invoice.component.css']
})
export class CustomerInvoiceComponent implements OnInit {
  CDN_PATH  = environment.CDN_PATH;
  CDN_DOMAIN = environment.CDN_DOMAIN;

  orders:any;
  products:any;
  vouchers:any;
  ordertotal:any;
  orderid:number = 0;
  queryParams = {};
  @ViewChild('htmlData') htmlData:ElementRef;
  
  constructor(private route:ActivatedRoute,
    private myprofileService:MyprofileService,@Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {

    this.route.params.subscribe(
      (params) =>{
        console.log(params);

        this.orderid = params['orderid']; 
        //console.log('params');
        this.myprofileService.getOrderInvoice(this.orderid).subscribe((order:any) => {
          this.orders = order;

          if (isPlatformBrowser(this.platformId)) {
            //$("#callbackloader").css('display','block'); 
            }
          
          setTimeout(()=>{
            this.openPDF();
          }, 500);

        });
             
      }
    );

  }
  public openPDF():void {
    let DATA = document.getElementById('htmlData');
      
    html2canvas(DATA).then(canvas => {
        
        let fileWidth = 208;
        let fileHeight = canvas.height * fileWidth / canvas.width;
        
        const FILEURI = canvas.toDataURL('image/png')
        let PDF = new jsPDF('p', 'mm', 'a4');
        let position = 0;
        PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight)
        var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAAyCAYAAACXpx/YAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MUUwOTJBRUQwMUFFMTFFQUI4QUZEQ0I1NTZBNzhGMjMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MUUwOTJBRUUwMUFFMTFFQUI4QUZEQ0I1NTZBNzhGMjMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxRTA5MkFFQjAxQUUxMUVBQjhBRkRDQjU1NkE3OEYyMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxRTA5MkFFQzAxQUUxMUVBQjhBRkRDQjU1NkE3OEYyMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgULZCIAABG/SURBVHja7FwJeFxVFT7vvZlJJsmkSUi6L3RDpNCKVSsta0GhbqAiIAp+AqKgRepS+RDRulvFCi6sIgqK4IYoioAVELBYC9IWKqVNaZPS0HRJmiYzmZn3rv957zxy8/pma6ZpC7nfd76Zect9957/nHPPOfe8MZRS5LcLFr6JytXSwxTV/c+iCfdUkF2pSJm0P1oCdAuIJ3Yf6LJydl6xw6Qtc9P00klp93u5WmUN0abVBm1c5X0vpS1fvrzf7wi9ehvP7W+gY+T3fNAzoJ/Sa6iZr+K5VWjg+u1qeo21VwvAh4OmBo51g34VODYedNgQwAdX+zhoDWgtaEng3CUgFTh23BDAB0+bBLpB+305aCXoCPm9C/SNwD1vHgL44AI42I4CPQt6q/wOAjx8COCDp/0D9Nsc5x4BTQGlQL8YAvjAbPWgCXnO26APgN4LeiBwLgZ6WL5fqR1vDOlnLOh0ed4QwIMcyz4t3vAFoNNAs8RjPoRzAnLdPaBTQceCntDuHwP6NGizEDcr8IyPgdZJH8+DJg4BPHhtO3nZqA9KguKvoGXiMbcIcfLiOtH0x0FzQN/T+viBfP5SPrPaOdbsmyRm5tYEOmMI4MFrDmhjjnNxMbfTyctSvQhaLOc+L+S340GPaULDbV6IA8Zt6RDAg9vWlXAtg3qXfP+eaDaJmW6T7/+Rz3sD9z4pgvBMsQ8zIH4m2wNjCOCBtMdKvP4s0EUasBwLnwxqkGMrJDnC6/vL5KUvR0pY9c9AX7xevy4UQsMD2I4e+E7Mgd54F+hrJd5zM+gOCZG+BLpWkh9bJUxi7X4Q9B65xo+fp4nzxqb/DaCZIhDvDYIb7TKoZ5RDO47KUmS3MQTwABp70StlrS2lXQj6MXnbhd8S7/tRcdh4T+3tct25IkCTcvTzxz2OKKh20qCt89KUGu5Q/GWz7NuhCtZBqddOouO8vbjnXPnsEWBZIzskOfIWOXe5eNeT8vQTNNuu9u6E5m6fnqXYzvKD65p/9JnGyG2s8caeBmKuhIy8rBwpx0aRt+lyUALMGryoxHsO1+bHWvgQqFdMNzfeVVpSRD9Lg9rLjlXH4TbZ8OOtTPkny8COmKhoyls8Lbaze1zC8f+JIH769aA68jZWPjVoJpql2kx7lK0qS5dfAW0Sc1tMupGdqnESZt0NulMY0ab1V6jxRsa2PeYG18uJKDLsPbeqytFswBZBZP66YxSZpkEb4NdXD+tnsv8ivsMKiQrYsYyGWZswgFnt3yHeI8ea7cIUTv2Nl892cX7+mGuQFnQl1aTIxkCj3QalE2oyvM6rZSBKtMuS7/z5c9Bskc5O6fvpQLe3kpd75vGNAA2Te07NMYyEfO6Qz07t+PsK8JlN+2fDzCdH5/F2k9qrsq655rlCoD8k3npe7KTPXXkttOmB3L0T7v1kRW3NcOhwR6zSM9dmxBVenv9YSe68TXjxUCGArwF9pkhB+5hcG2rmGNTuMQ61whGZcG+MzAzNguSfn6c/1s4F2u/qEIBJmPPrwDH2fu+XCfezdnnMd0WesWwVpvWEncwkFI14IupqMtdjReBwAegvA+SpRfDtymI1OQPBiUMUD4V/3/xf8BQjdmCue3toF5ywhwD2bmg1z/thgF6L852FAL5E+36VhBJPascWiBRep5m5ayXjFFyqKNZpUPusDFVtMWn4sqjTW9/vsttAvxEg2+SWy7WYc0cJVu1ZCXmeChyP5bh+VIi2/k7GsUZMenfO9BpskG0rGnt/zJ1jy2lpStep6/H9OGW4vHi7Zj12yjrOFmtzmMnPqcngRGo34raxihKNnmftgPudW41ssovaK8C5bNo9trunk7Z17SgcJm3UPLEu0L/F86yTY5AjNyF/nbbYs8k9Vkw6iROzCzB9BGtUNcIJp2Na9ob6VZEOdk6cvic+KGuJ32YEEgqlLm+s7f+i/nVYfk3iSRIP/0RMvK69G8ir8thcSgbLQQ+pRoeanoxSdatFG85MLeltUEtiHQab65USV3NbDTpzIGsya3Ik5jGHzXd1nXpFAPxlI4MVede20uJgH9SUdoyT8VsCmnNMwPbfJa77bfxgmGaCVN/gRNUOs7/HOSLwvIoC80zIsnA0aLJo+EZ59h/kmj8HAO6SzzvleeMEYN30/kwDl9fQMyTmZu1/EfQ/8jY6WrV7RkP8ToWJnp1qcmbGt5g3Tv155Y3NZ/VScqSiSHc/vupW5BAJb3hDhCtLbhdlYR/kEzJ2VpTtMrelfurV7uPdRJcPBsaoMA6i9aAW06S7Gkb3s7YFAVZaui6YviMN/OB+LZu2MZpTotjbhNRHBpCzZXCfC1ljuV0qAFwk5lBvyYCz5QOurxX+PZfJchN0OLldIYCwNs6XTFjM51BvoxOtajPdpahrYhYAG2Ez/YAsS3pMcYt8PpUjkXMx6BzQ+2XMLACPuFazz74d7U7IoQVYm9+nCXtZ4mBTYsvgMV3rnRz39ob4p7naEg3cVtHgOwJZK14uXsoR5/tAZ0Ke5Xu0Oric1pwSiDu/LN8/rmklO2MRs5d+lGxQ0F5IcTKnopytgZsUC3mj5Muna0tklSwlfjuD+ipBb5clkST7Vkv9d9u+X+5EhzWAPqwSrn2P9p1jv2bas4JjeC6vN4+Q+UytCQln1gfMsg/4iwELZkd3mwQ/g7oOtVl783nmunfve7znBHiSpL4dMX1uJIKtX9sVSMSMLDfAkTyeaqF2jUwyqWlmLkFQIZpeG4zMQuZjFgiXfA+6KSRJQuJg+i2rga+PzWLxyQxzvJSlKrjk+eOKaNYhOLdECJ+tHD5Lbz5BHijAivY+meMIKMkC/oATMPlWDi/bGoAlqcwBRqQoXrEzmTZc77oEP8PII3xBoKry8DkasDwHTC56vgy84RXPNL8wFCMwNABhC2O+E+INO/uBV5V7q0j7E2C7SICsHMuAlUeSizGTxYytcj/xJmjN9npLY38CXFEk850c4KsQpqgyzdcJAXgwNTcaEu4ZRSiKsS8ANkMYX+4SB1Wma2qKcLpyhW7OANf6Ulpwg3B3Hp7G8+U1zDIMJBnC6HJKuzGAcTo55ltoA3NdQCBIi7FjgwBsMKvcHKLV20KsTMfeZLIKrVXZEEDWDZLWFkqURHKYs3iBvjhzxOnLau3YTSWs9Xvb+HknS1JEz7TxJkiwIP8oufaN2rFbSwG4MYTR0cBv3pXhDedujRlfB71QJs31P4vZhMiEgJzIsbZlCwhG8F8AbqO+vW+1DwFmnj8UmNPpgdjcb+dSX1mSnwC6qhDAE0MYOTzwQF36J0hm51yNAZ8J6KGlPIjsEj1HUzOzKsSjNIrwwhOBc/GQJElYgmSNZJ6YaQ+D/lQmK0MFogG/SoOfzTtjvO/dkuNZrWK62VpyafHPimEqvwM0Xr5fL5+/J69agGuI+dURTtn9Xc75E+cict6p4cK2w8TEzXZHZVHK5M2GrFENqPNNNLiW12ng1GnH/Zz0xpAJH5mj71gAWF3YWkL4wDs+m/bRUhIU4EhgXT02xJKpkFCJd6G+W2q8dUGO9UhvvCd8Ssh17xLnZbWsD7N5WHYFXRvtMinSYzRkhql8z14jzB4nv88XU/+gSKdfUMa7OvwGw1u1e3lLr0f60Btfx+WyIzSAF2pM5FTphhBTzpsAi0S7zxGB/n2IxvESVWso6oiY/WyKFbIsGIEQkalX5jdDE17efOD3r7g6hHegLhEHjzW1Sxsr7zQ9JfM+Tfq/upQ1uNR2BQW3Dk36s9VLV9S+YPHTq9hUaykilwk1WYNi+NEVUam0RSebyjWJnNUaAXG4CdffHM8aF2dMNSNj0nFy/2LtKazJ747wWwYmrcHHFbjm23LdacqbPAGEbjyf/YTv+KESn4vb7pVdSUtdanh11IYwzL3P4SJ3h/6USBuUArd6IqoJY/TnwR+ZSvTBlY8xiLfZ4VZBjjHMV/R7mHxrClgUf9m4SoTQ/2uJHwSWox2aheMxceXJSLGk+np9S6gjU8b/yaoTE9korntLb616vHG9RUfcWQlNVuMci050uOBBkQ2mLQV3Xl5db9OWKodmbY1QXdqkrqiqsg31zqxJjRhZbVXWeKC90nm6NmMQgJ6bjKjDAFYtznejny3VWeNe/M7iPsKNZHlsm4Tn8N7pcFzj4PszAHcVb9KnImoUjvEW4f0VttGyrVK5KDX0AkBLTcQqMgtdjISwMG7tiYzxL8jgcysbsjSu26TRPeaJ+D3W8NbLDRCsFeuqHXp4TIYOmaxo4lQ4HVF6l2NTPVhrWBFaC8CX9fbQLMN4peplE/W9u+w3tipHCB8zcs09IRk/9iPmiRKwReK5cCG/u9l/6+L/7DOA+y9M/GIWdDD+nEVVf4hRus4BkIqgiRQBxxOYAmvEqvosdVQoOrzDoqmdFjWmTKrAvTUAtBZas6rBpvvGp2kMmHtCW5RYk9LoA4ynNBe9QTg21jjUWm27fYxImrQZDN8V9YAbjt+shetrbZq206LJuyzqjijXavBz7xuXIVbiea1RGotndOI+CB9BsCjO9U8xRUtHZ4kB5rGd9JJncZvRH18HmaSWhENJW5G53aDxMxTVQ7/aN3klNUx1WCCaJii3vipHIXvZ2qAAzF1yiScboWefNGhbh1fnC637ILRoOjjfg1M/xDw7GChmFDMyy8xIe79H91hUD61a0ZSlXlNNAQhxgL6qHuf5upFJV9tpQ8J2gY6JnLP5BNPr8IwF+FoJjXwUGn5fr0XHQ9sfHdnjmVr2D2BuCRbB9WAYUO67G+dwiCbAb2gCoMubMiyYR0DDL4IZ5/CKN/6TKYsWm269Cn2eLQcEZrHNpdIZuhLzz+Dzizhbh89rohXUduh0RaMPw7MwzlS3V1s1GADvk8J3CwyHaaLnl5m0E75tot41NJ8wsq7TwwVoXEPMr6P8EHQR+NMI8L7Nt2KdvRCmMr66PnszNK8HjJ+Ec3PA8xcB5KjNVc75wLcbWvojmMdhWEMXVEPBcP5BUBev85W26xBxuHG38mqdttdkqAZmt6m12jkaoLOHfwfit3Xom+PMmRCAxeh7N8BdiPF0Y1n4sbufiQ4aUsaF6LcTz8riHFeWpqDdceXxj6tJ2CBUQzMtmOfzAHoWoL4exx/AsYXZDC1c/5SR3QFXafIbYdVqvWpJ1mS1L6PqcgPMgzUBbuIQoLHKoLZmryJf3InHJIyaLWvLMvFQT5EQjIGogPaeGCVjLUBiRvIL2ucBaB5nHOc+HHUM9rTnigfMTp2jPMdpCvi12PAYdrt48hNx7JscfgCgs6Hdc6DFXPDGf3HIBXwr0DcLXLP0HZUwkWvKuHbxG26MYriCyc85HX3ME0NxsUz7J5IrcCsnAdql0NzZkhf4DXjySYBeHY9Q5/bNsB7dBk07zqEq8AXAk53mEtx9Z7bLCjCDG4V/2Py0Qa3PG8R1u9RX5cDm7augEyQ7M0pCnuWiba+XEIkL2rha/yvk1UmvFk9yriQAuA9+eYxrl7lC8nMSus3UnJBHRKCOl/Dqb5KnnSH3MSjzJaz4AnlvaXCI8oD0x+HeOKN/EoWtDtc785sJ1+aIhU3xlheJd8zXt+OsW3VR04C4COb5ucdNqoH4sEKMn6bcgvaQ948OPIB53X1prUFr/224FfnRSs/Zkna2APsOSYa8SbJg4wSUFmHOTAGYy3NvkBTomeKd+8XoMyV+5FdojpF48b9avHm7hBP85uBZkgRpk778cGWEPOcUsQYnSKw5UoSxRZsaW5JLxeKkZHfnqxLjPi+x9CKJbTsE6C7JI7f7SQrmRUWVV+e8dRN8CfRUi9GMhudt7z7AAWYTw1X3O9u8VyzYqVL9k4eLhJFXiev/Rdm248TDHNHGbVqS4AnR5h4JGTq1HDL/CcsqMaVvEO33/3qhR0BlC8DF7ndJEv4k0VR/x+UZycB9lLzKxTsllpwvwN2tjb1Wzs0RYbxMkiGmPMOWfDWJVfCTGAnxM2x9GYtEPUqyQ5hvp7ccuJTLi2aAufr+heUG8VpTUUX7ulVLcmWL5NBZa1fSQdTY0Rp/pHLNdKpMGpw3TBpqr772fwEGAAi3o7m5+JI1AAAAAElFTkSuQmCC';

        PDF.addImage(imgData, 'PNG', 10, 0, 20, 10);

        PDF.save('customer-invoice.pdf');
        if (isPlatformBrowser(this.platformId)) {
         // $("#callbackloader").css('display','none'); 
          $("#htmlData").css('display','none'); 
        }
        //this.router.navigate(['/']);
    });     
  }

}
