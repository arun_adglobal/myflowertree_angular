import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CustomerInvoiceComponent } from './customer-invoice.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ CustomerInvoiceComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: CustomerInvoiceComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class CustomerInvoiceModule{

}