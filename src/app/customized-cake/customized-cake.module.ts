import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CustomizedCakeComponent } from './customized-cake.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ CustomizedCakeComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: CustomizedCakeComponent }
]), CommonModule, ReactiveFormsModule, MainLayoutModule ]
})
export class CustomizedCakeModule{

}