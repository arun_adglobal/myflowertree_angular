import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from "@angular/forms";
import { FileUploadService } from "../shared/file-upload.service";
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-customized-cake',
  templateUrl: './customized-cake.component.html',
  styleUrls: ['./customized-cake.component.css']
})
export class CustomizedCakeComponent implements OnInit {

  preview: string;
  customOrderForm: FormGroup;
  percentDone: any = 0;
  users = [];
  selectedOption = '1kg';
  submit = false;
  CDN_PATH  = environment.CDN_PATH;

  constructor(
    public fb: FormBuilder,
    public router: Router,
    public fileUploadService: FileUploadService
  ) {
    // Reactive Form
    this.customOrderForm = this.fb.group({
      email : [null, Validators.required],
      name : [null, Validators.required],
      mobile : [null, Validators.required],
      city : [null, Validators.required],
      flavour : [null, Validators.required],
      quantity_new : [null, Validators.required],
      date : [null, Validators.required],
      other_box_value : [null, Validators.required],
      cake_image : [null, Validators.required],
      avatar: [null]
    })
  }

  ngOnInit(): void {
  }
  cakeQuantity(){
    this.selectedOption = this.customOrderForm.value.quantity_new
  }

    // Image Preview
    uploadFile(event) {
      const file = (event.target as HTMLInputElement).files[0];
      this.customOrderForm.patchValue({
        avatar: file
      });
      this.customOrderForm.get('avatar').updateValueAndValidity()
  
      // File Preview
      const reader = new FileReader();
      reader.onload = () => {
        this.preview = reader.result as string;
      }
      reader.readAsDataURL(file)
    }
    submitCustomOrder() {
      this.submit = true;
      var request_data = {};
      request_data['quantity'] = this.customOrderForm.value.quantity_new;
      if(this.customOrderForm.value.quantity_new=='others')
      {
        request_data['quantity'] = this.customOrderForm.value.other_box_value;
      }

      request_data['email'] = this.customOrderForm.value.email;
    request_data['name'] = this.customOrderForm.value.name;
    request_data['city'] = this.customOrderForm.value.city;
    request_data['flavour'] = this.customOrderForm.value.flavour;
    request_data['mobile'] = this.customOrderForm.value.mobile;    
    request_data['date'] = this.customOrderForm.value.date;
      request_data['avatar'] = this.customOrderForm.value.avatar;
      this.fileUploadService.customizedCake(request_data).subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.percentDone = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.percentDone}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.percentDone = false;
            this.router.navigate(['thankyou']);
        }
      })
    }
}
