import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute,Router} from '@angular/router';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { FileUploadService } from "../shared/file-upload.service";
import { FeedbackService } from '../feedback/feedback.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  preview: string;
  feedbackForm: FormGroup;
  queryParams = {};
  orderno = '';
  type = '';
  utype = '';
  message = '';
  percentDone: any = 0;
  constructor(private route:ActivatedRoute,public router: Router, private FeedbackService:FeedbackService,public fileUploadService: FileUploadService) {
   
  }

  ngOnInit(): void {
    combineLatest(this.route.params, this.route.queryParams).subscribe(
      ([params, qparams]) => {
          //console.log(qparams);

          if(qparams['orderno'].indexOf("delivered") != -1)
          {
            let orderInfo = qparams['orderno'].split("-");
            this.orderno  =   orderInfo[0];
            this.type     =   orderInfo[1];
            this.utype    =   orderInfo[2];
          }else{
            this.orderno  =   qparams['orderno'];
            this.type     =   qparams['type'];
            this.utype     =   qparams['utype'];
          }
          
      });



    this.feedbackForm = new FormGroup({
      'star' : new FormControl(null,Validators.required),
      'review' : new FormControl(null, Validators.required),
      'avatar' : new FormControl(null)
    });
  }
  // Image Preview
  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.feedbackForm.patchValue({
      avatar: file
    });
    this.feedbackForm.get('avatar').updateValueAndValidity()

    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.preview = reader.result as string;
    }
    reader.readAsDataURL(file)
  }
  valid_from = true;
  submitFeedbackForm()
  {
    var request_data = {};
    request_data['star'] = this.feedbackForm.value.star;
    request_data['review'] = this.feedbackForm.value.review;
    request_data['orderID'] = this.orderno;
    request_data['type'] = this.type;
    request_data['utype'] = this.utype;
    request_data['avatar'] = this.feedbackForm.value.avatar;
    //request_data['city'] = 0;
    //console.log(request_data);

    if(! this.feedbackForm.valid)
      {
        this.valid_from = false;
        this.feedbackForm.markAllAsTouched();
      }else{
        this.valid_from = true;
      }
    if(this.valid_from)
    {

          this.FeedbackService.usersFeedback(request_data).subscribe((event: HttpEvent<any>) => {
            switch (event.type) {
              case HttpEventType.UploadProgress:
                this.percentDone = Math.round(event.loaded / event.total * 100);
                this.message ='Successfully';
               // console.log(`Uploaded! ${this.percentDone}%`);
                setTimeout(()=>{
                  location.reload();
                },3000);
                
              }
          });
    }

  }

}
