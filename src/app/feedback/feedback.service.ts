import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders,HttpErrorResponse} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
APIURL  = environment.API_URL;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient) { }
  usersFeedback(request_data) {
    var formData: any = new FormData();
    formData.append("avatar", request_data['avatar']);
    formData.append("star", request_data['star']);
    formData.append("review", request_data['review']);
    formData.append("orderID", request_data['orderID']);
    formData.append("type", request_data['type']);
    formData.append("utype", request_data['utype']);
    
    return this.http.post(this.APIURL+'api/usersFeedback',formData, {
      reportProgress: true,
	  withCredentials: true,
      observe: 'events'
    }).pipe(map(data => {
    return data;
   }));
    
  }
usersOrderticket(request_data) {
  
  return this.http.post(this.APIURL+'api/usersOrderticket',request_data, {
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
    
  }
usersCompticket(request_data) {
  
  return this.http.post(this.APIURL+'api/usersCompticket',request_data, {
      withCredentials: true
    }).pipe(map(data => {
    return data;
   }));
    
  }
  usersComplaint(request_data) {
  
    return this.http.post(this.APIURL+'api/usersComplaint',request_data, {
        withCredentials: true
      }).pipe(map(data => {
      return data;
     }));
      
    }

  checkComplaints(orderid) {
    return this.http.get(this.APIURL+'api/checkComplaints?orderid='+orderid, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  
  orderTickets(orderid) {
    return this.http.get(this.APIURL+'api/orderTickets?orderid='+orderid, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  CompTickets(orderid) {
    return this.http.get(this.APIURL+'api/CompTickets?orderid='+orderid, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }

  
}
