import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { FeedbackComponent } from './feedback.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ FeedbackComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: FeedbackComponent }
]), CommonModule, ReactiveFormsModule, MainLayoutModule ]
})
export class FeedbackModule {

}