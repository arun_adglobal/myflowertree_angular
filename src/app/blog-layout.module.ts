import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { BlogHeaderComponent } from './blog-header/blog-header.component';
import { RouterModule } from '@angular/router';
@NgModule({
declarations: [  BlogHeaderComponent],
exports: [  BlogHeaderComponent],
imports: [ CommonModule, ReactiveFormsModule,RouterModule]
})
export class BlogLayoutModule{

}