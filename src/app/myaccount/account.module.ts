import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthGuardService } from '../auth-guard.service';

import { MyaccountComponent } from './myaccount.component';
import { MyaccountaddressComponent } from '../myaccountaddress/myaccountaddress.component';
import { MyprofileComponent } from '../myprofile/myprofile.component';
import { WalletComponent } from '../wallet/wallet.component';
import { RewardComponent } from '../reward/reward.component';
import { ChangepasswordComponent } from '../changepassword/changepassword.component';
import { MainLayoutModule } from '../main-layout.module';

@NgModule({
declarations: [ MyaccountComponent,MyaccountaddressComponent,MyprofileComponent,
    WalletComponent,RewardComponent,ChangepasswordComponent],
imports: [ RouterModule.forChild([
    { path: '', component: MyaccountComponent, canActivate:[AuthGuardService]},
    { path: 'address', component: MyaccountaddressComponent,canActivate:[AuthGuardService] },
    { path: 'edit', component: MyprofileComponent,canActivate:[AuthGuardService] },
    { path: 'wallet', component: WalletComponent,canActivate:[AuthGuardService] },
    { path: 'reward', component: RewardComponent,canActivate:[AuthGuardService] },
    { path: 'changepassword', component: ChangepasswordComponent,canActivate:[AuthGuardService] },

]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class AccountModule{

}