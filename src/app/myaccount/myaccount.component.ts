import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { CheckoutService } from '../checkout/checkout.service';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css']
})
export class MyaccountComponent implements OnInit {

  customer_data = {};
  private allSubscriptions:Array<Subscription> = [];
  constructor(private checkoutService: CheckoutService,
    private gtmService: GoogleTagManagerService,
     @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
    this.allSubscriptions.push(this.checkoutService.getCustomer().subscribe((customer_data:any[]) => {
      if(typeof customer_data['email'] !='undefined')
      {
        this.customer_data = customer_data;
        this.checkoutService.loggedIn.next(true);
        var gtmTag = {
          'event': 'user-login',
          'user_data': {
            'reward_points': this.customer_data['user_points'],
            'wallet': this.customer_data['user_cash'],
            'we_first_name': this.customer_data['we_first_name'],
            'we_last_name': this.customer_data['we_last_name'],
            'we_email': this.customer_data['we_email'],
            'we_phone': this.customer_data['we_phone'],
            'fname': this.customer_data['fname'],
            'telephone': this.customer_data['telephone'],
            'email': this.customer_data['email'],
            'login_medium': this.customer_data['login_medium'],
            'login_activity': this.customer_data['login_activity']
        }}
        this.gtmService.pushTag(gtmTag);
      }
    }));
  }

  }
  ngOnDestroy(){
   _.forEach(this.allSubscriptions,(sub:Subscription)=>sub.unsubscribe());
  }
}
