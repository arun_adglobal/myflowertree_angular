import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { PaypageComponent } from './paypage.component'; 
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ PaypageComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: PaypageComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class PaypageModule{

}