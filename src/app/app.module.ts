import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {TransferHttpCacheModule} from '@nguniversal/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationService } from './authentication.service';
import { AuthGuardService, AuthGuardService1 } from './auth-guard.service';
import { SocialLoginModule } from 'angularx-social-login';
import { MainLayoutModule } from './main-layout.module';
import { NgxSpinnerModule } from "ngx-spinner";

import {
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';
import { CookieService } from 'ngx-cookie-service';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { CartLayoutModule } from './cart-layout.module';
const config = new AuthServiceConfig([
  {
    //340661611105-vugskcd6psdd4jsv1j3uqeknpk1v0s4q.apps.googleusercontent.com 
    //230925890562-igljuufsa6aapnrv12a22h6bsph6kf65.apps.googleusercontent.com .in
    //234881798543-9vqjfirj2m6qdvu2qcvi9vbu03t7nj7c.apps.googleusercontent.com .com
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(
      '230925890562-igljuufsa6aapnrv12a22h6bsph6kf65.apps.googleusercontent.com'
    ),
  },{
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('567972374330237')
  }
]);
export function provideConfig() {
  return config;
}
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [   
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    SocialLoginModule,
    MainLayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxJsonLdModule,
    NgxUsefulSwiperModule,
    TransferHttpCacheModule,
    NgxSpinnerModule,
    CartLayoutModule
    ],
  providers: [AuthenticationService,AuthGuardService,CookieService,AuthGuardService1,
    {
    provide: AuthServiceConfig,
    useFactory: provideConfig
  },
  { provide: 'googleTagManagerId', useValue: 'GTM-TSLN5TT' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
