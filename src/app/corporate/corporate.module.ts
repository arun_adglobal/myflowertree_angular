import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CorporateComponent } from './corporate.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ CorporateComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: CorporateComponent }
]), CommonModule, ReactiveFormsModule, MainLayoutModule ]
})
export class CorporateModule{

}