import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HomeService } from '../home/home.service';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
@Component({
  selector: 'app-corporate',
  templateUrl: './corporate.component.html',
  styleUrls: ['./corporate.component.css']
})
export class CorporateComponent implements OnInit {
  corporateForm: FormGroup;
  constructor(private router:Router, private homeService: HomeService,public sanitizer: DomSanitizer) { }
  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  cssDy:any={};
  allSubscrption:Array<Subscription> = [];
  ngOnInit(): void {
    this.cssDy = { styleCss : this.getSafeCDNUrl('2019/new_home_page/stylesheets/style.css.gz')};
    this.corporateForm = new FormGroup({
      'email' : new FormControl(null,[Validators.required,Validators.email]),
      'name' : new FormControl(null, Validators.required),
      'mobile' : new FormControl(null, Validators.required),
      'company' : new FormControl(null, Validators.required)
    });
  }
  submitCorporateForm(){
    var request_data = {};
    request_data['email'] = this.corporateForm.value.email;
    request_data['name'] = this.corporateForm.value.name;
    request_data['company'] = this.corporateForm.value.company;
    request_data['mobile'] = this.corporateForm.value.mobile;
    this.allSubscrption.push(this.homeService.corporate(request_data).subscribe((response_data:any[]) => {
      this.router.navigate(['thankyou']);
    }));

  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  ngOnDestroy(){
    _.forEach(this.allSubscrption,(sub:Subscription)=>sub.unsubscribe());
  }
}
