import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { PayextraComponent } from './payextra.component'; 
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ PayextraComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: PayextraComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class PayextraModule{

}