import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { FormGroup,FormControl,Validators } from "@angular/forms";
import { ActivatedRoute} from '@angular/router';
import { combineLatest } from 'rxjs';
import { CheckoutService } from '../checkout/checkout.service';
import { ProductService } from '../miscellaneous/product/product.service';
import { isPlatformBrowser } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-payextra',
  templateUrl: './payextra.component.html',
  styleUrls: ['./payextra.component.css']
})
export class PayextraComponent implements OnInit {
  CDN_PATH  = environment.CDN_PATH;
  safeUrl: SafeResourceUrl;
  paypageForm: FormGroup;
  orderid = '';
  email = '';
  mode = '';
  amount = 0;
  extraa = 0;
  order_data:any;
  cssDy:any={};
  total = 0;
  default_pay = 'ebs';
  constructor(public sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private checkoutService: CheckoutService,
    private productService: ProductService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.productService.header.next(false);
     this.productService.footer.next(false);
    this.paypageForm = new FormGroup({
      'payment_method' : new FormControl(null,Validators.required),
    });
    this.cssDy = { cartCss : this.getSafeCDNUrl('2019/newcheckout/new/cart.css.gz')};
    combineLatest([this.route.params,this.route.queryParams]).subscribe(
      ([params,qparams]) =>{
        this.orderid = qparams['order_id'];
        this.email = qparams['email'];
        this.mode = qparams['mode'];
        this.amount = qparams['amount'];
        this.extraa = qparams['extraa'];        
        this.checkoutService.paypage(this.orderid).subscribe((order_data:any) => {          
           this.order_data = order_data;
           this.total = parseInt(order_data['total']);
        });
    });
  }
  submitPaypage() {
    var request = {};
    request['order_id'] = this.orderid;
    request['payment_method'] = this.paypageForm.value.payment_method;
    this.checkoutService.extraPay(request).subscribe((data:any) => {
      if (isPlatformBrowser(this.platformId)) {
        $('#payment_gateway_code').hide();
        $("#payment_gateway_code").html(data['order_data'][0]['payment_form']); 
        $('#payment_gateway_code input[type="submit"]').trigger('click');
      }
   });
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
}
