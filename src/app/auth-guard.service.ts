import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {AuthenticationService } from './authentication.service';
import * as redirection from '../../redirection';
import { MinLengthValidator } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private auth: AuthenticationService, private router: Router) { }
  canActivate (){
    // console.log('redirection url====',redirection);
    if(!this.auth.isLoggedIn()){
      // console.log('redirection url====',redirection);
      this.router.navigateByUrl('/')
      return false;
    }
    return true;
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService1 implements CanActivate {

  constructor(private auth: AuthenticationService, private router: Router) { }
  canActivate ( route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    const keys = Object.keys(redirection);
    const isExist = keys.find(item => state.url === item);
    let url =state.url;
    if(isExist){
      // url= url.replace(isExist,redirection[url]);
      this.router.navigateByUrl(redirection[url]);
      return false;
    }
    return true; 
  }
}
