import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PrivacyPolicyComponent } from './privacy-policy.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ PrivacyPolicyComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: PrivacyPolicyComponent }
]), CommonModule,MainLayoutModule ]
})
export class PrivacyPolicyModule{

}