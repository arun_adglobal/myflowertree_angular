import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PrivacyPolicyService } from './privacy-policy.service';
import { isPlatformBrowser } from '@angular/common';
import { SEOService } from '../seo.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css'],
  providers:[PrivacyPolicyService]
})
export class PrivacyPolicyComponent implements OnInit {
  termsdata :any = [];
  subscription:Subscription;
  constructor(private route:ActivatedRoute,
    private seoservice: SEOService,
    private PrivacyPolicyService:PrivacyPolicyService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.SEO();
    this.subscription = this.PrivacyPolicyService.getPageData().subscribe(termsdata =>{
      this.termsdata = termsdata;
  });
  }
  SEO()
  {
    this.seoservice.setTitle("Privacy Policy");
}
  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }

  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }

}
