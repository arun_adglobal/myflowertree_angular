import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  APIURL  = environment.API_URL;
  constructor(private http: HttpClient) {}
  // getMenu() {
  //   return this.http.get(this.APIURL+'api/menu/', {
  //     withCredentials: true
  //   }).pipe(map(data => {
  //     return data;
  //    }));
  // }
  cartTotalCount() {
    return this.http.get(this.APIURL+'api/cartCount/', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getPopupCity(request) {
    var api = this.APIURL+'api/deliveryPopcity?';
    if(request['city_name'] !="")
    {
      api += 'city_name='+request['city_name']; 
    }
    return this.http.get(api, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }

  userCouponRegister(request_data) {
    return this.http.post(this.APIURL+'api/userCouponRegister',request_data, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
      }));
  }
  callbackModal(request_data) {
    return this.http.post(this.APIURL+'api/callbackModal',request_data, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
      }));
  }
  newsletterform(request_data) {
    if(typeof(request_data.newsletter_email) != 'undefined')
    {
      return this.http.post(this.APIURL+'api/newsletterform',request_data, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
    }
  }
}
