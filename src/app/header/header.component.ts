import { Component, OnInit, OnDestroy,Injectable, Inject, PLATFORM_ID, SecurityContext, ViewEncapsulation, HostListener  } from '@angular/core';
import { HeaderService } from './header.service';
import { AuthenticationService } from '../authentication.service';
import { ActivatedRoute,Params,Router,NavigationEnd, NavigationStart } from '@angular/router';
import { ProductService } from '../miscellaneous/product/product.service';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
/* 
Why we are importing 'any' type as typescript already has a datatype 'any'
*/
// import { any } from 'sequelize/types/lib/operators';
import { isPlatformBrowser } from '@angular/common';
import { DeviceDetectorService, DeviceInfo } from 'ngx-device-detector';
import { ok } from 'node:assert';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit,OnDestroy {
  safeUrl: SafeResourceUrl;
  menu: any = {};
  cssDy: any = {};
  tmp_input = '';
  cartCount = 0;
  mobileaccount='';
  angmobile:any='';
  angdesktop:any='';
  deviceType='Desktop';
  CDN_PATH  = environment.CDN_PATH;
  CDN_DOMAIN  = environment.CDN_DOMAIN;
  APIURL  = environment.API_URL;
  header = 'one';
  href: string = "";
  deliverylocation = '';
  searchdesktop=false;
  currentRoute: string;
  deviceInfo:DeviceInfo;
  filter_city_name = '';
  allSubscription:Array<Subscription> = [];
  constructor(public auth: AuthenticationService,
    private headerService:HeaderService,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private deviceDetectorService: DeviceDetectorService,
    private router: Router,private ProductService: ProductService,
     @Inject(PLATFORM_ID) private platformId: object) {}

  ngOnInit() {


    if (isPlatformBrowser(this.platformId)) {

      if(localStorage.getItem('filter_city') !=null)
      {
        this.filter_city_name = localStorage.getItem('filter_city_name');
      }

    }

    this.cssDy = { bootstrapMin : this.getSafeCDNUrl('live-new/css/bootstrap.min.css.gz'), jquery_ui : this.getSafeCDNUrl('live-new/css/jquery-ui.css.gz'),nav_css : this.getSafeCDNUrl('live-new/css/nav.css.gz'),custom_css : this.getSafeCDNUrl('live-new/css/custom.css.gz')};
    // this.headerService.getMenu().subscribe(menu => {
    //      this.menu = menu;
    //      this.angmobile = this.getStyCss(this.menu.ang_mobile_menu);
    //      this.angdesktop = this.getStyCss(this.menu.ang_desktop_menu);
    // });
        // this.header = 'one';
        // this.headerService.getMenu().subscribe(menu => {
        //   this.menu = menu;

        //   this.angmobile = this.getStyCss(this.menu.ang_mobile_menu);
        //   this.angdesktop = this.getStyCss(this.menu.ang_desktop_menu);
        //   //console.log(this.angmobile);
        //   var currenturl = window.location.href;
        //   if(currenturl==this.APIURL)
        //   {
        //     var currUrl = 'home';
        //   }else
        //   {
        //     var currUrl = currenturl.replace(this.APIURL,'');
        //   }
        //   var str = 'recently-viewed,orders/info,account,order,buy-again,account/edit,account/address,account/wallet,account/reward';
        //   var n = str.includes(currUrl);
        //   this.deliverylocation = '';
        //   if(n==false)
        //   {
        //     this.deliverylocation = 'yes';
        //   }else{
        //     this.deliverylocation = 'no';
        //   }
        // });

        this.header = 'one';
        var currenturl = window.location.href;
        if(currenturl==this.APIURL)
        {
          var currUrl = 'home';
        }else
        {
          var currUrl = currenturl.replace(this.APIURL,'');
        }
        if (isPlatformBrowser(this.platformId)) {


          // if(window.location.href.includes('/cart') || window.location.href.includes('/checkout') || window.location.href.includes('/login')) {
          //   this.isCartHeader = true;
          //   } else {
          //   this.isCartHeader = false;
          // }
              this.deliverylocation = 'yes';
              if( window.location.href.includes('/news-room') || window.location.href.includes('/contact-us') || window.location.href.includes('/track-order') || window.location.href.includes('/recently-viewed') || window.location.href.includes('/orders/info') || window.location.href.includes('/account') || window.location.href.includes('/order') || window.location.href.includes('/buy-again') || window.location.href.includes('/account/edit') || window.location.href.includes('/account/address') || window.location.href.includes('/account/address') || window.location.href.includes('/account/wallet') || window.location.href.includes('/account/reward'))
              {
                this.deliverylocation = 'no';
              }else{
                this.deliverylocation = 'yes';
              }




          this.allSubscription.push(this.router.events.subscribe((event:NavigationEnd) => {
            if(event instanceof NavigationStart){

              //var str = 'recently-viewed,orders/info,account,order,buy-again,account/edit,account/address,account/wallet,account/reward';
             // console.log(event.url);
             // var n = str.includes('/'+event.url);
              //console.log(n);
              this.deliverylocation = 'yes';
              if(event.url.includes('/news-room') || event.url.includes('/contact-us') ||  event.url.includes('/track-order') || event.url.includes('/recently-viewed') || event.url.includes('/orders/info') || event.url.includes('/account') || event.url.includes('/order') || event.url.includes('/buy-again') || event.url.includes('/account/edit') || event.url.includes('/account/address') || event.url.includes('/account/address') || event.url.includes('/account/wallet') || event.url.includes('/account/reward'))
              {
                this.deliverylocation = 'no';
              }else{
                this.deliverylocation = 'yes';
              }
            }
          }));
          this.searchdesktop = true;
          this.deviceInfo = this.deviceDetectorService.getDeviceInfo();
        }
      this.deviceType = this.getDevice();

     // this.deviceType = this.deviceInfo['deviceType'];
      if (isPlatformBrowser(this.platformId)) {
        this.allSubscription.push(this.headerService.cartTotalCount().subscribe((cartCount:any) => {
          this.cartCount = cartCount['total'];
        }));
      }


    this.allSubscription.push(this.route.queryParams.subscribe((params: Params)=>
    {

      if(typeof(params['utm_source']) !='undefined')
      {
        if (isPlatformBrowser(this.platformId)) {
          localStorage.setItem('utm_source', params['utm_source']);
        }
      }
      if(typeof(params['utm_medium']) !='undefined')
      {
        if (isPlatformBrowser(this.platformId)) {
          localStorage.setItem('utm_medium', params['utm_medium']);
        }
      }
      if(typeof(params['utm_source']) !='undefined' && typeof(params['utm_medium']) !='undefined')
      {
        var source:any = JSON.stringify(params);
        if (isPlatformBrowser(this.platformId)) {
          localStorage.setItem('source', source);
        }
      }



    }));


    this.allSubscription.push(
      this.ProductService.headerCartCount.subscribe((cartCount:any) =>
        {
          console.log('cartCount', cartCount);
          this.cartCount  = cartCount['headerCountTotal'];
        }
      )
      
    );

  }
  getDevice() {
    var device = '';
    var ua = window.navigator.userAgent.toLowerCase();
    if (ua.match(/ipad/i) !== null || ua.match(/iphone/i) !== null || ua.match(/android/i) !== null)
    {
      device = 'Mobile';
    }else
    {
      device = 'Desktop';
    }
    return device;
   }
  ngOnDestroy():void {
  _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
  loadSearchTap()
  {
    this.router.navigate(['/st-search']);
  }
  deliverypopup(){
    if (isPlatformBrowser(this.platformId)) {
      $(document).ready(function(){
        var element = document.getElementById('myModal_delivery');
        if (typeof element !== "undefined" && typeof element !== null) {
          document.getElementById('myModal_delivery').style.display = "block";
        }
      });
    }


  }
  logOut(){
    this.allSubscription.push(this.auth.logout().subscribe((data) => {
      if (isPlatformBrowser(this.platformId)) {
        if(localStorage.getItem('userToken') !=null)
        {
          localStorage.removeItem('userToken');
        }
      }
      window.location.reload();
      //this.router.navigateByUrl('/');
    }));
  }

  firststep()
  {
    //$('.wsmenucontainer').addClass('wsoffcanvasopener');
    var element = document.getElementById("wsmenuss");
    element.classList.toggle("wsoffcanvasopener");
  }
  coseMe()
  {
    this.firststep();
  }
  secondstep(menuid,openTabid)
  {
    if (isPlatformBrowser(this.platformId)) {
    $('.wsmenu-list').removeClass('ws-activearrow"');
    //$('.wsmenu-submenu').css('display','none');
    }

    var element = document.getElementById(menuid);
    element.classList.toggle("ws-activearrow");
    if (isPlatformBrowser(this.platformId)) {
    $('#'+openTabid).toggle();
    }
  }
  thirdstep(subsubmenuid,arrowid)
  {
   // $('.wsmenu-submenu-sub').css('display','none');

    var element = document.getElementById(arrowid);
    element.classList.toggle("wsmenu-rotate");
    if (isPlatformBrowser(this.platformId)) {
      $('#'+subsubmenuid).toggle();
    }
  }

  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
  opencallbackmodal(){

    if (isPlatformBrowser(this.platformId)) {
      $(document).ready(function(){
        var element = document.getElementById('request-call-back-popup');
        if (typeof element !== "undefined" && typeof element !== null) {
          document.getElementById('request-call-back-popup').style.display = "block";
        }
      });
    }

  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  getStyCss(menudyn)
  {
    return this.sanitizer.bypassSecurityTrustHtml(menudyn);
  }

  @HostListener('click', ['$event'])
  onClick(e) {
    if (e.target.classList.contains("doc-type")) {
      const urlSegments = e.target.getAttribute('routerLink');
      this.router.navigateByUrl(urlSegments);
    };
  }

  onFocus() {
    console.log('on focus====')
    document.getElementById('searchModal').removeAttribute('style')
  }
}