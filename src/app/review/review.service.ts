import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
APIURL  = environment.API_URL;
  constructor(private http: HttpClient) {}
  getReviews() {
    return this.http.get(this.APIURL+'api/review/', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getAllReviews(page=1, queryString = '') {
    var url = '?page='+page;
    if(queryString !='')
    {
      url += '&query='+queryString; 
    }
    return this.http.get(this.APIURL+'api/reviews'+url, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getReviewsByProductId(productid) {
    return this.http.get(this.APIURL+'api/reviewall?product_id='+productid, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }

}