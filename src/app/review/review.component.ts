import { Component, OnInit } from '@angular/core';
import{ ReviewService } from './review.service';
import { environment } from '../../environments/environment';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css'],
  providers:[ReviewService]
})
export class ReviewComponent implements OnInit {
  review :any=[];
  CDN_PATH  = environment.CDN_PATH;
  reviewcount :any=['1','2','3','4','5'];
  subscription:Subscription;
  constructor(private reviewService:ReviewService) { }
  
  ngOnInit() {
    this.subscription = this.reviewService.getReviews().subscribe(review => {
      //console.log(review);
       this.review = review;
    });
 
    
  
    this.reviewcount = this.reviewcount;
  
  }

  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }

}
