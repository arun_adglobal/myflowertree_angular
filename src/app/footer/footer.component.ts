import { Component, OnInit,OnDestroy,Injectable, Inject, PLATFORM_ID  } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HeaderService } from '../header/header.service';

import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
declare const $ : any;
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit,OnDestroy {
  callBackForm: FormGroup;
  NewsForm: FormGroup;
  constructor(private headerService: HeaderService,
    @Inject(PLATFORM_ID) private platformId: object) { }
  footer = 'one';
  deviceType='Desktop';
  CDN_PATH  = environment.CDN_PATH;
  query_default_value = '';
  newsmsg = '';
  order_num_box = false;
  orderErrorMsg = false;
  callbackSucess = '';
  ngOnInit() {
  
    this.deviceType = this.getDevice();
      if (isPlatformBrowser(this.platformId)) {
      }
      this.callBackForm = new FormGroup({
        'name' : new FormControl(null,Validators.required),
        'query_related_to' : new FormControl('',Validators.required),
        'order_id' : new FormControl(null),
        'mobile' : new FormControl(null,[Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
      });

      this.NewsForm = new FormGroup({
        'newsletter_email' : new FormControl(null,[Validators.required,Validators.email])
        });

  //  this.loadScript('https://cdn.jsdelivr.net/npm/@searchtap/search-client/lib/index.min.js');
  //  this.loadStyle('https://d3cif2hu95s88v.cloudfront.net/live-site-2016/st/st.css.gz');
 //   this.loadScript('https://d3cif2hu95s88v.cloudfront.net/live-site-2016/st/stjs/st.js.gz');

}
getDevice() {
  var device = '';
  var ua = window.navigator.userAgent.toLowerCase();
  if (ua.match(/ipad/i) !== null || ua.match(/iphone/i) !== null || ua.match(/android/i) !== null)
  {
    device = 'Mobile';
  }else 
  {
    device = 'Desktop';	
  }
  return device;
 }
valid_form = true;
callBackFormSubmit(){
  if(! this.callBackForm.valid)
  {
    this.valid_form = false;
    this.callBackForm.markAllAsTouched();
  }else{
    this.valid_form = true;
  }
  if(this.valid_form)
  {
    var request_data = {};
    request_data['name'] = this.callBackForm.value.name;
    request_data['mobile'] = this.callBackForm.value.mobile;
    request_data['query_related_to'] = this.callBackForm.value.query_related_to;
    request_data['order_id'] = this.callBackForm.value.order_id;
    this.headerService.callbackModal(request_data).subscribe(data => {
      if(typeof(data['message'])!='undefined')
      {
        this.callbackSucess = data['message'];
      }
    });
  }

}

valid_form_news = true;
NewsletterFormSubmit(){
  if(! this.NewsForm.valid)
  {
    this.valid_form_news = false;
    this.NewsForm.markAllAsTouched();
  }else{
    this.valid_form_news = true;
  }
  if(this.valid_form_news)
  {
    var request_data = {};
    //console.log(this.NewsForm.value.newsletter_email);
    console.log('cmpts');
    request_data['newsletter_email'] = this.NewsForm.value.newsletter_email;
    this.headerService.newsletterform(request_data).subscribe(data => {

      console.log(data['status']);
      if(data['status']=='new')
      { 
        this.newsmsg = 'Thank you for subscribing on our newsletter.';
      }else{
        this.newsmsg = 'Already subscribed.';
      }

    });
  }

}

queryValue(){
  if(this.callBackForm.value.query_related_to=="Track my order" || this.callBackForm.value.query_related_to=="Raise a complaint")
  {
    this.order_num_box = true;
    this.callBackForm = new FormGroup({
      'name' : new FormControl(this.callBackForm.value.name,Validators.required),
      'query_related_to' : new FormControl(this.callBackForm.value.query_related_to,Validators.required),
      'order_id' : new FormControl(this.callBackForm.value.order_id,Validators.required),
      'mobile' : new FormControl(this.callBackForm.value.mobile,[Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
    });
  }else{
    this.order_num_box = false;
  }
}
/*  loadScript(url: string) 
  {
    const footer = <HTMLDivElement> document.getElementsByTagName('footer')[0];
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    footer.appendChild(script);
  }
  loadStyle(url: string) 
  {
    const footer = <HTMLDivElement> document.getElementsByTagName('footer')[0];
    const style = document.createElement('link');
    style.href = url;
    style.type = 'text/css';
    style.rel = 'stylesheet';
    footer.appendChild(style);
  }*/
  ngOnDestroy():void {
    
  }
  closeCallBackPopup(){

    if (isPlatformBrowser(this.platformId)) {
      $(document).ready(function(){
        var element = document.getElementById('request-call-back-popup');
        if (typeof element !== "undefined" && typeof element !== null) {
          document.getElementById('request-call-back-popup').style.display = "none";
        }
      });
    }


  }
}
