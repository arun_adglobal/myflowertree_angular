import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HeaderService } from '../header/header.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';

declare var $: any;
@Component({
  selector: 'app-special-coupon-popup',
  templateUrl: './special-coupon-popup.component.html',
  styleUrls: ['./special-coupon-popup.component.css']
})
export class SpecialCouponPopupComponent implements OnInit {
  specialOfferForm: FormGroup;
  CDN_PATH  = environment.CDN_PATH;
  allSubscription:Array<Subscription> = [];
  constructor( private headerService:HeaderService, 
    private gtmService: GoogleTagManagerService,
    private cookieService: CookieService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.specialOfferForm = new FormGroup({
      'email' : new FormControl(null,[Validators.required,Validators.email]),
      'name' : new FormControl(null, Validators.required),
      'mobile' : new FormControl(null, Validators.required),
      'city' : new FormControl(null)
    });
    if(! this.cookieService.check('userCouponRegister'))
    {
      if (isPlatformBrowser(this.platformId)) {
        $(document).ready(function(){
          var element = document.getElementById('myModal_offer');
          if (typeof element !== "undefined" && typeof element !== null) {
            setTimeout(()=>{
            document.getElementById('myModal_offer').style.display = "block";
           },10000);   
          }
        });
      }
    }
  /*  setTimeout(()=>{
      if(! this.cookieService.check('userCouponRegister'))
      {
        if (isPlatformBrowser(this.platformId)) {
          $(document).ready(function(){
            var element = document.getElementById('myModal_offer');
            if (typeof element !== "undefined" && typeof element !== null) {
              document.getElementById('myModal_offer').style.display = "block";
            }
          });
        }
            this.setCouponCookie();
      }
        },10000);  */ 
  }
  closemyModal_offer(){
    this.setCouponCookie();
    if (isPlatformBrowser(this.platformId)) {
      $(document).ready(function(){
        var element = document.getElementById('myModal_offer');
        if (typeof element !== "undefined" && typeof element !== null) {
          document.getElementById('myModal_offer').style.display = "none";
        }
      });
    }


  }
  expiredDate;
  setCouponCookie()
  {
    this.expiredDate = new Date();
    var time = this.expiredDate.getTime();
    time += 3600 * 1000;
    this.expiredDate.setTime(time);
    //this.expiredDate.setDate(this.expiredDate.toUTCString());
    this.cookieService.set('userCouponRegister','1',this.expiredDate,'/');
  }
  userData: any[] = [];
  cities  = [];
  lastkeydown1: number = 0;
  subscription: any;
  city_name = '';
  getUserIdsFirstWay($event) {
    let city_name = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;

    if (city_name.length > 2) {
      if ($event.timeStamp - this.lastkeydown1 > 200) {
        var request = {};
        this.city_name = request['city_name'] = city_name;
        this.allSubscription.push(this.headerService.getPopupCity(request).subscribe((city:any) => {
          this.cities = city;
        }));
      }
    }
  }  
  valid_form = true;
  submitSpecialOfferForm(){

    if(! this.specialOfferForm.valid)
  {
    this.valid_form = false;
    this.specialOfferForm.markAllAsTouched();
  }else{
    this.valid_form = true;
  }
  if(this.valid_form)
  {
      var request_data = {};
      request_data['email'] = this.specialOfferForm.value.email;
      request_data['name'] = this.specialOfferForm.value.name;
      request_data['city'] = 0;
      if(this.city_name !="")
      {
        for(let city of this.cities)
        {
          if(this.city_name==city['city_name'])
          {
            request_data['city'] = city['city_id'];
            if (isPlatformBrowser(this.platformId)) {
              localStorage.setItem('filter_city', city['city_id']);
              localStorage.setItem('filter_city_name', city['city_name']);
            }
          }
        }
      }
      request_data['mobile'] = this.specialOfferForm.value.mobile;
      this.setCouponCookie();

      this.allSubscription.push(this.headerService.userCouponRegister(request_data).subscribe((response_data:any[]) => {
        if(response_data['already_login']==0)
        {
          this.gtmService.pushTag({
            'event': 'user-login',
            'user_data': {
              'email': response_data['email'],
              'reward_points': response_data['user_points'],
              'wallet': response_data['user_cash'],
              'we_first_name': response_data['we_first_name'],
              'we_last_name': response_data['we_last_name'],
              'we_email': response_data['we_email'],
              'we_phone': response_data['we_phone'],
              'login_medium': response_data['login_medium'],
              'login_activity': response_data['login_activity']
            }
            });
            
            this.gtmService.pushTag({"pageType": "login", 'event': 'checkout'});
        }
      window.location.reload();
      }));
  }
      
  }

  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}
