import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SpecialCouponPopupComponent } from './special-coupon-popup/special-coupon-popup.component';
import { DeliverypoupComponent } from './deliverypoup/deliverypoup.component';
import { RouterModule } from '@angular/router';

@NgModule({
declarations: [  HeaderComponent, FooterComponent, SpecialCouponPopupComponent,DeliverypoupComponent],
exports: [  HeaderComponent, FooterComponent, SpecialCouponPopupComponent,DeliverypoupComponent],
imports: [ CommonModule, ReactiveFormsModule, RouterModule]
})
export class MainLayoutModule{

}