import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MyprofileService } from '../myprofile/myprofile.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class ChangepasswordComponent implements OnInit {
  changepassForm: FormGroup;
  successmsg:any;
  errormsg:any;
  subscription:Subscription;
  constructor(
    private myprofileService:MyprofileService) { }

  ngOnInit(): void {
    this.changepassForm = new FormGroup({
      'password' : new FormControl(null,Validators.required),
      'confirm' : new FormControl(null,[Validators.required]),
    });
  }
  submitpasswordForm()
  {
    var pass = this.changepassForm.value.password;
    var confirmpass = this.changepassForm.value.confirm;
    console.log('aaaa');
    console.log(pass);
    console.log(confirmpass);
    if(pass!=null && confirmpass!=null && pass==confirmpass)
    {
        var request_data = {};
        request_data['password'] = this.changepassForm.value.password;
        request_data['confirm'] = this.changepassForm.value.confirm;
        this.subscription = this.myprofileService.changepass(request_data).subscribe(response => {
          this.successmsg = "Profile updated successfully";
          this.errormsg = '';
          setTimeout(()=>{    //<<<---    using ()=> syntax
            this.successmsg = "";
            this.errormsg = '';
          }, 5000);

          this.changepassForm = new FormGroup({
            'password' : new FormControl(null,Validators.required),
            'confirm' : new FormControl(null,[Validators.required]),
          });

        });
    }else
    {
      if(pass!=null && confirmpass!=null && pass!=confirmpass)
      {
        this.errormsg = "Please enter valid confirm password";
          this.successmsg = '';
        setTimeout(()=>{    //<<<---    using ()=> syntax
          this.errormsg = "";
          this.successmsg = '';

        }, 5000);

      }
    }

  }
ngOnDestroy(){
  this.subscription.unsubscribe();
}
}