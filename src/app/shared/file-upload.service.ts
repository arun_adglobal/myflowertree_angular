import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class FileUploadService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  APIURL  = environment.API_URL;
  constructor(private http: HttpClient) { }
  custom_order(request_data) {
    var formData: any = new FormData();
    formData.append("description", request_data['description']);
    formData.append("avatar", request_data['avatar']);
    formData.append("custom_amount", request_data['custom_amount']);
    return this.http.post(this.APIURL+'api/custom_order',formData, {
        reportProgress: true,
		      withCredentials: true,
        observe: 'events'
      }).pipe(map(data => {
      return data;
     }));
  }

  customizedCake(request_data) {
    var formData: any = new FormData();
    formData.append("email", request_data['email']);
    formData.append("avatar", request_data['avatar']);
    formData.append("name", request_data['name']);
    formData.append("city", request_data['city']);
    formData.append("flavour", request_data['flavour']);
    formData.append("mobile", request_data['mobile']);
    formData.append("quantity", request_data['quantity']);
    formData.append("date", request_data['date']);
    return this.http.post(this.APIURL+'api/customizedCake',formData, {
        reportProgress: true,
		      withCredentials: true,
        observe: 'events'
      }).pipe(map(data => {
      return data;
     }));
  }
orderticketImageUpload(request_data) {
    var formData: any = new FormData();
    formData.append("avatar", request_data['avatar']);
    return this.http.post(this.APIURL+'api/orderticketImageUpload',formData, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
  }

  compliantImageUpload(request_data) {
    var formData: any = new FormData();
    formData.append("avatar", request_data['avatar']);
    return this.http.post(this.APIURL+'api/compliantImageUpload',formData, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
  } 

  //https://l2onzgtkli.execute-api.ap-south-1.amazonaws.com
  // productImageUpload(request_data) {
  //   var formData: any = new FormData();
  //   formData.append("avatar", request_data['avatar']);
  //   return this.http.post('https://l2onzgtkli.execute-api.ap-south-1.amazonaws.com/api/productImageUpload',formData, {
  //     withCredentials: true
  //   }).pipe(map(data => {
  //       return data;
  //      }));
  // }

  productImageUpload(request_data) {
    var formData: any = new FormData();
    formData.append("avatar", request_data['avatar']);
    return this.http.post(this.APIURL+'api/productImageUpload',formData, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
  }


  personalized(request_data) {
    var formData: any = new FormData();
    formData.append("orderid", request_data['orderid']);
    formData.append("suborderid", request_data['suborderid']);
    for(let i =0; i < request_data['images'].length; i++){
      formData.append("uploads[]", request_data['images'][i],request_data['images'][i]['name']);
    }
    return this.http.post(this.APIURL+'api/personalized',formData, {
      withCredentials: true
    }).pipe(map(data => {
        return data;
       }));
  }
   // Error handling 
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}