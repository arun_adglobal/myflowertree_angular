import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { BlogLayoutModule } from '../blog-layout.module';
import { PipeTransform, Pipe } from "@angular/core";
import { BlogCategoryComponent } from './blog-category.component';
import { BlogHomeComponent } from './blog-home/blog-home.component';
import { BlogPostComponent } from './blog-post/blog-post.component';
import { BlogTagComponent } from './blog-tag/blog-tag.component';
import { DomSanitizer } from '@angular/platform-browser';
@Pipe({ name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}


// Add OwlModule to imports at below section

@NgModule({
declarations: [ BlogCategoryComponent,BlogPostComponent,BlogHomeComponent,BlogTagComponent,SafeHtmlPipe ],
imports: [ RouterModule.forChild([
    { path: '', component: BlogHomeComponent },
    { path: 'page/:page', component: BlogHomeComponent },
    { path: 'category/:string', component: BlogCategoryComponent },
    { path: 'category/:string/page/:page', component: BlogCategoryComponent },
    { path: 'tag/:string', component: BlogTagComponent },
    { path: 'tag/:string/page/:page', component: BlogTagComponent },
    { path: ':string', component: BlogPostComponent },
]), CommonModule, ReactiveFormsModule,BlogLayoutModule ]
})
export class BlogCategoryModule {

}