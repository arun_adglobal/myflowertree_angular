import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogCategoryService } from '../blog-category.service';
import { SEOService } from '../../seo.service';
import { environment } from '../../../environments/environment';
import { combineLatest } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
declare var $:any;

@Component({
  selector: 'app-blog-tag',
  templateUrl: './blog-tag.component.html',
  styleUrls: ['./blog-tag.component.css'],
  providers: [BlogCategoryService]
})
export class BlogTagComponent implements OnInit {
  posts = [];
  showOld = false;
  showNew = false;
  oldUrl = '';
  newUrl = '';
  totalposts = 0;
  cat_name = '';
  detailcat:any = [];
  detailtags:any = [];
  recentpost:any = [];
  meta_desc = '';
  allSubscription:Array<Subscription> = [];
  CDN_DOMAIN  = environment.CDN_DOMAIN;
  constructor(private route: ActivatedRoute,
    private blogService: BlogCategoryService,
    private seoservice: SEOService,
    @Inject(PLATFORM_ID) private platformId: object) { }
  
  ngOnInit(): void {
  combineLatest([this.route.params, this.route.queryParams]).subscribe(
      ([params, qparams]) => {    
        this.allSubscription.push(this.blogService.getTag(params['string'],params['page'],qparams['s']).subscribe(page_data => {
      this.posts = page_data['posts'];
      this.totalposts = page_data['totalposts'];
      this.cat_name = page_data['name'];
      this.detailcat = page_data['sidebar_cat'];
      this.detailtags = page_data['sidebar_tags'];
      this.recentpost = page_data['recentpost'];
      this.meta_desc = page_data['meta_description'];
      this.SEO();
      if(typeof(params['page']) !="undefined" && params['page'] > 1)
      {
        this.showNew = true;
        this.newUrl = '/blog/tag/'+params['string']+'/page/'+(params['page']-1);
      }
      if(typeof(params['page']) =="undefined" && this.totalposts > 10)
      {
        this.showOld = true;
        this.oldUrl = '/blog/tag/'+params['string']+'/page/2';
      }else if(this.totalposts > 10 && (params['page']*10) <= this.totalposts)
      {
        this.showOld = true;
        this.oldUrl = '/blog/tag/'+params['string']+'/page/'+(parseInt(params['page'],10)+1);
      }
    }));
  });
  if (isPlatformBrowser(this.platformId)) {
    $(document).ready(function() { 
      $("#owl-demo").owlCarousel({   
          autoPlay: 3000, //Set AutoPlay to 3 seconds   
          items : 1,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]   
      });   
      });
  }
   
  }

  SEO()
  {
    this.seoservice.setTitle(this.cat_name+' | Blog - MyFlowerTree');
    this.seoservice.setMeta({name:"description", content:this.meta_desc});
  }

  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}
