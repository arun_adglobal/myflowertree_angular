import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogCategoryService } from '../blog-category.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SEOService } from '../../seo.service';
import { environment } from '../../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
declare var $:any;
@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.css'],
  providers: [BlogCategoryService]
})
export class BlogPostComponent implements OnInit {
  cart_data = [];
  totals = [];
  successmsg = '';
  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  CDN_DOMAIN  = environment.CDN_DOMAIN;
  filter_total = [];
  caturl:boolean;
  loginForm: FormGroup;
  detaildata:any = '';
  postImg:any = '';
  postthumbImg:any = '';
  detailcat:any = [];
  detailtags:any = [];
  recentpost:any = [];
  meta_desc = '';
  share_url = '';
  cat_description = '';
  allSubscription:Array<Subscription> = [];
  constructor(private blogService: BlogCategoryService, 
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private seoservice: SEOService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      this.allSubscription.push(this.blogService.getCategoryDetail(params['string']).subscribe(page_data => {
        this.detaildata = page_data['detail'];
        this.postImg = page_data['postImg'];
        this.postthumbImg = page_data['postthumbImg'];
        this.detailcat = page_data['sidebar_cat'];
        this.detailtags = page_data['sidebar_tags'];
        this.recentpost = page_data['recentpost'];
        this.meta_desc = page_data['meta_description'];
        this.cat_description = page_data['cat_description'];
        this.SEO();

        this.share_url = encodeURIComponent('https://www.myflowertree.com/blog/'+params['string']);
      }));
    });

    if (isPlatformBrowser(this.platformId)) {
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
          autoPlay: 3000, //Set AutoPlay to 3 seconds
          items : 1,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]

      });
    });
    }

    this.loginForm = new FormGroup({
      'email' : new FormControl(null,[Validators.required,Validators.email])
    });


  }
 /* socialShare(platfom,sharer){
    if(platfom=='fb')
    {
      sharer = sharer+'u='+this.share_url;
    }else if(platfom=='google')
    {
      sharer = sharer+'url='+this.share_url;
    }else if(platfom=='twitter')
    {
      var title = encodeURIComponent(this.detaildata['post_title']);
      sharer = sharer+'text='+title+'&url='+this.share_url;
    }else if(platfom=='linkedin')
    {
      var title = encodeURIComponent(this.detaildata['post_title']);
      sharer = sharer+'mini=true&url='+this.share_url;
    }
    window.open(sharer,'sharer','toolbar=0,status=0,width=648,height=395');
    return true;
  }
  */

  selectedoptions(slug)
  {
    var caturl = '/blog/category/'+slug;
    location.href = caturl;
  }

  valid_login = true;
  submitLoginForm()
  {

    if(! this.loginForm.valid)
      {
        this.valid_login = false;
        this.loginForm.markAllAsTouched();

      }else{
        this.valid_login = true;
      }



    var request_data = {};

    if(this.valid_login)
    {

      request_data['email'] = this.loginForm.value.email;

      this.allSubscription.push(this.blogService.sendnewsletter(request_data).subscribe(response => {
        this.successmsg = 'Newsletter send successfully';
        window.location.reload();
      }))


    }

  }





  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  SEO()
  {
    this.seoservice.setTitle(this.detaildata['post_title']+' | Blog - MyFlowerTree');
    this.seoservice.setMeta({name:"description", content:this.meta_desc});
  }
  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}