import { Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogCategoryService {
APIURL  = environment.API_URL;
  constructor(private http: HttpClient) {}

  getCategorySidebar() {
    return this.http.get(this.APIURL+'api/blog/categorysidebar', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getCategoryTags() {
    return this.http.get(this.APIURL+'api/blog/getCategoryTags', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  

  sendnewsletter(customerdata)
  {
    return this.http.post(this.APIURL+'api/blog/sendnewsletter',customerdata, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  

  getCategory(param,page,search) {
    var url = '';
    if(typeof(param) !="undefined")
    {
      url = '?url='+param;
    }else 
    {
      url = '?url=latest';
    }
    if(typeof(search) !="undefined")
    {
      url = '?s='+encodeURIComponent(search);
    }
    
    if(typeof(page) !="undefined")
    {
      url += '&page='+page;
    }else{
      url += '&page=1';
    }
    return this.http.get(this.APIURL+'api/blog/category'+url, {
      withCredentials: true
    }).pipe(map(data => {
     return data;
    }));
 }
 getTag(param,page,search) {
  var url = '';
  if(typeof(param) !="undefined")
  {
    url = '?url='+param;
  }else 
  {
    url = '?url=latest';
  }
  if(typeof(search) !="undefined")
  {
    url = '?s='+encodeURIComponent(search);
  }
  
  if(typeof(page) !="undefined")
  {
    url += '&page='+page;
  }else{
    url += '&page=1';
  }
  
  return this.http.get(this.APIURL+'api/blog/tag'+url, {
    withCredentials: true
  }).pipe(map(data => {
   return data;
  }));
}
 getCategoryDetail(param) {
    var url = '';
    if(typeof(param) !="undefined")
    {
      url = '?url='+param;
    }
    return this.http.get(this.APIURL+'api/blog/categorydetail'+url, {
      withCredentials: true
    }).pipe(map(data => {
     return data;
    }));
 }


 
}
