import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { BlogCategoryService } from './blog-category.service';
import { SEOService } from '../seo.service';
import { environment } from '../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { combineLatest, Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import * as _ from 'lodash';
declare var $:any;

@Component({
  selector: 'app-blog-category',
  templateUrl: './blog-category.component.html',
  styleUrls: ['./blog-category.component.css'],
  providers: [BlogCategoryService]
})
export class BlogCategoryComponent implements OnInit {
  posts = [];
  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  CDN_DOMAIN  = environment.CDN_DOMAIN;
  showOld = false;
  showNew = false;
  oldUrl = '';
  newUrl = '';
  loginForm: FormGroup;
  successmsg = '';
  totalposts = 0;
  cat_name = '';
  cat_description = '';
  detailcat:any = [];
  detailtags:any = [];
  recentpost:any = [];
  meta_desc = '';
  allSubscription:Array<Subscription> = [];
  constructor(private route: ActivatedRoute,
    private blogService: BlogCategoryService,
    public sanitizer: DomSanitizer,
    private seoservice: SEOService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
  combineLatest([this.route.params, this.route.queryParams]).subscribe(
      ([params, qparams]) => {
     this.allSubscription.push(this.blogService.getCategory(params['string'],params['page'],qparams['s']).subscribe(page_data => {
      this.posts = page_data['posts'];
      this.totalposts = page_data['totalposts'];
      this.cat_name = page_data['name'];
      this.cat_description = page_data['description'];
      this.detailcat = page_data['sidebar_cat'];
      this.detailtags = page_data['sidebar_tags'];
      this.recentpost = page_data['recentpost'];
      this.meta_desc = page_data['meta_description'];
      this.SEO();
      if(typeof(params['page']) !="undefined" && params['page'] > 1)
      {
        this.showNew = true;
        this.newUrl = '/blog/category/'+params['string']+'/page/'+(params['page']-1);
      }
      if(typeof(params['page']) =="undefined" && this.totalposts > 10)
      {
        this.showOld = true;
        this.oldUrl = '/blog/category/'+params['string']+'/page/2';
      }else if(this.totalposts > 10 && (params['page']*10) <= this.totalposts)
      {
        this.showOld = true;
        this.oldUrl = '/blog/category/'+params['string']+'/page/'+(parseInt(params['page'],10)+1);
      }
    }));
  });
  if (isPlatformBrowser(this.platformId)) {
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
          autoPlay: 3000, //Set AutoPlay to 3 seconds
          items : 1,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]
      });
    });
  }
    this.loginForm = new FormGroup({
      'email' : new FormControl(null,[Validators.required,Validators.email])
    });
  }

  selectedoptions(slug)
  {
    var caturl = '/blog/category/'+slug;
    location.href = caturl;
  }

  valid_login = true;
  submitLoginForm()
  {

    if(! this.loginForm.valid)
      {
        this.valid_login = false;
        this.loginForm.markAllAsTouched();

      }else{
        this.valid_login = true;
      }



    var request_data = {};

    if(this.valid_login)
    {

      request_data['email'] = this.loginForm.value.email;

      this.allSubscription.push(this.blogService.sendnewsletter(request_data).subscribe(response => {
        this.successmsg = 'Newsletter send successfully';
        window.location.reload();
      }))


    }

  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
  SEO()
  {
    this.seoservice.setTitle( this.cat_name+' | Blog - MyFlowerTree');
    this.seoservice.setMeta({name:"description", content:this.meta_desc});
  }
  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}