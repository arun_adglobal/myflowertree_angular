import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { CartLayoutModule } from '../cart-layout.module';

import { CartComponent } from './cart.component';
import { AuthGuardService1 } from '../auth-guard.service';
import { NgxSpinnerModule } from "ngx-spinner";
@NgModule({
declarations: [ CartComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: CartComponent, canActivate:[AuthGuardService1] }
]), CommonModule, ReactiveFormsModule,CartLayoutModule,NgxSpinnerModule ]
})
export class CartModule {

}