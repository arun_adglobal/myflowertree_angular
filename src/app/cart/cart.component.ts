import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from './cart.service';
import { ProductService } from '../miscellaneous/product/product.service';

import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { SEOService } from '../seo.service';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  providers: [CartService]
})
export class CartComponent implements OnInit {
  cart_data = [];
  totals = [];
  safeUrl: SafeResourceUrl;
  CDN_PATH = environment.CDN_PATH;
  filter_total = [];
  optionImage = '';
  allSubscription:Array<Subscription> = [];
  constructor(private cartService: CartService,
    private router: Router,
    public sanitizer: DomSanitizer,
    private ProductService:ProductService,
    private gtmService: GoogleTagManagerService, private NgxSpinner:NgxSpinnerService,
    private seoservice: SEOService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.NgxSpinner.show();
    if (isPlatformBrowser(this.platformId)) {
      this.SEO();
      this.getCart();
    }
  }
  cart_delete(cart_id, key) {
    this.NgxSpinner.show();
    //confirm("Are you sure ? This product is awesome :-)");
    if (confirm("Are you sure ? This product is awesome :-)")) {
      this.gtag_removeCart(cart_id);
      this.allSubscription.push(this.cartService.cart_delete(cart_id, key).subscribe((cart_data: any) => {

        if (cart_data[0]['cart_data'].length == 0) {
          this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
          this.router.navigate(['/']);
          //window.location.href = '/';
        }

        this.cart_data = cart_data[0]['cart_data'];
        this.totals = cart_data[0]['totals'];
        this.filter_total = cart_data[0]['filter_total'];
        this.gtmService.pushTag({ 'event': 'cartUpdated', 'updatedProducts': this.gtag_products() });
       this.NgxSpinner.hide();
       this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
      }));
    }
    //return false;
  }

  getCart() {
    //console.log('on cart page');
    this.allSubscription.push(this.cartService.getCartData().subscribe((cart_data: any) => {
      this.NgxSpinner.hide();
      //console.log('on cart page 1');

      if (cart_data[0]['cart_data'].length == 0) {
        this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
        this.router.navigate(['/']);
      }
      //console.log('on cart page 2');

      //console.log(cart_data[0]['totals']);
      this.cart_data = cart_data[0]['cart_data'];
      this.totals = cart_data[0]['totals'];
      this.filter_total = cart_data[0]['filter_total'];
      if (this.cart_data.length) {
        var gtmTag = {
          'contentGrouping': this.cart_data[0]['parent_category_path'],
          "pageType": "cart",
          'products': this.gtag_products(),
          'cartTotals': this.filter_total
        }
        //console.log('Cart Page GTM Tag ',gtmTag);
        this.gtmService.pushTag(gtmTag);
        this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});

      }
    }));
  }

  update_cart(cart_id, quantity) {
    this.NgxSpinner.show();
    this.allSubscription.push(this.cartService.cart_update(cart_id, quantity).subscribe((cart_data: any) => {
      this.cart_data = cart_data[0]['cart_data'];
      this.totals = cart_data[0]['totals'];
      this.filter_total = cart_data[0]['filter_total'];
      //this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});

      //console.log('Cart update Page GTM Tag ',{ 'event': 'cartUpdated', 'updatedProducts': this.gtag_products() })
      this.gtmService.pushTag({ 'event': 'cartUpdated', 'updatedProducts': this.gtag_products() });
     
        this.NgxSpinner.hide();
      
    }));
  }
  international_products = [];
  international_error = '';
  goToCheckout() {
    this.international_products = [];
    for (let product of this.cart_data) {
      if (product['product_type'] == "International") {
        this.international_products.push(product['cart_id']);
      }
    }
    if (this.international_products.length == 0) {
      //console.log('Cart to checkout Page GTM Tag ',{ 'event': 'checkout', 'products': this.gtag_products(), 'cartTotals': this.filter_total });
      this.gtmService.pushTag({ 'event': 'checkout', 'products': this.gtag_products(), 'cartTotals': this.filter_total });
      this.router.navigate(['/checkout']);
      this.international_error = '';
    } else if ((this.international_products.length != 0) && (this.international_products.length == this.cart_data.length)) {
      this.gtmService.pushTag({ 'event': 'checkout', 'products': this.gtag_products(), 'cartTotals': this.filter_total });
      this.router.navigate(['/checkout']);
      this.international_error = '';
    } else if (this.international_products.length != 0) {
      this.international_error = 'you can not have national and international products together in your cart.';
    }
  }
  gtag_products() {
    var products = [];
    for (let product of this.cart_data) {
      products.push({
        'cart_id': product['cart_id'],
        'key': product['key'],
        'queryParams': product['queryParams'],
        'product_id': product['product_id'],
        'product_type': product['product_type'],
        'thumb': product['thumb'],
        'name': product['name'],
        'model': product['model'],
        'cate_name': product['cate_name'],
        'parent_category_path': product['parent_category_path'],
        'option': product['option'],
        'quantity': product['quantity'],
        'delivery_date': product['delivery_date'],
        'shipping_time_slot': product['shipping_time_slot'],
        'price': product['price'],
        'pro_price': product['price'],
        'addon': product['addon'],
        'total': product['total'],
        'href': product['href'],
        'images': product['images'],
        'product_url': product['product_url'],
        'url': product['url'],
      });
    }
    return products;
  }
  gtag_removeCart(cart_id) {
    for (let product of this.cart_data) {
      if (product['cart_id'] == cart_id) {
        var product_detail = {
          'cart_id': product['cart_id'],
          'key': product['key'],
          'queryParams': product['queryParams'],
          'product_id': product['product_id'],
          'product_type': product['product_type'],
          'thumb': product['thumb'],
          'name': product['name'],
          'model': product['model'],
          'cate_name': product['cate_name'],
          'parent_category_path': product['parent_category_path'],
          'option': product['option'],
          'quantity': product['quantity'],
          'delivery_date': product['delivery_date'],
          'shipping_time_slot': product['shipping_time_slot'],
          'price': product['price'],
          'pro_price': product['price'],
          'addon': product['addon'],
          'total': product['total'],
          'href': product['href'],
          'images': product['images'],
          'product_url': product['product_url'],
          'url': product['url'],
        }
        //console.log('Cart removed Page GTM Tag ',product_detail)
        this.gtmService.pushTag({ 'event': 'removeFromCart', 'products': product_detail });
      }
    }
  }
  getSafeCDNUrl(url: string = '') {
    this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH + url);
    return this.safeUrl;
  }
  closeoptionImage() {
    document.getElementById('optionImage').style.display = "none";
  }
  optionImagePopup(img) {
    this.optionImage = this.CDN_PATH + 'option-images/' + img;
    document.getElementById('optionImage').style.display = "block";
  }
  SEO() {
    this.seoservice.setTitle('Shopping cart | MyFlowerTree');
  }
  createUrl(key) {
    return '/' + key;
  }
  ngOnDestroy(){
_.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}