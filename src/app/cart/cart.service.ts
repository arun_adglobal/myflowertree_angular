import { Injectable,Inject, PLATFORM_ID } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CartService {
APIURL  = environment.API_URL;
  constructor(private http: HttpClient, @Inject(PLATFORM_ID) private platformId: object) {}
  getCartData() {
    return this.http.get(this.APIURL+'api/cart', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  cart_delete(cart, key) {
     return this.http.get(this.APIURL+'api/cart/delete?cart_id='+cart+'&key='+key, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  private getToken():string{
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem('userToken');
    }
  }
  cart_update(cart_id, quantity) {
    return this.http.get(this.APIURL+'api/cart/update?cart_id='+cart_id+'&quantity='+quantity, {
      withCredentials: true
    }).pipe(map(data => {
     return data;
    }));
 }
}
