import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { ProductService } from '../miscellaneous/product/product.service';

@Component({
  selector: 'app-best-sellers',
  templateUrl: './best-sellers.component.html',
  styleUrls: ['./best-sellers.component.css']
})
export class BestSellersComponent implements OnInit {
  products : any = [];
  CDN_PATH  = environment.CDN_PATH;
  defaultImage = this.CDN_PATH+'2020/myflowertree-loader.jpg';
  subscription:Subscription;

  constructor( private ProductService:ProductService) { }

  ngOnInit(): void {
   this.subscription = this.ProductService.getBestSellerProducts().subscribe(data => {
      this.products = data;
    });
  }
ngOnDestroy(){
  this.subscription.unsubscribe();
}
}