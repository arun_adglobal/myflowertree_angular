import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { BestSellersComponent } from './best-sellers.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [BestSellersComponent],
imports: [ RouterModule.forChild([
    { path: '', component: BestSellersComponent }
]), CommonModule, LazyLoadImageModule,MainLayoutModule ]
})
export class BestSellersModule{

}