import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import{ BlogService } from './blog.service'

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers:[BlogService]
})
export class BlogComponent implements OnInit {
  blog :any=[];
  subscription:Subscription;
  //reviewcount :any=['1','2','3','4','5'];
  constructor(private blogService:BlogService) { }

  ngOnInit() {
    this.blogService.getBlogs().subscribe(blog => {

       this.blog = blog;
      // console.log(this.blog);
    });
  }
 ngOnDestroy(){
   this.subscription.unsubscribe();
 }
}