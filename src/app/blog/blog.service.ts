import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable()
export class BlogService {
APIURL  = environment.API_URL;
  constructor(private http: HttpClient) {}
  getBlogs() {
    return this.http.get(this.APIURL+'api/blog/', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }

}