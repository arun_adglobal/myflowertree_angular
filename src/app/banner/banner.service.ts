import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable()
export class BannerService {
  APIURL  = environment.API_URL;
  constructor(private http: HttpClient) {}
  getBanners() {
    return this.http.get(this.APIURL+'api/banner/', {
      withCredentials: true
    }).pipe(map((data) => {
      return data;
    }));
  }

}