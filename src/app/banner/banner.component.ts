import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import{ BannerService} from './banner.service'
import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css'],
  providers:[BannerService]
})
export class BannerComponent implements OnInit {
  banner :any = []
  CDN_PATH  = environment.CDN_PATH;
  bannerSubscription:Subscription;
  constructor(private bannerService:BannerService,@Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit() {
    this.bannerSubscription = this.bannerService.getBanners().subscribe(banner => {
      this.banner = banner;

    });
  }

  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }

ngOnDestroy(){
  this.bannerSubscription.unsubscribe();
}
}