import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { CsComponent } from './cs.component';
import { MainLayoutModule } from '../main-layout.module';

@NgModule({
declarations: [ CsComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: CsComponent }
]), CommonModule, ReactiveFormsModule, NgxJsonLdModule,NgxUsefulSwiperModule,MainLayoutModule]
})
export class CsModule{

}