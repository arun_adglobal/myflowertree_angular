import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
 
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Router } from '@angular/router';
import { FileUploadService } from "../shared/file-upload.service";
import { FeedbackService } from '../feedback/feedback.service';
import { SocialUser } from 'angularx-social-login';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../environments/environment';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
 
declare var $:any;
@Component({
 selector: 'app-cs',
 templateUrl: './cs.component.html',
 styleUrls: ['./cs.component.css']
})
export class CsComponent implements OnInit {
 CDN_PATH  = environment.CDN_PATH;
 CDN_DOMAIN  = environment.CDN_DOMAIN;
 preview: string;
 cs: FormGroup;
 errormsg='';
 errormsgwhat='';
 a:number=0;
 b:number=0;
 compdatas:any = [];
 total:number=0;
 file_2:any = 0;
 file_3:any = 0;
 customer_data = {};
 user: SocialUser;
 loggedIn: boolean;
 showForm = 0;
 allSubscriptions:Array<Subscription> = [];
 
 constructor(private router:Router,private FeedbackService: FeedbackService,public fileUploadService: FileUploadService,
   @Inject(PLATFORM_ID) private platformId: object) { }
 
 ngOnInit(): void {
 
   if (isPlatformBrowser(this.platformId)) {
     this.showForm = 1;
   }
 
   if (isPlatformBrowser(this.platformId)) {
     if(localStorage.getItem('userToken') !=null)
     {
       const token = localStorage.getItem('userToken');
       let payload;
       if(token){
         payload = token.split('.')[1];
         payload = window.atob(payload);
         const user =  JSON.parse(payload);
         if(user){
           if(! (user.exp > Date.now() / 1000))
           {
             this.router.navigateByUrl('/login');
 
           }
         }else{
           this.router.navigateByUrl('/login');
         }
       }else{
         this.router.navigateByUrl('/login');
       }
     }else{
       this.router.navigateByUrl('/login');
     }
   }
 
 
   this.a = Math.floor((Math.random() * 10) + 1);
   this.b = Math.floor((Math.random() * 10) + 1);
 
   this.total = this.a+this.b;
 
   this.cs = new FormGroup({
     'order_id' : new FormControl(null,Validators.required),
     'complaint_reason' : new FormControl(null, Validators.required),
     'notes' : new FormControl(null, Validators.required),
     'whatis' : new FormControl(null,Validators.required),
     'avatar' : new FormControl(null)
 });
 
 
 }
 
 uploadedImageArray = [];
 file_code = [];
 file_name = [];
 
   imageCode = '';
   fileUploadMsg = '';
   onUploadFile(event) {
     if (isPlatformBrowser(this.platformId)) {
       $("#callbackloader").css('display','block');
     }
     const file = (event.target as HTMLInputElement).files[0];
     this.cs.patchValue({
       avatar: file
     });
    this.cs.get('avatar').updateValueAndValidity()
 
     // File Preview
     const reader = new FileReader();
     reader.onload = () => {
       this.preview = reader.result as string;
     }
     reader.readAsDataURL(file);
     var request_data = {};
     request_data['avatar'] = this.cs.value.avatar;
     this.allSubscriptions.push(this.fileUploadService.compliantImageUpload(request_data).subscribe(data => {
       if (isPlatformBrowser(this.platformId)) {
         $("#callbackloader").css('display','none');
       }
       if(data['code'] !="")
       {
         this.fileUploadMsg = 'success';
         /*(this.uploadedImageArray = $.grep(this.uploadedImageArray, function(e){
           return e['option'] != product_option_id;
         });*/
         this.file_code.push(data['code']);
         this.file_name.push(data['filename']);
       }else{
         this.fileUploadMsg = 'fail';
       }
       //console.log("this.uploadedImageArray " + this.uploadedImageArray);
     }));
   }
 
 
 
 valid_from = true;
 submitCsForm()
 {
   if(! this.cs.valid)
     {
       this.valid_from = false;
       this.cs.markAllAsTouched();
 
     }else{
       this.valid_from = true;
     }
    // console.log(this.valid_from);
   if(this.valid_from)
   {
     this.errormsg = '';
     document.getElementById('resultLoading').style.display = "block";
     // console.log('dsdds');
     var request_data = {};
     request_data['order_id'] = this.cs.value.order_id;
     request_data['complaint_reason'] = this.cs.value.complaint_reason;
     request_data['notes'] = this.cs.value.notes;
     request_data['code'] = this.file_code.join(",");
     request_data['filename'] = this.file_name.join(",");
     if(this.cs.value.whatis!='' && this.cs.value.whatis!=this.total)
     {
       document.getElementById('resultLoading').style.display = "none";
       this.errormsgwhat = 'Please Enter Correct Number';
     }else
     {
       this.errormsgwhat = '';
 
       this.allSubscriptions.push(this.FeedbackService.checkComplaints(request_data['order_id']).subscribe(data => {
         //console.log(data);
         this.compdatas = data;
 
         if(this.compdatas.length==0)
         {
           this.FeedbackService.usersComplaint(request_data).subscribe((data) => {
             document.getElementById('resultLoading').style.display = "none";
           });
 
           setTimeout(function(){
             location.reload();
           },1000);
 
         }else{
 
           document.getElementById('resultLoading').style.display = "none";
           this.errormsg = 'Already exist';
         }
 
 
 
       }));
 
 
 
       /*this.FeedbackService.usersComplaint(request_data).subscribe((data) => {
         document.getElementById('resultLoading').style.display = "none";
       });
       location.reload();*/
 
 
     }
 
 
 
   }else{
     //console.log('ddsa');
     this.errormsg = 'Please fill all in the blanks';
   }
 }
 
 addMore()
 {
   if (isPlatformBrowser(this.platformId)) {
     if(this.file_2==0)
     {
       $('#file_block_2').show();
       this.file_2 = 1;
     }else if(this.file_3==0)
     {
       $('#file_block_3').show();
       this.file_3 = 1;
     }
   }
 }
 
 remove(id)
 {
   if (isPlatformBrowser(this.platformId)) {
     if(id===2)
     {
       $('#file_block_2').hide();
       this.file_2 = 0;
     }else if(id===3)
     {
       $('#file_block_3').hide();
       this.file_3 = 0;
     }
   }
 
 }
 
ngOnDestroy(){
  _.forEach(this.allSubscriptions,(sub:Subscription)=>sub.unsubscribe());
}
}
 

