import { Component, OnInit, OnDestroy, Inject, PLATFORM_ID  } from '@angular/core';
import { AppService } from './app.service';
import { ProductService } from './miscellaneous/product/product.service';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { isPlatformBrowser } from '@angular/common';
import { strictEqual } from 'node:assert';
declare var gtag;
declare const getDevice : any;
declare const getBrowser : any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})
export class AppComponent implements OnInit,OnDestroy{
  title = 'app';
  urlcus:string='';
  showHeader = true;
  showFooter = true;
  isCartHeader = false;
  isBlogPage = false;
  subscription:Subscription;
 // showcontent = false;
  //private footerSubscribe:Subscription;

  constructor(private ProductService: ProductService, private router: Router,private activatedRouter: ActivatedRoute,
    private gtmService: GoogleTagManagerService, @Inject(PLATFORM_ID) private platformId: object) {

      /*const navEndEvents = router.events.pipe(
        filter(event => event instanceof NavigationEnd),
      );
      navEndEvents.subscribe((event: NavigationEnd) => {
        gtag("config","UA-15925960-1");
      })

      */

     }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('deviceType', getDevice());
      localStorage.setItem('browser', getBrowser());
    }
    let urlcus = window.location.pathname;

    const string = urlcus;
const substring = "customer_invoice";

console.log(string.includes(substring)); // true
    
    //console.log(window.location.pathname);
    if(window.location.pathname === '/cart' || window.location.pathname === '/checkout' ) {
      this.isCartHeader = true;
      this.isBlogPage = false;
    } else if( window.location.pathname === '/floristlogin' || window.location.pathname === '/login' || window.location.href.includes('/blog') || window.location.href.includes('/customer_invoice')) {
      this.isBlogPage = true;
    }
    else {
      this.isCartHeader = false;
      this.isBlogPage = false;
    }

      this.subscription = this.router.events.subscribe((event:NavigationEnd) => {
        if(event instanceof NavigationStart){
          if(event.url ==='/cart' || event.url === '/checkout' ) {
            this.isCartHeader = true;
            this.isBlogPage = false;
            } else if( event.url==='/floristlogin' || event.url === '/login' || event.url.includes('/blog'))  {
              this.isBlogPage = true;
            }
            else {
              this.isBlogPage = false;
            this.isCartHeader = false;}
        }
      });
    // push GTM data layer for every visited page
    this.router.events.forEach(item => {
      if (item instanceof NavigationEnd) {
        this.gtmService.pushTag({ 'event': 'virtualTag'});

        const gtmTag = {
          event: 'page',
          pageName: item.url
        };
        this.gtmService.pushTag(gtmTag);

      }
    });
  }
  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }
}
