import { Component, OnInit, ViewEncapsulation,Injectable, Inject, PLATFORM_ID } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HomeService } from '../home/home.service';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../environments/environment';
//import { MyprofileService } from './myprofile.service';
import { SEOService } from '../seo.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
declare var $:any;
@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
  //encapsulation : ViewEncapsulation.None
})
export class ContactusComponent implements OnInit {
  CDN_PATH  = environment.CDN_PATH;
  CDN_DOMAIN  = environment.CDN_DOMAIN;
  addcontactus: FormGroup;
  description:any=[];
  errormsg='';
  errormsgwhat='';
  a:number=0;
  b:number=0;
  total:number=0;
  cssDy:any={};
  safeUrl: SafeResourceUrl;
  private allSubscription:Array<Subscription> = [];
  constructor(private router:Router,
    private seoservice: SEOService,
    public sanitizer: DomSanitizer,
    private homeService: HomeService, @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.SEO();
    this.cssDy = { contactcss : this.getSafeCDNUrl('css/contact.css')};

    this.a = Math.floor((Math.random() * 10) + 1);
    this.b = Math.floor((Math.random() * 10) + 1);

    this.total = this.a+this.b;

    this.addcontactus = new FormGroup({
      'name' : new FormControl(null,Validators.required),
      'email' : new FormControl(null, [Validators.required,Validators.email]),
      'mobile' : new FormControl(null, [Validators.required, Validators.minLength(8), Validators.maxLength(10)]),
      'enquiry' : new FormControl(null,Validators.required),
      'query_related_to' : new FormControl(null,Validators.required),
      'whatis' : new FormControl(null,Validators.required)
  });

  this.allSubscription.push(this.homeService.getData_Contactus().subscribe(postss => {
    this.description = postss[0].description;
  }));   


  }
  SEO()
  {
    this.seoservice.setTitle("Contact Us");
}
  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
  valid_from = true;
  submitContactForm()
  {
    //request_data['form_data'] = this.addcontactus.value;
      if(! this.addcontactus.valid)
      {
        this.valid_from = false;
        
      }else{
        this.valid_from = true;
      }

    if(this.valid_from)
    {
      this.errormsg = '';
      document.getElementById('resultLoading').style.display = "block";
      // console.log('dsdds');
      var request_data = {};
      request_data['name'] = this.addcontactus.value.name;
      request_data['email'] = this.addcontactus.value.email;
      request_data['mobile'] = this.addcontactus.value.mobile;
      request_data['enquiry'] = this.addcontactus.value.enquiry;
      request_data['query_related_to'] = this.addcontactus.value.query_related_to;
      if(this.addcontactus.value.whatis!='' && this.addcontactus.value.whatis!=this.total)
      {
        document.getElementById('resultLoading').style.display = "none";
        this.errormsgwhat = 'Please Enter Correct Number';
      }else
      {
        this.errormsgwhat = '';
        this.allSubscription.push(this.homeService.addcontactUs(request_data).subscribe(response => {
          document.getElementById('resultLoading').style.display = "none";
        }));
        location.reload();
      }


      
    }else{
      //console.log('ddsa');
      this.errormsg = 'Please fill all in the blanks';
    }


  }
  showFAQ()
  {
    if (isPlatformBrowser(this.platformId)) {
      $('.panel-default').css('display','block');
	    $('.show-btn').css('display','none');
    }
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  ngOnDestroy():void {
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}
