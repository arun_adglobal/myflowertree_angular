import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ContactusComponent } from './contactus.component';
import { DomSanitizer } from '@angular/platform-browser'
import { PipeTransform, Pipe } from "@angular/core";
import { MainLayoutModule } from '../main-layout.module';


@Pipe({ name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@NgModule({
declarations: [ ContactusComponent,SafeHtmlPipe ],
imports: [ RouterModule.forChild([
    { path: '', component: ContactusComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class ContactusModule {

}