import { Component, OnInit,ViewEncapsulation,Injectable, Inject, PLATFORM_ID  } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '../header/header.service';
import { isPlatformBrowser } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-deliverypoup',
  templateUrl: './deliverypoup.component.html',
  styleUrls: ['./deliverypoup.component.css']
})
export class DeliverypoupComponent implements OnInit {

  constructor( public router: Router,private headerService:HeaderService,@Inject(PLATFORM_ID) private platformId: object) { }

  staticCities = [{city_name: 'Delhi',city_id: btoa('216')},
  {city_name: 'Noida',city_id: btoa('217')},
  {city_name: 'Gurgaon',city_id: btoa('185')},
  {city_name: 'Mumbai',city_id: btoa('210')},
  {city_name: 'Hyderabad',city_id: btoa('190')},
  {city_name: 'Chennai',city_id: btoa('173')},
  {city_name: 'Bangalore',city_id: btoa('166')},
  {city_name: 'Lucknow',city_id: btoa('202')},
  {city_name: 'Noida',city_id: btoa('217')}];
  ngOnInit(): void {
  }
  closeDeliverypopup(){
    
    if (isPlatformBrowser(this.platformId)) {
      $(document).ready(function(){
        var element = document.getElementById('myModal_delivery');
        if (typeof element !== "undefined" && typeof element !== null) {
          document.getElementById('myModal_delivery').style.display = "none";
        }
      });
    }


  }
  setDeliveryPopCity(city_id,city_name){
    if (isPlatformBrowser(this.platformId)) {
      $("#callbackloader").css('display','block');

    }
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('filter_city', atob(city_id));
      localStorage.setItem('filter_city_name', city_name);
    }
    window.location.reload();
  }
  cities  = [];
  lastkeydown2: number = 0;
  city_name = '';
  getPopUpCity($event) {
    let city_name = (<HTMLInputElement>document.getElementById('userTypedCity')).value;
    if (city_name.length > 2) {
      if ($event.timeStamp - this.lastkeydown2 > 200) {
        var request = {};
        this.city_name = request['city_name'] = city_name;
        this.headerService.getPopupCity(request).subscribe((city:any) => {
          this.cities = city;
        });
      }
    }
  }  
  citySelected(e){
    if (isPlatformBrowser(this.platformId)) {
      $("#callbackloader").css('display','block');
    }
    if(e.target.value !="")
    {
      for(let city of this.cities)
      {
        if(e.target.value==city['city_name'])
        {
          if (isPlatformBrowser(this.platformId)) {
            localStorage.setItem('filter_city', city['city_id']);
            localStorage.setItem('filter_city_name', city['city_name']);
          }
        }
      }
    }
    window.location.reload();
  }
}
