import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from '../miscellaneous/product/product.service';
import { CheckoutService } from '../checkout/checkout.service';
import { CartService } from '../cart/cart.service';

import { AuthenticationService } from '../authentication.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angularx-social-login';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  CDN_PATH  = environment.CDN_PATH;
  safeUrl: SafeResourceUrl;
  loginForm: FormGroup;
  signupForm: FormGroup;
  loginOTPForm: FormGroup;
  cssDy:any={};
  step = 1;
  view = 1;
  is_customer = false;
  customer_data = {};
  error = '';
  otp = '';
  telephone = '';
  allSubscription:Array<Subscription> = [];
  cartSubscription:Subscription;
  constructor(private ProductService: ProductService,
    private checkoutService: CheckoutService,
    private auth: AuthenticationService,
    private cartService:CartService,
    public sanitizer: DomSanitizer,
    private router:Router,
    private socialAuthService: AuthService,
    private gtmService: GoogleTagManagerService) {
      this.ProductService.header.next(true);
      this.ProductService.footer.next(false);

     this.allSubscription.push(this.checkoutService.getCustomer().subscribe((customer_data:any[]) => {
        //console.log(customer_data['email']);
        if(typeof customer_data['email'] !='undefined')
        {
          window.location.href = '/account';
          //this.router.navigate(['/account']);
        }
      }));
 

   }

  ngOnInit(): void {
    //bootMin : this.getSafeCDNUrl('live-new/js/bootstrap.min.js.gz')
    this.cssDy = { faviconpng : this.getSafeCDNUrl('live-new/images/favicon.png'),bootstrapMin : this.getSafeCDNUrl('live-new/css/bootstrap.min.css.gz'),customCss : this.getSafeCDNUrl('live-new/css/custom.css.gz'),responsiveCss : this.getSafeCDNUrl('live-new/css/responsive.css.gz'),fontAwsCss : this.getSafeCDNUrl('live-new/font-awesome/css/font-awesome.min.css.gz')};


    this.loginForm = new FormGroup({
      'login_email' : new FormControl(null,[Validators.required,Validators.email])
    });




  }
  public socialSignIn_11(socialPlatform : string) {
   // console.log('dsdsdd');

    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;

    }
    //console.log(socialPlatformProvider);
    this.socialAuthService.signIn(socialPlatformProvider,{ux_mode:'redirect'}).then(
      (userData) => {


        //console.log(socialPlatform+" sign in data : " , userData);
        // Now sign-in with userData
        // ...
          if(socialPlatform == "facebook"){
            var request_data = {};
            //console.log(userData.facebook['email']);
            request_data['email'] = userData.facebook['email'];
            request_data['first_name'] = userData.facebook['first_name'];
            request_data['last_name'] = userData.facebook['last_name'];
            request_data['social_type'] = socialPlatform;
            request_data['id'] = userData.facebook['id'];

            this.checkoutService.addCustomerChkSocial(request_data).subscribe(response => {

              //console.log(response);
                  if(typeof response['email'] !='undefined')
                  {
                    this.customer_data = response;
                    this.auth.saveToken(response['token']);
                    var gtmTag = {
                      'event': 'user-login',
                      'user_data': {
                        'email': this.customer_data['email'],
                        'login_medium': this.customer_data['login_medium'],
                        'login_activity': this.customer_data['login_activity']
                    }}
                    //console.log('gtm tag in social login')
                    this.gtmService.pushTag(gtmTag);
                    this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
                    this.ProductService.header.next(true);
                    this.ProductService.footer.next(true);
                    this.cartSubscription = this.cartService.getCartData().subscribe((cart_data: any) => {
                      this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
                    });
                    window.location.href = '/account';
                    //this.router.navigate(['/account']);                 

                  }else if(typeof response['error'] !='undefined')
                  {
                    this.error = response['error'];
                    document.getElementById('resultLoading').style.display = "none";
                  }

            });
          }
      }
    );
  }
  public socialSignIn(socialPlatform : string) {
    var request_data = {};

    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;

    }
    //console.log(socialPlatformProvider);
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        if(socialPlatform == "facebook"){
          request_data['social_type'] = 'fb';
          request_data['email'] = userData.facebook['email'];
          request_data['first_name'] = userData.facebook['first_name'];
          request_data['last_name'] = userData.facebook['last_name'];
          request_data['id'] = userData.facebook['id'];
        }else if(socialPlatform == "google"){
          request_data['social_type'] = 'google';
          request_data['email'] = userData['email'];
          request_data['first_name'] = userData['firstName'];
          request_data['last_name'] = userData['lastName'];
          request_data['id'] = userData['id'];
        }
        this.allSubscription.push(this.checkoutService.sociallogin(request_data).subscribe((customer_data:any[]) => {
          if(typeof customer_data['email']  !='undefined')
          {
            this.customer_data = customer_data;
            this.auth.saveToken(customer_data['token']);
            this.view = 5;
            this.checkoutService.loggedIn.next(true);
            var gtmTag = {
              'event': 'user-login',
              'user_data': {
                'email': this.customer_data['email'],
                'login_medium': this.customer_data['login_medium'],
                'login_activity': this.customer_data['login_activity']
            }}
            //console.log('gtm login in social login page');
            this.gtmService.pushTag(gtmTag);
            this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
            this.ProductService.header.next(true);
            this.ProductService.footer.next(true);
            this.cartSubscription = this.cartService.getCartData().subscribe((cart_data: any) => {
              this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
            });
            window.location.href = '/account';
            //this.router.navigate(['/account']);

          }else if(typeof customer_data['error'] !='undefined')
          {
            this.error = customer_data['error'];
            document.getElementById('resultLoading').style.display = "none";
          }
         }));
      }
    );
  }


  submitLoginForm(){
    var request_data = {};
    request_data['email'] = this.loginForm.value.login_email;
    if(! this.is_customer)
    {
      this.allSubscription.push(this.checkoutService.check_customer(request_data).subscribe(response => {
        if(typeof(response['email']) !='undefined')
        {
          this.step = 2;
          this.loginForm = new FormGroup({
            'login_email' : new FormControl(response['email'],[Validators.required,Validators.email]),
            'login_password' : new FormControl(null, Validators.required)
          });
          this.is_customer = true;
        }else if(typeof(response['email']) =='undefined')
        {
          this.view = 2;
          this.signupForm = new FormGroup({
            'register_email' : new FormControl(request_data['email'],[Validators.required,Validators.email]),
            'register_name' : new FormControl(null, Validators.required),
            'register_password' : new FormControl(null, Validators.required),
            'register_telephone' : new FormControl(null, Validators.required),
            'gender' : new FormControl("Male", Validators.required)
          });
        }
      }));
    }else
    {
      document.getElementById('resultLoading').style.display = "block";

      request_data['password'] = this.loginForm.value.login_password;
      request_data['direct_login'] = 1;
      this.allSubscription.push(this.checkoutService.login(request_data).subscribe((customer_data:any[]) => {
        if(typeof customer_data['email'] !='undefined')
        {
          this.customer_data = customer_data;
          this.auth.saveToken(customer_data['token']);
          var gtmTag = {
            'event': 'user-login',
            'user_data': {
              'email': this.customer_data['email'],
              'login_medium': this.customer_data['login_medium'],
              'login_activity': this.customer_data['login_activity']
          }}
          this.gtmService.pushTag(gtmTag);
          this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
          this.ProductService.header.next(true);
          this.ProductService.footer.next(true);
          this.cartSubscription = this.cartService.getCartData().subscribe((cart_data: any) => {
            this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
          });
          window.location.href = '/account';
          //this.router.navigate(['/account']);

        }else if(typeof customer_data['error'] !='undefined')
        {
          this.error = customer_data['error'];
          document.getElementById('resultLoading').style.display = "none";
        }
      }));
    }
  }
  submitSignupForm(){
    document.getElementById('resultLoading').style.display = "block";

    var request_data = {};
    request_data['register_email'] = this.signupForm.value.register_email;
    request_data['register_name'] = this.signupForm.value.register_name;
    request_data['register_password'] = this.signupForm.value.register_password;
    request_data['register_country_code'] = '91';
    request_data['register_telephone'] = this.signupForm.value.register_telephone;
    request_data['gender'] = this.signupForm.value.gender;
    request_data['email'] = this.signupForm.value.register_email;
    request_data['password'] = this.signupForm.value.register_password;
    request_data['direct_login'] = 1;
    this.allSubscription.push(this.checkoutService.addCustomerChk(request_data).subscribe(response => {
      this.allSubscription.push(this.checkoutService.login(request_data).subscribe((customer_data:any[]) => {
        if(typeof customer_data['email'] !='undefined')
        {
          this.customer_data = customer_data;
          this.auth.saveToken(customer_data['token']);
          var gtmTag = {
            'event': 'user-login',
            'user_data': {
              'fname': response['fname'],
              'telephone': response['telephoneCode'],
              'email': this.customer_data['email'],
              'login_medium': this.customer_data['login_medium'],
              'login_activity': 'signup'
          }}
          console.log('gtm in save oken page')
          this.gtmService.pushTag(gtmTag);
          this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
          this.ProductService.header.next(true);
          this.ProductService.footer.next(true);
          this.cartSubscription = this.cartService.getCartData().subscribe((cart_data: any) => {
            this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
          });
          window.location.href = '/account';
          //this.router.navigate(['/account']);

        }else if(typeof customer_data['error'] !='undefined')
        {
          this.error = customer_data['error'];
          document.getElementById('resultLoading').style.display = "none";
        }
      }));
    }));
  }
  alreadyAUser(){
    this.step = 1;
    this.view = 1;
  }
  checkloginotp()
  {
    document.getElementById('resultLoading').style.display = "block";

    var request_data = {};
    request_data['email'] = this.loginForm.value.login_email;
      this.checkoutService.getotp(request_data).subscribe(responses => {
        this.otp = responses['otp'];
        this.telephone = responses['telephone'];
        document.getElementById('resultLoading').style.display = "none";
      });
      this.view = 3;
      this.loginOTPForm = new FormGroup({
        'login_OTP_email' : new FormControl(request_data['email'],[Validators.required,Validators.email]),
        'login_otp_password' : new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
        'matchotp' : new FormControl(this.otp)
      });
  }
  submitLoginwithOTPForm()
  {
    document.getElementById('resultLoading').style.display = "block";
    var request_data = {};
    request_data['email'] = this.loginOTPForm.value.login_OTP_email;
    request_data['direct_login'] = 1;
    if(this.loginOTPForm.value.login_otp_password==this.otp)
    {
      this.checkoutService.loginotp(request_data).subscribe((customer_data:any[]) => {
        if(typeof customer_data['email'] !='undefined')
        {
          this.customer_data = customer_data;
          this.auth.saveToken(customer_data['token']);
          var gtmTag = {
            'event': 'user-login',
            'user_data': {
              'email': this.customer_data['email'],
              'login_medium': this.customer_data['login_medium'],
              'login_activity': this.customer_data['login_activity']
          }}
          this.gtmService.pushTag(gtmTag);
          this.gtmService.pushTag({"pageType": "login",'event': 'checkout'});
          this.ProductService.header.next(true);
          this.ProductService.footer.next(true);
          this.cartSubscription = this.cartService.getCartData().subscribe((cart_data: any) => {
            this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
          });
          window.location.href = '/account';
          //this.router.navigate(['/account']);

        }else if(typeof customer_data['error'] !='undefined')
        {
          this.error = customer_data['error'];
          document.getElementById('resultLoading').style.display = "none";

        }
      });
    }else
    {
      document.getElementById('resultLoading').style.display = "none";

      this.error = 'Please enter correct OTP';
      this.loginOTPForm = new FormGroup({
        'login_OTP_email' : new FormControl(request_data['email'],[Validators.required,Validators.email]),
        'login_otp_password' : new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
        'matchotp' : new FormControl(this.otp)
      });
    }
  }
  resendcheckloginotp()
  {
    document.getElementById('resultLoading').style.display = "block";
    this.otp = '';
    this.telephone = '';
    var request_data = {};
    request_data['email'] = this.loginForm.value.login_email;
      this.checkoutService.getotp(request_data).subscribe(responses => {
        document.getElementById('resultLoading').style.display = "none";
        this.otp = responses['otp'];
        this.telephone = responses['telephone'];
      });
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
    if(this.cartSubscription)
    {
      this.cartSubscription.unsubscribe();

    }
  }
}
