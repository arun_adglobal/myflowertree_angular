import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { LoginComponent } from './login.component';
import { CartLayoutModule } from '../cart-layout.module';
@NgModule({
declarations: [ LoginComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: LoginComponent }
]), CommonModule, ReactiveFormsModule,CartLayoutModule ]
})
export class LoginModule{

}