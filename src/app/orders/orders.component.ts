import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute,Router} from '@angular/router';
import { MyprofileService } from '../myprofile/myprofile.service';
import { combineLatest } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders:any = [];
  orderid:number = 0;
  queryParams = {};
  url:any = '';
  constructor(private route:ActivatedRoute, 
    private router:Router, 
    private myprofileService:MyprofileService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
    combineLatest([this.route.params, this.route.queryParams]).subscribe(
      ([params, qparams]) => { 
        this.url = this.router.url.replace("/", "");
        this.url = this.url.split("?")[0];
        this.myprofileService.getOrders().subscribe((order:any) => {
          this.orders = order;
        });
      });
    }
  }
}
