import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthGuardService } from '../auth-guard.service';
import { OrdersComponent } from './orders.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ OrdersComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: OrdersComponent,canActivate:[AuthGuardService] },
    { path: 'buy-again', component: OrdersComponent,canActivate:[AuthGuardService] },
    
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class OrdersModule{

}