import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ProductService } from '../miscellaneous/product/product.service';
import { FileUploadService } from "../shared/file-upload.service";
import { ActivatedRoute} from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
declare var $:any;
@Component({
  selector: 'app-personalised',
  templateUrl: './personalised.component.html',
  styleUrls: ['./personalised.component.css']
})
export class PersonalisedComponent implements OnInit {
  CDN_PATH  = environment.CDN_PATH;
  safeUrl: SafeResourceUrl;
  images = [];
  filesToUpload: Array<File> = [];
  orderid = '';
  suborderid = '';
  message = '';
  error = '';
  subscriptions : Array<Subscription> = [];

  constructor(public sanitizer: DomSanitizer,
    private ProductService: ProductService,
    private route: ActivatedRoute,
    public fileUploadService: FileUploadService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.ProductService.header.next(false);
     this.ProductService.footer.next(false);
     combineLatest(this.route.params).subscribe(
      ([params]) =>{
        this.orderid = params['orderid']; 
        this.suborderid = params['suborderid'];         
    });
  }
  personalizedFormSubmit(){
    if(this.filesToUpload.length==0)
    {
      this.error = "You have not selected any image to upload";
    }else{
      if (isPlatformBrowser(this.platformId)) {
        $("#resultLoading").css('display','block');
      }
      var request_data = {};
      request_data['suborderid'] = this.suborderid;
      request_data['orderid'] = this.orderid;
      request_data['images'] = this.filesToUpload;
      this.subscriptions.push(
        this.fileUploadService.personalized(request_data).subscribe((response) => {
          this.message = "Image has been uploaded";
          if (isPlatformBrowser(this.platformId)) {
            $("#resultLoading").css('display','none');
          }
        })
      );
    }

  }
  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.filesToUpload.push(file);
  }
  AddMore() {
    if(this.images.length < 4)
    {
      this.images.push(this.images.length + 1);
    }
  }
  remove(element){
    this.images.splice(element, 1);
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }

  ngOnDestroy(){
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
    this.subscriptions = null;
  }
}
