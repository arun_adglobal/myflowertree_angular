import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { PersonalisedComponent } from './personalised.component';
import { CartLayoutModule } from '../cart-layout.module';

@NgModule({
declarations: [ PersonalisedComponent],
imports: [ RouterModule.forChild([
    { path: '', component: PersonalisedComponent},
]), CommonModule, ReactiveFormsModule,CartLayoutModule ]
})
export class PersonalisedModule{

}