import { Component, OnInit, OnDestroy, ViewChild,ElementRef, Inject, PLATFORM_ID } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { ActivatedRoute,Params } from '@angular/router';
import { environment } from '../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
declare var $:any;
declare const getDevice : any;

@Component({
  selector: 'app-blogheader',
  templateUrl: './blog-header.component.html',
  styleUrls: ['./blog-header.component.css']
})
export class BlogHeaderComponent implements OnInit,OnDestroy {
  @ViewChild('blogsearchquery') searchstring:ElementRef;
  safeUrl: SafeResourceUrl;
  menu: any = {};
  tmp_input = '';
  cartCount = 0;
  mobileaccount='';
  deviceType='Desktop';
  CDN_PATH  = environment.CDN_PATH;
  CDN_DOMAIN  = environment.CDN_DOMAIN;
  APIURL  = environment.API_URL;
  header = 'two';
  href: string = "";
  deliverylocation = 'no';
  cssDy:any="";
  currentRoute: string;
  filter_city_name = '';
  constructor(public auth: AuthenticationService,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    @Inject(PLATFORM_ID) private platformId: object) {

     }
 
  ngOnInit() {

    
    if (isPlatformBrowser(this.platformId)) {
    this.deviceType = getDevice();  
    }    
    this.route.queryParams.subscribe((params: Params)=>
    {

      if(typeof(params['utm_source']) !='undefined')
      {
        if (isPlatformBrowser(this.platformId)) {
          localStorage.setItem('utm_source', params['utm_source']);
        }
      }
      if(typeof(params['utm_medium']) !='undefined')
      {
        if (isPlatformBrowser(this.platformId)) {
          localStorage.setItem('utm_medium', params['utm_medium']);
        }
        
      }
      if(typeof(params['utm_source']) !='undefined' && typeof(params['utm_medium']) !='undefined')
      {
        var source:any = params;
        if (isPlatformBrowser(this.platformId)) {
          localStorage.setItem('source', source);
        }
      }
    });
    if (isPlatformBrowser(this.platformId)) {
    $(document).ready(function(e) {
    
      $('.responsive_popup a').click(function(e) {
        e.preventDefault();
              $('.nav').fadeToggle(600);
          });
          });
         
    }
    this.cssDy = { blog_style : this.getSafeCDNUrl('css/blog_style.css.gz'), owlcarousel : this.getSafeCDNUrl('css/owl.carousel.css.gz'),owltheme : this.getSafeCDNUrl('css/owl.theme.css.gz')};

  } 

  searchBlog(){
   // console.log(this.searchstring.nativeElement.value);
    window.location.href = '/blog?s='+encodeURIComponent(this.searchstring.nativeElement.value);
  }
  ngOnDestroy():void {
  }

  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
 
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
}
