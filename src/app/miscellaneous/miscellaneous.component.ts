import { Component, OnInit,OnDestroy } from '@angular/core';
import { MiscellaneousService } from './miscellaneous.service';
import {NavigationEnd, Router } from '@angular/router';
import { ProductService } from '../miscellaneous/product/product.service';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';

//declare var $: any;

@Component({
  selector: 'app-miscellaneous',
  templateUrl: './miscellaneous.component.html',
  styleUrls: ['./miscellaneous.component.css'],
  providers:[MiscellaneousService]
})
export class MiscellaneousComponent implements OnInit,OnDestroy{
  categoryShow = false;
  productShow = false;
  pageNotFound = false;
  showFooter = true;
  private routeSubscription:Subscription;
  private showFooterSubscription:Subscription;
  private pageTypeSubscription:Subscription;
  private childSubscription:Subscription;
  constructor(private misc: MiscellaneousService,
    private ProductService: ProductService,
    private NgxSpinner:NgxSpinnerService,
    private router: Router) { }
  ngOnInit() {
    this.NgxSpinner.show();
   // this.ProductService.header.next(true);
   // this.ProductService.footer.next(true);
   this.routeSubscription = this.router.events.subscribe(async (event:NavigationEnd) => {
     if(event instanceof NavigationEnd){
       var slug = event.url.replace("/", "");
       slug = slug.split("?")[0];
       this.categoryShow = false;
       this.productShow = false;
       this.pageNotFound = false;
       let isPageData = await this.getPageTypeDetails(slug);
       // Handling async call
       if(this.childSubscription && isPageData){
        this.childSubscription.unsubscribe();
      }
     }
  })
  //  this.activatedRouted.queryParams.subscribe(params => {

    var slug = this.router.url.replace("/", "");
     slug = slug.split("?")[0];
     this.categoryShow = false;
     this.productShow = false;
     this.pageNotFound = false;
     this.pageTypeSubscription = this.misc.getPageType(slug).subscribe(page_data => {
       if(page_data[0]=='category_id')
       {
        this.pageNotFound = false;
        this.categoryShow = true;
       }else if(page_data[0]=='product_id')
       {
       this.productShow = true;
       }else{
         this.pageNotFound = true;
       }
     });
    // })


  //  })
   this.showFooterSubscription = this.ProductService.footer.subscribe(flag =>
    {
         this.showFooter = flag;
    }
  );


  }
  getPageTypeDetails(slug) {
    return new Promise((resolve, reject) => {
     this.childSubscription = this.misc.getPageType(slug).subscribe(page_data => {
        if (page_data[0] == 'category_id') {
          this.pageNotFound = false;
          this.categoryShow = true;

          //console.log('load category page');
        } else if (page_data[0] == 'product_id') {
          this.productShow = true;
          //console.log('load product page');
        } else {
          this.pageNotFound = true;
        }
        resolve(page_data);
      }, (err) => {
        reject(err);
      });
    });
  }
  ngOnDestroy():void {
    if(this.showFooterSubscription){
      this.showFooterSubscription.unsubscribe();
    }
    if(this.pageTypeSubscription){
      this.pageTypeSubscription.unsubscribe();
    }
    if(this.routeSubscription){
      this.routeSubscription.unsubscribe();
    }
    this.routeSubscription = null;
    this.pageTypeSubscription = null;
    this.showFooterSubscription = null;
  }
}
