import { Component, OnInit, Inject, PLATFORM_ID, HostListener, OnDestroy } from '@angular/core';
import { CategoryService } from './category.service';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { ProductService } from '../product/product.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { SEOService } from '../../seo.service';
import { environment } from '../../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';
declare function gtag_productClick(data): any;
declare function gtag_contentGrouping(category_name): any;
declare function gtag_productImpressions(data): any;
declare const getDevice : any;
// declare var $:any;
import* as $ from 'jquery';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [CategoryService]
})
export class CategoryComponent implements OnInit,OnDestroy {
  category_slug:string = '';
  category_url:string = '';
  safeUrl: SafeResourceUrl;
  safetext:SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  queryParams = {};
  category_data:any = {};
  custom_layout:any = '';
  custom_layout_product:any = [];
  faqs:any = [];
  blogposts:any = [];
  quick_links:any = [];
  nearby_locations:any = [];
  products:any = [];
  deviceType='Desktop';
  showContent  = false;
  notScrolly = true;
  notEmptyProducts = true;
  offset:number = 0;
  limit = 20;
  can_url:any = '';
  breadcrumbSchema = {};
  itemListSchema = {};
  productSchema = {};
  faqSchema = {};
  category_header_banners = [];
  cssDy:any={};
  array = [];
  sum = 100;
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = "";
  isDataFetched : boolean;
  navigationSubscription:Subscription;
  productsSubscription:Subscription;
  routerSubscription:Subscription;
  allSubscription:Array<Subscription> = [];
  defaultImage = this.CDN_PATH+'2020/myflowertree-loader.jpg';
  totalCount: number;
  constructor(
    private categoryService: CategoryService,
    private ProductService: ProductService,
    private NgxSpinner:NgxSpinnerService,
    private router:Router,private gtmService: GoogleTagManagerService,
    private seoservice: SEOService,
    public sanitizer: DomSanitizer,
    @Inject(PLATFORM_ID) private platformId: object) {
      this.appendItems(0, this.sum);

     }

async ngOnInit() {
  this.resize();
  //  this.ProductService.header.next(true);
   this.ProductService.footer.next(true);
  this.cssDy = { birthdaycss : this.getSafeCDNUrl('2019/birthday/stylesheets/birthday.css.gz'),popperminjs : this.getSafeCDNUrl('live-new/js/popper.min.js.gz')};
    //  combineLatest([this.route.params, this.route.queryParams]).subscribe(
        if(this.productsSubscription){
          this.productsSubscription.unsubscribe();
        }
   let subscription = await this.getCategoryProductDetails() as Subscription;
    this.router.events.subscribe(async (evt) => {
    if (evt instanceof NavigationStart) {
      subscription.unsubscribe();
      }
    if (evt instanceof NavigationEnd) {
      this.productsSubscription =  await this.getCategoryProductDetails() as Subscription;
    }
    });

      let scope = this;
  $( window ).ready(function() {
    $(document).on("click", "#links a", function(event){
      event.preventDefault();
      let url = $(this).attr('href');
      url = url.replace('https://www.myflowertree.com','');
      scope.router.navigateByUrl(url);
//       // window.location.replace(url);
//       scope.isDataFetched = false;
//       scope.offset = 0;
//       scope.category_slug = url.replace("/", "");
//       scope.category_url = scope.category_slug.split("?")[0];
//       scope.queryParams['page'] = 1;
// let qparams = {};
//       scope.getCategoryData(qparams['sort'],qparams['order'],qparams['type'],qparams['price']);
     //console.log(' anchor tage click');
    });

    $(document).on("click", "#container a", function(event){
      event.preventDefault();
      let url = $(this).attr('href');
      url = url.replace('https://www.myflowertree.com','');
      scope.router.navigateByUrl(url);
      // window.location.replace(url);
//       scope.isDataFetched = false;
//       scope.offset = 0;
//       scope.category_slug = url.replace("/", "");
//       scope.category_url = scope.category_slug.split("?")[0];
//       scope.queryParams['page'] = 1;
// let qparams = {};
//       scope.getCategoryData(qparams['sort'],qparams['order'],qparams['type'],qparams['price']);
     //console.log(' anchor tage click');
    });



  });



   }

   @HostListener('window:resize',['$event'])
    resize(event?) {
        //console.log('event=s====',event);
        const width  = event ? event.target.innerWidth : window.innerWidth;
        if(width < 767) {
          this.totalCount = 2;
        } else {
          this.totalCount = 4;
        }
    }


   showBottomContent()
   {
    if (isPlatformBrowser(this.platformId)) {
      this.showContent = !this.showContent;
      if(this.showContent)
      {
        $("#showBottomContent").css('display','block');
      }else if(! this.showContent)
      {
        $("#showBottomContent").css('display','none');
      }
      }
   }
   addItems(startIndex, endIndex, _method) {
    for (let i = 0; i < this.sum; ++i) {
      this.array[_method]([i, " ", this.generateWord()].join(""));
    }
  }

  appendItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, "push");
  }

  prependItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, "unshift");
  }

  onScrollDown() {
    console.log("scrolled down!!");

    // add another 20 items
    const start = this.sum;
    this.sum += 20;
    this.appendItems(start, this.sum);

    this.direction = "down";
  }

  onUp() {
    console.log("scrolled up!");
    const start = this.sum;
    this.sum += 20;
    this.prependItems(start, this.sum);

    this.direction = "up";
  }
  generateWord() {
    // return chance.word();
   }

   toggleModal() {
     // = !this.modalOpen;
   }


  async onScroll() {
    if(this.notScrolly && this.notEmptyProducts)
    {
      if(this.productsSubscription){
        this.productsSubscription.unsubscribe();
       }
    //  this.spinner.show();
      this.offset = this.offset + 20;
      this.notScrolly = false;
     if(this.routerSubscription){
      this.routerSubscription.unsubscribe();
     }
     if(this.navigationSubscription){
      this.navigationSubscription.unsubscribe();
     }
     await this.getCategoryProducts(this.queryParams['sort'],this.queryParams['order'],this.queryParams['type'],this.queryParams['price']);
    }

  }
  getCategoryProducts(sort?,order?,type?,price?,offset?) {
    return new Promise((resolve,reject)=>{
    // this.queryParams = {}
    this.queryParams['sort'] = sort;
    this.queryParams['order'] = order;
    this.queryParams['type'] = type;
    this.queryParams['price'] = price;
    this.queryParams['offset'] = this.offset;
    if(offset >= 0) {
      this.queryParams['offset'] = offset
      this.offset = offset;
    }
    this.productsSubscription = this.categoryService.getCategoryProducts(this.category_slug,this.queryParams).subscribe((products : any[]) => {
      this.isDataFetched = true;
      this.NgxSpinner.hide();
      if(products?.length===0 || !products)
      {
        this.notEmptyProducts = false;
      } else {
        this.notEmptyProducts = true;
      }
      var current_page = '';
      if((this.queryParams['page'] !='1') && (products.length!=0))
      {
        current_page = '?page='+this.queryParams['page'];
        this.seoservice.setTitle(`Page ${this.queryParams['page']} of : `+this.category_data.meta_title);
      }
      if(this.category_data.custom_canonical !="")
      {
        this.can_url = this.category_data.custom_canonical+current_page;
        this.seoservice.createLinkForCanonicalURL({href: "https://www.myflowertree.com/"+this.can_url});
      }else{
        this.can_url = this.category_url+current_page;
        this.seoservice.createLinkForCanonicalURL({href: "https://www.myflowertree.com/"+this.can_url});
      }
      this.seoservice.createItemPropURL({href: "https://www.myflowertree.com/"+this.can_url});


      if((this.queryParams['page'] !='1') && (products.length!=0))
      {
        var previous_page = '';
        if(this.category_data.custom_canonical !="")
        {
          previous_page = '/'+this.category_data.custom_canonical+'?page='+(this.queryParams['page']-1);
          if(this.queryParams['page'] == 2)
          {
            previous_page = '/'+this.category_data.custom_canonical;
          }
        }else{
          previous_page = '/'+this.category_url+'?page='+(this.queryParams['page']-1);
          if(this.queryParams['page'] == 2)
          {
            previous_page = '/'+this.category_url;
          }
        }
        this.seoservice.createLinkForCanonicalPrevURL({href: previous_page});
      }
      if((this.queryParams['page']*(this.limit)) < this.category_data.product_total)
			{
        var next_page = '';
        if(this.category_data.custom_canonical !="")
        {
          next_page = '/'+this.category_data.custom_canonical+'?page='+(parseInt(this.queryParams['page'])+1);
        }else{
          next_page = '/'+this.category_url+'?page='+(parseInt(this.queryParams['page'])+1);
        }
        this.seoservice.createLinkForCanonicalNextURL({href: next_page});
			}

      this.products = this.products.concat(products);

      if(products?.length!=0)
      {
        this.itemListSchemaOfPage();
      }
      this.notScrolly  = true;
     /* var datalayer_productImpressions = {
        'products' :  this.products,
        'category_id' : this.category_data['category_id'],
        'category_name': this.category_data['full_category_name']
      }; */
     // gtag_productImpressions(datalayer_productImpressions);
     var gtmTag = {
      "event" : "categoryLagyLoad",
      "pageType":"product-impressions",
      'products': [...this.products],
      'category_id': this.category_data['category_id'],
      'category_name': this.category_data['full_category_name']
      }
     // console.log('gtm tag in get cateogyr product page ',gtmTag);
    if(this.seoservice.isBrowserPlatform()){
      this.gtmService.pushTag(gtmTag);
    }
     resolve(this.productsSubscription)
     })
    });

  }

  productClick(product_id)
  {
    _.forEach(this.products,(product)=>{
      if(product['product_id']==product_id)
      {
       var gtmTag =  {
        'event': 'productClick',
        'ecommerce': {
          'click': {
            'actionField': {'list': product['cate_name']},
            'products': [{
              'name': product['product_name'],
              'id': product['product_id'],
              'price': product['price'],
              'brand': 'MYFLOWERTREE',
              'category': product['cate_name'],
              'variant':  product['product_type'],
              'position': product['sort_order'],
            }]
          }
        },
        'eventCallback': function() {
        }
      }
      //console.log('gtm tag in product click page');
      if(this.seoservice.isBrowserPlatform()){
        this.gtmService.pushTag(gtmTag);
      }
      }
    });
    //this.router.navigate(['/'+datalayer_productClicked.href, { cate: datalayer_productClicked.queryParams}]);
  }


  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }

  createUrl(key)
  {
    return '/'+key;
  }

  SEO()
  {
   // this.seoservice.addCSSLink({href: "https://d3cif2hu95s88v.cloudfront.net/live-site-2016/live-new/css/nav.css.gz"});

    this.seoservice.setTitle(this.category_data.meta_title);
    this.seoservice.setMeta({ property:"og:title", content:this.category_data.meta_title});
    this.seoservice.setMeta({ name:"twitter:title", content:this.category_data.meta_title});

    this.seoservice.setMeta({id:"desc", name:"description", content:this.category_data.meta_description});
    this.seoservice.setMeta({property:"og:description", content:this.category_data.meta_description});
    this.seoservice.setMeta({name:"twitter:description", content:this.category_data.meta_description});

    if(this.category_data.meta_keyword !='')
    {
      this.seoservice.setMeta({name:"keywords", content:this.category_data.meta_keyword});
    }

    this.seoservice.setMeta({property:"og:image", content:this.category_data.banner});
    this.seoservice.setMeta({property:"og:url", content:"https://www.myflowertree.com/"+this.category_slug});
    this.seoservice.setMeta({name:"twitter:url", content:"https://www.myflowertree.com/"+this.category_slug});
    //this.seoservice.createLinkForCanonicalURL({href: "https://www.myflowertree.com/"+this.category_slug});
  }
  breadcrumbSchemaOfPage(){
    var breadcrumbs = []
    var i = 0;
    for(let bread of this.category_data.breadcrumbs)
    {
      if(this.category_data.breadcrumbs.length == i+1)
      {
        breadcrumbs.push({
          "@type": "ListItem",
          "position": i,
          "name": bread['text'],
          "item":{
            "@type" : "Thing",
            "@id" : bread['href']+'#'
          }
        });
      }else{
        breadcrumbs.push({
          "@type": "ListItem",
          "position": i,
          "name": bread['text'],
          "item":{
            "@type" : "Thing",
            "@id" : bread['href']
          }
        });
      }

      i++;
    }
    this.breadcrumbSchema = {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [...breadcrumbs]
    }
    breadcrumbs = null;
  }
  itemListSchemaOfPage(){
    var itemListElement = [];
    var j = 1;
    _.forEach(this.products,(product)=>{
      itemListElement.push({
        "@type": "ListItem",
        "position": j,
        "url": "https://www.myflowertree.com/"+product['keyw']
      });
      j++;
    this.itemListSchema = {
      "@context": "https://schema.org",
      "@type": "ItemList",
      "url": "https://www.myflowertree.com/"+this.category_url,
      "numberOfItems":  this.category_data['product_total'],
      "itemListElement": [...itemListElement]
    }
    });
    itemListElement = null;
  }
  productSchemaOfPage(){
    if(this.category_data['custom_rating'] !="")
    {
      this.productSchema = {
        "@context": "https://schema.org",
        "@type": "Product",
        "name": this.category_data['heading_title'].trim(),
        "aggregateRating": {
          "@type": "AggregateRating",
          "bestRating": 5,
          "ratingCount": this.category_data['custom_review'],
          "ratingValue": this.category_data['custom_rating']
        },
        "offers": {
          "@type": "AggregateOffer",
          "highPrice": parseInt(this.category_data['maxrange']),
          "lowPrice": parseInt(this.category_data['minrange']),
          "offerCount": this.category_data['product_total'],
          "priceCurrency": "INR"
        }
      }
    }
  }

  faqSchemaofPage(){
    var faq = [];
    _.forEach(this.faqs,(faqItem)=>{
      faq.push({
        "@type": "Question",
        "name": faqItem['question'],
        "acceptedAnswer": {
          "@type": "Answer",
          "text": faqItem['answer'].replace('<p>','').replace('</p>','')
        }
      });
    this.faqSchema = {
      "@context": "https://schema.org",
      "@type": "FAQPage",
      "mainEntity": [...faq]
    }

    });
    faq = null;
  }

  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  getSafetext(text:string = ''){
    this.safetext= this.sanitizer.bypassSecurityTrustResourceUrl(text);
    return this.safetext;
  }
  getCssUrl(url)
  {
    return this.CDN_PATH+url;
  }


  getCategoryData(sort?,order?,type?,price?,offset?) {
    //REPEATING AGAIN AND AGAIN
     return new Promise(async (resolve,reject)=>{

      this.navigationSubscription = this.categoryService.getCategoryPageData(this.category_slug).subscribe((page_data) => {
        this.products = [];
        this.category_data = page_data[0];

        if(Object.keys(this.category_data).length != 0)
        {
            var gtmTags = {
            "event" : "categoryViewAngular",
            'category_id': this.category_data['category_id'],
            'category_name': this.category_data['full_category_name'],
            'pageType':'category-new-detail'
            }
            this.gtmService.pushTag(gtmTags);

          this.SEO();
          this.breadcrumbSchemaOfPage();
          this.productSchemaOfPage();
          this.faqs = this.category_data['faqs'];
          if(this.faqs?.length !=0)
          {
            this.faqSchemaofPage();
          }
        }
        this.custom_layout = '';
        this.custom_layout_product = '';
        if(Object.keys(page_data).length != 0 &&  (page_data[0]['custom_layout']!='&lt;p&gt;&lt;br&gt;&lt;/p&gt;') && typeof(price)=='undefined')
        {
         /* this.categoryService.getCategoryCustomroducts(this.category_slug).subscribe(page_data => {

          });*/
          // console.log('custome');
          this.custom_layout = page_data[0]['custom_layout'];
          this.custom_layout_product = page_data[0]['custom_layout_product'];
          //console.log(this.custom_layout_product);

        }
        this.category_header_banners = this.category_data['category_header_banners'];
        //console.log('cat banner ',this.category_header_banners);
        var gtmTag =  {'contentGrouping': this.category_data['full_category_name']}
        if(this.seoservice.isBrowserPlatform()){
          this.gtmService.pushTag(gtmTag);
        }
       });
       let productSub =  await this.getCategoryProducts(sort,order,type,price,offset);

       this.allSubscription.push(this.categoryService.getBottomContent(this.category_slug).subscribe((bottomcontent) => {
          //this.faqs = bottomcontent['faqs'];
          this.blogposts = bottomcontent['blogs'];
          /*if(this.faqs?.length !=0)
          {
            this.faqSchemaofPage();
          }
          */
          this.quick_links = bottomcontent['quick_links'];
          this.nearby_locations = bottomcontent['nearby_locations'];
         // console.log("this.quick_links "+ this.quick_links);
       }));


       if (isPlatformBrowser(this.platformId)) {
        this.deviceType = getDevice();
        if(this.deviceType=='Mobile')
         {
          $("#showBottomContent").css('display','none');
         }
        }
        resolve(productSub);
     })
  }

   getCategoryProductDetails() {
    return new Promise(async (resolve,reject)=>{
      let qparams = {};
      this.custom_layout='';
      let paramString = '{';
      const paramsUrl= this.router.url.split('?')[1]?.split('&&') || [];
      paramsUrl.forEach(item=>{
        const paramArray = item.split('=');
        paramArray[0] = '"'+paramArray[0]+'"';
        paramArray[1] = '"'+paramArray[1]+'"';
        paramString += paramArray.join(':');

      })
      paramString+= '}';
      //qparams = JSON.parse(paramString);
      qparams = paramString;

       this.isDataFetched = false;
       this.NgxSpinner.show();
       this.offset = 0;
       this.category_slug = this.router.url.replace("/", "");
       this.category_url = this.category_slug.split("?")[0];
       this.queryParams['page'] = 1;
       if(typeof(qparams['page']) !="undefined")
       {
         this.queryParams['page'] = qparams['page'];
         if((this.queryParams['page'] - 1) * this.limit>0){
           this.offset = this.limit*(this.queryParams['page'] - 1);
         }else{
           this.offset = (this.queryParams['page'] - 1) * this.limit;
         }
       }
       this.can_url = this.category_url;
      let data = await this.getCategoryData(qparams['sort'],qparams['order'],qparams['type'],qparams['price']);
        resolve(data);
    })
  }
  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
    this.category_data = null;
    this.products = null;
    this.faqs = null;
  }
}
