import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxSpinnerModule } from 'ngx-spinner';

import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';

import { DomSanitizer } from '@angular/platform-browser'
import { PipeTransform, Pipe } from "@angular/core";

import { MiscellaneousComponent } from './miscellaneous.component';
import { CategoryComponent } from './category/category.component';
import { CategorybannerComponent } from './categorybanner/categorybanner.component';
import { ProductComponent } from './product/product.component';
import { AddonComponent } from './product/addon/addon.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { MainLayoutModule } from '../main-layout.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { AuthGuardService1 } from '../auth-guard.service';
@Pipe({ name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@NgModule({
declarations: [ MiscellaneousComponent,SafeHtmlPipe,CategoryComponent,CategorybannerComponent,ProductComponent,AddonComponent,PageNotFoundComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: MiscellaneousComponent,canActivate:[AuthGuardService1] }
]), CommonModule, ReactiveFormsModule, NgxJsonLdModule,InfiniteScrollModule,LazyLoadImageModule,
NgxSpinnerModule,NgxUsefulSwiperModule,MainLayoutModule, NgxSkeletonLoaderModule]
})
export class MiscellaneousModule{

}

