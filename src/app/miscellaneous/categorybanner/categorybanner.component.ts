import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
// declare var $: any;
import* as $ from 'jquery';
@Component({
  selector: 'app-categorybanner',
  templateUrl: './categorybanner.component.html',
  styleUrls: ['./categorybanner.component.css']
})
export class CategorybannerComponent implements OnInit {
  CDN_PATH  = environment.CDN_PATH;
  @Input() bannerData : any;
  category_data : any;
  category_header_banners = [];

  constructor(private route: Router) {}

  ngOnInit() {
    this.category_data = this.bannerData;
    this.category_header_banners = this.category_data['category_header_banners'];
    let scope = this;
    $( window ).ready(function() {
      $('#container a').click(function(event) {
        event.preventDefault();
        let url = $(this).attr('href');
        url = url.replace('https://www.myflowertree.com','');
        scope.route.navigateByUrl(url); 
        // window.location.replace(url);
       console.log(' anchor tage click');
      });
      $('#cat_headerbanner a').click(function(event) {
        event.preventDefault();
        let url = $(this).attr('href');
        url = url.replace('https://www.myflowertree.com','');
        scope.route.navigateByUrl(url); 
        // window.location.replace(url);
       console.log(' anchor tage click');
      });

      
    }); 
  
   }


   ngOnChanges(changes) {
     if(changes && changes.bannerData.previousValue !== undefined && JSON.stringify(changes.bannerData.previousValue) !== JSON.stringify(changes.bannerData.currentValue)) {
       this.category_data = changes.bannerData.currentValue;
       this.category_header_banners = this.category_data['category_header_banners'];

     } 
   }
   


}
