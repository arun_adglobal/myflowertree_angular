import { Injectable,EventEmitter } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable({providedIn: 'root'})
export class MiscellaneousService {
  //catePage = new EventEmitter<boolean>();
 // productPage = new EventEmitter<boolean>();
 APIURL  = environment.API_URL;
  constructor(private http: HttpClient) {}
  
  getPageType(slug) {
    return this.http.get(this.APIURL+'api/pageType?page='+slug, {
      withCredentials: true
    }).pipe(map(posts => {
      return posts;
     }));
  }

}
