import { Component, OnInit,OnDestroy } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { ProductService  } from '../product.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { Subscription } from 'rxjs';
import { ViewportScroller } from '@angular/common';
import { CartService } from '../../../cart/cart.service';

@Component({
  selector: 'app-addon',
  templateUrl: './addon.component.html',
  styleUrls: ['./addon.component.css']
})
export class AddonComponent implements OnInit,OnDestroy {
  addOnDisplay = false;
  addon_data = {};
  carttype = '';
  topselling = [];
  sweets = [];
  dry_friuts = [];
  diwalispecial = [];
  chocolates = [];
  rakhispecial = [];
  cakes = [];
  flowers = [];
  gifts =  [];
  greeting_cards = [];
  forher = [];
  forhim = [];
  added_addonProducts = [];
  products = [];
  choosed_product_count:number = 0;
  choosed_product_amount:number = 0;
  product_data = {};
  fragment: string = 'section1';
  subscriptions : Array<Subscription> = [];

  //private productSelectedSubscribe:Subscription;
  private addONSubscribe:Subscription;

  constructor(private ProductService:ProductService, 
    private router: Router,
    private cartService:CartService,

    private gtmService: GoogleTagManagerService,
    public route: ActivatedRoute,
    private viewportScroller: ViewportScroller) { }

  ngOnInit() {

  // this.productSelectedSubscribe =  this.ProductService.productSelected.subscribe(productSelected =>
  //         {
  //           this.product_data = productSelected;
  //           this.carttype = productSelected['name'];

  //           if(productSelected['product_type'] !="International")
  //           {
  //             this.processAddON();
  //           }
  //         }
  //     );
    this.subscriptions.push(
      this.ProductService.showAddon.subscribe(product_data =>
          {
            this.product_data = product_data;
            
            if(product_data['product_type'] !="International")
            {
            // console.log('test');
              this.processAddON();
            }else
            {
              this.goToCart();
            }
            //this.addOnDisplay = true;
            
            //console.log(Object.keys(this.addon_data).length);

          // this.addOnDisplay = true;
            //this.ProductService.hideProduct.next(true);


          //   if(Object.keys(this.addon_data).length) {
          //     this.addOnDisplay = true;
          //     this.ProductService.hideProduct.next(true);
          //  }else
          //  {
          //     this.goToCart();
          //  } 
            
          }
        )
      );
     // this.route.fragment.subscribe(fragment => { this.fragment = fragment; });
  }

  processAddON(){
  this.subscriptions.push(
    this.ProductService.getProductAddOn(this.product_data).subscribe(addon_data => {
  
      this.addon_data = addon_data;
      if(Object.keys(this.addon_data).length) {
        this.addOnDisplay = true;
        this.ProductService.hideProduct.next(true);
      }else
      {
          this.goToCart();
      } 
      
      
      if(this.addon_data['topselling'] !==undefined)
      {
        this.topselling = this.addon_data['topselling'];
        this.getAddonProducts(this.topselling);
      }
      if(this.addon_data['sweets'] !==undefined)
      {
        this.sweets = this.addon_data['sweets'];
        this.getAddonProducts(this.sweets);
      }
      if(this.addon_data['dry_friuts'] !==undefined)
      {
        this.dry_friuts = this.addon_data['dry_friuts'];
        this.getAddonProducts(this.dry_friuts);
      }
      if(this.addon_data['diwalispecial'] !==undefined)
      {
        this.diwalispecial = this.addon_data['diwalispecial'];
        this.getAddonProducts(this.diwalispecial);
      }
      if(this.addon_data['chocolates'] !==undefined)
      {
        this.chocolates = this.addon_data['chocolates'];
        this.getAddonProducts(this.chocolates);
      }
      if(this.addon_data['rakhispecial'] !==undefined)
      {
        this.rakhispecial = this.addon_data['rakhispecial'];
        this.getAddonProducts(this.rakhispecial);
      }
      if(this.addon_data['cakes'] !==undefined)
      {
        this.cakes = this.addon_data['cakes'];
        this.getAddonProducts(this.cakes);
      }
      if(this.addon_data['flowers'] !==undefined)
      {
        this.flowers = this.addon_data['flowers'];
        this.getAddonProducts(this.flowers);
      }
      if(this.addon_data['gifts'] !==undefined)
      {
        this.gifts = this.addon_data['gifts'];
        this.getAddonProducts(this.gifts);
      }
      if(this.addon_data['greeting_cards'] !==undefined)
      {
        this.greeting_cards = this.addon_data['greeting_cards'];
        this.getAddonProducts(this.greeting_cards);
      }
      if(this.addon_data['forher'] !==undefined)
      {
        this.forher = this.addon_data['forher'];
        this.getAddonProducts(this.forher);
      }
      if(this.addon_data['forhim'] !==undefined)
      {
        this.forhim = this.addon_data['forhim'];
        this.getAddonProducts(this.forhim);
      }
     })
    )
  }

  goToCart(){
    if(Object.keys(this.product_data).length) 
    {
      if(this.added_addonProducts.length)
      {
        var cart_data = {};
        cart_data['product_id'] = this.added_addonProducts.join("-");
        cart_data['key'] = this.product_data['product_id'];
        cart_data['quantity'] = 1;
        cart_data['addon'] = 'Addon';
          this.addONSubscribe = this.ProductService.addOnToCart(cart_data).subscribe((data:any) => {
            if(data.length > 0)
            {
                for(let details of data)
                {
                  this.gtmService.pushTag({'event': 'addToCartAddOn', 'products':details});
                  this.gtmService.pushTag({'event': 'weAddToCartWithAddON'});   
                }

                this.cartService.getCartData().subscribe((cart_data: any) => {
                  this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
                }
                );
            } 
            window.location.href = '/cart';
            //this.router.navigate(['/cart']);                  
          });
      }else{
        window.location.href = '/cart';
        //this.router.navigate(['/cart']);
      }
    }
    //previously navigation was here to add to cart
  }
  onClick(elementId: string): void { 
    this.fragment = elementId; 
    this.viewportScroller.scrollToAnchor(elementId);
}
  ngOnDestroy():void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
    this.subscriptions = null;
   // this.productSelectedSubscribe.unsubscribe();
   if(this.addONSubscribe)
   {
     this.addONSubscribe.unsubscribe();
   }
  }

  addAddonProduct(product_id) {
    if(this.added_addonProducts.includes(product_id))
    {
      var index = this.added_addonProducts.indexOf(product_id);
      if (index > -1) {
        this.added_addonProducts.splice(index, 1);
      }
    }else
    {
      this.added_addonProducts.push(product_id);
    }
    var p_count = 0;
    var p_amount = 0;
    for(let p of this.products)
    {
      if(this.added_addonProducts.includes(p.product_id))
      {
        p_count += 1;
        p_amount += p.price;     
      }
    }
    this.choosed_product_count = p_count;
    this.choosed_product_amount = p_amount; 
  }
  temp_array = [];
  getAddonProducts(data = [])
  {
    for(let rs of data)
    {
      if(! this.temp_array.includes(rs.product_id))
      {
        this.temp_array.push(rs.product_id);
        this.products.push({'product_id':rs.product_id, 'price':rs.price});
      }
      
    }

    


  }





}
