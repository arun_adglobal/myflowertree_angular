import { Component, OnInit, ViewChild, ElementRef,OnDestroy, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { FormBuilder, FormGroup } from "@angular/forms";
import { ProductService } from './product.service';
import { CartService } from '../../cart/cart.service';

import { combineLatest } from 'rxjs';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { SEOService } from '../../seo.service';
import { SwiperOptions } from 'swiper';
import { FileUploadService } from "../../shared/file-upload.service";
import { environment } from '../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';

declare const setRecentlyViewedProduct : any;
declare const getRecentlyViewedProduct : any;

//declare const getproducttimer : any;
declare var $:any;
declare const getDevice : any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit,OnDestroy {

  product_slug:string = '';
  queryParams = {};
  product_data:any = {};
  reviewss:any = [];
  checkavail:any = {};
  relatedpro:any = [];
  result_1:any = {};
  result_2:any = {};
  messageontexts:any = {};
  hours:string = '';
  minutes:string = '';
  seconds:string = '';
  sss:string = '';
  product_options = [];
  product_upgrades:any = [];
  productDisplay = true;
  cate = '';
  filter_city='';
  breadcrumbSchema = {};
  countdown = "";
  timer:string ='';
  countdowntoggle = 'YES';
  CDN_PATH  = environment.CDN_PATH;
  productImageForm: FormGroup;
  productPageSchema= {};
  deviceType = '';

  price_tmp = 0;
  special_tmp = 0;
  mainproduct_id = 0;
  best_products = [];
  //product_banner = [];
  interval:any;
  subscriptions : Array<Subscription> = [];

  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    spaceBetween: 5,
	  loop: true,
    centeredSlides: true,
   // autoHeight: true,
   // allowTouchMove: true,
    autoplay: {
      delay: 4500,
      disableOnInteraction: false
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
  };

  isDataFetched:boolean;
  @ViewChild('messageoncake') messageoncake:ElementRef;
  @ViewChild('messageontext') messageontext:ElementRef;

  private hideProductSubscribe:Subscription;
  messgtext="";

  constructor(
    public fb: FormBuilder,
    private route: ActivatedRoute,
    private ProductService:ProductService,
    private cartService:CartService,
    private router:Router,
    private gtmService: GoogleTagManagerService,
    public fileUploadService: FileUploadService,
    private seoservice: SEOService,
    private NgxSpinner:NgxSpinnerService,

    @Inject(PLATFORM_ID) private platformId: object) {
      // Reactive Form
      this.productImageForm = this.fb.group({
        avatar: [null]
      })
     }

  ngOnInit() {
  this.subscriptions.push(
    combineLatest([this.route.params, this.route.queryParams]).subscribe(
      ([params, qparams]) => {

        //console.log("product page fired");
       // this.product_slug = params['string'];
      // console.log();


        this.product_slug =  ((this.router.url.replace("/", "")).split("?"))[0];
         this.cate = qparams['cate'];
        this.isDataFetched = false;
        this.subscriptions.push(this.ProductService.getProductPageData(this.product_slug, this.cate).subscribe(page_data => {
          this.isDataFetched = true;
          this.NgxSpinner.hide();
          this.product_data = page_data;
          //console.log(this.product_data.options);
          for(let po of this.product_data.options)
          {
            if(po['type']=='textarea')
            {
              this.messgtext = 'yes';
            }
          }
          //console.log(page_data, 'hi i m page data===>>>');
          
          /*
          this.product_banner.push({
            'alt_tag' : page_data['alt_tag'],
            'name' : page_data['name'],
            'image' : page_data['image1'],
          });
          if(page_data['additional_images'].length > 0)
          {
            for(let additional_image of page_data['additional_images'])
            {
            this.product_banner.push({
              'alt_tag' : page_data['alt_tag'],
              'name' : page_data['name'],
              'image' : additional_image,
            });
            }
          }

          */

          this.mainproduct_id  = this.product_data.product_id;
          this.product_upgrades = page_data['product_upgrades'];
          this.price_tmp = this.product_data.price;
          this.special_tmp = this.product_data.special;
          this.deviceType = localStorage.getItem('deviceType');


          this.timer = this.product_data.timer;
          this.getproducttimer();
          //     this.interval = setInterval(this.getproducttimer, 1000);

          if (isPlatformBrowser(this.platformId)) {
            setRecentlyViewedProduct(this.product_data.product_id); // set recently viewed Product
          }
          this.SEO();
          this.gtag_productDetail();
          this.productSchemaOfPage();
          this.breadcrumbSchemaOfPage();
          this.ProductService.productSelected.next(this.product_data);
          this.NgxSpinner.hide();
        }));
        this.subscriptions.push(this.ProductService.getproReviews(this.product_slug).subscribe(reviewss => {
          this.reviewss = reviewss;
        }));

        this.filter_city = '';
        if(localStorage.getItem('filter_city')!=null)
        {
          this.filter_city = localStorage.getItem('filter_city');
        }

       this.subscriptions.push(this.ProductService.getproductsrelated(this.product_slug, localStorage.getItem('deviceType'),this.filter_city).subscribe(relatedpro => {
          if(relatedpro && typeof(relatedpro['product_related']) !='undefined')
          {
            this.relatedpro = relatedpro['product_related'];
          }
          if(typeof(relatedpro['best_products']) !='undefined')
          {
            this.best_products = relatedpro['best_products'];
          }
        }));
        }
      )
  );

  this.subscriptions.push(
    this.hideProductSubscribe = this.ProductService.hideProduct.subscribe(flag =>
        {
          this.productDisplay = false;
        }
      )
  );


  }

  ngOnDestroy():void {
    _.forEach(this.subscriptions,(sub)=>sub.unsubscribe());
    if(this.hideProductSubscribe){
      this.hideProductSubscribe.unsubscribe();
    }
    clearInterval(this.interval);
    this.subscriptions = null;
    this.hideProductSubscribe = null;
    this.product_data = null;
  }
  /*getproducttimer()
  {
    console.log("timer fired");
    var countDownDate = new Date(this.timer).getTime();
    console.log('111');
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    if (distance < 0) {
      this.countdowntoggle = 'NO';
    }else
    {
      if(hours.toString().length==1){  this.hours = '0'+hours; } else { this.hours = ''+hours; }
      if(minutes.toString().length==1){  this.minutes = '0'+minutes; } else { this.minutes = ''+minutes; }
      if(seconds.toString().length==1){   this.seconds = '0'+seconds; } else { this.seconds = ''+seconds; }
      this.countdown = this.hours +":"+ this.minutes +":"+ this.seconds + " Hrs";
    }
  } */
  getproducttimer()
  {
    //console.log("timer fired");
    var countDownDate = new Date(this.timer).getTime();
    if (isPlatformBrowser(this.platformId)) {
      this.interval = setInterval(()=> {

      var now = new Date().getTime();
      var distance = countDownDate - now;
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      if (distance < 0) {
        this.countdowntoggle = 'NO';
      }else
      {
        if(hours.toString().length==1){  this.hours = '0'+hours; } else { this.hours = ''+hours; }
        if(minutes.toString().length==1){  this.minutes = '0'+minutes; } else { this.minutes = ''+minutes; }
        if(seconds.toString().length==1){   this.seconds = '0'+seconds; } else { this.seconds = ''+seconds; }
        this.countdown = this.hours +":"+ this.minutes +":"+ this.seconds + " Hrs";
      }

      }, 1000);
    }

  }

  message_suggestion_1 = '';
  message_suggestion_2 = '';
  public checkpincodeAvailability(pincodeavail:any): void {
    this.message_suggestion_1 = '';
    this.message_suggestion_2 = '';
    if(this.product_data.getCourier)
    {
      var ordertype = 'courier';
    }else
    {
      var ordertype = 'express';
    }
    if(pincodeavail!='')
    {
      if(ordertype=='express')
      {
          this.subscriptions.push(
            this.ProductService.getPincodesDetail(pincodeavail).subscribe(async (result_1:any[]) => {

              if(result_1.length > 0)
              {

           await this.getPindCodeAvailability(pincodeavail,ordertype,result_1)
          }else
          {
          this.subscriptions.push(
            this.ProductService.getPincodesAvailabilityIS(pincodeavail,this.product_data.product_id).subscribe((result_3:any[]) => {
              //console.log(result_3);
              if(result_3.length>0)
              {
                var url = '/gifts';
                var message_suggestion = "This product is not available at given pincode. <a target='_blank' href='"+url+"'>Check Available Products</a>";
                this.checkavail['message_suggestion'] = message_suggestion;
                this.message_suggestion_2 = message_suggestion;
                this.checkavail['message_success'] = '';
                this.checkavail['message_error'] = '';
              }else
              {
                this.checkavail['message_suggestion'] = '';
                this.message_suggestion_2 = '';
                this.checkavail['message_success'] = '';
                this.checkavail['message_error'] = "Ahh. We don't deliver here yet.";
              }

            })

          );
          }
        })
      );
      }else if(ordertype == 'courier')
      {
          this.subscriptions.push(
            this.ProductService.getPincodesAvailabilityIS(pincodeavail,this.product_data.product_id).subscribe((result_1:any[]) => {

                if(result_1.length>0)
                {
                  var availbility = [];

                  availbility['product_id'] = this.product_data.product_id;
                  availbility['product_type'] = ordertype;
                  availbility['same_day_limit'] = result_1[0].tat_datetime/24;
                  availbility['city_name'] = result_1[0].area;
                  //same_day_limit = $result_1.tat_datetime/24;


                  var dateis = this.productAvailablityDate(availbility);
                  this.checkavail['message_success'] = 'Earliest Available Delivery: '+dateis;
                  this.checkavail['message_error'] = '';
                }else
                {
                  this.checkavail['message_error'] = "Ahh. We don't deliver here yet.";
                  this.checkavail['message_success'] = '';
                }
            })
          );
      }
    }else
    {

        this.checkavail['message_error'] = "Please enter pincode";
        this.checkavail['message_success'] = '';
    }
    //console.log(ordertype);

    //this.product_data.price = parseInt(this.product_data.price)+parseInt('100');
   }
     getPindCodeAvailability(pincodeavail,ordertype,result_1){
       return new Promise((resolve,reject)=>{
        this.subscriptions.push(this.ProductService.getPincodesAvailability(pincodeavail,this.product_data.product_id).subscribe((result_2:any[]) => {
          // console.log(result_2);
          // console.log('b');
            if(result_2.length>0)
            {
              var availbility = [];

              availbility['product_id'] = this.product_data.product_id;
              availbility['product_type'] = ordertype;
              availbility['same_day_limit'] = result_1[0].same_day_limit;
              availbility['city_name'] = result_1[0].area;

              const datesss =  this.productAvailablityDate(availbility);
              //console.log(datesss);
              this.checkavail['message_success'] = 'Earliest Available Delivery: '+datesss;
              this.checkavail['message_suggestion'] = '';
              this.message_suggestion_1 = '';
              this.checkavail['message_error'] = '';
            }else
            {

              var link = 'product/search?pincode='+pincodeavail+'&product_type='+this.product_data.product_type;
              var message_suggestion = "This product is not available at given pincode. <a target='_blank' href='"+link+"'>Check Available Products</a>";
              this.checkavail['message_suggestion'] = message_suggestion;
              this.message_suggestion_1 = message_suggestion;
              this.checkavail['message_success'] = '';
              this.checkavail['message_error'] = '';
            }
           resolve(true);
      }))
       })
     }
  public productAvailablityDate(availbility:any): void {

      //console.log(availbility);
              var addholiday = 0;
              var adddays_new = 0;

              if(availbility.product_type=='courier')
              {

                var d = new Date();
                var weekday: number = d.getDay();

                var checkworkingday = weekday + parseInt(this.product_data.expected_delivery);
                if(weekday == 6){
                 var addholidays = 1;
                }else if(weekday == 7){
                  var addholidays = 1;
                }else{
                  var addholidays = 0;
                }
                addholiday = addholidays;
              }

              if(availbility.product_type=='courier')
              {
                var expected_delivery = 0;
              }else
              {
                var expected_delivery = parseInt(this.product_data.expected_delivery);
              }


              var today:any = 'Today';
              var Tomorrow:any = 'Tomorrow';

              var date = new Date();
              var year = date.getFullYear();
              var months = date.getMonth()+1;
              var days  = date.getDate();
              var hourss = date.getHours();
              var minutes = date.getMinutes()

              var month = ("0" + months).slice(-2);
              var day = ("0" + days).slice(-2);
              var hours = ("0" + hourss).slice(-2);


              var earliest_delivery_date = expected_delivery;

              var datecontroll = 0;
              if (earliest_delivery_date == 0 || earliest_delivery_date == null )
              {
                var add_days :number = earliest_delivery_date + datecontroll;


                var dayss = parseInt(day) + add_days;

                var earliest_date = dayss+'-'+month+'-'+year;

              }
              else
              {

                var add_days : number = expected_delivery + datecontroll;

                var dayss = parseInt(day) + add_days;

                var earliest_date = dayss+'-'+month+'-'+year;

              }

              var setdate = earliest_date;

              if(availbility.product_type=='courier')
		          {

                      var currentTime = parseInt(hours);
                      var setdate_array = setdate.split("-");

                      var lastDay =  new Date(date.getFullYear(), date.getMonth() + 1, 0);
                      var lstday = lastDay.getDate();

                      var dataYear = setdate_array[2];
                      var dataMonth = parseInt(setdate_array[1]);

                    if(this.product_data.product_type!='International')
                    {
                        if(currentTime >= 17)
                        {
                          date.setDate(date.getDate() + 1);


                          var curdate = date.getDate();
                          if(parseInt(day)== lstday && parseInt(hours) < 20)
                          {
                             dataMonth = Number(dataMonth)+ 1;
                          }
                        }
                        else
                        {
                          var curdate = parseInt(day);

                        }
                        var dataDay = curdate + parseInt(availbility.same_day_limit);

                        var dates = dataYear+'-'+dataMonth+'-'+dataDay;

                        var weekd = new Date(dates).getDay();

                        var d = new Date();
                        var addholidayssa: number = d.getDay();

                        if(addholidayssa==6 || addholidayssa==7)
                        {
                          var dataDay =  curdate + parseInt(availbility.same_day_limit) + 1;
                        }
                        else if(weekd==0){
                          var dataDay =  curdate +  parseInt(availbility.same_day_limit) + 1;
                        }else
                        {

                          var dataDay =  curdate +  parseInt(availbility.same_day_limit);

                        }

                    }


                      if(dataDay > 31)
                      {

                         dataMonth =   Number(dataMonth) + 1;

                        dataDay = dataDay - 31;
                      }

                      var calendar_date = dataYear+'/'+dataMonth+'/'+dataDay;




              }else if(availbility.product_type=='express')
              {
                      var currentTime = parseInt(hours);
                      var setdate_array = setdate.split("-");

                      var curTim = hours+':'+minutes;
                      var lastDay =  new Date(date.getFullYear(), date.getMonth() + 1, 0);
                      var lstday = lastDay.getDate();

                      var dataYear = setdate_array[2];
                      var dataMonth = Number(setdate_array[1]);
                      var datadays = setdate_array[0];
                      var city_array = [];
                      var citycheck = '';

                      this.subscriptions.push(
                        this.ProductService.getAllCloseCities().subscribe((closecity:any[]) => {

                          for(let clscity of closecity)
                          {

                            if(clscity.cityname == 'Nagpur')
                            {

                              citycheck = 'True';
                            }


                          }


                          //console.log(citycheck);

                            //console.log(array);
                          /*for(var i=0; i<closecity.length; i++){

                            var cityCheck = "'"+"dd"+"'";

                            city_array.push(closecity[i]['cityname']);
                          }*/
                          //column.push(closecity);

                          //return column;
                        })
                      );

                      if(currentTime >= availbility.same_day_limit)
                      {

                        if(availbility.product_type=='International')
                        {
                          var dataDay = parseInt(datadays);

                        }else
                        {
                          //console.log(datadays);
                          var dataDay = parseInt(datadays)+1;

                        }
                      }
                      else
                      {
                       //console.log('else');
                        if(curTim >= this.product_data.cutoff && this.product_data.cutoff!='' && curTim <= '20.00')
                        {
                          var dataDay = parseInt(datadays)+1;

                        }else{
                          var dataDay = parseInt(datadays);

                        }
                      }

                      calendar_date = dataYear+'/'+dataMonth+'/'+dataDay;

              }
              //console.log(dataDay);
              if(dataDay== parseInt(day))
              {
                return today;
              }else if(dataDay == parseInt(day)+1)
              {
                return Tomorrow;
              }else
              {
                var delivaerydate :any = '';
                var caldate =  new Date(calendar_date);
                var delivdate = caldate.getDate();
                var deldate = ("0" + delivdate).slice(-2);
                var monthname = caldate.toLocaleString('default', { month: 'long' })
                 delivaerydate =  deldate+' '+monthname;

                return delivaerydate;

              }


   }

   selectoptions(productprice,optionid){
    if(this.product_options.includes(optionid))
    {
      var index = this.product_options.indexOf(optionid);
      if (index > -1) {
        this.product_options.splice(index, 1);
      }
      if(this.product_data.special==null)
      {
        this.product_data.price = parseInt(this.product_data.price)-parseInt(productprice);
      }else
      {
        this.product_data.special = parseInt(this.product_data.special)-parseInt(productprice);
      }
    }else
    {
      this.product_options.push(optionid);
      if(this.product_data.special==null)
      {
        this.product_data.price = parseInt(this.product_data.price)+parseInt(productprice);
      }else
      {
        this.product_data.special = parseInt(this.product_data.special)+parseInt(productprice);
      }

    }
    //console.log(this.product_options);

   }

   index = 0;
   tmp_product_1 = [];
   tmp_product_2 = [];
   product_1 = [];
   product_2 = [];
   selectedoptions(optionid,item_index=0){
    this.product_options = [];
    if (isPlatformBrowser(this.platformId)) {
    $('input:checkbox').prop('checked', false);
    $('input:radio').prop('checked', false);
    }

    this.product_data.price = this.price_tmp;
    this.product_data.special = this.special_tmp;
    this.index = 0;

    this.tmp_product_1 = [];
    this.tmp_product_2 = [];
    for(let opriton of this.product_data.options)
    {
      if(opriton.type=='select')
      {
        this.index = this.index + 1;
        for(let selectoption of opriton.product_option_value)
        {
          if(this.index==1)
          {
            this.tmp_product_1.push(selectoption.product_option_value_id);
          }else if(this.index==2)
          {
            this.tmp_product_2.push(selectoption.product_option_value_id);
          }
        }
      }
    }


    for(let opriton of this.product_data.options)
    {
      if(opriton.type=='select')
      {
        for(let selectoption of opriton.product_option_value)
        {
          if((this.tmp_product_1.indexOf(parseInt(optionid,10)) != -1) && (optionid==selectoption.product_option_value_id))
            {
              this.product_1 = [];
              this.product_1.push(optionid);
              if(this.product_data.special==null)
              {
                this.product_data.price = parseInt(this.product_data.price) + parseInt(selectoption.price);
              }else
              {
                this.product_data.special = parseInt(this.product_data.special) + parseInt(selectoption.price);
              }
            }
            if((this.tmp_product_2.indexOf(parseInt(optionid,10)) != -1) && (optionid==selectoption.product_option_value_id))
            {
              this.product_2 = [];
              this.product_2.push(optionid);
              if(this.product_data.special==null)
              {
                this.product_data.price = parseInt(this.product_data.price) + parseInt(selectoption.price);
              }else
              {
                this.product_data.special = parseInt(this.product_data.special) + parseInt(selectoption.price);
              }
            }

            if((item_index==1 && (this.product_2[0]==selectoption.product_option_value_id)) || (item_index==2 && (this.product_1[0]==selectoption.product_option_value_id)))
            {
              if(this.product_data.special==null)
              {
                this.product_data.price = parseInt(this.product_data.price) + parseInt(selectoption.price);
              }else
              {
                this.product_data.special = parseInt(this.product_data.special) + parseInt(selectoption.price);
              }
            }

        }
      }
    }
    if(this.product_1.length !=0)
    {
      this.product_options.push(this.product_1[0]);
    }
    if(this.product_2.length !=0)
    {
      this.product_options.push(this.product_2[0]);
    }

   }

  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }

  getUpgrade(product_id){

    this.NgxSpinner.show();
    this.subscriptions.push(
      this.ProductService.getProductUpgrade(this.mainproduct_id, product_id, this.cate).subscribe(page_data => {
        //this.product_data.additional_images = [];
       this.product_data = page_data;
        this.NgxSpinner.hide();
        if(page_data['image2'] == "")
        {
          this.product_data['image2'] = page_data['image1'];
        }
        this.product_upgrades = page_data['product_upgrades'];

        this.price_tmp = this.product_data.price;
        this.special_tmp = this.product_data.special;

        this.timer = this.product_data.timer;

      })
    );
  }
  required_images = 0;
  required_text = 0;
  AddToCart(){
    this.required_images = 0;
    this.required_text = 0;
    var cart_data = {};

    cart_data['product_id'] = this.product_data.product_id;
    cart_data['key'] = this.product_data.product_id;
    cart_data['product_type'] = this.product_data.product_type;
    cart_data['quantity'] = 1;
    cart_data['cate_id'] = this.cate;
    cart_data['product_options'] = this.product_options;
    var tmp_array = [];
    for(let img of this.uploadedImageArray)
    {
      tmp_array.push(img['code']);
      tmp_array.push(img['option']);
    }
    cart_data['product_images'] = tmp_array;

    for(let po of this.product_data.options)
    {
      if(po['type']=='file')
      {
        this.required_images++;
      }
      if(po['type']=='textarea')
      {
        this.required_text++;
      }
    }
    if(this.uploadedImageArray.length != this.required_images)
    {
      this.fileUploadMsg = 'empty';
      if(localStorage.getItem('deviceType')=='Mobile')
      {
        window.scrollTo(0,600);
      }
    }

    if(this.required_text && (this.messageontext?.nativeElement.value==''))
    {
      if(this.fileUploadMsg != 'empty')
      {
        if(localStorage.getItem('deviceType')=='Mobile')
        {
          window.scrollTo(0,1100);
        }
      }
      this.TextUploadMsg = 'empty';
    }

    if (isPlatformBrowser(this.platformId)) {
      if(this.messgtext)
      {
        cart_data['producttext'] = [];
        cart_data['producttext'].push(this.messageontext.nativeElement.id);
        cart_data['producttext'].push(this.messageontext.nativeElement.value);
        if(this.messageontext.nativeElement.value)
        {
          this.TextUploadMsg = '';
        }
      }

    }
    //console.log(this.product_options);


    //console.log(this.TextUploadMsg);
    //console.log('cart data ',cart_data);
    //console.log('file upload ',this.fileUploadMsg);
    if(this.fileUploadMsg !="empty")
    {
      if(this.TextUploadMsg !="empty")
      {

        if (isPlatformBrowser(this.platformId)) {
          localStorage.setItem('messageoncake', this.messageoncake.nativeElement.value);
        }
       // console.log('dsdds');
        this.subscriptions.push(
          this.ProductService.addToCart(cart_data).subscribe(data => {
            if(this.seoservice.isBrowserPlatform()){
              this.gtmService.pushTag({'event': 'addToCart', 'product_options':data['product_option']});
              this.gtmService.pushTag({'event':'weAddToCart'});
            }
            this.ProductService.footer.next(false);

            this.cartService.getCartData().subscribe((cart_data: any) => {
              this.ProductService.headerCartCount.next({'headerCountTotal': cart_data[0]['cart_data'].length});
            }
            );

            
            



            this.ProductService.showAddon.next(this.product_data);
          })
        );
      }
    }

  }

  // Image Preview
  uploadedImageArray = [];
  imageCode = '';
  fileUploadMsg = '';
  TextUploadMsg ='';
  onUploadFile(event,product_option_id) {

    // var isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;

    //   if (isMobile) {
    //     $('html, body').animate({
    //       scrollTop: $("#imgll").offset().top-150
    //     }, 100);
    //   }
    //   $('#autocomplete').focus().css('border','1px solid red');

    // if (isPlatformBrowser(this.platformId)) {
    //   $("#callbackloader").css('display','block');
    // }
    this.NgxSpinner.show();
    const file = (event.target as HTMLInputElement).files[0];
    if(file)
    {
     
        this.productImageForm.patchValue({
          avatar: file
        });
        this.productImageForm.get('avatar').updateValueAndValidity()

        // File Preview
        const reader = new FileReader();

        reader.onload = () => {
          if (isPlatformBrowser(this.platformId)) {
          $("#filePreviewBlock"+product_option_id).css('display','block');
          $("#filePreview"+product_option_id).attr('src',reader.result as string);
          }
        }
        reader.readAsDataURL(file);
        var request_data = {};
        request_data['avatar'] = this.productImageForm.value.avatar;
        this.subscriptions.push(
          this.fileUploadService.productImageUpload(request_data).subscribe(dataa => {

            this.NgxSpinner.hide();
            //console.log(dataa);
            // if (isPlatformBrowser(this.platformId)) {
            // $("#callbackloader").css('display','none');
            // }
            
            if(dataa['code'] !="")
            {
              this.fileUploadMsg = 'success';
              this.uploadedImageArray = $.grep(this.uploadedImageArray, function(e){
                return e['option'] != product_option_id;
              });
              this.uploadedImageArray.push({
                'code': dataa['code'],
                'option': product_option_id,
              });
            }else{
              this.fileUploadMsg = 'fail';
            }
            //console.log("this.uploadedImageArray " + this.uploadedImageArray);
          })
        );
      }else
      {
        // if (isPlatformBrowser(this.platformId)) {
        //   $("#callbackloader").css('display','none');
        // }
        this.NgxSpinner.hide();
      }
  }
  gtag_productDetail()
  {
    var gtmTag = {
      'event': 'productViewAngular',
      'contentGrouping': this.product_data.category_name,
      'pageType':'product-detail',
        'ecommerce': {
          'detail': {
            'actionField': {'list': this.product_data.category_name},
            'products': [{
              'name': this.product_data.name,
              'product_type': this.product_data.product_type,
              'id': this.product_data.product_id,
              'price': this.product_data.price,
              'brand': 'MYFLOWERTREE',
              'category': this.product_data.category_name,
              'variant': this.product_data.product_type,
              'images': this.product_data.image1,
              'quantity': 1,
              'url' : this.product_data.href,
             }]
           }
         }
      }
      //console.log('Product page GTM Tag ',gtmTag);
      if(this.seoservice.isBrowserPlatform()){
        this.gtmService.pushTag(gtmTag);
      }
  }
  SEO()
  {
    this.seoservice.setTitle(this.product_data.meta_title);
    this.seoservice.setMeta({ property:"og:title", content:this.product_data.meta_title});
    this.seoservice.setMeta({ name:"twitter:title", content:this.product_data.meta_title});

    if(this.product_data.meta_description !='')
    {
    this.seoservice.setMeta({id:"desc", name:"description", content:this.product_data.meta_description});
    this.seoservice.setMeta({property:"og:description", content:this.product_data.meta_description});
    this.seoservice.setMeta({name:"twitter:description", content:this.product_data.meta_description});
    }
    if(this.product_data.meta_keyword !='')
    {
      this.seoservice.setMeta({name:"keywords", content:this.product_data.meta_keyword});
    }
    this.seoservice.setMeta({property:"og:image", content:this.product_data.image1});
    this.seoservice.setMeta({property:"og:url", content: "https://www.myflowertree.com/"+this.product_slug});
    this.seoservice.setMeta({name:"twitter:url", content: "https://www.myflowertree.com/"+this.product_slug});
    this.seoservice.createLinkForCanonicalURL({href: "https://www.myflowertree.com/"+this.product_slug});
  }
  breadcrumbSchemaOfPage(){
    var breadcrumbs = []
    var i = 0;
    for(let bread of this.product_data.breadcrumbs)
    {
      if(this.product_data.breadcrumbs.length == i+1)
      {
        breadcrumbs.push({
          "@type": "ListItem",
          "position": i,
          "name": bread['text'],
          "item":{
            "@type" : "Thing",
            "@id" : bread['href']+'#'
          }
        });
      }else{
        breadcrumbs.push({
          "@type": "ListItem",
          "position": i,
          "name": bread['text'],
          "item": {
            "@type" : "Thing",
            "@id" : bread['href']
          }
        });
      }

      i++;
    }
    this.breadcrumbSchema = {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": breadcrumbs
    }
  }
  removeTags(str)
{
if ((str===null) || (str===''))
return false;
else
str = str.toString();
return str.replace( /(<([^>]+)>)/ig, '');
}
  productSchemaOfPage(){

    var review = [];
    for(let r of this.reviewss)
    {
      var d = new Date(r['date_added']);
      var month_array = [1,2,3,4,5,6,7,8,9];
      var month:any = (d.getMonth()+1);
      if(month_array.includes(month) !=false)
      {
        month = '0'+month;
      }
      var date = d.getFullYear()+'-'+month+'-'+d.getDate();
      review.push( {
        "@type": "Review",
        "name": r['text'],
        "description": r['text'],
        "datePublished": date,
        "reviewRating": {
          "@type": "Rating",
          "worstRating": "1",
          "ratingValue": r['rating'],
          "bestRating": "5"
        },
        "author": {
          "@type": "Person",
          "name": r['author']
        }
      });
    }
    var price = parseInt(this.product_data.price);
     if(this.product_data.special)
    {
      price = this.product_data.special;
    }
    this.productPageSchema = {
      "@context": "https://schema.org/",
      "@type": "Product",
      "productID" : this.product_data.product_id,
      "name": this.product_data.name,
      "image": this.product_data.image1,
      "description": this.removeTags(this.product_data.description),
      "sku": this.product_data.product_id,
      "mpn": this.product_data.product_id,
      "brand": {
        "@type": "Thing",
        "name": "MyFlowerTree"
      },
      "review": review,
      "aggregateRating": {
        "@type": "AggregateRating",
        "reviewCount": Math.ceil(this.product_data.product_rating),
        "ratingValue": this.product_data.product_rating,
        "bestRating": "5",
        "worstRating": "1"
      },
      "offers": [
        {
          "@type": "Offer",
          "url": this.product_data.phref,
          "price": price,
          "priceCurrency": "INR",
          "itemCondition": "https://schema.org/NewCondition",
          "availability": "https://schema.org/InStock"
        }
      ]
    }
  }
  loadProduct(proUrl)
  {
    window.location.href = proUrl;
  }
}
