import { Injectable, EventEmitter, Inject, PLATFORM_ID } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import{map} from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';

@Injectable({providedIn: 'root'})
export class ProductService {
APIURL  = environment.API_URL;
  productSelected = new Subject<object>();
    showAddon = new Subject<object>();
  hideProduct = new Subject<boolean>();
  //catePage = new EventEmitter<boolean>();
  header = new Subject<boolean>();
  footer = new Subject<boolean>();
  headerCartCount = new Subject<object>();


  constructor(private http:HttpClient,
    @Inject(PLATFORM_ID) private platformId: object) { }
  getProductPageData(product_slug,category_id){    
    return this.http.get(this.APIURL+'api/product?product='+product_slug+'&cate='+category_id, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getProductUpgrade(mainproduct_id, product_id,category_id){    
    return this.http.get(this.APIURL+'api/productUpgrade?product='+mainproduct_id+'&upgrade_id='+product_id+'&cate='+category_id, {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getProductAddOn(product_data){
   
    var url = this.APIURL+'api/product/addon?producttype=express';
    url += '&product_type='+product_data.product_type;
    url += '&type='+product_data.name;
    url += '&manufacturer_id='+product_data.manufacturer_id;
    return this.http.get(url, {
      withCredentials: true
    }).pipe(map(data => {
      
      return data;
     }));
  }
  addToCart(cart_data){    
    var url = this.APIURL+'api/cart/add?quantity='+cart_data['quantity'];
    url += '&product_id='+cart_data.product_id;
    if(cart_data.cate_id !=undefined)
    {
      url += '&cate_id='+cart_data.cate_id;
    }
    if(cart_data.product_type != undefined)
    {
      url += '&product_type='+cart_data.product_type;
    }
    if(cart_data.addon != undefined)
    {
      url += '&addon='+cart_data.addon;
    }
    if(cart_data.key != undefined)
    {
      url += '&key='+cart_data.key;
    }
    if (isPlatformBrowser(this.platformId)) {
      if(localStorage.getItem('utm_source') !=null)
      {
        url += '&utm_source='+localStorage.getItem('utm_source');
        localStorage.removeItem('utm_source');
      }
    
      if(localStorage.getItem('utm_medium') !=null)
      {
        url += '&utm_medium='+localStorage.getItem('utm_medium');
        localStorage.removeItem('utm_medium');
      }
      if(localStorage.getItem('source') !=null)
      {
        url += '&source='+localStorage.getItem('source');
        localStorage.removeItem('source');
      }
    }
    let params = new HttpParams();
    if(cart_data.product_options != undefined)
    {
      params = params.append('product_options', cart_data['product_options'].join(','));
    }
    if(cart_data.product_images != undefined)
    {
      params = params.append('product_images', cart_data['product_images'].join(','));
    }
    if(cart_data.producttext != undefined)
    {
      params = params.append('producttext', cart_data['producttext'].join(','));
    }
    return this.http.get(url,{ params: params,withCredentials: true }).pipe(map(data => {
      return data;
     }));
  }
  addOnToCart(cart_data){    
    var url = this.APIURL+'api/cart/addon?quantity='+cart_data['quantity'];
    url += '&product_id='+cart_data.product_id;
    if(cart_data.cate_id !=undefined)
    {
      url += '&cate_id='+cart_data.cate_id;
    }
    if(cart_data.product_type != undefined)
    {
      url += '&product_type='+cart_data.product_type;
    }
    if(cart_data.addon != undefined)
    {
      url += '&addon='+cart_data.addon;
    }
    if(cart_data.key != undefined)
    {
      url += '&key='+cart_data.key;
    }

    let params = new HttpParams();
    if(cart_data.product_options != undefined)
    {
      params = params.append('product_options', cart_data['product_options'].join(','));
    }
    if(cart_data.product_images != undefined)
    {
      params = params.append('product_images', cart_data['product_images'].join(','));
    }
    if(cart_data.producttext != undefined)
    {
      params = params.append('producttext', cart_data['producttext'].join(','));
    }
    return this.http.get(url,{ params: params,withCredentials: true }).pipe(map(data => {
      return data;
     }));
  }
  getproReviews(product_slug){
    
    return this.http.get(this.APIURL+'api/productreview?product='+product_slug, {
      withCredentials: true
    }).pipe(map(revi => {
      return revi;
     }));
  }
  getBestSellerProducts(){    
    return this.http.get(this.APIURL+'api/bestSellerProducts', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }
  getRecentlyViewedProducts(products){    
    return this.http.get(this.APIURL+'api/recentlyViewedProducts?products='+products, {
      withCredentials: true
    }).pipe(map(revi => {
      return revi;
     }));
  }
  getproductsrelated(product_slug, devicetype,filter_city){
    return this.http.get(this.APIURL+'api/productalsolike?product='+product_slug+'&devicetype='+devicetype+'&filter_city='+filter_city, {
      withCredentials: true
    }).pipe(map(alsolike => {
      return alsolike;
     }));
  }
  getPincodesDetail(pincode){
    
    return this.http.get(this.APIURL+'api/pincodedetail?product='+pincode, {
      withCredentials: true
    }).pipe(map(pincheck => {
      return pincheck;
     }));
  }
  getPincodesAvailability(pincode,product_id){
    
    return this.http.get(this.APIURL+'api/pincodeAvail?pincode='+pincode+'&product='+product_id, {
      withCredentials: true
    }).pipe(map(pinAvail => {
      return pinAvail;
     }));
  }

  getAllCloseCities(){
    
    return this.http.get(this.APIURL+'api/closecity', {
      withCredentials: true
    }).pipe(map(pinAvail => {
      return pinAvail;
     }));
  }
  getPincodesAvailabilityIS(pincode,product_id){
    
    return this.http.get(this.APIURL+'api/pincodeAvailIS?pincode='+pincode+'&product='+product_id, {
      withCredentials: true
    }).pipe(map(pinAvail => {
      return pinAvail;
     }));
  }
}
