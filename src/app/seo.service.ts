import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { Title,Meta } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
   providedIn: 'root'
})
export class SEOService {
   constructor(private title: Title,
    @Inject(PLATFORM_ID) private platformId: object,
    private meta: Meta,
    @Inject(DOCUMENT) private doc) {
   }
   setTitle(title: string) {
      this.title.setTitle(title);
   }
   setMeta(obj: {}) {
    this.meta.updateTag(obj);
    }
   /*getPageTitle() {
      return this.title.getTitle();
   }*/
   createLinkForCanonicalURL(obj) {
      var element = document.querySelector('[rel="canonical"]');
      if (typeof(element) != 'undefined' && element != null)
      {
         element['href'] = obj.href;
      }else{
         let link: HTMLLinkElement = this.doc.createElement('link');
         link.setAttribute('rel', 'canonical');
         this.doc.head.appendChild(link);
         if(obj.href !='undefined')
         {
            link.setAttribute('href', obj.href);
         }
      }
   }
   createLinkForCanonicalPrevURL(obj) {
      var element = document.querySelector('[rel="prev"]');
      if (typeof(element) != 'undefined' && element != null)
      {
         element['href'] = obj.href;
      }else{
         let link: HTMLLinkElement = this.doc.createElement('link');
         link.setAttribute('rel', 'prev');
         this.doc.head.appendChild(link);
         if(obj.href !='undefined')
         {
            link.setAttribute('href', obj.href);
         }
      }
   }
   createLinkForCanonicalNextURL(obj) {
      var element = document.querySelector('[rel="next"]');
      if (typeof(element) != 'undefined' && element != null)
      {
         element['href'] = obj.href;
      }else{
         let link: HTMLLinkElement = this.doc.createElement('link');
         link.setAttribute('rel', 'next');
         this.doc.head.appendChild(link);
         if(obj.href !='undefined')
         {
            link.setAttribute('href', obj.href);
         }
      }
   }
   createItemPropURL(obj) {
      var element = document.querySelector('[itemprop="url"]');
      if (typeof(element) != 'undefined' && element != null)
      {
         element['href'] = obj.href;
      }else{
         let link: HTMLLinkElement = this.doc.createElement('link');
         link.setAttribute('itemprop', 'url');
         this.doc.head.appendChild(link);
         if(obj.href !='undefined')
         {
            link.setAttribute('href', obj.href);
         }
      }
   }
  /* addCSSLink(obj) {
      var element = document.querySelector('[href='+obj.href+']');
      if (typeof(element) == 'undefined' && element !== null)
      {
         let link: HTMLLinkElement = this.doc.createElement('link');
         link.setAttribute('rel', 'stylesheet');
         this.doc.head.appendChild(link);
         link.setAttribute('href', obj.href);
      }
   }
   removeCSSLink(obj) {
      var element = document.querySelector('[href='+obj.href+']');
      if (typeof(element) != 'undefined' && element != null)
      {
         element.remove();
      }
   } */
   isBrowserPlatform(){
    return (isPlatformBrowser(this.platformId));
   }
}
