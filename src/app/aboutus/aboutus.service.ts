import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import{map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AboutusService {
APIURL  = environment.API_URL;
  constructor(private http:HttpClient) { }

  getPageData(){    
    
    return this.http.get(this.APIURL+'api/pages?pages=4', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }



}
