import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DomSanitizer } from '@angular/platform-browser'
import { PipeTransform, Pipe } from "@angular/core";
import { MainLayoutModule } from '../main-layout.module';

import { AboutusComponent } from './aboutus.component';

@Pipe({ name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@NgModule({
declarations: [ AboutusComponent,SafeHtmlPipe ],
imports: [ RouterModule.forChild([
    { path: '', component: AboutusComponent }
]), CommonModule,MainLayoutModule ]
})
export class AboutusModule{

}