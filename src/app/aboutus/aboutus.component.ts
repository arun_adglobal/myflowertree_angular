import { Component, OnInit, Inject, PLATFORM_ID  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AboutusService } from './aboutus.service';
import { SEOService } from '../seo.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css'],
  providers:[AboutusService]
})

export class AboutusComponent implements OnInit {

  aboutusdata :any = [];
  description = '';
  allSubscriptions:Array<Subscription> = [];
  pageDataSubscription:Subscription;
  constructor(private route: ActivatedRoute,
    private seoservice: SEOService,
    private AboutusService:AboutusService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {

    this.pageDataSubscription = this.AboutusService.getPageData().subscribe(aboutusdata => {
      this.description = aboutusdata[0].description;
      this.SEO();
    });

  }
  SEO()
  {
    this.seoservice.setTitle("About Us");
}
  /*toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
  */
ngOnDestroy(){
  this.pageDataSubscription.unsubscribe();
}


}
