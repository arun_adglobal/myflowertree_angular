import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { OffersComponent } from './offers.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ OffersComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: OffersComponent }
]), CommonModule,ReactiveFormsModule,MainLayoutModule ]
})
export class OffersModule{

}