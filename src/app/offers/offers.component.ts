import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { OffersService } from './offers.service';
import { SEOService } from '../seo.service';
import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';

declare var $: any;
@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css'],
  providers:[OffersService]
})

export class OffersComponent implements OnInit {
  offersdata:any=[];
  tnc_text = '';
  subscription:Subscription;
  CDN_PATH  = environment.CDN_PATH;

  constructor(private route: ActivatedRoute, private OffersService:OffersService,private seoservice: SEOService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.subscription = this.OffersService.getOffers().subscribe(offersdata =>{
        this.offersdata = offersdata;
    });
    this.SEO();
  }

  getTNC(id){
    this.tnc_text = '';
    for(let offer of this.offersdata)
    {
      if(offer.id==id)
      {
        this.tnc_text = offer.tnc;
        break;
      }
    }
    document.getElementById('myModalSpecial').style.display = "block";
  }
  getCode(id){
    if (isPlatformBrowser(this.platformId)) {
    $(".copy_text").css('display','none');
    $(".splbutton").css('display','inline');
    }
    document.getElementById("text"+id).style.display = "none";
    document.getElementById(id).style.display = "block";
  }
  copyCode(inputElement){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Coupon Copied";
  }
  outFunc(){
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copy to clipboard";
  }
  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
  closeTNC() {
    document.getElementById('myModalSpecial').style.display = "none";
  }
  SEO()
  {
    this.seoservice.setTitle('Special Offers | MyFlowerTree');
  }

  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }
}
