import { Component, OnInit, OnDestroy, Inject, PLATFORM_ID } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { environment } from '../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
declare const getDevice : any;

@Component({
  selector: 'app-cartheader',
  templateUrl: './cart-header.component.html',
  styleUrls: ['./cart-header.component.css']
})
export class CartHeaderComponent implements OnInit,OnDestroy {
  safeUrl: SafeResourceUrl;
  menu: any = {};
  tmp_input = '';
  cartCount = 0;
  mobileaccount='';
  deviceType='Desktop';
  CDN_PATH  = environment.CDN_PATH;
  APIURL  = environment.API_URL;
  cssDy:any={};
  href: string = "";
  
  
  currentRoute: string;
  
  constructor(public auth: AuthenticationService,
    public sanitizer: DomSanitizer,
    @Inject(PLATFORM_ID) private platformId: object) {
      
      
     }
 
  ngOnInit() {
    this.cssDy = { bootstrapcss : this.getSafeCDNUrl('live-new/css/bootstrap.min.css.gz')};
    if (isPlatformBrowser(this.platformId)) {
    this.deviceType = getDevice();
    }
  } 
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }
  ngOnDestroy():void {
  }
}
