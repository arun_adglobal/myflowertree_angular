import { Component, OnInit } from '@angular/core';
import { ProductService } from '../miscellaneous/product/product.service';
import { Subscription } from 'rxjs';

declare const getRecentlyViewedProduct: any;

@Component({
  selector: 'app-recentlyviewed',
  templateUrl: './recentlyviewed.component.html',
  styleUrls: ['./recentlyviewed.component.css']
})
export class RecentlyviewedComponent implements OnInit {

  products:any = [];
  subscription:Subscription;
  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    if(getRecentlyViewedProduct() !="")
    {
      this.subscription = this.productService.getRecentlyViewedProducts(getRecentlyViewedProduct()).subscribe((products : any[]) => {
        this.products = products;
      });
    }
  }

  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }

}
