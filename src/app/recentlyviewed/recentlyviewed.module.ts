import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { RecentlyviewedComponent } from './recentlyviewed.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ RecentlyviewedComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: RecentlyviewedComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class RecentlyviewedModule{

}