import { Component, OnInit, Inject, PLATFORM_ID  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TestService } from './test.service';
import { SEOService } from '../seo.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css'],
  providers:[TestService]
})

export class TestComponent implements OnInit {
  
  array = [];
  sum = 100;
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = "";
  modalOpen = false;
  subscription:Subscription;
  constructor(private route: ActivatedRoute, 
    private seoservice: SEOService,
    private AboutusService:TestService,
    @Inject(PLATFORM_ID) private platformId: object) {
      this.appendItems(0, this.sum);

     }

  ngOnInit(): void {
    this.subscription = this.AboutusService.getPageData().subscribe(aboutusdata => {          
      this.SEO();
    });
    
    
  }
  addItems(startIndex, endIndex, _method) {
    for (let i = 0; i < this.sum; ++i) {
      this.array[_method]([i, " ", this.generateWord()].join(""));
    }
  }

  appendItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, "push");
  }

  prependItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, "unshift");
  }

  onScrollDown() {
    console.log("scrolled down!!");

    // add another 20 items
    const start = this.sum;
    this.sum += 20;
    this.appendItems(start, this.sum);

    this.direction = "down";
  }

  onUp() {
    console.log("scrolled up!");
    const start = this.sum;
    this.sum += 20;
    this.prependItems(start, this.sum);

    this.direction = "up";
  }
  generateWord() {
   // return chance.word();
  }

  toggleModal() {
    this.modalOpen = !this.modalOpen;
  }
  SEO()
  {
    this.seoservice.setTitle("About Us");
}
  /*toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
  */
  
  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }
  
}
