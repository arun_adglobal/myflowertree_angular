import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { DomSanitizer } from '@angular/platform-browser'
import { PipeTransform, Pipe } from "@angular/core";
import { MainLayoutModule } from '../main-layout.module';

import { TestComponent } from './test.component';

@Pipe({ name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@NgModule({
declarations: [ TestComponent,SafeHtmlPipe ],
imports: [ RouterModule.forChild([
    { path: '', component: TestComponent }
]), CommonModule,MainLayoutModule,InfiniteScrollModule ]
})
export class TestModule{

}