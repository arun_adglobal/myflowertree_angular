import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TermsOfUseComponent } from './terms-of-use.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ TermsOfUseComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: TermsOfUseComponent }
]), CommonModule,MainLayoutModule ]
})
export class TermsOfUseModule{

}