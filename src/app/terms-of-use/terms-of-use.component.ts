import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TermsOfUseService } from './terms-of-use.service';
import { isPlatformBrowser } from '@angular/common';
import { SEOService } from '../seo.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-terms-of-use',
  templateUrl: './terms-of-use.component.html',
  styleUrls: ['./terms-of-use.component.css'],
  providers:[TermsOfUseService]
})
export class TermsOfUseComponent implements OnInit {
  termsdata :any = [];
  subscription:Subscription;
  constructor(private route:ActivatedRoute,
    private seoservice: SEOService,
    private TermsOfUseService:TermsOfUseService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.SEO();
    this.subscription = this.TermsOfUseService.getPageData().subscribe(termsdata =>{
      this.termsdata = termsdata;
  });
  }
  SEO()
  {
    this.seoservice.setTitle("Terms & Conditions");
}
  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }

  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }

}
