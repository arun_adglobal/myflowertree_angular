import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { RefundPolicyService } from './refund-policy.service';
import { isPlatformBrowser } from '@angular/common';
import { SEOService } from '../seo.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-refund-policy',
  templateUrl: './refund-policy.component.html',
  styleUrls: ['./refund-policy.component.css'],
  providers:[RefundPolicyService]
})
export class RefundPolicyComponent implements OnInit {
  termsdata :any = [];
  subscription:Subscription;
  constructor(private route:ActivatedRoute,
    private seoservice: SEOService,
    private RefundPolicyService:RefundPolicyService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    this.SEO();
    this.subscription = this.RefundPolicyService.getPageData().subscribe(termsdata =>{
      this.termsdata = termsdata;
  });
  }
  SEO()
  {
    this.seoservice.setTitle("Refund Policy");
}
  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }

  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }

}
