import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RefundPolicyComponent } from './refund-policy.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ RefundPolicyComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: RefundPolicyComponent }
]), CommonModule,MainLayoutModule ]
})
export class RefundPolicyModule{

}