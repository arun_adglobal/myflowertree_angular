import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TrackorderComponent } from './trackorder.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ TrackorderComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: TrackorderComponent },
    { path: ':orderid', component: TrackorderComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class TrackorderModule{

}