import { Component, OnInit, ViewEncapsulation,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MyprofileService } from '../myprofile/myprofile.service';
import { CheckoutService } from '../checkout/checkout.service';
import { environment } from '../../environments/environment';
import { SEOService } from '../seo.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-trackorder',
  templateUrl: './trackorder.component.html',
  styleUrls: ['./trackorder.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class TrackorderComponent implements OnInit {
  trackorderForm: FormGroup;
  view = 1;
  courier={};
  express={};
  safeUrl: SafeResourceUrl;
  CDN_PATH  = environment.CDN_PATH;
  order_id={};
  email={};
  errormsg:string="";
  trackhtml:string="";
  trackcourhtml:string="";
  formSubmitted = false;
  cssDy:any={};

bannerlink:string = '';
bannerImage:string = '';
bannerTitle:string = '';
subscriptions : Array<Subscription> = [];

  constructor(private router:Router,private route:ActivatedRoute,  public sanitizer: DomSanitizer,
    private checkoutService:CheckoutService,private myprofileService:MyprofileService,private seoservice: SEOService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void { 
    this.cssDy = { trackcss : this.getSafeCDNUrl('trackorder/track.css')};
    this.express = '';
    this.courier = '';

    this.trackorderForm = new FormGroup({
      'orderid' : new FormControl(null,Validators.required),
      'email' : new FormControl(null, [Validators.required,Validators.email])
    });
    this.subscriptions.push( 
    this.route.params.subscribe(params => { 
      if(typeof(params['orderid']) !="undefined")
      {
        this.subscriptions.push( 
        this.checkoutService.getCustomer().subscribe((customer_data:any[]) => {
          if(typeof customer_data['email'] !='undefined')
          {
            this.trackorderForm.value.orderid = params['orderid'];
            this.trackorderForm.value.email = customer_data['email'];
            this.submittrackForm();
          }
        })
        );
      } 
    })
    );

    this.SEO();
    this.subscriptions.push( 
    this.checkoutService.getTrackOrderBanner().subscribe((data) => {
      if(Object.keys(data).length != 0)
      {
        this.bannerlink = data['link'];
        this.bannerTitle = data['title'];
        this.bannerImage = data['image'];

      }
    })
    );
  }
  valid_form = true;
  submittrackForm()
  {
    if(! this.trackorderForm.valid)
    {
      this.valid_form = false;
      this.trackorderForm.markAllAsTouched();
    }else{
      this.valid_form = true;
    }
    if(this.valid_form)
    {
    var request_data = {};
    request_data['orderid'] = this.trackorderForm.value.orderid;
    request_data['email'] = this.trackorderForm.value.email;
    this.subscriptions.push( 
      this.myprofileService.trackorder(request_data).subscribe(response => {
        
      //console.log(response);
      this.express = '';
      this.courier = '';
        if(response['express']=='' && response['courier']=='' )
        {
          this.formSubmitted = false;
          this.errormsg = 'Whoops. This does not matches our record. Please recheck your order and try again.';
        }else{
          this.formSubmitted = true;
        }


      if(response['courier']!='')
      {
        this.courier = response['courier'];
        var cour = response['courier'];
      }
      if(response['express']!='')
      {
        this.express = response['express'];
        var exx = response['express'];
      }
      //console.log(response['express']);
      var html = '';
      var chtml ='';
        if(response['express']!='')
        {
          var $k=0;
          for (const [$key, $fd] of Object.entries(exx)) 
          {
          for (const [$keys, $ssd] of Object.entries($fd)) 
            {
              $k++;
              var $inpro : {};
              var $inPre : {};
              var $outForD : {};
              var $delivered : {};
              
              var ordTime = $ssd[0]['shippingtimeslot'];
              //$ordTime1 = $ordTime-1;
            // $curtTime = date('H');	
            var date = new Date();
            var current_hour = date.getHours();
            var current_minutes = date.getMinutes();

              var d = new Date();
              var month = d.getUTCMonth() + 1; 
              if (month < 10) { month = 0 + month; }
              var day = d.getUTCDate();
              var year = d.getUTCFullYear();
            //var str = new String( month );
              
              
              var datecur = year+'-'+month+'-'+day;

              console.log(datecur);
              var ordTime = $ssd[0]['shippingtimeslot'].substr(0, 2);
              var $ordTime1 = ordTime-1;

              console.log($ordTime1);
              console.log($ssd[0]['deliverydate']);
              console.log(current_hour);

              var $curtTime = current_hour;	
              if($ssd[0]['order_status_id']=='7')
              {
                $inpro = '';
              }
              else if($ssd[0]['order_status_id']=='23')
              {
                $inpro = '';
              }else if($ssd[0]['order_status_id']=='21' && $ssd[0]['deliverydate'] > datecur)
              {

                $inpro = 'completed';
              }
              else if($ssd[0]['order_status_id']=='21' && $curtTime < $ordTime1 && datecur == $ssd[0]['deliverydate'])
              {
                $inpro = 'completed';
              }else if($ssd[0]['vender_id']!='0' && $ssd[0]['order_status_id']!='29' && $ssd[0]['order_status_id']!='17' && $curtTime >= $ordTime1)
              {
                $inpro = 'completed';
                $inPre = 'completed';
              }
              else if($ssd[0]['vender_id']!=0 && $ssd[0]['order_status_id']=='29')
              {
                $inpro = 'completed';
                $inPre = 'completed';
                $outForD = 'completed';
              }
              else if($ssd[0]['vender_id']!=0 && $ssd[0]['order_status_id']=='17')
              {

                $inpro = 'completed';
                $inPre = 'completed';
                $outForD = 'completed';
                $delivered = 'completed';
              }




              console.log($ssd[0]['order_status_id']);
              html += '<div class="track-wrap"><div class="status-wrap"> ';
              if($ssd[0]['order_status_id']=='1')
              {
                html+='<div class="highlight-text"><span class="not-paid">You have not yet paid for this order.</span> <a target="_blank" class="btn-paynow" routerLink="/paypage/'+request_data['orderid']+'" target="_blank">Pay Now</a></div>';
              }
              else if($ssd[0]['order_status_id']=='7' || $ssd[0]['order_status_id']=='23')
              {
                html+='<div class="highlight-text"><span class="canceled">This order is canceled.</span> <a target="_blank" routerLink="contact-us" class="link">Contact us</a> for further support';
              }
              else
              {
                html+='<span class="payment-done status"><span class="title">';
                    if($ssd[0]['order_status_id']=='7')
                    {
                      html+='Cancel';
                    }
                    else if($ssd[0]['order_status_id']=='23')
                    {
                      html+='Void';
                    }else
                    {
                      html+='Payment Received';
                    }

                html+='</span>';
                if($inpro=='completed')
                {
                  $inpro=='completed';
                }    
                html+='<span class="'+$inpro+' dd round-icon"></span>';
                html+='</span>';

                html+='<span class="preparation status"><span class="title">In Preparation</span>';
                if($inPre=='completed')
                {
                  $inPre=='completed';
                }else
                {
                  $inPre = 'half';
                }  
                html+='<span class=" '+$inPre+'  grey round-icon"></span></span>';
                html+='<span class="ofd status"><span class="title">Out For Delivery</span>';
                if($outForD=='completed')
                {
                  $outForD=='completed';
                }else
                {
                  $outForD = 'half';
                }  
                html+='<span class="'+$outForD+'  grey round-icon"></span></span>';

                html+='<span class="delivered status"><span class="title">Delivered</span>';
                if($delivered=='completed')
                {
                  $delivered=='completed';
                }else
                {
                  $delivered = 'half';
                } 
                html+='<span class="'+$delivered+' grey round-icon"></span></span>';

              }

              html+='</div></div>';
              
              html+='<div class="pro-items"><ul>';

              for (const [$keyss, $sd] of Object.entries($ssd)) 
              {
                html+='<li class="items"><div class="pro-img"><img loading="lazy"  src="'+$sd['image']+'"></div><div class="pro-desc"><div class="pro-name">'+$sd['name']+'</div><div class="qty">Qty: <span class="qty-item">'+$sd['quantity']+'</span></div><div class="price">Rs.'+Number($sd['total'])+'</div></div></li>';
              } 
              html+='</ul></div>';
              html+='<div class="delivery-details"><div class="del-date">Delivery Date : <span class="date">'+$ssd[0]['deliverydate']+'</span></div><div class="del-slot">Delivery Slot : <span class="slot">';
              if($ssd[0]['shippingtimeslot']=='23:30 - 00:30 hrs')
                { 
                  html+="23:00 - 23:59 hrs"; 
                }
                else{ 
                  html+=$ssd[0]['shippingtimeslot']; 
                } 
              html+='</span></div></div>';


            }
          }
          
        }
        this.trackhtml = html;
        //======================= Courier =======================//
        if(response['courier']!='')
        {
          for (const [$key, $csbD] of Object.entries(cour)) 
          {
          for (const [$keys, $ccSbd] of Object.entries($csbD)) 
            {
              $inpro = '';
              $inPre = '';
              $outForD = '';
              $delivered = '';
              if($ccSbd[0]['order_status_id']=='21')
              {
                $inpro = 'completed';
              }else if($ccSbd[0]['vender_id']!=0 && ($ccSbd[0]['order_status_id']=='2' || $ccSbd[0]['order_status_id']=='19' || $ccSbd[0]['order_status_id']=='15'))
              {
                $inpro = 'completed';
                $inPre = 'completed';
                $outForD = 'completed';
              }
              else if($ccSbd[0]['vender_id']!=0 && $ccSbd[0]['order_status_id']=='17')
              {
                $inpro = 'completed';
                $inPre = 'completed';
                $outForD = 'completed';
                $delivered = 'completed';
              }

            // var chtml = '';
              chtml+='<div class="track-wrap"><div class="status-wrap">';
                if($ccSbd[0]['order_status_id']=='1')
                {
                  chtml+='<div class="highlight-text"><span class="not-paid">You have not yet paid for this order. </span><a target="_blank" routerLink="/paypage/'+request_data['orderid']+'" target="_blank">Pay Now</a></div>';
                }else if($ccSbd[0]['order_status_id']=='7' || $ccSbd[0]['order_status_id']=='23')
                {
                  chtml+='This order is canceled. <a target="_blank" routerLink="contact-us">Contact us for further support</a>';
                }else{
                  chtml+='<span class="payment-done status"><span class="title">Order Confirmed</span>';
                  if($inpro=='completed')
                  {
                    $inpro = 'completed';
                  }
                  chtml+='<span class="'+$inpro+' dd round-icon"></span></span>';

                  /*chtml+='<span class="preparation status"><span class="title">In Preparation</span>';
                  if($inPre=='completed')
                  {
                    $inPre = 'completed';
                  }else
                  {
                    $inPre = 'half';
                  }
                  chtml+='<span class="'+$inPre+' grey round-icon"></span></span>';
                  */
                  chtml+='<span class="ofd status"><span class="title">Shipped</span>';
                  if($outForD=='completed')
                  {
                    $outForD = 'completed';
                  }else
                  {
                    $outForD = 'half';
                  }
                  chtml+='<span class="'+$outForD+' grey round-icon"></span></span>';

                  chtml+='<span class="delivered status"><span class="title">Delivered</span>';
                  if($delivered=='completed')
                  {
                    $delivered = 'completed';
                  }else
                  {
                    $delivered = 'half';
                  }
                  chtml+='<span class="'+$delivered+' grey round-icon"></span></span>';



                }




                chtml+='</div>';

                chtml+='<div class="pro-items"><ul>';
                for (const [$keyss, $cSd] of Object.entries($ccSbd)) 
                {
                    chtml+='<li class="items"><div class="pro-img"><img loading="lazy"  src="'+$cSd['image']+'"></div><div class="pro-desc"><div class="pro-name">'+$cSd['name']+'</div><div class="qty">Qty: <span class="qty-item">'+$cSd['quantity']+'</span></div><div class="price">Rs.'+Number($cSd['total'])+'</div></div></li>';
                }
                chtml+='</ul></div>';

                chtml+='<div class="delivery-details"><div class="del-date">Delivery Date : <span class="date">'+$ccSbd[0]['deliverydate']+'</span></div><div class="del-slot">Delivery Slot : <span class="slot">'+$ccSbd[0]['shippingtimeslot']+'</span></div></div>';
                chtml+='<div class="courier-details"><ul><li class="leaf first partner">Courier Partner : <span class="cour-company">'+$ccSbd[0]['courier_partner']+'</span></li><li class="leaf first awd">AWD Number : <span class="awd-no">'+$ccSbd[0]['awb']+'</span></li>';
                if($ccSbd[0]['courier_partner']=='BlueDart'){ 
                    chtml+='<li class="leaf first link"><a href="https://bluedart.com/tracking" target="_blank">Track Shippment</a></li>';
                  }
                chtml+='</ul></div>';



                chtml+='</div>';

            }
          }
        }

        this.trackcourhtml = chtml;





      this.order_id = request_data['orderid'];
      this.email = request_data['email'];
      // console.log(this.express);
      // console.log(this.courier);
      })
    );

    /*this.myprofileService.trackorder(request_data).subscribe(response => {
     console.log(response);
      
    });*/
    
  }

  }

  SEO()
  {
   // this.seoservice.addCSSLink({href: "https://d3cif2hu95s88v.cloudfront.net/live-site-2016/live-new/css/nav.css.gz"});

    this.seoservice.setTitle('Track Order');
  }


  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
  getSafeCDNUrl(url:string = ''){
    this.safeUrl= this.sanitizer.bypassSecurityTrustResourceUrl(this.CDN_PATH+url);
    return this.safeUrl;
  }

  ngOnDestroy(){
    _.forEach(this.subscriptions,(sub:Subscription)=>sub.unsubscribe());
  }

}
