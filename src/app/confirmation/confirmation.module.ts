import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ConfirmationComponent } from './confirmation.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ ConfirmationComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: ConfirmationComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule ]
})
export class ConfirmationModule{

}