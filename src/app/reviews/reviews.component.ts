import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { ReviewService } from '../review/review.service';
import { ActivatedRoute,Router} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '../../environments/environment';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css'],
  providers:[ReviewService]
})
export class ReviewsComponent implements OnInit {
  @ViewChild('blogsearchquery') searchstring:ElementRef;
  reviews:any;
  total_reviews = 0;
  total_rating = 0;
  five_rating = 0;
  four_rating = 0;
  three_rating = 0;
  two_rating = 0;
  one_rating = 0;
  CDN_PATH  = environment.CDN_PATH;
  tabForm: FormGroup;
  searchForm: FormGroup;
  config: any;
  queryString = '';
  allSubscription:Array<Subscription> = [];
  constructor(private route: ActivatedRoute,private reviewService:ReviewService,
    private router: Router) { 
    this.config = {
      currentPage: 1,
      itemsPerPage: 10      
    };     
     
  }
  pageChange(newPage: number) {
    if(this.queryString !="")
    {
      this.router.navigate(['/reviews'], { queryParams: { page: newPage, query : this.queryString } });
    }else{
      this.router.navigate(['/reviews'], { queryParams: { page: newPage } });
    }
  }
  searchReviews(event: any){
      var query = '';
      if(event.target.value !='')
      {
        query = event.target.value;
      }
      this.queryString = query;
      this.allSubscription.push(this.reviewService.getAllReviews(1, query).subscribe(reviews => {          
      this.reviews = reviews['reviews'];
      this.total_reviews = reviews['total_reviews'];
      this.config.totalItems = this.total_reviews - 10;
      this.total_rating = reviews['rating'];

      this.five_rating = reviews['5star_rating'];
      this.four_rating = reviews['4star_rating'];
      this.three_rating = reviews['3star_rating'];
      this.two_rating = reviews['2star_rating'];
      this.one_rating = reviews['1star_rating'];
     }));
  }
   getReview(){
    this.allSubscription.push(this.route.queryParams.subscribe(params => {
      this.config.currentPage= params['page']?params['page']:1;
      var query = '';
      if(this.searchstring.nativeElement.value !='')
      {
        query = this.searchstring.nativeElement.value;
      }
      this.allSubscription.push(this.reviewService.getAllReviews(this.config.currentPage, query).subscribe(reviews => {          
      this.reviews = reviews['reviews'];
      this.total_reviews = reviews['total_reviews'];
      this.config.totalItems = this.total_reviews - 10;
      this.total_rating = reviews['rating'];

      this.five_rating = reviews['5star_rating'];
      this.four_rating = reviews['4star_rating'];
      this.three_rating = reviews['3star_rating'];
      this.two_rating = reviews['2star_rating'];
      this.one_rating = reviews['1star_rating'];
     }));

    }));
   }
  ngOnInit(): void {
    this.allSubscription.push(this.route.queryParams.subscribe(params => {
      this.config.currentPage= params['page']?params['page']:1;
      var query = '';
      if(typeof(params['query']) !='undefined')
      {
        query = params['query'];
      }
      this.allSubscription.push(this.reviewService.getAllReviews(this.config.currentPage, query).subscribe(reviews => {          
      this.reviews = reviews['reviews'];
      this.total_reviews = reviews['total_reviews'];
      this.config.totalItems = this.total_reviews - 10;
      this.total_rating = reviews['rating'];

      this.five_rating = reviews['5star_rating'];
      this.four_rating = reviews['4star_rating'];
      this.three_rating = reviews['3star_rating'];
      this.two_rating = reviews['2star_rating'];
      this.one_rating = reviews['1star_rating'];
     }));

    }));

    this.tabForm = new FormGroup({
      'login_email' : new FormControl(null,[Validators.required,Validators.email])
    });
    this.searchForm = new FormGroup({
      'login_email' : new FormControl(null,[Validators.required,Validators.email])
    });

  }

  ngOnDestroy(){
    _.forEach(this.allSubscription,(sub:Subscription)=>sub.unsubscribe());
  }
}
