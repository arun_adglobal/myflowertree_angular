import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';


import { ReactiveFormsModule } from '@angular/forms';

import { ReviewsComponent } from './reviews.component';
import { MainLayoutModule } from '../main-layout.module';
@NgModule({
declarations: [ ReviewsComponent ],
imports: [ RouterModule.forChild([
    { path: '', component: ReviewsComponent }
]), CommonModule, ReactiveFormsModule,MainLayoutModule,NgxPaginationModule ]
})
export class ReviewsModule{

}