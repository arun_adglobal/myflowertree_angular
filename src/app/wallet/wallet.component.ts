import { Component, OnInit , Inject, PLATFORM_ID, ChangeDetectionStrategy } from '@angular/core';
import { MyprofileService } from '../myprofile/myprofile.service';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class WalletComponent implements OnInit {
  wallet=[];
  remaining_balance = 0;
  subscription:Subscription;
  constructor(private myprofileService:MyprofileService,
    @Inject(PLATFORM_ID) private platformId: object) { }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      let walletData = [];
      this.subscription = this.myprofileService.getWallet().subscribe((data:any) => {
      this.remaining_balance = data['balance'];
      for(let w of data['wallet'])
      {
        if(w['cr_cash'] ==0 && w['dr_cash'] ==0)
        {
          continue;
        }

        var describtion = '';
        if(w['cr_cash'] !=0)
        {
          describtion = "Earned for Order "+w['order_id'];
        }else if(w['dr_cash'] !=0){
          describtion =  w['transaction'].replace("Redeem #","Redeemed for Order ");
          describtion =  describtion.replace("Usage on order no.","Redeemed for Order ");
        }

        var balance = '';
        if(w['cr_cash'] !=0)
          balance =  "<span class='add-balance'>+ &#8360; "+w['cr_cash']+'</span>';
	    	else if(w['dr_cash'] !=0)
          balance =  "<span class='remove-balance'>- &#8360; "+w['dr_cash']+'</span>';

          var date = new Date(w['date_added']);
          var dat = date.getDate().toString();
          if(dat.length==1)
          {
            dat = '0'+dat;
          }
          var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
          var d = dat+' '+month[date.getMonth()]+' '+date.getFullYear();


        walletData.push({
          'describtion' : describtion,
          'balance' : balance,
          'running_balance' : w['running_balance'],
          'customer_id' : w['customer_id'],
          'order_id'    : w['order_id'],
          'cash'        : w['cash'],
          'dr_cash'     : w['dr_cash'],
          'cr_cash'     : w['cr_cash'],
          'transaction' : w['transaction'],
          'date_added'  : d,
        });
      }
    });
   this.wallet = walletData;
  }

  }

  ngOnDestroy():void {
    //this.footerSubscribe.unsubscribe();
    this.subscription.unsubscribe();
  }
}


