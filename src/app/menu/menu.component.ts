import { Component, OnInit,Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { MenuService } from './menu.service';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [MenuService]
})
export class MenuComponent implements OnInit {

  menu: any = {};
  tmp_input = '';
  menuSubcription:Subscription;
  constructor(private menuService: MenuService,
    @Inject(PLATFORM_ID) private platformId: object) { }
  ngOnInit() {
    this.menuSubcription = this.menuService.getMenu().subscribe(menu => {
      this.menu = menu;
    });

  }

  toHTML(input) : any {
    if (isPlatformBrowser(this.platformId)) {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
    }
  }
 ngOnDestroy(){
   this.menuSubcription.unsubscribe();
 }
}
