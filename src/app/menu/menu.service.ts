import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable()
export class MenuService {
APIURL  = environment.API_URL;
  constructor(private http: HttpClient) {}
  getMenu() {
    return this.http.get(this.APIURL+'api/menu/', {
      withCredentials: true
    }).pipe(map(data => {
      return data;
     }));
  }

}
