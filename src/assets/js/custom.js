var apiUrl = "http://localhost:4600/";

	$(document).on('click','#accountshow',function(){
	
	  $(".overlay").show('');
	  $(".account-box").show('slow');
	});
	$(document).on('click','#accountshow',function(event){
	
	  event.stopPropagation();
	});
	$(document).on('click','body',function(){

	  $(".account-box").hide('slow');
	  $(".overlay").hide('');
	});
	$(document).on('click','#searchshow',function(){
	
	  $(".search-full-box").slideToggle('slow');
	});
	$(document).on('click','#close-search',function(){
	  $(".search-full-box").slideToggle('slow');
	});

var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
  });
}

$(document).on('change', '#content_custom', function (e) {
	
     var request = $("#content_custom").find("select, textarea, input").serialize();
	 	
	 $.ajax({
        url: apiUrl+'api/customize',
        type: 'post',
        dataType: 'json',
        data:request,
		xhrFields: {
       withCredentials: true
    },
        beforeSend: function() {
          
        },
        complete: function() {
          
        },
        success: function(json) {
         $('.price_updator').html(json[0].totalPricie);
         $('.dec_updator').html(json[0].discrption);
         $(".custom_description").val(json[0].discrption);
         
         $(".custom_amount").val(json[0].totalPricie);

         
        }
      });
});

  function getOptionName(){
    var getOptionName = $('#chocolates option:selected').attr('name'); 
    $("#chocolate_name").val(getOptionName);
  }

$(document).on('click','#button-cart',function(){



  if($('.custom_description').val() !=''){
  $.ajax({
    url: apiUrl+'api/PersonolizedOrder',
    type: 'post',
    data: $('#content_custom input[type=\'text\'], #content_custom input[type=\'hidden\'], #content_custom input[type=\'radio\']:checked, #content_custom input[type=\'checkbox\']:checked, #content_custom select, #content_custom textarea'),
    dataType: 'json',
	xhrFields: {
       withCredentials: true
    },
    beforeSend: function() {
     // $('#button-cart').button('loading');
    },
    complete: function() {
      //$('#button-cart').button('reset');
    },
    success: function(json) {
		
		location = '/cart';
   
      /*if (json['success']) {
		 
		$('#cart-total').html(json['total']);

      }*/
    },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
  });
}else{
  alert("Please fill in all of the required fields");
  return false;
}

});

function getToken()
{
   return localStorage.getItem('userToken');
}
function getDevice()
{
	var ua = window.navigator.userAgent.toLowerCase();
	if (ua.match(/ipad/i) !== null || ua.match(/iphone/i) !== null || ua.match(/android/i) !== null)
	{
		return 'Mobile';
	}else 
	{
		return 'Desktop';	
	}
}
function getBrowser()
{
	var ua = window.navigator.userAgent.toLowerCase();	
	if (ua.match(/chrome/gi) !== null)
	{
		return 'Chrome';	
	}else if (ua.match(/firefox/gi) !== null) 
	{
		return 'Firefox';	
	}else
	{
		return 'Other';	
	}
}

var walletRunningBalance = 0;
var order_total = 0;
function getWallet()
{
	$.ajax({
	url: apiUrl+"api/getMFTWallet",
	headers: {
	'Authorization':getToken()
	},
	dataType: "json",
	xhrFields: {
       withCredentials: true
    },
	success: function(data){
		//console.log('data.running_balance' + data.running_balance);
		//console.log('data.total' + data.total);
		order_total = parseInt(data.total);
		if(data.running_balance > 0){
			$(".cash-wallet").show();
			$(".checkout-left #w-balance").html(data.running_balance);
			walletRunningBalance = parseInt(data.running_balance);			
			//console.log('walletRunningBalance'+walletRunningBalance);
			//console.log('order_total'+order_total);
			if(order_total <= walletRunningBalance){
				sufficientWallet = true;
				if($('input[name ="payByWalletCash"]').is(':checked')){
					$(".w-btn").show();
					$(".tabs-wrap").hide();
					$(".payment-wrap").hide();
				}
				payNowText = "Proceed to Pay Rs. "+ (order_total);
				$.each($(".pay-now"),function(){
					$(this).val(payNowText);
				});
				$(".m-pay-now").val(payNowText);
			}else{
				sufficientWallet = false;
				if($('input[name ="payByWalletCash"]').is(':checked')){
					payNowText = "Proceed to Pay Rs. "+ (order_total - walletRunningBalance);
				}else{
					payNowText = "Proceed to Pay Rs. "+ (order_total);
				}
				$.each($(".pay-now"),function(){
					$(this).val(payNowText);
				});
				$(".m-pay-now").val(payNowText);
			
			}
		}else{
			sufficientWallet = false;
			payNowText = "Proceed to Pay Rs. "+ (order_total);
			$.each($(".pay-now"),function(){
				$(this).val(payNowText);
			});
			$(".m-pay-now").val(payNowText);

		}
	}
});
}
$(document).on('click','input[name ="payByWalletCash"]',function(){
	if(order_total <= walletRunningBalance){
		sufficientWallet = true;	
	}else{
		sufficientWallet = false;
	}
	if(sufficientWallet){
		if($('input[name ="payByWalletCash"]').is(':checked')){

			$(".w-btn").show();
			$(".tabs-wrap").hide();
			$(".payment-wrap").hide();
			payNowText = "Proceed to Pay Rs. "+(order_total);
			$.each($(".pay-now"),function(){
				$(this).val(payNowText);
				$(this).data('pg',"CW");	
			});
			$(".m-pay-now").val(payNowText);
			$(".m-pay-now").data('pg',"CW");
		}else{
			$(".w-btn").hide();
			$(".tabs-wrap").show();
			$(".payment-wrap").show();
			payNowText = "Proceed to Pay Rs. "+(order_total);
			$.each($(".pay-now"),function(){
				$(this).val(payNowText);
			});
			$(".m-pay-now").val(payNowText);
		}
	}else{
		$(".tabs-wrap").show();
		$(".payment-wrap").show();
		$(".w-btn").hide();
		if($('input[name ="payByWalletCash"]').is(':checked')){
			payNowText = "Proceed to Pay Rs. "+(order_total - walletRunningBalance);
			var pnpn = 0;
			$.each($(".pay-now"),function(){
				pnpn++;
				$(this).val(payNowText);
			});
			$(".m-pay-now").val(payNowText);
		}else{
			payNowText = "Proceed to Pay Rs. "+(order_total);
			$.each($(".pay-now"),function(){
				$(this).val(payNowText);
			});
			$(".m-pay-now").val(payNowText);
		}
	}
});
function gottogateway(form_data){
	$("input[type='text'], input[type='password']").val("");
	if($('input[name ="payByWalletCash"]').is(':checked')){
		form_data.pbw = 1;
	}else{
		form_data.pbw = 0;
	}
	$.ajax({
		url: apiUrl+"api/confirm",
		headers: {
			'Authorization':getToken()
		},
		type: "post",
		data: form_data,
		dataType: "json",
		xhrFields: {
       withCredentials: true
    },
		beforeSend: function() { $("#callbackloader").css('display','block'); },
		complete: function() { },
		success: function(e) {
			$("#callbackloader").css('display','none');
			if(e.reload && (e.reload=='yes')){
				location.reload();
			}else if(e.errors){
				$.each(e.errors, function(e, t) {
					if ("shipping_method" !== e && "payment_method" !== e) {
						-1 !== $.inArray(e, ["payment_country", "payment_zone", "shipping_country", "shipping_zone"]) ? e += "_id" : 0 === e.indexOf("custom_field") ? e = "custom_field[" + (e = e.replace("custom_field", "")) + "]" : 0 === e.indexOf("payment_custom_field") ? e = "payment_custom_field[" + (e = e.replace("payment_custom_field", "")) + "]" : 0 === e.indexOf("shipping_custom_field") && (e = "shipping_custom_field[" + (e = e.replace("shipping_custom_field", "")) + "]");
						var o = $('.journal-checkout [name="' + e + '"]');
						o.focus(), o.closest(".form-group").addClass("has-error"), o.after('<div class="text-danger">' + t + "</div>")
					}
				});
			}else{
				$('#checkout-payment').trigger("click");
				$("#payment_gateway_code").hide();
				$("#payment_gateway_code").html(e.order_data[0]['payment_form']);
				$('#payment_gateway_code input[type="submit"]').trigger("click");
			}				
		}
	});
}
function getActiveMethods()
{
	$.ajax({
		url: apiUrl+"api/getactivemethods",
	  headers: {
        'Authorization':getToken()
       },
		type: "get",
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
      $("#nbBankcode").html("<option value=''>Other Banks</option>");
      $("#m_nbBankcode").html("<option value=''>Other Banks</option>");
      var popBankCount = 0;
      $.each(data['netbanking'], function( key, value ) {
        if(key == "HDFB" && popBankCount > 3){
          $(".bank-hdfc").removeClass("bank-hide");
          $(".m-bank-hdfc").removeClass("bank-hide");
          popBankCount++;
        }
        if(key == "SBIB" && popBankCount > 3){
          $(".bank-sbi").removeClass("bank-sbi");
          $(".m-bank-sbi").removeClass("bank-sbi");
          popBankCount++;
        }
        if(key == "ICIB" && popBankCount > 3){
          $(".bank-icici").removeClass("bank-hide");
          $(".m-bank-icici").removeClass("bank-hide");
          popBankCount++;
        }
        if(key == "AXIB" && popBankCount > 3){
          $(".bank-axis").removeClass("bank-hide");
          $(".m-bank-axis").removeClass("bank-hide");
          popBankCount++;
        }
        if(key == "162B" && popBankCount > 3){
          $(".bank-kotak").removeClass("bank-hide");
          $(".m-bank-kotak").removeClass("bank-hide");
          popBankCount++;
        }
        if(key == "PNBB" && popBankCount > 3){
          $(".bank-pnb").removeClass("bank-hide");
          $(".m-bank-pnb").removeClass("bank-hide");
          popBankCount++;
        }
        $("#nbBankcode").append("<option value='"+key+"'>"+value['title']+"</option>");
        $("#m_nbBankcode").append("<option value='"+key+"'>"+value['title']+"</option>");
      });
	  
	 activeWallet = [];
	  
     if(typeof(data['cashcard']['AMZPAY']) != "undefined" && data['cashcard']['AMZPAY'] !== null) {
        data['cashcard']['AMZPAY']= "Amazon Pay";
		activeWallet.push('AMZPAY');
      }
	  if(typeof(data['cashcard']['MOBIKWIK']) != "undefined" && data['cashcard']['MOBIKWIK'] !== null) {
        data['cashcard']['MOBIKWIK']= "Mobikwik wallet";
		activeWallet.push('MOBIKWIK');
      }
	 if(typeof(data['cashcard']['FREC']) != "undefined" && data['cashcard']['FREC'] !== null) {
        data['cashcard']['FREC']= "FreeCharge";
		activeWallet.push('FREC');
      }
      if(typeof(data['cashcard']['PAYZ']) != "undefined" && data['cashcard']['PAYZ'] !== null) {
        data['cashcard']['PAYZ']= "HDFC PayZapp";
		activeWallet.push('PAYZ');
      }
      if(typeof(data['cashcard']['ITZC']) != "undefined" && data['cashcard']['ITZC'] !== null) {
        data['cashcard']['ITZC']= "ItzCash";
		activeWallet.push('ITZC');
      }
      if(typeof(data['cashcard']['JIOM']) != "undefined" && data['cashcard']['JIOM'] !== null) {
        data['cashcard']['JIOM']= "Jio Money";
		activeWallet.push('JIOM');
      }
      if(typeof(data['cashcard']['OLAM']) != "undefined" && data['cashcard']['OLAM'] !== null) {
        data['cashcard']['OLAM']= "OlaMoney";
		activeWallet.push('OLAM');
      }
      if(typeof(data['cashcard']['OXICASH']) != "undefined" && data['cashcard']['OXICASH'] !== null) {
        data['cashcard']['OXICASH']= "Oxigen Wallet";
		activeWallet.push('OXICASH');
      }
      if(typeof(data['cashcard']['PAYTM']) != "undefined" && data['cashcard']['PAYTM'] !== null) {
        data['cashcard']['PAYTM'] = "Paytm";
		activeWallet.push('PAYTM');
      }
      if(typeof(data['cashcard']['PHONEPE']) != "undefined" && data['cashcard']['PHONEPE'] !== null) {
        data['cashcard']['PHONEPE'] = "PhonePe";
		activeWallet.push('PHONEPE');
        }
        if(typeof(data['cashcard']['YESW']) != "undefined" && data['cashcard']['YESW'] !== null) {
          data['cashcard']['YESW'] = "YES PAY";
		  activeWallet.push('YESW');
        }
      
      var dwallets = "";
      var mwallets = "";

      $.each(data['cashcard'], function(key, value){	
		dwallets += '<span class="bank"><label class="bank-label"><span class="bank-icon dd"><img loading="lazy"  src="https://d3cif2hu95s88v.cloudfront.net/live-site-2016/images/'+key+'-wallet.png" /></span><span class="bank-title">'+value+'</span> <span class="radio-bank"><input type="radio" name="wallet" class="bankcose-radio" value="'+key+'"></span></label></span>';
        mwallets += '<span class="bank"><label class="bank-label"><span class="bank-icon ss"><img loading="lazy"  src="https://d3cif2hu95s88v.cloudfront.net/live-site-2016/images/'+key+'-wallet.png" /></span><span class="bank-title">'+value+'</span> <span class="radio-bank"><input type="radio" name="m_wallet" class="bankcose-radio" value="'+key+'"></span></label></span>';
     }); 
	  
      $(".wallets").html(dwallets);
      $(".m-wallets").html(mwallets);				
		}
	});

}

    $(document).on("click", ".naccs .menu div", function() {
      var numberIndex = $(this).index();    
      if (!$(this).is("active")) {
      $(".naccs .menu div").removeClass("active");
      $(".naccs ul li").removeClass("active");
    
      $(this).addClass("active");
      $(".naccs ul").find("li:eq(" + numberIndex + ")").addClass("active");
    
      var listItemHeight = $(".naccs ul").find("li:eq(" + numberIndex + ")").innerHeight();
      $(".naccs ul").height(listItemHeight + "px");
      }
    });
    
        var ccerror = false;
        var dcerror = false;
        var ccCardError = false;
        var dcCardError = false;
        var ccNumber = "";
        var dcNumber = "";  
        
        var ccCardFetched = false;
        var dcCardFetched = false;
        var m_ccCardFetched = false;
        var m_dcCardFetched = false;
        $(function() { 
          var ccCardNumber = $('#ccNumber');
          var dcCardNumber = $('#dcNumber');
          var m_ccCardNumber = $('#m_ccNumber');
          var m_dcCardNumber = $('#m_dcNumber');
    
    
          if(ccCardNumber.length !== 0){
            ccCardNumber.payform('formatCardNumber');
            ccCardNumber.keyup(function() {
              if($("#ccNumber").val().replace(/ /g, "").length < 6 ){
                ccCardFetched = false;
                if($("#ccNumber").val().replace(/ /g, "").length < 2){
                  $("#ccIco").removeClass();
                }else{
                    if($.payform.validateCardNumber(ccCardNumber.val()) == false) {
                      ccCardNumber.parent().addClass('field-error');
                      ccCardNumber.parent().removeClass('field-success');
                        $("#ccNumberError").show();
                        $("#ccIco").removeClass();
                        ccerror = true;
                        ccCardError = true;
                    } else {
                      ccCardNumber.parent().addClass('field-success');
                      ccCardNumber.parent().removeClass('field-error');
                        $("#ccNumberError").hide();
                        ccerror = false;
                        ccCardError = false;
                    }
                }
              }
              if($("#ccNumber").val().replace(/ /g, "").length >= 6){
                if(ccCardFetched == false){
                  ccCardFetched = true;				  
				$.ajax({
					url: apiUrl+"api/validatecard",
					headers: {
						'Authorization':getToken()
					},
					type: "post",
					data: {
                    card_number:$("#ccNumber").val().replace(/ /g, "")
					},
					dataType: "json",
					xhrFields: {
						withCredentials: true
					},
					success: function(data) {
						if(data.cardType == "Unknown"){
                        var ccCardType = $.payform.parseCardType($("#ccNumber").val().replace(/ /g, ""));
                        bankcode = ccCardType;
                        $("#ccBankcode").val(bankcode);
                      $("#ccIco").removeClass();
                    $("#ccIco").addClass("card-"+ccCardType);
                    ccCardError = true;
                      }else{
                        ccCardError = false;
                        $("#ccBankcode").val(data.cardType);
                        var bankcode = data.cardType;
                        $("#ccBankcode").val(bankcode);
                        $("#ccIco").removeClass();
                        $("#ccIco").addClass("card-"+bankcode);
                      }	
					}
				});
				  
				  
				  
                }
              }
            });
          }
          if(dcCardNumber.length !== 0){
            dcCardNumber.payform('formatCardNumber');
            dcCardNumber.keyup(function() {
              if($("#dcNumber").val().replace(/ /g, "").length < 6 ){
                dcCardFetched = false;
                if($("#dcNumber").val().replace(/ /g, "").length < 2){
                  $("#dcIco").removeClass();
                }else{
                    if ($.payform.validateCardNumber(dcCardNumber.val()) == false) {
                      dcCardNumber.parent().addClass('field-error');
                      dcCardNumber.parent().removeClass('field-success');
                        $("#dcNumberError").show();
                        $("#dcIco").removeClass();
                        dcerror = true;
                        dcCardError = true;
                    } else {
                      dcCardNumber.parent().addClass('field-success');
                      dcCardNumber.parent().removeClass('field-error');
                        $("#dcNumberError").hide();
                        dcerror = false;
                        dcCardError = false;
                    }
                }
              }
              if($("#dcNumber").val().replace(/ /g, "").length >= 6){
                if(dcCardFetched == false){
                  			  
				  	$.ajax({
						url: apiUrl+"api/validatecard",
						type: "post",
						headers: {
							'Authorization':getToken()
						},
						data: {
									card_number:$("#dcNumber").val().replace(/ /g, "")
							},
						dataType: "json",
						xhrFields: {
							withCredentials: true
						},
						success: function(data) {
								                 if(data.cardType == "Unknown"){
                        var dcCardType = $.payform.parseCardType($("#dcNumber").val().replace(/ /g, ""));
                        var bankcode = dcCardType;
                        $("#dcBankcode").val(bankcode);
                      $("#dcIco").removeClass();
                    $("#dcIco").addClass("card-"+dcCardType);
                    dcerror = true;
                          dcCardError = true;
                      }else{
                        $("#dcBankcode").val(data.cardType);
                        bankcode = data.cardType;
                        $("#dcBankcode").val(bankcode);
                        $("#dcIco").removeClass();
                        $("#dcIco").addClass("card-"+bankcode);
                        dcerror = false;
                          dcCardError = false;
                          dcCardFetched = true;
                      }		
						}
					});
				  
				  
                }
              }
            });
          } 
		  if(m_ccCardNumber.length !== 0){
				m_ccCardNumber.payform('formatCardNumber');
				m_ccCardNumber.keyup(function() {
	    	if($("#m_ccNumber").val().replace(/ /g, "").length < 6 ){
	    		m_ccCardFetched = false;
	    		if($("#m_ccNumber").val().replace(/ /g, "").length < 2){
		    		$("#m_ccIco").removeClass();
		    	}else{
			        if($.payform.validateCardNumber(m_ccCardNumber.val()) == false) {
			        	m_ccCardNumber.parent().addClass('field-error');
			        	m_ccCardNumber.parent().removeClass('field-success');
			            /*$("#m_ccNumberError").show();*/
			            $("#m_ccIco").removeClass();
			            ccerror = true;
			            ccCardError = true;
			        } else {
			        	m_ccCardNumber.parent().addClass('field-success');
			        	m_ccCardNumber.parent().removeClass('field-error');
			            $("#m_ccNumberError").hide();
			            ccerror = false;
			            ccCardError = false;
			        }
		    	}
	    	}
	    	if($("#m_ccNumber").val().replace(/ /g, "").length >= 6){
	    		if(m_ccCardFetched == false){
	    			m_ccCardFetched = true;
		    							
					$.ajax({
						url: apiUrl+"api/validatecard",
						headers: {
							'Authorization':getToken()
						},
						type: "post",
						data: {
							card_number:$("#m_ccNumber").val().replace(/ /g, "")
						},
						dataType: "json",
						xhrFields: {
							withCredentials: true
						},
						success: function(data) {
								if(data.cardType == "Unknown"){
				      		var m_ccCardType = $.payform.parseCardType($("#m_ccNumber").val().replace(/ /g, ""));
				      		bankcode = m_ccCardType;
				      		$("#m_ccBankcode").val(bankcode);
					    	$("#m_ccIco").removeClass();
							$("#m_ccIco").addClass("card-"+m_ccCardType);
							ccCardError = true
				      	}else{
				      		ccCardError = false;
				      		$("#m_ccBankcode").val(data.cardType);
					      	bankcode = data.cardType;
					      	$("#m_ccBankcode").val(bankcode);
					      	$("#m_ccIco").removeClass();
					      	$("#m_ccIco").addClass("card-"+bankcode);
				      	}			
						}
					});
	    		}
	    	}
	    	
	    });
    }	if(m_dcCardNumber.length !== 0){
		m_dcCardNumber.payform('formatCardNumber');
	    m_dcCardNumber.keyup(function() {
	    	if($("#m_ccNumber").val().replace(/ /g, "").length < 6 ){
	    		m_ccCardFetched = false;
	    		if($("#m_dcNumber").val().replace(/ /g, "").length < 2){
		    		$("#m_dcIco").removeClass();
		    	}else{
			        if ($.payform.validateCardNumber(m_dcCardNumber.val()) == false) {
			        	m_dcCardNumber.parent().addClass('field-error');
			        	m_dcCardNumber.parent().removeClass('field-success');
			            $("#m_dcNumberError").show();
			            $("#m_dcIco").removeClass();
			            dcerror = true;
			            dcCardError = true;
			        } else {
			        	m_dcCardNumber.parent().addClass('field-success');
			        	m_dcCardNumber.parent().removeClass('field-error');
			            $("#m_dcNumberError").hide();
			            dcerror = false;
			            dcCardError = false;
			        }
		    	}
	    	}
	    	if($("#m_dcNumber").val().replace(/ /g, "").length >= 6){
	    		if(m_ccCardFetched == false){
	    			m_ccCardFetched = true;
					
					$.ajax({
						url: apiUrl+"api/validatecard",
						headers: {
							'Authorization':getToken()
						},
						type: "post",
						data: {
							card_number:$("#m_dcNumber").val().replace(/ /g, "")
						},
						dataType: "json",
						xhrFields: {
							withCredentials: true
						},
						success: function(data) {
							if(data.cardType == "Unknown"){
				      		var m_dcCardType = $.payform.parseCardType($("#m_dcNumber").val().replace(/ /g, ""));
				      		bankcode = m_dcCardType;
				      		$("#m_dcBankcode").val(bankcode);
					    	$("#m_dcIco").removeClass();
							$("#m_dcIco").addClass("card-"+m_dcCardType);
				      	}else{
				      		$("#m_dcBankcode").val(data.cardType);
					      	bankcode = data.cardType;
					      	$("#m_dcBankcode").val(bankcode);
					      	$("#m_dcIco").removeClass();
					      	$("#m_dcIco").addClass("card-"+bankcode);
				      	}			
						}
					});
	    		}
	    	}
	    });
	}
    
        });

        $(document).on('change','input[name="nbradio"]',function() {
          $("#nbBankError").hide();
          var selectedOption = $(this).val();
          $.each($("#nbBankcode option"),function(){
            if($(this).val() == selectedOption){
              $(this).prop('selected',true);
            }else{
              $(this).prop('selected',false);
            }
          });
        });
        $(document).on("change","#nbBankcode",function(){
          $("#nbBankError").hide();
          var selectedOption = $(this).val();
          $.each($('input[name="nbradio"]'),function(){
            if($(this).val() == selectedOption){
              $(this).prop('checked',true);
              $(this).closest(".bank-hide").addClass("checked");
            }else{
              $(this).prop('checked',false);
              $.each($(".bank-hide"),function(){
                $(this).removeClass("checked");
              });
            }
          });
        });

        $(document).on("change","input[name='wallet']",function(){
          $("#walletError").hide();
          var selectedOption = $(this).val();
        });
        $(document).on("change","input[name='m_wallet']",function(){
          $("#m_walletError").hide();
          var selectedOption = $(this).val();
        });

        $(document).on("click",'input[name="wallet"]', function(){
          $(this).parent().addClass('checked');
        });
        $(document).on("click",'input[name="m_wallet"]', function(){
          $(this).parent().addClass('checked');
        });

        $(document).on('focus',"input[name='TEZvpa']",function(){
          $("#TEZvpa-error").hide();
        });
        $(document).on('focus',"input[name='m_TEZvpa']",function(){
          $("#TEZvpa-error").hide();
        });
        $(document).on('focus',"input[name='vpa']",function(){
          $("#vpa-error").hide();
        });
        $(document).on('focus',"input[name='m_vpa']",function(){
          $("#vpa-error").hide();
        });

	$(document).on('click','.pay-now',function(){
          ccerror = false;
          dcerror = false;
          var pg = $(this).data('pg');
          var payment_method = "payu";
          var wallet_method = "wallet";
          var bankcode = "";
          var form_data = "";
		if(pg == "CW"){
			form_data = {payment_method:wallet_method,pg:pg};
			gottogateway(form_data);
		}
          if(pg == "CC"){
            var ccName = $("#ccName").val();        

				$.ajax({
					url: apiUrl+"api/validatecard",
					headers: {
						'Authorization':getToken()
					},
					type: "post",
					data: {
						card_number:$("#ccNumber").val().replace(/ /g, "")
					},
					dataType: "json",
					xhrFields: {
						withCredentials: true
					},
					success: function(data) {
						 if(data.cardType == "Unknown"){
                    var ccCardType = $.payform.parseCardType($("#ccNumber").val().replace(/ /g, ""));
							bankcode = ccCardType;
							$("#ccBankcode").val(bankcode);
						  $("#ccIco").removeClass();
						$("#ccIco").addClass("card-"+ccCardType);
						ccerror = true;
							  ccCardError = true;
						  }else{
							$("#ccBankcode").val(data.cardType);
							bankcode = data.cardType;
							$("#ccBankcode").val(bankcode);
							$("#ccIco").removeClass();
							$("#ccIco").addClass("card-"+bankcode);
							ccerror = false;
							ccCardError = false;
							ccCardFetched = true;
						  }			
					}
				});			  
        
            if(ccName.length < 3 ){
              ccerror = true;
              $("#ccNameError").show();
              $("#ccNameError").text("Please enter full name");
            }
            var ccNumber = $("#ccNumber").val();
            ccNumber = ccNumber.replace(/ /g, "");
            if(ccNumber==""){
              $("#ccNumberError").text("Please enter card number");
              $("#ccNumberError").show();
              ccerror = true;
            }
            if(ccCardError){
              $("#ccNumberError").text("Please enter valid card number");
              $("#ccNumberError").show();
              ccerror = true;	
            }
            var ccMM = $("#ccMM").val();
            if(parseInt(ccMM) > 12 || ccMM.trim() == "" ){
              ccerror = true;
              $("#ccMMError").text("Exp date Invalid");
              $("#ccMMError").show();
            }
            var ccYY = $("#ccYY").val();
            var dt = new Date();
            var fullyear = dt.getFullYear();
            var month = dt.getMonth();
            if(parseInt(ccYY) <= parseInt(fullyear)-2000){
              if(parseInt(ccMM) < parseInt(month)){
                ccerror = true;
                $("#ccMMError").text("Exp date Invalid");
                $("#ccMMError").show();
              }
            }
            if(parseInt(ccYY) < parseInt(fullyear)-2000 || ccYY.trim() == ""){
              ccerror = true;
              $("#ccYYError").text("Exp date Invalid");
              $("#ccYYError").show();
            }
            var bankcode = $("#ccBankcode").val();
            var ccCVV = $("#ccCVV").val();
            if(bankcode == "AMEX"){
              if(ccCVV.length < 4){
                ccerror = true;
                $("#ccCVVError").text("Invalid CVV");
                $("#ccCVVError").show();
              }
            }else{
              if(ccCVV.length < 3){
                ccerror = true;
                $("#ccCVVError").text("Invalid CVV");
                $("#ccCVVError").show();
              }
            }
            if(!ccerror){
				$.ajax({
					url: apiUrl+"api/validatecard",
					headers: {
						'Authorization':getToken()
					},
					type: "post",
					data: {card_number:ccNumber},
					dataType: "json",
					xhrFields: {
						withCredentials: true
					},
					success: function(res) {
						$("#ccBankcode").val(res['cardType']);
						bankcode = res['cardType'];
						$("#ccIco").removeClass();
						$("#ccIco").addClass("card-"+bankcode);
					  var form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode,name:ccName,number:ccNumber,mm:ccMM,yy:ccYY,cvv:ccCVV};
					  gottogateway(form_data);			
					}
				});
			}
          }else if(pg == "DC"){
            var dcName = $("#dcName").val();
			  
			$.ajax({
				url: apiUrl+"api/validatecard",
				headers: {
					'Authorization':getToken()
				},
				type: "post",
				data: {
                card_number:$("#dcNumber").val().replace(/ /g, "")
              },
				dataType: "json",
				xhrFields: {
					withCredentials: true
				},
				success: function(data) {
					if(data.cardType == "Unknown"){
                    var dcCardType = $.payform.parseCardType($("#dcNumber").val().replace(/ /g, ""));
                    bankcode = dcCardType;
                    $("#dcBankcode").val(bankcode);
                  $("#dcIco").removeClass();
                $("#dcIco").addClass("card-"+dcCardType);
                dcerror = true;
                      dcCardError = true;
                  }else{
                    $("#dcBankcode").val(data.cardType);
                    bankcode = data.cardType;
                    $("#dcBankcode").val(bankcode);
                    $("#dcIco").removeClass();
                    $("#dcIco").addClass("card-"+bankcode);
                    dcerror = false;
                      dcCardError = false;
                      dcCardFetched = true;
                  }			
				}
			});
			  
            if(dcName.length < 3 ){
              dcerror = true;
              $("#dcNameError").text("Please enter full name");
              $("#dcNameError").show();
            }
            var dcNumber = $("#dcNumber").val();
            dcNumber = dcNumber.replace(/ /g, "");
            if(dcNumber==""){
              $("#dcNumberError").text("Please enter card number");
              $("#dcNumberError").show();
              dcerror = true;
            }
            if(dcCardError){
              $("#dcNumberError").show();
              $("#dcNumberError").text("Please enter valid card number");
              dcerror = true;
            }
            var dcMM = $("#dcMM").val();
            if(parseInt(dcMM) > 12 || dcMM.trim() == "" ){
              $("#dcMMError").show();
              $("#dcMMError").text("Exp date Invalid");
              dcerror = true;
            }
            var dcYY = $("#dcYY").val();
            var dt = new Date();
            var dy = dt.getFullYear();
            var dm = dt.getMonth();
            if(parseInt(dcYY) <= parseInt(dy)-2000){
              if(parseInt(dcMM) < parseInt(dm)){
                $("#dcMMError").text("Exp date Invalid");
                $("#dcMMError").show();
                dcerror = true;
              }
            }
            if(parseInt(dcYY) < parseInt(dy)-2000 || dcYY == ""){
              $("#dcYYError").text("Exp date Invalid");
              $("#dcYYError").show();
              dcerror = true;
            }
            var bankcode = $("#dcBankcode").val();
            var dcCVV = $("#dcCVV").val();
            if(bankcode == "AMEX"){
              if(dcCVV.length != 4){
                dcerror = true;
                $("#dcCVVError").text("Invalid CVV");
                $("#dcCVVError").show();
              }
            }else{
              if(dcCVV.length != 3){
                dcerror = true;
                $("#dcCVVError").text("Invalid CVV");
                $("#dcCVVError").show();
              }
            }
            if(!dcerror){
				
				$.ajax({
					url: apiUrl+"api/validatecard",
					headers: {
						'Authorization':getToken()
					},
					type: "post",
					data: {
					card_number:dcNumber
					},
					dataType: "json",
					xhrFields: {
						withCredentials: true
					},
					success: function(res) {
                    $("#dcBankcode").val(res['cardType']);
                    bankcode = res['cardType'];
                    $("#dcIco").removeClass();
                    $("#dcIco").addClass("card-"+bankcode);
                 var form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode,name:dcName,number:dcNumber,mm:dcMM,yy:dcYY,cvv:dcCVV};
                gottogateway(form_data);			
					}
				});
            }
          }else if(pg == "PAYTM"){
            pg = "CASH";
            var bankcode = "PAYTM";
            gottogateway({payment_method:payment_method,pg:pg,bankcode:bankcode});
          }else if(pg == "PAYPAL"){
            pg = "PAYPAL";
            var bankcode = "PAYPAL";
            gottogateway({payment_method:payment_method,pg:pg,bankcode:bankcode});
          }else if(pg == "NB"){
            bankcode = $("#nbBankcode").val();
            if(parseInt(bankcode) == 0){
              $("#nbBankError").show();
              $("#nbBankError").text("Please select bank");
            }else{
              gottogateway({payment_method:payment_method,pg:pg,bankcode:bankcode});
            }
          }else if(pg == "CASH"){
            var bankcode = $("input[name='wallet']:checked").val();
            if($("input[name='wallet']:checked").length == 0){
              $("#walletError").show();
              $("#walletError").text("Please select wallet");
            }else{
              gottogateway({payment_method:payment_method,pg:pg,bankcode:bankcode});	
            }
          }else if(pg == "UPI"){
            $("#resultLoading").show();
            var bankcode = "UPI";
            var vpa = $("input[name='vpa']").val();
            if(vpa.trim() == ""){
                  $("#vpa-error").show();
              $("#vpa-error").text("VPA is empty");
                  $("#resultLoading").hide();
                  return false;
            }
			  
			$.ajax({
				url: apiUrl+"api/validatevpa",
				headers: {
					'Authorization':getToken()
				},
				type: "post",
				data: {vpa:vpa},
				dataType: "json",
				xhrFields: {
					withCredentials: true
				},
				success: function(res) {
              if(res['isVPAValid'] != 1){
                $("#vpa-error").show();
                $("#vpa-error").text("Please enter valid VPA");
                $("#resultLoading").hide();
                return false;
              }else{
                gottogateway({payment_method:payment_method,pg:pg,bankcode:bankcode,vpa:vpa});  	
              }				
				}
			});  
			  
			  
          }
          else if(pg == "TEZ"){
            $("#resultLoading").show();
            pg = "UPI";
            var bankcode = "TEZ";
              var selectedTezBank = $("#tez_bank").find(":selected").val();
              if($("input[name='TEZvpa']").val().trim() == ""){
                  $("#TEZvpa-error").show();
              $("#TEZvpa-error").text("VPA is empty");
                  $("#resultLoading").hide();
                  return false;
            }
              if(selectedTezBank == ""){
                  $("#TEZvpa-error").show();
                $("#TEZvpa-error").text("please select vpa bank");
                  $("#resultLoading").hide();
                  return false;
              }
            var vpa = $("input[name='TEZvpa']").val()+selectedTezBank;
			  
			$.ajax({
				url: apiUrl+"api/validatevpa",
				headers: {
					'Authorization':getToken()
				},
				type: "post",
				data: {
                vpa:vpa
              },
				dataType: "json",
				xhrFields: {
					withCredentials: true
				},
				success: function(res) {
                if(res['isVPAValid'] != 1){
                  $("#TEZvpa-error").show();
                  $("#TEZvpa-error").text("Please enter valid VPA");
                  $("#resultLoading").hide();
                  return false;
                }else{
                  gottogateway({payment_method:payment_method,pg:pg,bankcode:bankcode,vpa:vpa});
                }			
				}
			});
			  
			  
          }
    
        });
    
        $(document).on('focus',"#ccName",function(){
          $("#ccNameError").hide();
        });
        $(document).on('keypress','#ccName',function (e) {
            var regex = new RegExp(/^[a-zA-Z\s]+$/);
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if(regex.test(str)) {
                return true;
            } else {
            e.preventDefault();
            return false;
            }
        });
        $(document).on('focus',"#ccNumber",function(){
          $("#ccNumberError").hide();
        });
        $(document).on('focus',"#ccMM",function(){
          $("#ccMMError").hide();
        });
        $(document).on("keyup", "#ccMM",function(e){
          var ccmmval = $(this);
          /*remAlpha(e,$(this));*/
          if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) { 
            if(ccmmval.val().length >= 2){
              if(parseInt(ccmmval.val()) > 12 || isNaN(parseInt(ccmmval.val()))){
                $("#ccMMError").show();
                $("#ccMMError").text("Exp date Invalid");
              }else{	
                $("#ccYY").focus();
              }
            }else if(ccmmval.val().length >= 0){
              $("#ccMMError").hide();
            }
          }else{
            if(e.keyCode != 8){
              $(this).val(ccmmval.val().slice(0,-1));
            }
          }
        });
        $(document).on('focus',"#ccYY",function(){
          $("#ccYYError").hide();
        });
        $(document).on("keyup", "#ccYY", function(e){
          var ccyyval = $(this);
          /*remAlpha(e,$(this));*/
          var d = new Date();
          if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) { 
            if(ccyyval.val().length >= 2){
              if(parseInt(ccyyval.val()) < parseInt(d.getFullYear().toString().substr(2,2)) || isNaN(parseInt(ccyyval.val()))){
                $("#ccYYError").show();
                $("#ccYYError").text("Exp date Invalid");
              }/*else{	
                $("#ccCVV").focus();
              }*/
            }
            if(ccyyval.val().length >= 0){
              $("#ccYYError").hide();
            }
          }else{
            if(e.keyCode != 8){
              $(this).val(ccyyval.val().slice(0,-1));
            }else if(ccyyval.val().length == 0 && e.keyCode == 8){
              $("#ccMM").focus();
            }
          }
        });
        $(document).on('focus',"#ccCVV",function(){
          $("#ccCVVError").hide();
          var maxlength = 4;
          var bankcode = $("#ccBankcode").val();
          if(bankcode == "AMEX"){
            maxlength = 4;
          }
          $(this).attr('maxlength',maxlength);
        });
    
        $(document).on("keyup", "#ccCVV", function(e){
          var cccvvval = $(this);
        });
    
        $(document).on('focus',"#dcName", function(){
          $("#dcNameError").hide();
        });
        $(document).on('keypress','#dcName',function (e) {
            var regex = new RegExp(/^[a-zA-Z\s]+$/);
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            e.preventDefault();
            return false;
            }
        });
        $(document).on('focus',"#dcNumber",function(){
          $("#dcNumberError").hide();
        });
        $(document).on('focus',"#dcMM", function(){
          $("#dcMMError").hide();
        });
        $(document).on("keyup", "#dcMM", function(e){
          var dcmmval = $(this);
          /*remAlpha(e,$(this));*/
          if(dcmmval.val().length >= 2){
            if(parseInt(dcmmval.val()) > 12 || isNaN(parseInt(dcmmval.val()))){
              $("#dcMMError").show();
              $("#dcMMError").text("Exp date Invalid");
            }else{	
              $("#dcYY").focus();
            }
          }else if(dcmmval.val().length >= 0){
            $("#dcMMError").hide();
          }
        });
        $(document).on('focus',"#dcYY", function(){
          $("#dcYYError").hide();
        });
        $(document).on("keyup", "#dcYY", function(e){
          var dcyyval = $(this);
          /*remAlpha(e,$(this));*/
          var d = new Date();
          if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) { 
            if(dcyyval.val().length >= 2){
              if(parseInt(dcyyval.val()) < parseInt(d.getFullYear().toString().substr(2,2)) || isNaN(parseInt(dcyyval.val()))){
                $("#dcYYError").show();
                $("#dcYYError").text("Exp date Invalid");
              }
            }
            if(dcyyval.val().length >= 0){
              $("#dcYYError").hide();
            }
          }else{
            if(e.keyCode != 8){
              $(this).val(dcyyval.val().slice(0,-1));
            }else if(dcyyval.val().length == 0 && e.keyCode == 8){
              $("#dcMM").focus();
            }
          }
        });
        $(document).on('focus',"#dcCVV", function(){
          $("#dcCVVError").hide();
          var maxlength = 4;
          var bankcode = $("#dcBankcode").val();
          if(bankcode == "AMEX"){
            maxlength = 4;
          }
          $(this).attr('maxlength',maxlength);
        });
        $(document).on("keyup", "#dcCVV", function(e){
          var dccvvval = $(this);
        });
		
/*********MOBILE CREDIT CARD START***********/
$("#m_ccName").on('focus',function(){
	$("#m_ccNameError").hide();
});
$("#m_ccName").on("keyup",function(event){
    var myString = $(this).val();
	var lastChar = myString;
	if(myString.length > 1){
		lastChar = myString[myString.length -1];
	}
	if (!lastChar.match(/[a-z\s]/i)) {
		$(this).val(myString.slice(0,-1));
	}
});
$(document).on('focus',"#m_ccNumber",function(){
	$("#m_ccNumberError").hide();
});
$(document).on('focus',"#m_ccMM", function(){
	$("#m_ccMMError").hide();
});
$(document).on("keyup","#m_ccMM", function(e){
	var m_ccmmval = $(this);
	/*remAlpha(e,$(this));*/
	if(m_ccmmval.val().length >= 2){
		if(parseInt(m_ccmmval.val()) > 12 || isNaN(parseInt(m_ccmmval.val()))){
			$("#m_ccMMError").show();
			$("#m_ccMMError").text("Exp date Invalid");
		}else{	
			$("#m_ccYY").focus();
		}
	}else if(m_ccmmval.val().length >= 0){
		$("#m_ccMMError").hide();
	}
});
/*var activeTabMob = "";*/
$(document).on("click",".colaps", function(event){
	var currTab = $(this);
	$(".m-pay-now").data("pg",$(this).data('pg'));
	$(".colaps").each(function(){
		$(this).siblings('h2').removeClass("payment-title-active");
		if(!$(this).is(":checked")){
			if($(this).attr('id') != currTab.attr('id')){
				$(this).prop("checked", true);
				//console.log($(this).parent().offset().top);
				$('html, body').animate({scrollTop: $(this).siblings('h2').offset().top}, 1000);
			}
		}
	});
	currTab.siblings('h2').addClass("payment-title-active");
	/*if(activeTabMob != ""){
		$("#"+activeTabMob).attr("checked", "checked");
	}
	
	activeTabMob = $(this).attr('id');*/
});
$(document).on('focus',"#m_ccYY", function(){
	$("#m_ccYYError").hide();
});
$(document).on("keyup", "#m_ccYY", function(e){
	var m_ccyyval = $(this);
	/*remAlpha(e,$(this));*/
	var d = new Date();
	if(m_ccyyval.length >= 2){
		if(parseInt(m_ccyyval.val()) < parseInt(d.getFullYear().toString().substr(2,2)) || isNaN(parseInt(m_ccyyval.val()))){
			$("#m_ccYYError").show();
			$("#m_ccYYError").text("Exp date Invalid");
		}/*else{	
			$("#m_ccCVV").focus();
		}*/
	}
	if(m_ccyyval.val().length >= 0){
		$("#m_ccYYError").hide();
	}
});
$(document).on('focus',"#m_ccCVV", function(){
	$("#m_ccCVVError").hide();
	var maxlength = 4;
	var bankcode = $("#m_ccBankcode").val();
	if(bankcode == "AMEX"){
		maxlength = 4;
	}
	$(this).attr('maxlength',maxlength);
});
$(document).on("keyup", "#m_ccCVV", function(e){
	var m_cccvvval = $(this);
	/*remAlpha(e,m_cccvvval);*/
});
/*********MOBILE CREDIT CARD END***********/


/*********MOBILE DEBIT CARD END***********/
$(document).on('focus',"#m_dcName", function(){
	$("#m_dcNameError").hide();
});
$(document).on("keyup","#m_dcName", function(event){
	var myString = $(this).val();
	var lastChar = myString;
	if(myString.length > 1){
		lastChar = myString[myString.length -1];
	}
	if (!lastChar.match(/[a-z\s]/i)) {
		$(this).val(myString.slice(0,-1));
	}
});
$(document).on('focus',"#m_dcNumber", function(){
	$("#m_dcNumberError").hide();
});
$(document).on('focus',"#m_dcMM", function(){
	$("#m_dcMMError").hide();
});
$(document).on("keyup", "#m_dcMM", function(e){
	var m_dcmmval = $(this);
	/*remAlpha(e,$(this));*/
	if(m_dcmmval.val().length >= 2){
		if(parseInt(m_dcmmval.val()) > 12 || isNaN(parseInt(m_dcmmval.val()))){
			$("#m_dcMMError").show();
			$("#m_dcMMError").text("Exp date Invalid");
		}else{	
			$("#m_dcYY").focus();
		}
	}else if(m_dcmmval.val().length >= 0){
		$("#m_dcMMError").hide();
	}
});
$(document).on('focus',"#m_dcYY", function(){
	$("#m_dcYYError").hide();
});
$(document).on("keyup", "#m_dcYY", function(e){
	var m_dcyyval = $(this);
	/*remAlpha(e,$(this));*/
	var d = new Date();
	if(m_dcyyval.val().length >= 2){
		if(parseInt(m_dcyyval.val()) < parseInt(d.getFullYear().toString().substr(2,2)) || isNaN(parseInt(m_dcyyval.val()))){
			$("#m_dcYYError").show();
			$("#m_dcYYError").text("Exp date Invalid");
		}/*else{	
			$("#m_dcCVV").focus();
		}*/
	}
	if(m_dcyyval.val().length >= 0){
		$("#m_dcYYError").hide();
	}
});
$(document).on('focus',"#m_dcCVV", function(){
	$("#m_dcCVVError").hide();
	var maxlength = 4;
	var bankcode = $("#m_dcBankcode").val();
	if(bankcode == "AMEX"){
		maxlength = 4;
	}
	$(this).attr('maxlength',maxlength);
});
$(document).on("keyup","#m_dcCVV", function(e){
	var m_dccvvval = $(this);
	/*remAlpha(e,m_dccvvval);*/
});

/*********MOBILE DEBIT CARD END***********/

/**** Mobile Pay ********/
$(document).on('click','.m-pay-now',function(){
	ccerror = false;
	dcerror = false;
	var pg = $(".m-pay-now").data('pg');
	if(pg == "none"){
		alert("Please select payment option");
		return false;
	}
	var payment_method = "payu";
	var bankcode = "";
	var form_data = "";

	if(pg == "CC"){
		var ccName = $("#m_ccName").val();
		
		$.ajax({
			url: apiUrl+"api/validatecard",
			headers: {
				'Authorization':getToken()
			},
			type: "post",
			data: {
	      card_number:$("#m_ccNumber").val().replace(/ /g, "")
	    },
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(data) {
				if(data.cardType == "Unknown"){
	      		var ccCardType = $.payform.parseCardType($("#m_ccNumber").val().replace(/ /g, ""));
	      		bankcode = ccCardType;
	      		$("#m_ccBankcode").val(bankcode);
		    	$("#m_ccIco").removeClass();
				$("#m_ccIco").addClass("card-"+ccCardType);
				ccerror = true;
            	ccCardError = true;
	      	}else{
	      		$("#m_ccBankcode").val(data.cardType);
		      	bankcode = data.cardType;
		      	$("#m_ccBankcode").val(bankcode);
		      	$("#m_ccIco").removeClass();
		      	$("#m_ccIco").addClass("card-"+bankcode);
		      	ccerror = false;
            	ccCardError = false;
            	ccCardFetched = true;
	      	}			
			}
		});
		
		if(ccName.length < 3 ){
			ccerror = true;
			$("#m_ccNameError").show();
			$("#m_ccNameError").text("Enter Full Name");
			/*console.log("Enter Full Name");*/
		}
		var ccNumber = $("#m_ccNumber").val();
		ccNumber = ccNumber.replace(/ /g, "");
		//console.log(ccNumber);
		if(ccNumber==""){
			$("#m_ccNumberError").text("Enter Card Number");
			$("#m_ccNumberError").show();
			ccerror = true;
		}
		if(ccCardError){
			$("#m_ccNumberError").text("Enter Valid Credit Card Number");
			$("#m_ccNumberError").show();
			ccerror = true;
		}
		var ccMM = $("#m_ccMM").val();
		if(parseInt(ccMM) > 12 || ccMM.trim() == "" ){
			ccerror = true;
			$("#m_ccMMError").text("Exp date Invalid");
			$("#m_ccMMError").show();
		}
		var ccYY = $("#m_ccYY").val();
		var dt = new Date();
		if(ccYY.trim() != "" ){
			if(parseInt(ccYY) <= parseInt(dt.getFullYear())-2000){
				if(parseInt(ccMM) < parseInt(dt.getMonth())){
					ccerror = true;
					$("#m_ccMMError").text("Exp date Invalid");
					$("#m_ccMMError").show();
				}
			}
		}
		if(ccMM.trim() != "" ){
			if(parseInt(ccYY) < parseInt(dt.getFullYear())-2000 || ccYY.trim() == ""){
				ccerror = true;
				$("#m_ccYYError").text("Exp date Invalid");
				$("#m_ccYYError").show();
			}
		}
		var bankcode = $("#m_ccBankcode").val();
		var ccCVV = $("#m_ccCVV").val();
		if(bankcode == "AMEX"){
			if(ccCVV.length < 4){
				ccerror = true;
				$("#m_ccCVVError").text("Invalid CVV");
				$("#m_ccCVVError").show();
			}
		}else{
			if(ccCVV.length < 3){
				ccerror = true;
				$("#m_ccCVVError").text("Invalid CVV");
				$("#m_ccCVVError").show();
			}
		}
		if(!ccerror){
			
		$.ajax({
		url: apiUrl+"api/validatecard",
		headers: {
			'Authorization':getToken()
		},
		type: "post",
		data: {card_number:ccNumber},
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function(res) {
			$("#m_ccBankcode").val(res['cardType']);
			bankcode = res['cardType'];
			$("#m_ccIco").removeClass();
			$("#m_ccIco").addClass("card-"+bankcode);
			form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode,name:ccName,number:ccNumber,mm:ccMM,yy:ccYY,cvv:ccCVV};
			gottogateway(form_data);			
		}
	});
		}
	}if(pg == "DC"){
		var dcName = $("#m_dcName").val();
		$.ajax({
		url: apiUrl+"api/validatecard",
		headers: {
			'Authorization':getToken()
		},
		type: "post",
		data: {
	      card_number:$("#m_dcNumber").val().replace(/ /g, "")
	    },
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function(data) {
			if(data.cardType == "Unknown"){
	      		var dcCardType = $.payform.parseCardType($("#m_dcNumber").val().replace(/ /g, ""));
	      		bankcode = dcCardType;
	      		$("#m_dcBankcode").val(bankcode);
		    	$("#m_dcIco").removeClass();
				$("#m_dcIco").addClass("card-"+dcCardType);
				dcerror = true;
            	dcCardError = true;
	      	}else{
	      		$("#m_dcBankcode").val(data.cardType);
		      	bankcode = data.cardType;
		      	$("#m_dcBankcode").val(bankcode);
		      	$("#m_dcIco").removeClass();
		      	$("#m_dcIco").addClass("card-"+bankcode);
		      	dcerror = false;
            	dcCardError = false;
            	dcCardFetched = true;
	      	}			
		}
		});
		
		
		if(dcName.length < 3 ){
			dcerror = true;
			$("#m_dcNameError").text("Enter Full Name");
			$("#m_dcNameError").show();
		}
		var dcNumber = $("#m_dcNumber").val();
		dcNumber = dcNumber.replace(/ /g, "");
		if(dcNumber==""){
			$("#m_dcNumberError").text("Enter Card Number");
			$("#m_dcNumberError").show();
			dcerror = true;
		}
		if(dcCardError){
			$("#m_dcNumberError").show();
			$("#m_dcNumberError").text("Enter Valid Card Number");
			dcerror = true;
		}
		var dcMM = $("#m_dcMM").val();
		if(parseInt(dcMM) > 12 || dcMM.trim() == "" ){
			$("#m_dcMMError").show();
			$("#m_dcMMError").text("Exp date Invalid");
			dcerror = true;
		}
		var dcYY = $("#m_dcYY").val();
		var dt = new Date();
		if(dcYY.trim() != "" ){
			if(parseInt(dcYY) <= parseInt(dt.getFullYear())-2000){
				if(parseInt(dcMM) < parseInt(dt.getMonth())){
					$("#m_ccMMError").show();
					$("#m_ccMMError").text("Exp date Invalid");
					dcerror = true;
				}
			}
		}
		if(dcMM.trim() != "" ){
			if(parseInt(dcYY) < parseInt(dt.getFullYear())-2000 || dcYY.trim() == ""){
				$("#m_dcYYError").show();
				$("#m_dcYYError").text("Exp date Invalid");
				dcerror = true;
			}
		}
		var bankcode = $("#m_dcBankcode").val();
		var dcCVV = $("#m_dcCVV").val();
		if(bankcode == "AMEX"){
			if(dcCVV.length != 4){
				dcerror = true;
				$("#m_dcCVVError").text("Invalid CVV");
				$("#m_dcCVVError").show();
			}
		}else{
			if(dcCVV.length != 3){
				dcerror = true;
				$("#m_dcCVVError").text("Invalid CVV");
				$("#m_dcCVVError").show();
			}
		}
		if(!dcerror){			
			
			$.ajax({
				url: apiUrl+"api/validatecard",
				headers: {
					'Authorization':getToken()
				},
				type: "post",
				data: {
		      card_number:dcNumber
		    },
				dataType: "json",
				xhrFields: {
					withCredentials: true
				},
				success: function(res) {
		      	$("#m_dcBankcode").val(res['cardType']);
		      	bankcode = res['cardType'];
		      	$("#m_dcIco").removeClass();
		      	$("#m_dcIco").addClass("card-"+bankcode);
		    	form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode,name:dcName,number:dcNumber,mm:dcMM,yy:dcYY,cvv:dcCVV};
				gottogateway(form_data);			
				}
			});
		}	
	}if(pg == "PAYTM"){
		pg = "CASH";
		var bankcode = "PAYTM";
		form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode};
		gottogateway(form_data);
	}
	if(pg == "PAYPAL"){
		pg = "PAYPAL";
		var bankcode = "PAYPAL";
		form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode};
		gottogateway(form_data);
	}if(pg == "NB"){
		bankcode = $("#m_nbBankcode").find(":selected").val();
		if(bankcode == 0){
			$("#m_nbBankError").show();
			$("#m_nbBankError").text("Please Select Bank");
		}else{
			form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode};
			gottogateway(form_data);
		}
	}if(pg == "CASH"){
		var bankcode = $("input[name='m_wallet']:checked").val();
		if($("input[name='m_wallet']:checked").length == 0){
			$("#m_walletError").show();
			$("#m_walletError").text("Please select wallet");
		}else{
			form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode};
			gottogateway(form_data);	
		}
	}
	if(pg == "TEZ"){
		pg = "UPI";
		var bankcode = "TEZ";
	    var selectedTezBank = $("#m_tez_bank").find(":selected").val();
	    if($("input[name='m_TEZvpa']").val().trim() == ""){
	      	$("#m_TEZvpa-error").show();
			$("#m_TEZvpa-error").text("VPA is empty");
	      	$("#m_resultLoading").hide();
	      	return false;
		}
	    if(selectedTezBank == ""){
	      	$("#m_TEZvpa-error").show();
	    	$("#m_TEZvpa-error").text("please select vpa bank");
	      	$("#m_resultLoading").hide();
	      	return false;
	    }
		var vpa = $("input[name='m_TEZvpa']").val()+selectedTezBank;
	
		$.ajax({
		url: apiUrl+"api/validatevpa",
		headers: {
			'Authorization':getToken()
		},
		type: "post",
		data: {vpa:vpa},
		dataType: "json",
		xhrFields: {
			withCredentials: true
		},
		success: function(res) {
	      if(res['isVPAValid'] != 1){
	      	$("#m_TEZvpa-error").show();
	      	$("#m_TEZvpa-error").text("Please enter valid VPA");
	      	$("#m_resultLoading").hide();
	      	return false;
	      }else{
	      	form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode,vpa:res['vpa']};
	      	gottogateway(form_data);
	      }
		}
		});
		
		
	}
	if(pg == "UPI"){
		$("#m_resultLoading").show();
		var bankcode = "UPI";
		var vpa = $("input[name='m_vpa']").val();
		if(vpa.trim() == ""){
	      	$("#m_vpa-error").show();
			$("#m_vpa-error").text("VPA is empty");
	      	$("#m_resultLoading").hide();
	      	return false;
		}
		
		$.ajax({
			url: apiUrl+"api/validatevpa",
			headers: {
				'Authorization':getToken()
			},
			type: "post",
			data: {vpa:vpa},
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			success: function(res) {
					      if(res['isVPAValid'] != 1){
	      	$("#m_vpa-error").show();
	      	$("#m_vpa-error").text("Please enter valid VPA");
	      	$("#m_resultLoading").hide();
	      	return false;
	      }else{
	    	form_data = {payment_method:payment_method,pg:pg,bankcode:bankcode,vpa:vpa};
	    	gottogateway(form_data);  	
	      }			
			}
		});
		
	}
});
$(document).on('change','input[name="m_nbradio"]',function() {
	$("#m_nbBankError").hide();
	var selectedOption = $(this).val();
	$.each($("#m_nbBankcode option"),function(){
		if($(this).val() == selectedOption){
			$(this).prop('selected',true);
		}else{
			$(this).prop('selected',false);
		}
	});
});
$(document).on("change","#m_nbBankcode",function(){
	$("#m_nbBankError").hide();
	var selectedOption = $(this).val();
	$.each($('input[name="m_nbradio"]'),function(){
		if($(this).val() == selectedOption){
			$(this).prop('checked',true);
			$(this).closest(".bank-hide").addClass("checked");
		}else{
			$(this).prop('checked',false);
			$.each($(".bank-hide"),function(){
				$(this).removeClass("checked");
			});
		}
	});
});
/**************************/

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
function setRecentlyViewedProduct(productid)
{
	var rv_String = '';
	var recentlyViewed = getCookie("recently_viewed");
	if (recentlyViewed != "") {
		recentlyViewed = recentlyViewed.split(',');
		recentlyViewed.push(productid);
		
		const distinct = (value, index, self) => {
			return self.indexOf(value) === index;
		}
		recentlyViewed = recentlyViewed.filter(distinct);
		rv_String = recentlyViewed.join();
	} else {
		rv_String = productid;
	}

	var now = new Date();
	var time = now.getTime();
	time += 30 * 24 * 60 * 60 * 1000;
	now.setTime(time);
	document.cookie = 'recently_viewed='+rv_String+'; expires=' + now.toUTCString() + '; path=/';
}
function getRecentlyViewedProduct()
{
	var recentlyViewed = getCookie("recently_viewed");
	if (recentlyViewed == "") {
		return '';
	}
	return recentlyViewed;
}