const errorLog = require('../library/errorLog');

const seoURLModel = require('../models/seo-url-model');
var seoURL = function(registery){
  var router = registery.router;
  /* router.get('/', (req, res)=>{  
    res.send({status:1});    
  }); */
  router.get('/pageType', (req, res)=>{
    const page = req.query.page;    
    seoURLModel.getPageType(page).then(result =>{
      res.send(result);
    }).catch((err) => {
      errorLog.writeLog('/pageType', process.env.ERROR_LOG_DIR, err); 
    });     
  });
}
module.exports = seoURL;