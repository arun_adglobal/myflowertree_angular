import 'zone.js/dist/zone-node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import { join } from 'path';

import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';

import {Response} from 'express';
import {REQUEST, RESPONSE} from '@nguniversal/express-engine/tokens';
import 'localstorage-polyfill';
const domino = require('domino');
const fs = require('fs');
const rqt = require('request');

import { enableProdMode } from '@angular/core';
enableProdMode();

const compression = require('compression');


const path = require('path');
const template = fs.readFileSync(path.join('.', 'dist/mft-app/browser', 'index.html')).toString();
const win = domino.createWindow(template);

const redirection = require('./redirection');


global['localStorage'] = localStorage;

global['atob'] = require("atob");
global['btoa'] = require("btoa");

// tslint:disable-next-line:no-string-literal
global['window'] = win;
// tslint:disable-next-line:no-string-literal
global['document'] = win.document;
// tslint:disable-next-line:no-string-literal
global['DOMTokenList'] = win.DOMTokenList;
// tslint:disable-next-line:no-string-literal
global['Node'] = win.Node;
// tslint:disable-next-line:no-string-literal
global['Text'] = win.Text;
// tslint:disable-next-line:no-string-literal
global['HTMLElement'] = win.HTMLElement;
// tslint:disable-next-line:no-string-literal
global['navigator'] = win.navigator;
// tslint:disable-next-line:no-string-literal
global['MutationObserver'] = getMockMutationObserver();

 
function getMockMutationObserver() {
return class {
observe(node, options) {}
disconnect() {}
takeRecords() {
return [];
}
};
}

// The Express app is exported so that it can be used by serverless Functions.
export function app() {
  const server = express();
  server.use(compression({
    level: 6
  }));
  

  const distFolder = join(process.cwd(), 'dist/mft-app/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
  server.engine('html', ngExpressEngine({
    bootstrap: AppServerModule,
  }));

  server.set('view engine', 'html');
  server.set('views', distFolder);

  server.use(function(req, res, next) {
    res.setHeader('X-Frame-Options', 'sameorigin');
    next();
  });
  
server.get('/xmlsitemap/categories-0.xml', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/categories-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
server.get('/xmlsitemap/categories', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/categories-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
server.get('/xmlsitemap/products-0.xml', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/products-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
server.get('/xmlsitemap/products', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/products-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});


server.get('/xmlsitemap/informations-0.xml', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/informations-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
server.get('/xmlsitemap/informations', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/informations-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
server.get('/xmlsitemap/quotes-0.xml', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/quotes-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});

server.get('/xmlsitemap/quotes', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/quotes-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
server.get('/xmlsitemap/international-categories-0.xml', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/international-categories-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
server.get('/xmlsitemap/international-categories', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/international-categories-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
server.get('/xmlsitemap/international-products-0.xml', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/international-products-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
server.get('/xmlsitemap/international-products', (req, res)=>{
    rqt('https://opr.myflowertree.com/xmlsitemap/international-products-0.xml', function (error, response, body) {
      res.type('application/xml');
      res.send(body);   
    });
});
  

  // Example Express Rest API endpoints
  // app.get('/api/**', (req, res) => { });
  // Serve static files from /browser
  server.get('*.*', express.static(distFolder, {
    maxAge: '1y'
  }));

  // All regular routes use the Universal engine
  server.get('*', (req, res) => {
   	let new_url = req.url;
	let query_url = '';
	if(new_url.includes('utm_source'))
	{
		const url_Array = new_url.split("?");
		new_url = url_Array[0];
		query_url = url_Array[1];
	}
    if (redirection.hasOwnProperty((new_url).toLowerCase())) {
		if(query_url)
		{
			new_url = redirection[(new_url).toLowerCase()]+'?'+query_url;
			return res.redirect(301, new_url);
		}else{
			return res.redirect(301, redirection[(new_url).toLowerCase()]);
		}
    }else
    {
      res.render(indexHtml, { req, providers: 
        [
          { provide: APP_BASE_HREF, useValue: req.baseUrl },
          { provide: RESPONSE, useValue: res },
          { provide: REQUEST, useValue: req }
        
        ]
      
      }); 
      
      // if (req.headers.host.match(/^www/) == null ) {
      //   return res.redirect(301,'https://www.myflowertree.com'+req.url);
      //  }else
      //  {
        
      //     res.render(indexHtml, { req, providers: 
      //       [
      //         { provide: APP_BASE_HREF, useValue: req.baseUrl },
      //         { provide: RESPONSE, useValue: res },
      //         { provide: REQUEST, useValue: req }
            
      //       ]
          
      //     });     
      //  }
    }
  });

  

  return server;
}

function run() {
  const port = 3000;

  // Start up the Node server
  const server = app();

  

  server.listen(port, () => {
    console.log(`server listening on port ${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
} 

//run();
export * from './src/main.server';
